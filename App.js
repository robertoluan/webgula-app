import React from "react";
import Setup from "./src/boot/setup";
import { View } from "react-native";
import authContext from "./src/infra/auth";

export default class App extends React.Component {
  state = {
    authloaded: false
  };

  componentWillMount = async () => {
    await authContext.setup();
    this.setState({ authloaded: true });
  };

  render = () => {
    if (!this.state.authloaded) {
      return <View />;
    }

    return <Setup />;
  };
}
