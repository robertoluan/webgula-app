
const INITIAL_STATE = {
  open: false
};

export default function menu(state = INITIAL_STATE, action) {
  const { type, isDelivery } = action;

  switch (type) {
    case "LAYOUT.MENU.OPEN":
      return { ...state, open: true, isDelivery };
    case "LAYOUT.MENU.CLOSE":
      return { ...state, open: false, isDelivery };
  }

  return state;
}
