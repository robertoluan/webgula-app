const INITIAL_STATE = {
    openModalAuth: false
};

export default function layout(state = INITIAL_STATE, action) {

    const { type } = action;

    switch (type) {
        case "LAYOUT.MODAL_AUTH.OPEN":
            return { ...state, openModalAuth: true };
        case "LAYOUT.MODAL_AUTH.CLOSE":
            return { ...state, openModalAuth: false };
    }

    return state;

}