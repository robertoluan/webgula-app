import {combineReducers} from 'redux';

import menu from './menu.reducer';
import global from './global.reducer';
import layout from './layout.reducer';

export default combineReducers({
    menu,
    global,
    layout
})