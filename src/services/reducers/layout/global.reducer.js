const INITIAL_STATE = {
  loading: false,
  loadingMessage: "Carregando...",
  networkActivityCount: 0
};

export default function global(state = INITIAL_STATE, action) {
  const { type, loading, loadingMessage, current } = action;

  switch (type) {
    case "LAYOUT.GLOBAL.SET_LOADING":
      return {
        ...state,
        loading,
        loadingMessage: loadingMessage || "Carregando..."
      };

    case "LAYOUT.GLOBAL.SET_NETWORK_ACTIVITY":
      return {
        ...state,
        networkActivityCount: current
      };
    default:
      return state;
  }
}
