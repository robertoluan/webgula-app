export const INITIAL_STATE = {};

export const LIST_INITIAL_STATE = {
	items: [],
	loading: false,
	filter: null
};

export default function list(state = INITIAL_STATE, action) {
	const { type, list, keepFilters } = action;

	if (!type.startsWith('SHARED.LIST.')) return state;

	if (type == 'SHARED.LIST.DESTROY') {
		var newState = { ...state };

		if (keepFilters) {
			delete newState[list].items;
		} else {
			delete newState[list];
		}
		return newState;
	}
	const formState = state[list] || { ...LIST_INITIAL_STATE };

	const formNewState = listInternal(formState, list, action);

	if (formState == formNewState) return state;

	return { ...state, [list]: formNewState };
}

function listInternal(state = LIST_INITIAL_STATE, list, action) {
	const { type, items, error, filter } = action;

	switch (type) {
		case 'SHARED.LIST.FETCHING':
			return { ...state, loading: true, error: null };

		case 'SHARED.LIST.FETCH_SUCCESS':
			return { ...state, items, loading: false, filter, error: null };

		case 'SHARED.LIST.FETCH_ERROR':
			return { ...state, loading: false, error };

		case 'SHARED.LIST.UPDATE_FILTER':
			return { ...state, filter: { ...state.filter, ...filter } };
		default:
			return state;
	}
}
