import { combineReducers } from 'redux';

import list from './list.reducer';
import cidade from './cidade.reducer'
import estado from './estado.reducer'

export default combineReducers({
    list,
    cidade,
    estado
})