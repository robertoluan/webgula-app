const INITIAL_STATE = {
    isFetching: true,
    estados: [],
    error: null
}

export default function estado(state = INITIAL_STATE, action) {
    switch (action.type) {
        case 'APP.SHARED.LOAD_ESTADOS':
            return { ...state, isFetching: true }
        case 'APP.SHARED.LOAD_ESTADOS_SUCCESS':
            return { ...state, isFetching: false, estados: action.payload }
        case 'APP.SHARED.LOAD_ESTADOS_ERROR':
            return { ...state, isFetching: false, error: action.payload }
        default:
            return state
    }
}