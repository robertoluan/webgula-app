const INITIAL_STATE = {
    isFetching: true,
    cidades: [],
    error: null
}

export default function cidade(state = INITIAL_STATE, action) {
    switch (action.type) {
        case 'APP.SHARED.LOAD_CIDADES':
            return { ...state, isFetching: true }
        case 'APP.SHARED.LOAD_CIDADES_SUCCESS':
            return { ...state, isFetching: false, cidades: action.payload }
        case 'APP.SHARED.LOAD_CIDADES_ERROR':
            return { ...state, isFetching: false, error: action.payload }
        default:
            return state
    }
}