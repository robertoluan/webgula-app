const INITIAL_STATE = {
  items: [],
  modalidadeId: null,
  endereco: {},
  item: {}
};

export default function delivery(state = INITIAL_STATE, action) {
  const { type, payload } = action;

  switch (type) {
    case "APP.DELIVERY.REMOVE_CART":
      return { ...state, ...payload };

    case "APP.DELIVERY.SET_ORIGIN_PEDIDO":
      return { ...state, ...payload };

    case "APP.DELIVERY.ADD_CART":
      return { ...state, ...payload };

    case "APP.DELIVERY.LOAD_ITEM":
      return { ...state, ...payload };

    case "APP.DELIVERY.LOAD":
      return { ...state, ...payload };

    case "APP.DELIVERY.UPDATE_ITEM":
      return { ...state, ...payload };

    case "APP.DELIVERY.CLEAR_CART":
      return { ...state, ...payload };

    default:
      return state;
  }
}
