import { combineReducers } from "redux";

import empresa from "./empresa.reducer";
import dashboard from "./dashboard.reducer";
import delivery from "./delivery.reducer";
import auth from "./auth.reducer";
import endereco from "./endereco.reducer";

export default combineReducers({
  empresa,
  delivery,
  dashboard,
  endereco,
  auth
});
