const INITIAL_STATE = {
	empresas: []
};

export default function empresa(state = INITIAL_STATE, action) {
	const { type, payload } = action;

	switch (type) {
		case 'APP.EMPRESA.LOAD_EMPRESAS':
			return { ...state, loading: true, ...payload };

		case 'APP.EMPRESA.EMPRESAS_SUCCESS':
			return { ...state, loading: false, ...payload };

		case 'APP.EMPRESA.EMPRESA_SELECTED':
			return { ...state, loading: false, ...payload };

		case 'APP.EMPRESA.FAVORITAR':
			return { ...state, loading: false, ...payload };

		case 'APP.EMPRESA.EMPRESAS_ERROR':
			return { ...state, loading: false, ...payload };

		default:
			return state;
	}
}
