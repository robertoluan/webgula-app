const INITIAL_STATE = {
  userInfo: null,
  token: null
};

export default function auth(state = INITIAL_STATE, action) {
  const { type, token, userInfo, isAuth, isClientCredentials, endereco } = action;

  switch (type) {
    case "APP.AUTH.SET_AUTH_USERINFO":
      return {
        ...state,
        token,
        isAuth,
        isClientCredentials,
        userInfo
      };
    case "APP.AUTH.SET_AUTH_ENDERECO":
      return {
        ...state,
        endereco
      };
    case "APP.AUTH.LOAD":
      return {
        ...state,
        token,
        isAuth,
        isClientCredentials,
        userInfo
      };
    default:
      return state;
  }
}
