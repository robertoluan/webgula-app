const INITIAL_STATE = {
    isFetching: true,
    notificacoes: [],
    error: null
}

export default function dashboard(state = INITIAL_STATE, action) {
    switch (action.type) {
        case 'APP.DASHBOARD.LOAD_DASHBOARD':
            return { ...state, isFetching: true }
        case 'APP.DASHBOARD.LOAD_DASHBOARD_SUCCESS':
            return { ...state, isFetching: false, notificacoes: action.payload }
        case 'APP.DASHBOARD.LOAD_DASHBOARD_ERROR':
            return { ...state, isFetching: false, error: action.payload }
        default:
            return state
    }
}