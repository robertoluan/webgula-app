const INITIAL_STATE = {
  items: [],
  atual: {}
};

export default function endereco(state = INITIAL_STATE, action) {
  const { type, items, atual, menu } = action;

  switch (type) {
    case "APP.ENDERECO.CHANGE":
      return {
        ...state,
        items,
        atual,
        menu
      };
    case "APP.ENDERECO.REMOVE":
      return {
        ...state,
        items,
        atual,
        menu
      };
    case "APP.ENDERECO.LOAD":
      return {
        ...state,
        items,
        atual,
        menu
      };
    default:
      return state;
  }
}
