import { combineReducers } from 'redux';

import layout from './layout';
import shared from './shared';
import app from './app'

export default combineReducers({
    layout,
    shared,
    app
})