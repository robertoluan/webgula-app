import ApiBase from "../../infra/http/api.base";
import Http from "../../infra/http/http";
import AsyncStorageRemote from "./asyncStorage.remote";
import moment from "moment";
import store from "../../infra/store";
import { parseQueryString, getParams } from "../resolvers/shared/querystring";
import { diffMinutes } from "../resolvers/shared/cache";
import authContext from "../../infra/auth";
import { BASE_URL_TACTO } from "../../infra/config";

const asyncStorageRemote = new AsyncStorageRemote();

export default class EmpresaRemote extends ApiBase {
  constructor() {
    super(null, { auth: true });
    this.http = new Http();
  }

  async verificarExpiracao(collection, time = 10) {
    let expiracao = await asyncStorageRemote.get(collection + "_exipira_em");

    if (
      expiracao &&
      diffMinutes(expiracao) > 0 &&
      diffMinutes(expiracao) <= time
    ) {
      return true;
    }

    return false;
  }

  async findEmpresasMenu(params = {}) {
    const stringParams = parseQueryString(getParams(params));

    let url = `api/empresas/proximas-menu?${stringParams}`;

    let empresas = await this.cacheIntecept(url);

    if (empresas) {
      empresas = empresas.map(item => {
        if (!!item.logoUrl && item.logoUrl.indexOf("http") === -1) {
          item.logoUrl = this.createImage(
            item.chaveGrupoEmpresarial,
            item.id,
            item.logoUrl
          );
        }

        return { ...item };
      });
    }

    return empresas || [];
  }

  async findEmpresasDelivery(params = {}) {
    if (params.latitude) {
      params.distancia = 10;
    }

    const stringParams = parseQueryString(getParams(params));

    let url = `api/empresas/proximas?${stringParams}`;

    let empresas = await this.cacheIntecept(url);

    if (empresas) {
      empresas = empresas.map(item => {
        if (!!item.logoUrl && item.logoUrl.indexOf("http") === -1) {
          item.logoUrl = this.createImage(
            item.chaveGrupoEmpresarial,
            item.id,
            item.logoUrl
          );
        }

        return { ...item };
      });
    }

    return empresas || [];
  }

  createImage(chaveEmpresarial, empresaId, name) {
    return (
      BASE_URL_TACTO +
      "/v1/empresas/" +
      empresaId +
      "/imagem?c=" +
      chaveEmpresarial +
      "&b=" +
      name
    );
  }

  async findAllVips(
    params = {
      isEmpresaVip: true
    }
  ) {
    const authData = await authContext.userDataAsync();

    params.pessoaId = authData.userInfo.pessoaId;

    const stringParams = parseQueryString(getParams(params));

    const url = `api/empresas/vips?${stringParams}`;

    let empresas = await this.cacheIntecept(url);

    empresas = empresas.map(item => {
      if (!!item.logoUrl && item.logoUrl.indexOf("http") === -1) {
        item.logoUrl = this.createImage(
          item.chaveGrupoEmpresarial,
          item.id,
          item.logoUrl
        );
      }

      return { ...item };
    });

    return empresas;
  }

  async findInformacaoCompletaEmpresa() {
    const empresa = await this.getEmpresaDefault();
    let data = await this.getHeaders(
      `api/empresas/informacao-completa?empresaId=${empresa.id}`
    );

    data.chaveGrupoEmpresarial = data.grupoEmpresarialChave;

    delete data.grupoEmpresarialChave;

    if (!!data.logoUrl && data.logoUrl.indexOf("http") === -1) {
      data.logoUrl = this.createImage(
        data.chaveGrupoEmpresarial,
        data.id,
        data.logoUrl
      );
    }

    return data;
  }

  async findGrupos(grupoPaiId = null) {
    let grupos = [];

    grupos = await this.getHeaders(`grupos`, {
      expiration: 10
    });

    // Caso seja informado um grupo pai
    if (!!grupoPaiId) {
      grupos = grupos.filter(x => x.grupoPaiId == grupoPai);
    } else {
      grupos = grupos.filter(x => !x.grupoPaiId);
    }

    return grupos;
  }

  async findCardapio(grupoId = null, pdvId = 1) {
    let empresa = await this.getEmpresaDefault();

    let cardapio = {};

    if (await this.isNotExpirated("cardapios", 10)) {
      cardapio = (await asyncStorageRemote.get("cardapios")) || {};

      if (Array.isArray(cardapio)) cardapio = {};
    }

    let cardapioAtual = cardapio[empresa.id];

    if (
      !cardapioAtual ||
      !cardapioAtual.produtos ||
      !cardapioAtual.produtos.length
    ) {
      const data = await this.getHeaders(`cardapios?pdvId=${pdvId}`, {
        expiration: 5
      });

      let { produtos, variacoes, variacoesItens, produtoVariacaoItem } = data;

      produtos.forEach(produto => {
        produto.empresaId = empresa.id;

        produto.variacoes = produtoVariacaoItem.filter(
          pvi => pvi.itemId == produto.id
        );

        produto.variacoes = produto.variacoes.map(p => {
          const variacao = variacoes.filter(v => v.id == p.variacaoId)[0];

          const produtosDaVariacao = variacoesItens
            .filter(variacaoItem => variacaoItem.variacaoId == variacao.id)
            .map(variacaoItem => {
              const produtoVariacao = produtos.filter(
                pro => pro.id == variacaoItem.itemId
              )[0];

              return {
                ...produtoVariacao,
                preco: variacaoItem.valor,
                tamanhoId: p.tamanhoId,
                variacaoId: variacao.id
              };
            });

          return {
            nome: variacao.nome,
            nomeMostra: variacao.nomeMostra,
            qtdeMax: variacao.qtdeMax,
            qtdeMin: variacao.qtdeMin,
            flgTipo: variacao.flgTipo,
            id: variacao.id,
            items: produtosDaVariacao
          };
        });
      });

      let cardapioNovo = {
        empresaId: empresa.id, // empresa.id pq ela nao se repete
        produtos: produtos.filter(p => p.mostrarCardapioCliente)
      };

      cardapio[empresa.id] = cardapioNovo;

      asyncStorageRemote.save("cardapios", cardapio);

      asyncStorageRemote.save(
        "cardapios_exipira_em",
        moment().add("minutes", 5)
      );

      cardapioAtual = cardapioNovo;
    }

    if (grupoId !== null) {
      cardapioAtual = cardapioAtual.produtos.filter(x => x.grupoId == grupoId);
    }

    return cardapioAtual;
  }

  async findCardapioItem(itemId) {
    const data = await this.getHeaders(`cardapios/item/${itemId}`);
    return data;
  }

  async findSaida(chave) {
    const authData = await authContext.userDataAsync();
    const data = await this.getHeaders(
      `acompanhamento-conta/solicitar-saida?chave=${chave}&nomeUsuario=${
        authData.userInfo.nome
      }`
    );
    return data;
  }

  async findVenda(saidaId) {
    const data = await this.getHeaders(
      `acompanhamento-conta/venda-detalhada/${saidaId}?saidaId=${saidaId}`,
      {
        disabledCache: true
      }
    );
    return data;
  }

  // LISTA OS USUARIOS QUE ESTAO CADASTRADOS EM UMA SAIDA
  async findUsuariosVenda(saidaId) {
    const data = await this.getHeaders(
      `acompanhamento-conta/lista-usuarios?saidaId=${saidaId}`,
      { disabledCache: true }
    );
    return data;
  }

  // LISTA AS ULTIMAS CONTAS EM ACOMPANHAMENTO PELO USUARIO
  async findContasAcompanhamento() {
    const data = await this.getHeaders(`acompanhamento-conta/lista-contas`, {
      disabledCache: true
    });
    return data;
  }

  async findInfoSemana() {
    let empresa = (await this.getEmpresaDefault()) || {};

    const data = await this.getHeaders(
      `api/empresas-info-semanal/${empresa.id}/1/lista`
    );

    return data;
  }

  async avaliar(model) {
    let empresa = await this.getEmpresaDefault();

    model.empresaId = empresa.id;

    const response = await this.postHeaders(`avaliacoes-empresas`, model);

    return response;
  }

  async logVisita() {
    const webgulaDevice = await asyncStorageRemote.getWebgulaDeviceId();

    if (webgulaDevice && webgulaDevice.id) {
      const response = await this.postHeaders(`log/visita-empresa`, {
        dispositivoMobileId: webgulaDevice.id
      });

      return response;
    }
  }

  async favoritar(empresa) {
    let state = store.getState();

    let user = state.app.auth.userInfo;

    let model = { flgFavorito: empresa.flgFavorito };

    const request = await this.putHeaders(
      `pessoas-favoritos/${user.pessoaId}/${empresa.id}/favorito`,
      model
    );

    if (request.type == "exception") {
      throw "Nao foi possivel favoritar a empresa!";
    }

    this.setEmpresaDefault(empresa);

    return empresa;
  }

  async setVip(empresa) {
    let state = store.getState();

    let user = state.app.auth.userInfo;

    let model = {
      flgAceitoCliente: true,
      flgNotificacaoPromo: true,
      alteracaoData: moment().toDate()
    };

    const request = await this.put(
      `pessoas-favoritos/${user.pessoaId}/${empresa.id}`,
      model
    );

    return request;
  }

  async registarUsuario(model) {
    const request = await this.registrar(model);

    return request;
  }

  async procurarCep(cep) {
    const { data } = await this.get(`api/ceps/${cep}`);

    return data;
  }
}
