import ApiBase from '../../infra/http/api.base';
import Http from '../../infra/http/http'

const headers = { 'Content-Type': 'application/json' }

export default class DashboardRemote extends ApiBase {
    constructor() {
        super(null, { auth: true });
        this.http = new Http()
    }

    async getDashboard() {
        const request = await this.get(`dashboard`)
        return request.data
    }

}
