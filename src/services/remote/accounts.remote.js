import ApiBase from '../../infra/http/api.base';
import Http from '../../infra/http/http';
import authContext from '../../infra/auth';
import store from '../../infra/store';
import EmpresaRemote from './empresa.remote';
import { Platform } from 'react-native';
// import DeviceInfo from 'react-native-device-info';
import AsyncStorageRemote from './asyncStorage.remote';
import { setAuth } from '../actions/app/auth.actions';

const asyncStorageRemote = new AsyncStorageRemote();
const empresaRemote = new EmpresaRemote();

export default class AccountsRemote extends ApiBase {
  constructor() {
    super(null, { auth: true });
    this.http = new Http();
  }

  async simplesResponse(request) {
    try {
      let response = await request(
        (headers = {
          Authorization: 'Bearer ' + authContext.token
        })
      );

      let data = null;

      if (response && typeof response.json === 'function') {
        data = await response.json();
      } else {
        data = await response;
      }

      if (response.status >= 200 && response.status <= 299) {
        return { success: true, data: data };
      }

      return { success: false, error: data };
    } catch (e) {
      console.log('RESPONSE ERROR', e);

      return { success: false, error: e };
    }
  }

  async registrarPhoneNumber(model) {
    const body = JSON.stringify(model);

    return await this.simplesResponse(
      async () => await this.http._registrarPhoneNumber(body)
    );
  }

  // novo
  async confirmarSMS(codigoSMS) {
    const body = JSON.stringify({ codigoSMS });

    return await this.simplesResponse(
      async headers => await this.http._confirmarSMS(body, headers)
    );
  }

  // novo
  async requisitarAutorizacaoTrocarTelefone(novoTelefone) {
    const body = JSON.stringify({ novoTelefone });

    return await this.simplesResponse(
      async headers =>
        await this.http._requisitarAutorizacaoTrocarTelefone(body, headers)
    );
  }

  // novo
  async confirmarSMSComTelefoneTrocado(novoTelefone, codigoSms, autorizacao) {
    const body = JSON.stringify({ novoTelefone, codigoSms, autorizacao });

    return await this.simplesResponse(
      async headers =>
        await this.http._confirmarSMSComTelefoneTrocado(body, headers)
    );
  }

  // novo
  async recuperarSenha(email) {
    const body = JSON.stringify({ email });

    return await this.simplesResponse(
      async headers => await this.http._recuperarSenha(body, headers)
    );
  }

  // novo
  async pedirSMS(telefone) {
    const body = JSON.stringify({ telefone });

    return await this.simplesResponse(
      async headers => await this.http._pedirSMS(body, headers)
    );
  }

  // novo
  async registrarUsuario(model) {
    const body = JSON.stringify(model);

    const res = await this.simplesResponse(async headers =>
      this.http._registrarUsuario(body, headers)
    );

    // DEPOIS QUE CADASTRAR O USUARIO NO ACCOUNTS
    // CADASTRAR A PESSOA
    if (res.success) {
      // FAZER LOGIN
      await authContext.login(model.email, model.senha);

      await this.atualizarToken();
    }

    return res;
  }

  // novo
  async atualizarUsuario(model) {
    const body = JSON.stringify(model);

    let res = await this.simplesResponse(async headers =>
      this.http._atualizarUsuario(body, headers)
    );

    let pessoa = {
      ...model
    };

    let authData = await authContext.userDataAsync();

    pessoa.usuarioAccid = authData.id;

    authData.userInfo = {
      ...authData.userInfo,
      ...pessoa
    };

    authContext.setAuth({ ...authData });

    return res;
  }

  async uploadImage(file) {
    const imagesData = new FormData();
    const uuid = +new Date();

    imagesData.append('file', {
      uri: file.uri,
      type: file.type || 'image/jpeg',
      name: `${file.fileName}-${uuid}.jpg`
    });

    const data = await this.http.request('pessoas/imagem', 'POST', imagesData, {
      'Content-Type':
        'multipart/form-data; charset=utf-8; boundary="another cool boundary";',
      Authorization: 'Bearer ' + authContext.token
    });

    return data;
  }

  async getPessoaPorUsuarioID() {
    const usuarioAccid = authContext.userInfo.id;

    try {
      let { data } = await this.get(
        '/pessoas/usuario?usuarioACCID=' + usuarioAccid
      );

      if (data) {
        const authData = await authContext.userDataAsync();

        authData.userInfo = {
          pessoaId: data.id,
          ...data,
          ...authData.userInfo
        };

        authContext.setAuth({ ...authData });

        store.dispatch({
          type: 'APP.AUTH.LOAD',
          userInfo: authData.userInfo,
          isAuth: true,
          token: authContext.token
        });
      }

      return data;
    } catch (e) {
      return false;
    }
  }

  async cadastrarPessoa(model) {
    let pessoa = {
      ...model
    };

    const fcmToken = await asyncStorageRemote.getTokenDevice();

    pessoa.soId = Platform.OS === 'android' ? 3 : 2;
    pessoa.token = fcmToken;
    //pessoa.deviceInfo = DeviceInfo.getDeviceName();
    pessoa.usuarioAccid = authContext.userInfo.id;
    pessoa.email = authContext.userInfo.sub;

    return await this.post('/pessoas', pessoa);
  }

  async atualizarToken(params = {}) {
    try {
      const authData = (await authContext.userDataAsync()) || {};

      const fcmToken = await asyncStorageRemote.getTokenDevice();

      if (!fcmToken) return;

      const webgulaDevice = await asyncStorageRemote.getWebgulaDeviceId();

      let pessoaId = null;

      if (authData && authData.userInfo) {
        pessoaId = authData.userInfo.pessoaId;
      }

      const model = {
        deviceInfo: 'devince-sem-nome-porenquanto', //DeviceInfo.getDeviceName(),
        soId: Platform.OS === 'android' ? 3 : 2,
        token: fcmToken,
        pessoaId: pessoaId,
        ...params
      };

      let url = `/dispositivo-mobile`;

      let data = null;

      if (!webgulaDevice) {
        let res = await this.post(url, model);

        if (res.data) {
          await asyncStorageRemote.setWebgulaDeviceId(res.data);
        }
      } else {
        model.id = webgulaDevice.id;
        let res = await this.put(url + '/' + model.id, model);
        data = res.data;
      }

      if (data) {
        return { success: true, data: data };
      }

      return { success: false, data: data };
    } catch (e) {
      //debugger;
      return { success: false, error: e };
    }
  }

  async getEnderecosPessoa() {
    const { pessoaId } = authContext.userInfo;

    let empresa = (await empresaRemote.getEmpresaDefault()) || {};

    let { data } = await this.get(
      `/api/pessoas-enderecos/${pessoaId}/PessoaId?pessoaId=` +
        pessoaId +
        '&empresaGlobalId=' +
        (empresa.id || 0)
    );

    await asyncStorageRemote.save('enderecos', data);

    return data;
  }

  async saveEnderecosPessoa(endereco) {
    const { pessoaId } = authContext.userInfo;

    endereco.pessoaId = pessoaId;

    let response = {};

    if (!endereco.id) {
      response = await this.post(`/api/pessoas-enderecos/`, endereco);
    } else {
      response = await this.put(
        `/api/pessoas-enderecos/${endereco.id}`,
        endereco
      );
    }

    return response;
  }

  async deleteEnderecosPessoa(endereco) {
    let response = {};

    response = await this.delete(`/api/pessoas-enderecos/` + endereco.id, {});

    return response.data;
  }

  async setPrincipalEnderecosPessoa(enderecoId) {
    let response = {};

    response = await this.put(
      `/api/pessoas-enderecos/${enderecoId}/ativar`,
      {}
    );

    return response.data;
  }

  async getTelefones() {
    const { pessoaId } = authContext.userInfo;

    let { data } = await this.get(
      `pessoas-telefones/${pessoaId}/PessoaId?pessoaId=` + pessoaId
    );

    return data;
  }

  async saveTelefonePessoa(telefone) {
    const { pessoaId } = authContext.userInfo;

    telefone.pessoaId = pessoaId;
    telefone.tipo = 1;
    telefone.codPais = 55;
    telefone.codDdd = Number(telefone.codDdd);

    let response = {};

    if (!telefone.id) {
      response = await this.post(`pessoas-telefones/`, telefone);
    } else {
      response = await this.put(`pessoas-telefones/${telefone.id}`, telefone);
    }

    return response.data;
  }

  async deleteTelefonePessoa(telefone) {
    let response = {};

    response = await this.delete(`pessoas-telefones/` + telefone.id, {});

    return response.data;
  }
}
