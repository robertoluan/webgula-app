import { AsyncStorage } from "react-native";

export default class AsyncStorageRemote {
  save = async (key, data) => {
    try {
      return await AsyncStorage.setItem(key, data ? JSON.stringify(data) : "");
    } catch (error) {
      //throw error;
    }
  };

  get = async key => {
    try {
      let res = await AsyncStorage.getItem(key);

      let response = JSON.parse(res);

      return response;
    } catch (error) {
      return null;
    }
  };

  getWebgulaDeviceId = async () => {
    return await this.get("webgulaDeviceId");
  };

  setWebgulaDeviceId = async data => {
    return await this.save("webgulaDeviceId", data);
  };

  getTokenDevice = async () => {
    const token = (await AsyncStorage.getItem("fcmToken")) || "x-x-x";

    return token;
  };

  setTokenDevice = async token => {
    await AsyncStorage.setItem("fcmToken", token);
  };

  remove = async key => {
    try {
      return await AsyncStorage.removeItem(key);
    } catch (error) {
      //throw error;
    }
  };
}
