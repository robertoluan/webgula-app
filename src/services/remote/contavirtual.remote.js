import ApiBase from "../../infra/http/api.base";
import Http from "../../infra/http/http";
import EmpresaRemote from "./empresa.remote";
import AsyncStorageRemote from "./asyncStorage.remote";
import store from "../../infra/store";
import moment from "moment";

const empresaRemote = new EmpresaRemote();
const asyncStorageRemote = new AsyncStorageRemote();

export default class ContaVirtualRemote extends ApiBase {
  constructor() {
    super(null, { auth: true });
    this.http = new Http();
  }

  async fazerPedido() {
    const items = store.getState().app.delivery.items;

    const empresa = await empresaRemote.getEmpresaDefault();

    let algumItemPossuiComissao = false;
    let valorTotalProdutos = 0;
    let valorTaxaServicoPedido = 0;

    items.forEach(item => {
      item.pedidoItemVariacao = [];

      item.variacoesUsadas.forEach(variacao => {
        const divisaoMultipla = variacao.flgTipo == "M";

        const itemsSelecionados = variacao.items.filter(x => x.quantidade);

        let quantidadesSelecionada = 0;

        itemsSelecionados.forEach(
          item => (quantidadesSelecionada += item.quantidade)
        );

        if (divisaoMultipla && quantidadesSelecionada > 0) {
          possuiVariacaoMultiplaSelecionada = true;
        }

        let totalItems = 0;

        itemsSelecionados.forEach(variacaoitem => {
          let quantidade = variacaoitem.quantidade;

          if (divisaoMultipla) {
            if (itemsSelecionados.length > 1) {
              quantidade = quantidade / quantidadesSelecionada;
            } else {
              quantidade = 1;
            }
          }

          totalItems += quantidade * variacaoitem.preco;

          const fakeId = Number(Math.ceil(Math.random() * 9999999));

          item.pedidoItemVariacao.push({
            id: fakeId,
            pedidoItemId: null,
            tamanhoId: variacaoitem.tamanhoId,
            itemId: variacaoitem.id,
            variacaoId: variacao.id,
            qtde: quantidade,
            valorUnitario: variacaoitem.preco,
            valorTotal: +Number(totalItems).toFixed(2),
            observacao: variacaoitem.observacao
          });

          valorTotalItem += totalItems;
        });
      });

      valorTotalProdutos += item.valorTotal;
    });

    let pedidoIdFake = 1;

    const pedidoItems = items.map(item => {
      const precoSelecionado = item.precos.filter(x => x.selected)[0];

      const fakeId = Number(Math.ceil(Math.random() * 999999));

      let valorTotalItemVariacao = 0;

      let valorTotalVariacoesM = 0;

      item.pedidoItemVariacao.forEach(variacao => {
        if (variacao.flgTipo === "M") {
          valorTotalVariacoesM += variacao.valorTotal;
        }

        valorTotalItemVariacao += +Number(variacao.valorTotal);
      });

      // Caso exista valor nas variacoes do Tipo M, ela se torna o preço unitario
      if (valorTotalVariacoesM > 0) {
        valorUnitario = valorTotalVariacoesM;
      } else {
        valorUnitario = valorTotalItemVariacao;
      }

      if (item.comissionado) {
        algumItemPossuiComissao = true;
      }

      let valorComissao = item.valorTotal * (Number(item.taxaComissao) || 0);

      valorTaxaServicoPedido += valorComissao;

      const wgPedidoConsumoVariacao = item.pedidoItemVariacao.map(
        itemVariacao => ({
          ...itemVariacao,
          pedidoItemId: fakeId
        })
      );

      return {
        id: fakeId,
        pedidoId: pedidoIdFake, // fake
        itemId: item.id,
        valorUnitario: +Number(item.valorTotal / item.quantidade).toFixed(2),
        qtde: item.quantidade,
        valorNormal: precoSelecionado.valorPreco * item.quantidade,
        valorTotal: +Number(item.valorTotal).toFixed(2),
        observacao: item.observacao || "",
        tamanhoId: precoSelecionado.tamanhoId,
        wgPedidoConsumoVariacao: wgPedidoConsumoVariacao,
        valorTotalVariacao: +Number(
          valorTotalItemVariacao * item.quantidade
        ).toFixed(2),
        valorDesconto: item.valorDesconto,
        valorComissao: valorComissao,
        percComissao: item.taxaComissao || 0,
        flgComissionado: item.comissionado
      };
    });

    let valorAcrescimo = 0;
    let valorDesconto = 0;

    let model = {
      id: pedidoIdFake, // fake
      empresaId: empresa.empresaBaseId,
      nomeApoio: "",
      valorAcrescimo: valorAcrescimo,
      valorDesconto: valorDesconto,
      valorTaxaServico: valorTaxaServicoPedido,
      flgComissionado: algumItemPossuiComissao,
      valorProduto: +Number(valorTotalProdutos).toFixed(2),
      valorTotal: +Number(
        valorTotalProdutos +
          valorAcrescimo +
          valorTaxaServicoPedido -
          valorDesconto
      ).toFixed(2),
      observacao: "--nenhuma observação--",
      wgPedidoConsumoItem: pedidoItems
    };

    const request = await this.post(`pedido-consumo/pre-pago`, model, {
      "Tacto-Grupo-Empresarial": empresa.chaveGrupoEmpresarial,
      empresaId: empresa.empresaBaseId,
      "Content-Type": "application/json"
    });

    if (request.type === "exception") {
      return {
        success: false,
        error: request.exception
      };
    }

    model.id = request.data.id;
    model.data = moment().format();

    model.wgPedidoConsumoItem = model.wgPedidoConsumoItem.map(item => ({
      ...item,
      pedidoId: model.id
    }));

    const pedidosCreditoLocal =
      (await asyncStorageRemote.get("pedidos_credito")) || [];

    pedidosCreditoLocal.push({
      ...model
    });

    await asyncStorageRemote.save("pedidos_credito", pedidosCreditoLocal);

    return {
      success: true,
      data: null
    };
  }

  async abrirContaVirtual(agenciaId) {
    const data = await this.postHeaders(`api/coin/abertura-conta`, {
      agenciaId
    });
    return data;
  }

  async extratoContaVirutal(contaCorrenteId, force) {
    const data = await this.getHeaders(
      `api/coin/extrato-conta-id/${contaCorrenteId}`,
      {
        disabledCache: force
      }
    );
    return data;
  }

  async getUltimosPedidos(force = true) {
    const data = await this.getHeaders(
      `dados/pedidos/consumo/ultimos-pedidos-pre-pago`,
      {
        disabledCache: force
      }
    );
    return data;
  }

  async getPedidoDetalhado(pedidoId) {
    const data = await this.getHeaders(
      `dados/pedidos/consumo/detalhado?pedidoId=${pedidoId}`
    );
    return data;
  }
}
