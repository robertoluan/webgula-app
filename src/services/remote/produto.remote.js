import ApiBase from "../../infra/http/api.base";
import Http from "../../infra/http/http";

export default class ProdutoRemote extends ApiBase {
  constructor() {
    super(null, { auth: true });
    this.http = new Http();
  }
}
