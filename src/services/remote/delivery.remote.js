import ApiBase from "../../infra/http/api.base";
import Http from "../../infra/http/http";
import AsyncStorageRemote from "./asyncStorage.remote";
import moment from "moment";
import store from "../../infra/store";
import EmpresaRemote from "./empresa.remote";
import authContext from "../../infra/auth";

const asyncStorageRemote = new AsyncStorageRemote();
const empresaRemote = new EmpresaRemote();

export default class DeliveryRemote extends ApiBase {
  constructor() {
    super(null, { auth: true });
    this.http = new Http();
  }

  async findAll() {
    const data = await this.get(`wg-pedido`);
    return data;
  }

  async especialidades() {
    const especialidades = await this.getHeaders(`api/especialidades`);
    return especialidades;
  }

  async getModalidadesPagamento() {
    const data = await this.getHeaders(`modalidades-caixas`);
    return data;
  }

  async getAcompanhamento(pedidoId) {
    const data = await this.getHeaders(`wg-pedido/${pedidoId}/acompanhamento`);
    return data;
  }

  async checkout() {
    const modalidadeId = store.getState().app.delivery.modalidadeId;
    const modalidadeNome = store.getState().app.delivery.modalidadeNome;
    const valorPagamento = store.getState().app.delivery.valorPagamento;
    const observacao = store.getState().app.delivery.observacao;
    const items = store.getState().app.delivery.items;
    const endereco = store.getState().app.endereco.atual;

    let valorTroco = 0;

    const empresa = await empresaRemote.getEmpresaDefault();

    const authData = await authContext.userDataAsync();

    let valorTotal = 0;

    let possuiVariacaoMultiplaSelecionada = false;

    // considerar a flag flgTipo
    // flgTipo == "M" multiplo
    // flgTipo == "U" unico
    items.forEach(item => {
      const precoSelecionado = item.precos.filter(x => x.selected)[0];

      item.pedidoItemVariacao = [];

      item.variacoesUsadas.forEach(variacao => {
        const divisaoMultipla = variacao.flgTipo == "M";

        const itemsSelecionados = variacao.items.filter(x => x.quantidade);

        let quantidadesSelecionada = 0;

        itemsSelecionados.forEach(
          item => (quantidadesSelecionada += item.quantidade)
        );

        if (divisaoMultipla && quantidadesSelecionada > 0) {
          possuiVariacaoMultiplaSelecionada = true;
        }

        let totalItems = 0;

        itemsSelecionados.forEach(variacaoitem => {
          let quantidade = variacaoitem.quantidade;

          if (divisaoMultipla) {
            if (itemsSelecionados.length > 1) {
              quantidade = quantidade / quantidadesSelecionada;
            } else {
              quantidade = 1;
            }
          }

          totalItems += quantidade * variacaoitem.preco;

          valorTotal += totalItems * (item.quantidade || 1);

          item.pedidoItemVariacao.push({
            tamanhoId: variacaoitem.tamanhoId,
            itemId: variacaoitem.id,
            variacaoId: variacao.id,
            qtde: quantidade,
            valorUnitario: variacaoitem.preco,
            valorTotal: totalItems,
            observacao: variacaoitem.observacao
          });
        });
      });

      if (!possuiVariacaoMultiplaSelecionada) {
        valorTotal += precoSelecionado.valorPreco * item.quantidade;
      }
    });

    valorTotal += endereco.taxaEntrega;

    if (modalidadeNome.toLowerCase().indexOf("dinheiro") > -1) {
      valorTroco = valorPagamento - valorTotal;
    }

    let model = {
      empresaGlobalId: empresa.id,
      empresaId: empresa.empresaBaseId,
      pessoaGlobalId: authData.userInfo.pessoaId,
      dataHoraPedido: moment().format(),
      modalidadeId: modalidadeId,
      endLogradouro: endereco.logradouro,
      endNumero: endereco.numero,
      endComplemento: endereco.complemento,
      endReferencia: endereco.referencia,
      endCep: endereco.cep,
      endBairroNome: endereco.nomeBairro,
      endCidadeId: endereco.cidadeId,
      endLatitude: endereco.latitude,
      endLongitude: endereco.longitude,
      valorDesconto: 0,
      valorProduto: valorTotal - endereco.taxaEntrega,
      valorTotal: valorTotal,
      valorTroco: valorTroco,
      valorTxEntrega: endereco.taxaEntrega,
      situacaoId: 1, // Em Aberto
      observacao: observacao,
      pedidoItem: items.map(item => {
        const precoSelecionado = item.precos.filter(x => x.selected)[0];

        return {
          itemId: item.id,
          valorUnitario: item.valorTotal / item.quantidade,
          qtde: item.quantidade,
          valorNormal: item.valorTotal / item.quantidade,
          valorTotal: item.valorTotal,
          observacao: item.observacao || "",
          tamanhoId: precoSelecionado.tamanhoId,
          wgPedidoItemVariacao: item.pedidoItemVariacao
        };
      }),
      pedidoItemOpcional: []
    };

    const request = await this.post(`wg-pedido`, model, {
      "Tacto-Grupo-Empresarial": empresa.chaveGrupoEmpresarial,
      empresaId: empresa.empresaBaseId,
      "Content-Type": "application/json"
    });

    if (request.type === "exception") throw request.exception;

    model.id = request.data.id;
    model.empresa = { ...empresa };

    model.items = items;

    const pedidosLocal = (await asyncStorageRemote.get("pedidos")) || [];

    pedidosLocal.push({
      ...model
    });

    await asyncStorageRemote.save("pedidos", pedidosLocal);

    return request.data;
  }
}
