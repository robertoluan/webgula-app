import ApiBase from "../../infra/http/api.base";
import Http from "../../infra/http/http";
import EmpresaRemote from "./empresa.remote";

export default class FidelidadeRemote extends ApiBase {
  constructor() {
    super(null, { auth: true });
    this.http = new Http();
  }

  async findHistoricoLancamento(fidelidadeContaId) {
    const data = await this.getHeaders(
      `api/fidelidade/hitorico-movimento/${fidelidadeContaId}`
    );
    return data;
  }

  async findHistoricoResgate(fidelidadeId) {
    const data = await this.getHeaders(
      `api/fidelidade/historico-resgate-premio/${fidelidadeId}`
    );
    return data;
  }

  async solicitarResgatePremio(resgate) {
    const data = await this.postHeaders(
      `api/fidelidade/solicitar-resgate-premio`,
      resgate
    );
    return data;
  }

  // QUANDO O USUARIO ESTA VISUALIZANDO A SAIDA ELE PODE RESGATAR OS PONTOS REFERENTE A VENDA
  async resgatarPontosSaida(saidaId) {
    const data = await this.postHeaders(
      `api/fidelidade/resgatar-pontos-saida/${saidaId}`
    );

    return data;
  }

  async alterarLiderFidelidade(saidaId, usuarioAccId) {
    const data = await this.putHeaders(
      `acompanhamento-conta/${saidaId}/${usuarioAccId}/tornar-lider`
    );

    return data;
  }

  async alterarGanhadorFidelidade(saidaId, usuarioAccId) {
    const data = await this.putHeaders(
      `acompanhamento-conta/${saidaId}/${usuarioAccId}/fidelizar`
    );

    return data;
  }

  // QUANDO O USUARIO SE FIDELIZA ATRAVEZ DO MENU ELE IRA CADASTRAR UMA CONTA
  async cadastrarContaFidelidade(fidelidadeId) {
    const data = await this.postHeaders(`api/fidelidade/cadastrar-conta`, {
      fidelidadeId
    });
    return data;
  }

  async findPremiacoes(fidelidadeId) {
    const data = await this.getHeaders(
      `api/fidelidade/premiacoes/${fidelidadeId}`
    );
    return data;
  }

  async findAllFidelidade() {
    const url = `api/fidelidade/empresas`;

    let empresas = await this.cacheIntecept(url);

    empresas = empresas.map(item => {
      if (!!item.logoUrl && item.logoUrl.indexOf("http") === -1) {
        item.logoUrl = new EmpresaRemote().createImage(
          item.chaveGrupoEmpresarial,
          item.cadEmpresaGlobalId,
          item.logoUrl
        );
      }

      return { ...item };
    });

    return empresas;
  }

  async findFidelidadeCampanhaAtiva() {
    const empresa = await this.getEmpresaDefault();
    const empresaGlobalId = empresa.id;
    const data = await this.getHeaders(
      `api/fidelidade/campanha-ativa/${empresaGlobalId}`
    );

    return data;
  }

  async getTipoLancamento() {
    return [
      {
        id: 1,
        nome: "Crédito por Compra",
        flgDebitoCredto: "C"
      },

      {
        id: 2,
        nome: "Débito por Utilização",
        flgDebitoCredto: "D"
      },

      {
        id: 3,
        nome: "Débito por Estorno de Compra",
        flgDebitoCredto: "D"
      },

      {
        id: 4,
        nome: "Crédito por Transferência",
        flgDebitoCredto: "C"
      },
      {
        id: 5,
        nome: "Débito por Transferência",
        flgDebitoCredto: "D"
      },

      {
        id: 6,
        nome: "Crédito de Bônus",
        flgDebitoCredto: "C"
      },

      {
        id: 7,
        nome: "Pontos Expirados",
        flgDebitoCredto: "D"
      },
      {
        id: 8,
        nome: "Crédito por Adesão",
        flgDebitoCredto: "C"
      }
    ];
  }
}
