import store from "../../../infra/store";
import AsyncStorageRemote from "../../remote/asyncStorage.remote";

const asyncStorageRemote = new AsyncStorageRemote();

export function removeAll() {
  return async dispatch => {
    dispatch({
      type: "APP.DELIVERY.CLEAR_CART",
      payload: {
        items: [],
        modalidadeId: null,
        origin: null,
        pedidoId: null,
        observacao: null
      }
    });

    return true;
  };
}

export function removeItem(uuid) {
  return async dispatch => {
    let state = store.getState();

    let items = state.app.delivery.items;

    items = items.filter(item => item.uuid != uuid);

    dispatch({
      type: "APP.DELIVERY.REMOVE_CART",
      payload: {
        items: items
      }
    });

    return true;
  };
}

export function addItem(item) {
  return function(dispatch, getState) {
    let state = store.getState();

    let items = state.app.delivery.items || [];

    let existe = !!items.filter(x => x.uuid == item.uuid)[0];

    if (existe) {
      items.forEach((x, index) => {
        if (x.uuid === item.uuid) {
          items[index] = { ...item };
        }
      });
    } else {
      item.uuid = +new Date();
      item.quantidade = item.quantidade || 1;
      items.push(item);
    }

    dispatch({
      type: "APP.DELIVERY.ADD_CART",
      payload: {
        items
      }
    });

    return true;
  };
}

export function getItem(uuid) {
  return async dispatch => {
    let state = store.getState();

    let item = state.app.delivery.items.filter(item => item.uuid == uuid)[0];

    dispatch({
      type: "APP.DELIVERY.LOAD_ITEM",
      payload: {
        item
      }
    });

    return true;
  };
}

export function getPedidos() {
  return async dispatch => {
    const pedidos = (await asyncStorageRemote.get("pedidos")) || [];

    dispatch({
      type: "APP.DELIVERY.LOAD",
      payload: {
        pedidos
      }
    });

    return true;
  };
}

export function updateItem(item) {
  return async dispatch => {
    let state = store.getState();

    let items = state.app.delivery.items;

    items.forEach((itemFor, key) => {
      if (itemFor.uuid == item.uuid) {
        items[key] = {
          ...item
        };
      }
    });

    dispatch({
      type: "APP.DELIVERY.UPDATE_ITEM",
      payload: {
        items
      }
    });

    return true;
  };
}
