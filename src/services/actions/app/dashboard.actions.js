import DashboardRemote from '../../remote/dashboard.remote'

const dashboardInstance = new DashboardRemote()
import { setLoading } from '../layout/global.actions';

export function loadDashboard() {
    return async dispatch => {
        dispatch({ type: 'APP.DASHBOARD.LOAD_DASHBOARD' })
        try {
            dispatch(setLoading(true, 'Carregando...'));
            const dashboard = await dashboardInstance.getDashboard()
            dispatch(dashboardSuccess(dispatch, dashboard))
            dispatch(setLoading(false));
        } catch (error) {
            dispatch(handleError(error))
        }
    }
}

function dashboardSuccess(dispatch, dashboard) {
    return dispatch => {
        dispatch({
            type: 'APP.DASHBOARD.LOAD_DASHBOARD_SUCCESS',
            payload: dashboard
        })
    }
}

function handleError(error) {
    return {
        type: 'APP.DASHBOARD.LOAD_DASHBOARD_ERROR',
        payload: error
    }
}

