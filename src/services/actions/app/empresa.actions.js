import EmpresaRemote from "../../remote/empresa.remote";

const EmpresaRemoteApi = new EmpresaRemote();

export function loadEmpresas(params = {}) {
  return async dispatch => {
    dispatch({ type: "APP.EMPRESA.LOAD_EMPRESAS" });

    try {
      const empresas = await EmpresaRemoteApi.findEmpresasMenu(params);

      dispatch(empresasSuccess(dispatch, empresas));

      return true;
    } catch (error) {
      dispatch(empresaError(error));
      return false;
    }
  };
}

function empresasSuccess(dispatch, empresas) {
  return dispatch => {
    dispatch({
      type: "APP.EMPRESA.EMPRESAS_SUCCESS",
      payload: {
        empresas
      }
    });
  };
}

function empresaError(error) {
  return {
    type: "APP.EMPRESA.EMPRESAS_ERROR",
    payload: {
      error
    }
  };
}
