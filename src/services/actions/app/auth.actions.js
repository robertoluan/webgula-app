import AsyncStorageRemote from "../../remote/asyncStorage.remote";
import EmpresaRemote from "../../remote/empresa.remote";
import { GA_KEY } from "../../../infra/config";
import { getValueEndereco } from "../../resolvers/shared/list.resolvers";
import { Alert } from "react-native";

const empresaRemote = new EmpresaRemote();
const asyncStorageRemote = new AsyncStorageRemote();

export function setAuth(token, userInfo, isAuth, refresh_token) {
  return function(dispatch, getState) {
    dispatch({
      type: "APP.AUTH.SET_AUTH_USERINFO",
      token: token,
      refresh_token,
      isAuth,
      isClientCredentials: isAuth ? false : true,
      userInfo
    });
  };
}

export function getEndereco() {
  return async function(dispatch, getState) {
    const menu = (await asyncStorageRemote.get("endereco_menu")) || {};
    const atual = (await asyncStorageRemote.get("endereco_atual")) || {};
    const items = (await asyncStorageRemote.get("enderecos")) || [];

    dispatch({
      type: "APP.ENDERECO.LOAD",
      items,
      atual,
      menu
    });
  };
}

export function setEnderecoAtual(endereco) {
  return async function(dispatch, getState) {
    const menu = (await asyncStorageRemote.get("endereco_menu")) || {};
    let items = (await asyncStorageRemote.get("enderecos")) || [];

    let atual = {};

    if (endereco) {
      atual = { ...endereco };

      items.forEach(item => {
        item.flgPrincipal = false;
        if (item.id == atual.id) {
          item.flgPrincipal = true;
        }
      });

      await asyncStorageRemote.save("endereco_atual", atual);
      await asyncStorageRemote.save("enderecos", items);
    } else {
      await asyncStorageRemote.save("endereco_atual", null);
    }

    dispatch({
      type: "APP.ENDERECO.CHANGE",
      items,
      atual,
      menu
    });
  };
}

export function setEnderecoMenu(endereco) {
  return async function(dispatch, getState) {
    const atual = (await asyncStorageRemote.get("endereco_atual")) || {};
    const items = (await asyncStorageRemote.get("enderecos")) || [];
    const menu = { ...endereco };

    await asyncStorageRemote.save("endereco_menu", menu);

    dispatch({
      type: "APP.ENDERECO.CHANGE",
      items,
      atual,
      menu
    });
  };
}

export function removeEndereco(id) {
  return async function(dispatch, getState) {
    let items = getState().endereco.items || [];
    const menu = getState().endereco.menu || {};
    const atual = getState().endereco.atual || {};

    items = items.filter(x => x.id !== id);

    await asyncStorageRemote.save("enderecos", items);

    dispatch({
      type: "APP.ENDERECO.CHANGE",
      items,
      atual,
      menu
    });
  };
}

export function clearAuth() {
  return function(dispatch, getState) {
    dispatch({
      type: "APP.AUTH.SET_AUTH_USERINFO",
      token: null,
      isAuth: false,
      isClientCredentials: false,
      userInfo: null
    });
  };
}

export function checkAlertAuth(navigation, message = null) {
  return Alert.alert(
    "Aviso",
    message ||
      "Você precisa estar autênticado para visualizar este recurso. Deseja fazer login agora ?",
    [
      {
        text: "Voltar"
      },
      {
        text: "Fazer Login",
        onPress: () => navigation.navigate("Login")
      }
    ]
  );
}
export async function getCompleteAddressToLocation() {
  return new Promise((resolve, reject) => {
    navigator.geolocation.getCurrentPosition(
      async position => {
        // busca na api do google o endereço
        let uri = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${
          position.coords.latitude
        },${position.coords.longitude}&key=${GA_KEY}`;

        const promise = await fetch(uri);

        const result = await promise.json();

        let endereco = {};

        if (result.results && result.results.length) {
          const address_components = result.results[0].address_components;

          const numero = getValueEndereco("street_number", address_components);
          const logradouro = getValueEndereco("route", address_components);
          const bairro = getValueEndereco(
            "sublocality_level_1",
            address_components
          );
          const cidade = getValueEndereco(
            "administrative_area_level_2",
            address_components
          );
          const estado = getValueEndereco(
            "administrative_area_level_1",
            address_components
          );
          const pais = getValueEndereco("country", address_components);
          const cep = getValueEndereco(
            "postal_code",
            address_components
          ).replace("-", "");

          let cepLimpo =
            (cep &&
              cep
                .toString()
                .replace(/\-/g, "")
                .replace(/\./g, "")
                .replace(/ /g, "")) ||
            "";

          endereco = {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            numero,
            logradouro,
            nomeBairro: bairro,
            nomeCidade: cidade,
            nomeEstado: estado,
            pais,
            cep: cepLimpo,
            nome: ""
          };

          if (cepLimpo) {
            try {
              const res = await empresaRemote.procurarCep(cepLimpo);

              endereco.logradouro = res.logradouro;
              endereco.complemento = res.complemento;
              endereco.referencia = res.referencia;
              endereco.bairroId = res.bairroId;
              endereco.cepId = res.cepId;
            } catch (e) {}
          }
        }

        setTimeout(() => resolve(endereco), 200);
      },
      reject,
      { enableHighAccuracy: false, timeout: 20000, maximumAge: 1000 }
    );
  });
}
