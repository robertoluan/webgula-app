export function listLoading(list, loading = true) {
	return {
		type: 'SHARED.LIST.FETCHING',
		loading,
		list
	};
}

export function listSuccess(list, items, filter) {
	return {
		type: 'SHARED.LIST.FETCH_SUCCESS',
		items,
		list,
		filter
	};
}

export function destroy(list, keepFilters = false) {
	return {
		type: 'SHARED.LIST.DESTROY',
		list,
		keepFilters
	};
}

export function listFail(list, error, exception) {
	return {
		type: 'SHARED.LIST.FETCH_FAIL',
		error,
		exception,
		list
	};
}

export function updateFilter(list, filter) {
	return {
		type: 'SHARED.LIST.UPDATE_FILTER',
		list,
		filter
	};
}
