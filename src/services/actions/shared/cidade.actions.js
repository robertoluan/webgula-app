import CidadesRemote from '../../remote/cidades.remote'

const cidadesInstance = new CidadesRemote()
import { setLoading } from '../layout/global.actions';

export function loadCidades(id) {
    return async dispatch => {
        dispatch({ type: 'APP.SHARED.LOAD_CIDADES' })
        try {
            dispatch(setLoading(true, 'Carregando...'));
            const cidades = await cidadesInstance.getCidadeByEstadoId(id)
            dispatch(loadCidadesSuccess(dispatch, cidades))
            dispatch(setLoading(false));
        } catch (error) {
            dispatch(handleError(error))
        }
    }
}

function loadCidadesSuccess(dispatch, cidades) {
    return dispatch => {
        dispatch({
            type: 'APP.SHARED.LOAD_CIDADES_SUCCESS',
            payload: cidades
        })
    }
}

function handleError(error) {
    return {
        type: 'APP.SHARED.LOAD_CIDADES_ERROR',
        payload: error
    }
}

