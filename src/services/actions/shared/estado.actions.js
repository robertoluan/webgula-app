import EstadosRemote from '../../remote/estados.remote'

const estadosInstance = new EstadosRemote()
import { setLoading } from '../layout/global.actions';

export function loadEstados() {
    return async dispatch => {
        dispatch({ type: 'APP.SHARED.LOAD_ESTADOS' })
        try {
            dispatch(setLoading(true, 'Carregando...'));
            const estados = await estadosInstance.getEstados()
            dispatch(loadEstadosSuccess(dispatch, estados))
            dispatch(setLoading(false));
        } catch (error) {
            dispatch(handleError(error))
        }
    }
}

function loadEstadosSuccess(dispatch, estados) {
    return dispatch => {
        dispatch({
            type: 'APP.SHARED.LOAD_ESTADOS_SUCCESS',
            payload: estados
        })
    }
}

function handleError(error) {
    return {
        type: 'APP.SHARED.LOAD_ESTADOS_ERROR',
        payload: error
    }
}

