export function toggleMenu(isDelivery = false) {
  return async function(dispatch, getState) {
    const state = getState();
    const menuOpen = state.layout.menu.open;

    const actionName = menuOpen ? "CLOSE" : "OPEN";

    dispatch({
      type: `LAYOUT.MENU.${actionName}`,
      isDelivery: isDelivery
    });
  };
}
