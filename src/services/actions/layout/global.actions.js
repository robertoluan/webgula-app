import Http from "../../../infra/http/http";
import store from "../../../infra/store";

export function setLoading(loading, message) {
  return {
    type: "LAYOUT.GLOBAL.SET_LOADING",
    loading,
    message
  };
}

let requests = 0;

Http.on("request-begin", () => {
  store.dispatch({
    type: "LAYOUT.GLOBAL.SET_NETWORK_ACTIVITY",
    current: ++requests
  });
});

Http.on("request-end", () => {
  store.dispatch({
    type: "LAYOUT.GLOBAL.SET_NETWORK_ACTIVITY",
    current: --requests
  });
});

Http.on("request-fail", () => {
  store.dispatch({
    type: "LAYOUT.GLOBAL.SET_NETWORK_ACTIVITY",
    current: --requests
  });
});
