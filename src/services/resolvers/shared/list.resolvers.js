const emptyArray = [];
const emptyObject = {};

export function resolveList(state, listName) {
  const list = state.shared.list[listName];
  let items = emptyArray;

  if (list) {
    items = list.items || emptyArray;
  }

  return items;
}

export function resolveListLoading(state, listName) {
  const list = state.shared.list[listName];
  let loading = false;

  if (list) {
    loading = list.loading || false;
  }

  return loading;
}
export function resolveFilter(state, listName) {
  const list = state.shared.list[listName];
  let filter = emptyObject;

  if (list) {
    filter = list.filter || emptyObject;
  }

  return filter;
}

export function getValueEndereco(type, address_components) {
  return (address_components.filter(x => x.types.indexOf(type) > -1)[0] || {})
    .long_name;
}
