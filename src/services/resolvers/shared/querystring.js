export function parseQueryString(obj) {
  var str = [];
  for (var p in obj)
    if (obj.hasOwnProperty(p)) {
      str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
    }
  return str.join("&");
}

export function getParams(params) {
  let outherParams = {};

  for (let prop in params) {
    if (params[prop] !== undefined) {
      outherParams[prop] = params[prop];
    }
  }

  return outherParams;
}
