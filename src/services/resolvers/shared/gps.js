import { PermissionsAndroid, PermissionStatus, Platform } from "react-native";

export async function requestLocationPermissionAll() {
  if (Platform.OS == "android") {
    return await requestLocationPermissionAndroid();
  }

  return true;
}

export async function requestLocationPermissionAndroid() {
  try {
    // Android
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      return true;
    } else {
      return false;
    }
  } catch (err) {
    return false;
  }
}

export async function requestCurrentPosition() {
  return new Promise((resolve, reject) => {
    navigator.geolocation.getCurrentPosition(
      async position => {
        setTimeout(() => resolve(position.coords), 200);
      },
      reject,
      { enableHighAccuracy: false, timeout: 20000, maximumAge: 1000 }
    );
  }, 300);
}
