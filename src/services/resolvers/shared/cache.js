import moment from "moment";

export function diffMinutes(start) {
  return moment(moment(start)).diff(moment(), "minutes");
}
