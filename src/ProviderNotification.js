import React from "react";
import { Alert } from "react-native";
import { Clipboard } from "react-native";
import firebase from "react-native-firebase";
import AsyncStorageRemote from "./services/remote/asyncStorage.remote";
import AccountsRemote from "./services/remote/accounts.remote";

const asyncStorageRemote = new AsyncStorageRemote();

class ProviderNotification extends React.Component {
  componentDidMount = async () => {
    await this.checkPermissionFB();

    this.createNotificationListeners();
  };

  componentWillUnmount = () => {
    //this.notificationListener();
    this.notificationOpenedListener();
  };

  createNotificationListeners = async () => {
    this.notificationListener = firebase
      .notifications()
      .onNotification(notification => {
        const { title, body } = notification;

        //alert("Recebeu uma notificacao com o app fechado com o titulo: " + title);

        // alert("notificationListener " + title);
      });

    /*
     * If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
     * */
    this.notificationOpenedListener = firebase
      .notifications()
      .onNotificationOpened(notificationOpen => {
        const action = notificationOpen.action;

        const { title, body } = notificationOpen.notification;

        // alert("Recebeu uma notificacao com o titulo: " + title);

        var seen = [];

        // alert(
        //   JSON.stringify(notificationOpen, function(key, val) {
        //     if (val != null && typeof val === "object") {
        //       if (seen.indexOf(val) >= 0) {
        //         return;
        //       }
        //       seen.push(val);
        //     }
        //     return val;
        //   })
        // );

        // this.showAlert(title, body);
      });

    /*
     * If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
     * */
    const notificationOpen = await firebase
      .notifications()
      .getInitialNotification();

    if (notificationOpen) {
      const { title, body } = notificationOpen.notification;

      alert("NOTIFICACAO COM APP FECHADO!!" + title);
      //this.showAlert(title, body);
    }
    /*
     * Triggered for data only payload in foreground
     * */
    this.messageListener = firebase.messaging().onMessage(message => {
      //process data message
      // alert(JSON.stringify(message));
    });
  };

  showAlert = (title, body) => {
    Alert.alert(
      title,
      body,
      [{ text: "OK", onPress: () => console.log("OK Pressed") }],
      { cancelable: false }
    );
  };

  checkPermissionFB = async () => {
    const enabled = await firebase.messaging().hasPermission();

    if (enabled) {
      await this.getToken();
    } else {
      this.requestPermission();
    }
  };

  getToken = async () => {
    let fcmToken = await asyncStorageRemote.getTokenDevice();

    if (fcmToken && fcmToken.toLowerCase() === "x-x-x") fcmToken = null;

    if (!fcmToken) {
      fcmToken = await firebase.messaging().getToken();

      if (fcmToken) {
        // user has a device token
        await asyncStorageRemote.setTokenDevice(fcmToken);
      }
    }
  };

  requestPermission = async () => {
    try {
      await firebase.messaging().requestPermission();
      // User has authorised
      return this.getToken();
    } catch (error) {
      // User has rejected permissions
      console.log("permission rejected");
    }
  };

  render = () => <React.Fragment>{this.props.children}</React.Fragment>;
}

export default ProviderNotification;
