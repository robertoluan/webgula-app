import { Platform } from "react-native";

export default {
  statusBarHeight: Platform.OS == "android" ? 24 : 0
};
