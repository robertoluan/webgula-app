import Http from '../http/http';
import { AsyncStorage } from 'react-native';
import moment from 'moment';
import store from '../store';
import { setAuth } from '../../services/actions/app/auth.actions';

const KEY_AUTH = "auth-webgula-novo";

class AuthContext {
	authData = null;

	constructor() {
		this.http = new Http();
	}

	setup = async () => {
		const data = await AsyncStorage.getItem(KEY_AUTH);

		if (data) {
			this.authData = JSON.parse(data);
		}
	};

	refreshToken = async () => {
		const options = {
			grant_type: 'refresh_token',
			client_id: 'webgula_mobile',
			audience: 'tacto-webgula-api profile openid'
		};

		const userData = await this.userDataAsync();

		try {
			const response = await this.http._login(
				{
					refresh_token: userData.refresh_token
				},
				options
			);

      console.log("refresh token response", response);

      if (response.success) {
        const { data } = response;

        const { userInfo, access_token, refresh_token } = data;

        this.token = access_token;

        console.log("data auth", refresh_token);

        this.authData = {
          ...data,
          refresh_token: refresh_token,
          token: access_token,
          isClientCredentials: 0,
          userInfo,
          created: +new Date()
        };

        await this.setAuth(this.authData);

        return { success: true, ...this.authData };
      }

      return { success: false, error: "Código incorreto!" };
    } catch (e) {
      //debugger;
      return { success: false, error: e };
    }
  };

  login = async (username, password) => {
    const options = {
      grant_type: "password",
      client_id: "webgula_mobile",
      audience: "tacto-webgula-api profile openid"
    };

    try {
      let response = await this.http._login(
        {
          username,
          password
        },
        options
      );

      if (response.success) {
        let { data } = response;

        let { userInfo, access_token } = data;

        this.token = access_token;

        this.authData = {
          ...data,
          token: access_token,
          isClientCredentials: 0,
          userInfo,
          created: +new Date()
        };

        await this.setAuth({ ...this.authData });

        return { success: true, ...this.authData };
      }

      return { success: false, error: "Usuário ou senha incorreto!" };
    } catch (e) {
      //debugger;
      return { success: false, error: e };
    }
  };

  loginClientCredentials = async () => {
    try {
      const response = await this.http._loginClientCredentials();

      if (response.success) {
        const { data } = response;

        const { access_token } = data;

        this.token = access_token;

        const authData = {
          ...data,
          token: access_token,
          isClientCredentials: 1,
          created: new Date()
        };

        await AsyncStorage.setItem(KEY_AUTH, JSON.stringify(authData));

        return { success: true, data: authData, token: access_token };
      }

      return {
        success: false,
        error:
          "Houve um problema na aplicação, entre em contato com o adminstrador do sistema!"
      };
    } catch (e) {
      //debugger;
      return { success: false, error: e };
    }
  };

  updateUserInfo = async () => {
    const data = await this.http._userInfo(this.token);

    if (data) {
      const authData = { ...this.authData };

      this.authData = {
        ...authData,
        userInfo: { ...authData.userInfo, ...data }
      }; 
      
      await this.setAuth(this.authData);
    }
  };

  async setAuth(authData) {
    const res = await AsyncStorage.setItem(KEY_AUTH, JSON.stringify(authData));

    store.dispatch(
      setAuth(authData.token, authData.userInfo, true, authData.refresh_token)
    );

		return res;
	}

	async registrar(model) {
		//debugger;
		try {
			const response = await this.http._registrar(model, {
				Authorization: `Bearer ${this.token}`
			});

			if (response) {
				return { success: true, data: response };
			}

			return { success: false, data: response };
		} catch (e) {
			//debugger;
			return { success: false, error: e };
		}
	}

	async atualizar(model) {
		//debugger;
		try {
			const response = await this.http._atualizar(model, {
				Authorization: `Bearer ${this.token}`
			});

			if (response) {
				return { success: true, data: response };
			}

			return { success: false, data: response };
		} catch (e) {
			//debugger;
			return { success: false, error: e };
		}
	}

	async logout() {
		//const webgulaDeviceId = await AsyncStorage.getItem("webgulaDeviceId");

		// await AsyncStorage.removeItem("firstLogin");
		// await AsyncStorage.removeItem("endereco_atual");
		// await AsyncStorage.removeItem("enderecos");
		// await AsyncStorage.removeItem("pedidos");
		await AsyncStorage.clear();
		///await AsyncStorage.setItem("webgulaDeviceId", webgulaDeviceId);

		return true;
	}

	isExpiresAuth = async () => {
		const data = await this.userDataAsync();

		const expires_in = data.expires_in;

		const created = data.created;

		// esta deslogado
		if (moment().diff(created, 'seconds') >= expires_in) {
			return true;
		}

		return false;
	};

	isAuthAsync = async () => {
		const data = await this.userDataAsync();

		if (!data || !data.created) {
			return false;
		}

		if (data.isClientCredentials) {
			return false;
		}

		const expires_in = data.expires_in;

		const created = data.created;

		// esta deslogado
		if (moment().diff(created, 'seconds') >= expires_in) {
			return false;
		}

		return true;
	};

	isClientCredentialsAsync = async () => {
		const data = await this.userDataAsync();

		if (!data || !data.created) return false;

		if (!data.isClientCredentials) return false;

		const expires_in = data.expires_in;

		const created = data.created;

		// esta deslogado
		if (moment().diff(created, 'seconds') >= expires_in) {
			return false;
		}

		return true;
	};

	// obsolete
	get isAuth() {
		const token = this.token != null;

		if (!token) return false;

		let expires_in = this.authData.expires_in;

		let created = this.authData.created;

		// esta deslogado
		if (moment().diff(created, 'seconds') >= expires_in) {
			return false;
		}

		return token;
	}

	get token() {
		if (this.authData) return this.authData.token;

		return null;
	}

	userDataAsync = async () => {
		const userDataParse = (await AsyncStorage.getItem(KEY_AUTH)) || '{}';

		const authData = JSON.parse(userDataParse);

		this.authData = authData;

		return authData;
	};

	get userInfo() {
		if (this.authData) return this.authData.userInfo;
		return null;
	}
}

export default new AuthContext();
