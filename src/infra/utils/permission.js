import { PermissionsAndroid, Platform } from "react-native";

export async function checkPermissionsAndroidSMS() {
  let hasPermissions = false;
  try {
    hasPermissions = await PermissionsAndroid.check(
      PermissionsAndroid.PERMISSIONS.READ_SMS
    );

    if (!hasPermissions) return false;

    // hasPermissions = await PermissionsAndroid.check(
    //   PermissionsAndroid.PERMISSIONS.SEND_SMS
    // );

    if (!hasPermissions) return false;
  } catch (e) {
    console.error(e);
  }
  return hasPermissions;
}

export async function requestPermissionsAndroidSMS() {
  let granted = {};
  try {
    granted = await PermissionsAndroid.requestMultiple(
      [
        PermissionsAndroid.PERMISSIONS.READ_SMS
        // PermissionsAndroid.PERMISSIONS.SEND_SMS
      ]
      //   {
      //     title: "Example App SMS Features",
      //     message: "Example SMS App needs access to demonstrate SMS features",
      //     buttonNeutral: "Ask Me Later",
      //     buttonNegative: "Cancel",
      //     buttonPositive: "OK"
      //   }
    );
    console.log(granted);
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      console.log("You can use SMS features");
    } else {
      console.log("SMS permission denied");
    }
  } catch (err) {
    console.warn(err);
  }
}
