export function cleanNumber(number) {
  number = (number || "").toString();
  number = number.replace(/[^0-9]/g, "");

  return number;
}

export function isNumber(number) {
  let regexSomentoNumero = /^[0-9]*$/g;

  return regexSomentoNumero.test(number);
}
