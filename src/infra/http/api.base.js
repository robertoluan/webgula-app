import Http, { methods } from "./http";
import authContext from "../auth";
import AsyncStorageRemote from "../../services/remote/asyncStorage.remote";
import { diffMinutes } from "../../services/resolvers/shared/cache";
import moment from "moment";

const asyncStorageRemote = new AsyncStorageRemote();

class ApiBase {
  _options = {};

  constructor(endpoint, options = {}, http) {
    this.http = http || new Http();
    this.endpoint = endpoint;
    this._options = options;
  }

  async request(url, method, body, headers = {}) {
    if (authContext.isAuth) {
      this.http.defaultHeaders = {
        Authorization: `Bearer ${authContext.token}`
      };

      headers.Authorization = `Bearer ${authContext.token}`;
    }

    try {
      let finalUrl = this.endpoint || "";

      if (url) finalUrl += `${url}`;

      headers["content-type"] = headers["content-type"] || "application/json";

      const response = await this.http.requestJson(
        finalUrl,
        method,
        body,
        headers
      );

      return { type: "success", data: response };
    } catch (e) {
      return {
        type: "exception",
        exception: e,
        error: "Algo inesperado aconteceu, tente novamente!"
      };
    }
  }

  async login(username, password) {
    return await this._login({ username, password });
  }

  async registrar(model) {
    this.http.defaultHeaders = {
      Authorization: `Bearer ${authContext.token}`
    };

    return await this._registrar(model);
  }

  get(url, headers) {
    return this.request(url, methods.GET, null, headers);
  }

  post(url, body, headers) {
    return this.request(url, methods.POST, body, headers);
  }

  put(url, body, headers) {
    return this.request(url, methods.PUT, body, headers);
  }

  delete(url, body, headers) {
    return this.request(url, methods.DELETE, body, headers);
  }

  async cacheIntecept(
    url,
    options = {
      disabledCache: false
    }
  ) {
    const headers = options.headers || {};
    const expiration = options.expiration || 2; // 2 minutos default para o cache

    let data = [];

    if (!options.disabledCache && (await this.isNotExpirated(url))) {
      
      data = (await asyncStorageRemote.get(url)) || [];
    } 

    if (data.length > 0) return data;

    const request = await this.request(url, methods.GET, null, headers);

    if (!options.disabledCache && request.type !== "exception") {
  
      data = request.data;

      asyncStorageRemote.save(url, data);
      asyncStorageRemote.save(
        url + "_exipira_em",
        moment().add("minutes", expiration)
      );
    } 
 
    if(options.disabledCache && request.type !== "exception") {
      data = request.data;
    }

    return data;
  }

  async isNotExpirated(collection) {
    let expiracao = await asyncStorageRemote.get(collection + "_exipira_em");

    if (expiracao && diffMinutes(expiracao) > 0) {
      return true;
    }

    return false;
  }

  async getHeaders(url, options = {}) {
    options = options || {};

    let empresa = (await this.getEmpresaDefault()) || {};

    let headers = {
      "Content-Type": "application/json"
    };

    if (empresa.empresaBaseId) {
      headers["empresaId"] = empresa.empresaBaseId;
    }

    if (empresa.chaveGrupoEmpresarial) {
      headers["Tacto-Grupo-Empresarial"] = empresa.chaveGrupoEmpresarial;
    }

    const fullUrl =
      url +
        "#empresaId=" +
        (options.prefixCache || empresa.empresaBaseId) +
        "-grupo-empresarial=" +
        empresa.chaveGrupoEmpresarial || "";

    return this.cacheIntecept(fullUrl, {
      headers,
      ...options
    });
  }

  async postHeaders(url, model) {
    let empresa = await this.getEmpresaDefault();

    let headers = {};

    if (empresa.empresaBaseId) {
      headers["empresaId"] = empresa.empresaBaseId;
    }

    if (empresa.chaveGrupoEmpresarial) {
      headers["Tacto-Grupo-Empresarial"] = empresa.chaveGrupoEmpresarial;
    }

    return this.post(url, model, headers);
  }

  async putHeaders(url, model) {
    let empresa = (await this.getEmpresaDefault()) || {};

    headers = {
      empresaId: empresa.empresaBaseId,
      "Content-Type": "application/json",
      "Tacto-Grupo-Empresarial": empresa.chaveGrupoEmpresarial
    };

    return this.put(url, model, headers);
  }

  async setEmpresaDefault(empresa) {
    return asyncStorageRemote.save("empresa", empresa);
  }

  async getEmpresaDefault() {
    return await asyncStorageRemote.get("empresa");
  }
}

export default ApiBase;
