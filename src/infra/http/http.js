import { BASE_URL, ACCOUNTS_URL } from "../config";
import EventEmitter from "eventemitter3";
import axios from "axios";
import store from "../store";
import { clearAuth } from "../../services/actions/app/auth.actions";
import { Toast } from "native-base";
import { AsyncStorage } from "react-native"; 

const baseUrl = BASE_URL;

export const methods = {
  GET: "GET",
  POST: "POST",
  PUT: "PUT",
  DELETE: "DELETE",
  HEAD: "HEAD",
  OPTIONS: "OPTIONS"
};

const EE = new EventEmitter();

class Http {
  constructor(defaultHeaders) {
    this.defaultHeaders = defaultHeaders || null;
  }

  async request(url, method, body, headers = {}) {
    //debugger;

    let urlEnd = url;
    if (urlEnd) {
      urlEnd = "/" + urlEnd;
    }

    const finalUrl = `${baseUrl}${urlEnd}`;

    try {
      //EE.emit("request-begin", { url, method, body, headers }, this);

      const response = await axios({
        url: finalUrl,
        method,
        data: body,
        headers
      });

      // fetch(finalUrl, {
      //   method,
      //   headers,
      //   body
      // });

      EE.emit(
        "request-end",
        {
          url,
          method,
          body,
          headers,
          response: response.data,
          fullResponse: response
        },
        this
      );

      return response.data;
    } catch (e) {
      if (e.response && e.response.status === 401) {
        // Pode dar ruim
        await AsyncStorage.clear();

        store.dispatch(clearAuth());
        Toast.show({
          text: "Você precisa fazer login antes de continuar!",
          duration: 5000
        });
 
      }

      EE.emit("request-fail", { url, method, body, headers, error: e }, this);

      throw e;
    }
  }

  async _login(body, options = {}) {
    options = {
      client_id: "webgula",
      client_secret: "secret",
      ...options
    };

    const url = `${ACCOUNTS_URL}/connect/token`;
    const method = "POST";
    const headers = {
      "Content-Type": "application/x-www-form-urlencoded"
    };

    body = {
      ...body,
      ...options
    };

    body = Object.keys(body)
      .map(key => {
        return encodeURIComponent(key) + "=" + encodeURIComponent(body[key]);
      })
      .join("&");

    try {
      EE.emit("request-begin", { url, method, body, headers }, this);

      const r = await fetch(url, {
        method,
        headers,
        body
      });

      if (r.status === 200) {
        let res = null;

        if (typeof r.json === "function") {
          res = await r.json();
        } else {
          res = r;
        }

        if (res.access_token) {
          let userInfo = {};

          if (
            options.grant_type === "refresh_token" ||
            options.grant_type === "phone" ||
            options.grant_type === "password"
          ) {
            userInfo = await this._userInfo(res.access_token);
          }

          let response = {
            success: true,
            data: {
              userInfo,
              ...res
            }
          };

          EE.emit(
            "request-end",
            { url, method, body, headers, response },
            this
          );

          return response;
        } else {
          EE.emit(
            "request-fail",
            { url, method, body, headers, error: e },
            this
          );
          return null;
        }
      }

      return r;
    } catch (e) {
      //debugger;
      EE.emit("request-fail", { url, method, body, headers, error: e }, this);

      return e;
    }
  }

  async _loginClientCredentials() {
    let options = {
      grant_type: "client_credentials",
      // audience: "tacto-webgula-api",
      scope: "openid profile tacto-webgula-api",
      client_id: "webgula",
      client_secret: "secret",
      scope: "tacto-webgula-api IdentityServerApi"
    };

    return this._login({}, options);
  }

  async _registrar(body, headers, method = "POST") {
    const url = `${ACCOUNTS_URL}/accounts/user/register`;

    headers = {
      "Content-Type": "application/json",
      ...headers
    };

    body = Object.keys(body)
      .map(key => {
        return encodeURIComponent(key) + "=" + encodeURIComponent(body[key]);
      })
      .join("&");

    try {
      EE.emit("request-begin", { url, method, body, headers }, this);

      const r = await fetch(url, {
        method,
        headers,
        body
      });

      if (r.status === 200) {
        let res = null;

        if (typeof r.json === "function") {
          res = await r.json();
        } else {
          res = r;
        }

        if (res) {
          EE.emit(
            "request-end",
            { url, method, body, headers, response },
            this
          );

          return res;
        } else {
          EE.emit(
            "request-fail",
            { url, method, body, headers, error: e },
            this
          );
          return false;
        }
      }

      return r;
    } catch (e) {
      //debugger;
      EE.emit("request-fail", { url, method, body, headers, error: e }, this);

      return false;
    }
  }

  async _registrarPhoneNumber(body) {
    const method = "POST";

    const url = `${ACCOUNTS_URL}/accounts/phone/register`;

    headers = {
      "Content-Type": "application/json"
    };

    try {
      const r = await fetch(url, {
        method,
        headers,
        body
      });

      if (r.status === 200) {
        let res = null;

        if (typeof r.json === "function") {
          res = await r.json();
        } else {
          res = r;
        }

        if (res) {
          return res;
        } else {
          return false;
        }
      }

      return r;
    } catch (e) {
      return false;
    }
  }

  async simplesRequest(url, method, headers, body) {
    headers = {
      "Content-Type": "application/json",
      ...headers
    };

    try {
      const r = await fetch(url, {
        method,
        headers,
        body
      });

      return r;
    } catch (e) {
      return e;
    }
  }
  // METODO NOVO
  async _confirmarSMS(body, headers = {}) {
    const method = "PUT";

    const url = `${ACCOUNTS_URL}/api/usuario-logado/telefone/confirmacao-sms`;

    return this.simplesRequest(url, method, headers, body);
  }
  // METODO NOVO
  async _requisitarAutorizacaoTrocarTelefone(body, headers = {}) {
    const method = "POST";

    const url = `${ACCOUNTS_URL}/api/usuario-logado/novo-telefone`;

    return this.simplesRequest(url, method, headers, body);
  }
  // METODO NOVO
  async _confirmarSMSComTelefoneTrocado(body, headers = {}) {
    const method = "PUT";

    const url = `${ACCOUNTS_URL}/api/usuario-logado/novo-telefone`;

    return this.simplesRequest(url, method, headers, body);
  }
  // METODO NOVO
  async _recuperarSenha(body, headers = {}) {
    const method = "POST";

    const url = `${ACCOUNTS_URL}/api/usuario-logado/recuperar-senha`;

    return this.simplesRequest(url, method, headers, body);
  }

  // METODO NOVO
  async _pedirSMS(body, headers = {}) {
    const method = "POST";

    const url = `${ACCOUNTS_URL}/api/usuario-logado/telefone/confirmacao-sms`;

    return this.simplesRequest(url, method, headers, body);
  }

  // METODO NOVO
  async _registrarUsuario(body, headers = {}) {
    const method = "POST";

    const url = `${ACCOUNTS_URL}/api/usuarios`;

    return this.simplesRequest(url, method, headers, body);
  }

  // METODO NOVO
  async _atualizarUsuario(body, headers = {}) {
    const method = "PUT";

    const url = `${ACCOUNTS_URL}/api/usuario-logado`;

    return this.simplesRequest(url, method, headers, body);
  }

  async _atualizar(body, headers) {
    return this._registrar(body, headers, "PUT");
  }

  async _userInfo(token) {
    const url = `${ACCOUNTS_URL}/connect/userinfo`;
    const method = "POST";
    const headers = {
      Authorization: "Bearer " + token
    };

    try {
      const r = await fetch(url, {
        method,
        headers
      });

      if (r.status === 200) {
        let data = null;

        if (typeof r.json === "function") {
          data = await r.json();
        } else {
          data = r;
        }

        try {
          if (data.pessoaglobalid) {
            // Busca no webgula as infos da pessoa
            const requestPessoa = await fetch(
              `${BASE_URL}/pessoas/${data.pessoaglobalid}`,
              {
                method: "GET",
                headers
              }
            );

            let pessoaData = await requestPessoa.json();

            return {
              ...pessoaData,
              telefone: pessoaData.telefoneCelular,
              pessoaId: pessoaData.id,
              ...data
            };
          }
        } catch (e) {
          console.log("error no userinfor", JSON.stringify(e));
        }

        return data;
      }

      return r;
    } catch (e) {
      return e;
    }
  }

  async requestJson(url, method, body = {}, headers) {
    body = JSON.stringify(body);

    const _headers = this.defaultHeaders || {};

    let response = await this.request(url, method, body, {
      ..._headers,
      ...headers
    });

    return response;
  }

  get(url, headers) {
    return this.requestJson(url, methods.GET, null, headers);
  }

  post(url, body, headers) {
    return this.requestJson(url, methods.POST, body, headers);
  }

  put(url, body, headers) {
    return this.requestJson(url, methods.PUT, body, headers);
  }

  delete(url, body, headers) {
    return this.requestJson(url, menubar.DELETE, body, headers);
  }
}

Http.on = (...args) => EE.on(...args);
Http.once = (...args) => EE.once(...args);
Http.removeListener = (...args) => EE.removeListener(...args);

export default Http;
