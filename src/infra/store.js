import { createStore, applyMiddleware, compose } from 'redux';
import reducers from '../services/reducers';
import { createLogger } from 'redux-logger';
import thunk from 'redux-thunk';
import { REDUX_DEBUG_VERBOSE } from './config'

const logger = createLogger();

const composeEnhancers =
    typeof window === 'object' &&
        window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
        window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
            // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
        }) : compose;

const middlewares = [thunk];

  if (REDUX_DEBUG_VERBOSE) middlewares.push(logger);

const enhances = composeEnhancers(
    applyMiddleware(...middlewares)
);

export default createStore(
    reducers,
    enhances
); 