import React from "react";
import { Platform } from "react-native";
import RouterStack from "./RouterStack";
import StatusBar from "./shared/components/StatusBar";
import ProviderNotification from "./ProviderNotification";
import GlobalLoader from "./shared/components/GlobalLoader";
import SplashScreen from "react-native-splash-screen";
import authContext from "./infra/auth";

const MINUTES_TIMEOUT = 60000 * 30;

class AppComponent extends React.Component {
  timeout = null;

  state = {
    componentInitialize: false
  };

  componentWillMount = async () => {
    this.setState({ componentInitialize: true });

    if (Platform.OS === "android") {
      setTimeout(() => SplashScreen.hide(), 300);
    }

    setTimeout(() => this.checkUpdateRefreshToken(), MINUTES_TIMEOUT);
  };

  checkUpdateRefreshToken = async () => {
    console.log("checkupdate");

    if (await authContext.isAuthAsync()) {
      const res = await authContext.refreshToken();

      if (!res.success) {
        clearTimeout(this.timeout);
        return;
      }
    }

    console.log("checkupdate depois");

    this.timeout = setTimeout(
      () => this.checkUpdateRefreshToken(),
      MINUTES_TIMEOUT
    );
  };

  componentWillUnmount = () => {
    clearTimeout(this.timeout);
  };

  render = () => {
    const { componentInitialize } = this.state;

    if (!componentInitialize) return <StatusBar />;

    return (
      <ProviderNotification>
        <GlobalLoader />
        <RouterStack />
      </ProviderNotification>
    );
  };
}
export default AppComponent;
