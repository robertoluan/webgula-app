import React from "react";
import { connect } from "react-redux";
import { Platform, View, TouchableOpacity } from "react-native";
import Icon from "./Icon";
import ButtonHeaderRight from "./ButtonHeaderRight";
import TextCustom from "./TextCustom";
import { colors } from "../../theme";
import { withNavigationFocus } from "react-navigation";

let iconMenu = Platform.select({
  android: "fa-shopping-cart",
  ios: "fa-shopping-cart"
});

const mapStateToProps = (state, props) => {
  const app = state && state.app;
  const delivery = app.delivery;
  const itemsDelivery = (delivery && delivery.items) || [];
  const origem = (delivery && delivery.origem) || null;

  return { itemsDelivery, origem };
};

const mapDispatchToProps = (dispatch, props) => ({});

export default withNavigationFocus(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(
    class ButtonHeaderDeliveryRight extends React.Component {
      componentDidUpdate = prevProps => {
        // Hack para atualizar o redux
        if (
          prevProps.isFocused !== this.props.isFocused &&
          this.props.isFocused
        ) {
          console.log("caiu aqui focus");

          // this.forceUpdate();
        }
      };

      renderButton = () => {
        const { itemsDelivery } = this.props;

        let total = itemsDelivery.length;

        if (!total) return null;

        return (
          <TouchableOpacity
            style={{
              paddingRight: 16,
              marginRight: 6,
              position: "relative"
            }}
            onPress={() => this.openCart()}
          >
            <View
              style={{
                position: "absolute",
                backgroundColor: "white",
                top: 0,
                right: 4,
                width: 15,
                height: 15,
                borderRadius: 15
              }}
            >
              <TextCustom
                style={{
                  fontSize: 10,
                  paddingTop: 1,
                  textAlign: "center",
                  color: colors.primaryColor
                }}
              >
                {total}
              </TextCustom>
            </View>

            <Icon
              size={20}
              style={{ color: "rgba(255, 255, 255, .7)" }}
              name={iconMenu}
            />
          </TouchableOpacity>
        );
      };

      openCart = () => {
        if (this.props.origem === "conta-credito") {
          this.props.navigation.navigate("EstabelecimentoCreditoResumoPedido");
        } else {
          this.props.navigation.navigate("DeliveryCarrinho");
        }
      };

      render = () => {
        return (
          <React.Fragment>
            {this.renderButton()}
            <ButtonHeaderRight
              navigation={this.props.navigation}
              delivery={true}
            />
          </React.Fragment>
        );
      };
    }
  )
);
