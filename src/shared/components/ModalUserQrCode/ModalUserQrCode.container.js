import { connect } from "react-redux";
import component from "./ModalUserQrCode";

const mapStateToProps = (state, props) => {
  let app = state.app;

  return {
    auth: (app.auth && app.auth.userInfo) || {}
  };
};

const mapDispatchToProps = (dispatch, props) => ({
  init() {}
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(component);
