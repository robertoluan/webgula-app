import React from "react";
import { Modal, StyleSheet, View } from "react-native";
import { colors } from "../../../theme";
import TextCustom from "../TextCustom";
import ButtonLoading from "../ButtonLoading";
import QRCode from "react-native-qrcode";
import ContainerPage from "../ContainerPage";

class ModalUserQrCode extends React.PureComponent {
  render() {
    const { item, open, auth, onClose } = this.props;

    return (
      <Modal animationType="slide" transparent={false} visible={open}>
        <ContainerPage>
          <View style={styles.center}>
            <QRCode
              value={auth.id}
              size={150}
              bgColor="black"
              fgColor="white"
            />
          </View>

          <View
            style={{
              flexDirection: "column"
            }}
          >
            <View
              style={{
                marginTop: 32,
                backgroundColor: "#efefef",
                height: 70
              }}
            >
              <View style={{ height: 70 }}>
                <TextCustom style={styles.title}>{auth.nome}</TextCustom>
              </View>
            </View>
            <View>
              <TextCustom style={styles.title}>{item.nome}</TextCustom>
            </View>
          </View>

          <View>
            <TextCustom style={styles.subtitle}>
              ({item.tipoFavoritoNome})
            </TextCustom>
          </View>

          <View
            style={{
              margin: 16,
              position: "absolute",
              left: 0,
              bottom: 0,
              right: 0
            }}
          >
            <View style={{ marginBottom: 10 }}>
              <ButtonLoading
                titleColor={colors.white}
                backgroundColor={colors.primaryColor}
                onPress={() => onClose()}
                title="Voltar"
              />
            </View>
          </View>
        </ContainerPage>
      </Modal>
    );
  }
}

export default ModalUserQrCode;

const styles = StyleSheet.create({
  title: {
    marginTop: 22,
    marginBottom: 10,
    fontSize: 20,
    textAlign: "center",
    alignItems: "center",
    justifyContent: "center",
    fontFamily: "Montserrat-Bold",
    color: "#333"
  },
  subtitle: {
    marginTop: 10,
    marginBottom: 20,
    fontSize: 16,
    textAlign: "center",
    alignItems: "center",
    justifyContent: "center",
    color: "#666"
  },
  icon: {
    color: "#56b910"
  },
  center: {
    marginTop: 40,
    textAlign: "center",
    alignItems: "center",
    justifyContent: "center"
  }
});
