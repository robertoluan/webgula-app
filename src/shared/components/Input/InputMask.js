import React from "react";
import { View, StyleSheet } from "react-native";
import { TextInputMask, MaskService } from "react-native-masked-text";

import TextCustom from "../TextCustom";
import styled from "styled-components";
import { colors } from "../../../theme";

const Label = styled(View)`
  top: 12;
  left: 8;
  z-index: 1;
  position: absolute;
`;

export default class InputMask extends React.Component {
  state = {
    focused: false,
    text: ""
  };

  componentWillMount() {
    this.setState({
      text: this.props.value
    });
  }

  componentWillUpdate(nextProps) {
    if (nextProps.value != this.props.value) {
      this.setState({
        text: nextProps.value
      });
    }
  }

  onFocus(e) {
    this.setState({
      focused: true
    });
    if (typeof this.props.onFocus == "function") {
      this.props.onFocus(e);
    }
  }

  onBlur(e) {
    this.setState({
      focused: false
    });
    if (typeof this.props.onBlur == "function") {
      this.props.onBlur(e);
    }
  }

  onChangeText = text => {
    const { type } = this.props;

    text = MaskService.toRawValue(type, text);

    this.setState({
      text
    });

    if (this.props.onChangeText) this.props.onChangeText(text);
  };

  clear = () => {
    this.setState({ text: "" });
    if (this.props.onChangeText) this.props.onChangeText("");
  };

  render() {
    const props = this.props;

    const { focused } = this.state;

    const { type, options, value = "", dark, label, borderless } = props;

    const containerStyle = [styles.container];

    const inputStyles = [styles.input];

    if (dark) {
      inputStyles.push(styles.inputDark);
    }

    if (borderless) {
      inputStyles.push(styles.borderless);
    }

    let styleLabel = {};

    if (focused && !borderless) {
      inputStyles.push(styles.inputFocus);
    }

    const displayLabel = label && (focused || this.state.text);

    if (displayLabel) {
      styleLabel.transform = [{ translateY: -30 }];
      styleLabel.left = 0;
      styleLabel.fontSize = 8;
    }

    return (
      <View style={containerStyle}>
        {label ? (
          <Label
            onPress={() => this.setState({ focused: true })}
            style={{ ...styleLabel }}
          >
            <TextCustom style={{ color: dark ? "#FFF" : "#666", fontSize: 13 }}>
              {label}
            </TextCustom>
          </Label>
        ) : null}

        <TextInputMask
          {...props}
          type={type}
          style={inputStyles}
          options={options || {}}
          includeRawValueInChangeText={true}
          value={MaskService.toMask(type, value || "")}
          onChangeText={maskValue => this.onChangeText(maskValue)}
          onFocus={e => this.onFocus(e)}
          onBlur={e => this.onBlur(e)}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop: 16,
    marginBottom: 5
  },
  transparent: {
    // backgroundColor: "#FFFFFF22"
  },
  input: {
    height: 40,
    borderRadius: 4,
    fontSize: 16,
    paddingLeft: 8,
    fontFamily: "Montserrat-Regular",
    paddingRight: 8,
    backgroundColor: "#FFFFFF11",
    color: "#333",
    backgroundColor: "#FFFFFF",
    borderColor: "#ddd",
    borderWidth: 1
  },
  inputFocus: {
    borderColor: colors.primaryColor
  },
  inputDark: {
    backgroundColor: "#00000011",
    color: "#FFFFFF"
  },
  borderless: {
    borderWidth: 0
  },
  inputDisabled: {
    borderBottomWidth: 0
  }
});
