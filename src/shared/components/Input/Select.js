import React from 'react';
import {
  Picker,
  View,
  Modal,
  Platform,
  StyleSheet,
  Alert,
  TouchableWithoutFeedback,
  ActivityIndicator
} from 'react-native';
import Input from './Input';
import styled from 'styled-components';
import TextCustom from '../TextCustom';

const Label = styled(View)``;

export default class InputSelect extends React.PureComponent {
  state = {
    isPickerVisible: false
  };

  getDisplaySelectedValue(value) {
    const { items, keyValue, keyText } = this.props;

    const valueText =
      items.filter(item => item[keyValue || 'text'] == value)[0] || {};

    return valueText[keyText || 'text'] || '';
  }

  getDropdown() {
    const {
      value,
      onChange,
      label,
      items,
      placeholder,
      keyValue,
      keyText,
      disabled,
      isLoading
    } = this.props;

    const isAndroid = Platform.OS.toLowerCase() === 'android';

    return (
      <View>
        {isAndroid && label ? (
          <Label>
            <TextCustom style={{ color: '#333' }}>{label}</TextCustom>
          </Label>
        ) : null}

        <View style={isAndroid ? styles.inputAndroid : null}>
          {isLoading ? (
            <ActivityIndicator />
          ) : (
            <Picker
              mode={isAndroid ? 'dialog' : 'dialog'}
              style={{ height: isAndroid ? 40 : null }}
              disabled={disabled}
              selectedValue={value}
              onValueChange={(itemValue, itemIndex) => {
                onChange(itemValue);

                this.setState({
                  isPickerVisible: false
                });
              }}
            >
              {isAndroid ? (
                <Picker.Item
                  label={placeholder || '--Selecione--'}
                  value={null}
                />
              ) : null}

              {items.map(item => (
                <Picker.Item
                  label={item[keyText || 'text']}
                  value={item[keyValue || 'text']}
                />
              ))}
            </Picker>
          )}
        </View>
      </View>
    );
  }

  render() {
    const { value, disabled, label } = this.props;
    const { isPickerVisible } = this.state;

    const isAndroid = Platform.OS.toLowerCase() === 'android';

    return (
      <View
        style={{
          marginTop: 16
        }}
      >
        {!isAndroid ? (
          <Input
            label={label}
            transparent
            disabled={disabled}
            value={this.getDisplaySelectedValue(value)}
            onFocus={event => {
              if (!this.props.items || !this.props.items.length) {
                // Alert.alert("Aviso", "Não existe items nessa lista");
                event.preventDefault();
                return false;
              }

              this.setState({
                isPickerVisible: true
              });
            }}
          />
        ) : (
          this.getDropdown()
        )}

        <Modal
          animationType="slide"
          visible={isPickerVisible}
          onDismiss={() => {
            this.setState({
              isPickerVisible: false
            });
          }}
          onRequestClose={() => {
            this.setState({
              isPickerVisible: false
            });
          }}
        >
          <TouchableWithoutFeedback onPress={() => this.cancelPressed()}>
            {this.getDropdown()}
          </TouchableWithoutFeedback>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  inputAndroid: {
    borderRadius: 4,
    marginBottom: 6,
    paddingLeft: 8,
    paddingRight: 8,
    backgroundColor: '#FFFFFF',
    borderColor: '#ddd',
    borderWidth: 1
  }
});
