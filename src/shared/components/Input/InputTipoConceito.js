import React from "react";
import Select from "./Select";

export const conceitos = [
  {
    id: 9,
    text: "Bar, Boteco e Pubs ",
    informacao:
      "Estabelecimentos aonde o forte é a venda de bebidas alcoólicas e musica ao vivo. Ficando as opções de comidas restritas aos tira gostos. Pode ser simples ou sofisticado."
  },
  {
    id: 10,
    text: "Boates e Danceteria",
    informacao:
      "Muita Musica e agitação,  aonde o forte é a venda de bebidas alcoólicas"
  },
  {
    id: 4,
    text: "Cafés,Padarias ou Bistrôs",
    informacao:
      "Estes três tipos de estabelecimento possuem particularidades e semelhanças. Apesar de ambos possuírem estrutura e ambiente descontraído, cada um oferece diferentes tipos de comida.\r\nEnquanto o menu do Café oferece café, bolo, doces e sanduíches o Bistrô dispõe em seu cardápio algumas sugestões de lanches e até mesmo refeições completas."
  },
  {
    id: 2,
    text: "Estilo Familiar",
    informacao:
      "Estabelecimento que busca manter aquele clima de “jantar em família”, oferecendo serviços de mesa e pratos com preços moderados. Sua principal característica é misturar pratos clássicos junto a molhos gourmets e outras iguarias."
  },
  {
    id: 5,
    text: "Fast Food",
    informacao:
      "Atualmente é o tipo de estabelecimento mais popular do mundo. Atua com um rápido serviço de comida, conveniência e preços acessíveis.\r\nO seu maior público é o público jovem, mas conquista frequentemente diferentes públicos através da sua praticidade."
  },
  {
    id: 6,
    text: "Food Truck",
    informacao:
      "São os restaurantes de “caminhões de alimentos” móveis. Estes conseguem atingir um grande número de público com alimentos de baixo custo. A sua mobilidade faz com que consiga estar em diversos locais, encontrando e atraindo públicos por onde passa."
  },
  {
    id: 1,
    text: "Rápido e Casual",
    informacao:
      "Diferente dos Fast Food oferecem uma culinária mais sofisticada, trabalhando com pratos gourmets e ingredientes orgânicos."
  },
  {
    id: 7,
    text: "Restaurante Buffet",
    informacao:
      "Conceito que vem desde a Idade Média, ainda hoje continua sendo uma das escolhas mais populares para muitos clientes. Nele a refeição é servida pelo próprio consumidor que possui a liberdade de escolher entre as variedades de pratos que o estabelecimento oferece."
  },
  {
    id: 3,
    text: "Restaurante Fino",
    informacao:
      "Restaurante que mantém a elegância, o luxo e a sofisticação do ambiente em harmonia com belos pratos gourmets. Busca diariamente ser único oferecendo ao cliente o melhor serviço, a melhor comida e a melhor recepção. Isso justifica o alto custo na hora de pagar a conta."
  },
  {
    id: 8,
    text: "Restaurantes Pop Up",
    informacao:
      "Restaurantes em lugares improváveis, funcionando por um curto período de tempo, proporcionando uma experiência única ao consumidor."
  },
];

export default class InputTipoConceito extends React.PureComponent {
  render() {
    const { value, onChange, label, placeholder } = this.props;

    return (
      <Select
        label={label}
        placeholder={placeholder}
        items={conceitos}
        keyValue="id"
        value={value}
        onChange={onChange}
      />
    );
  }
}
