import React from "react";
import Select from "./Select";

const items = [
  {
    id: 27,
    nome: "Acre",
    sigla: "AC",
    paisId: 1,
    op: 0
  },
  {
    id: 14,
    nome: "Alagoas",
    sigla: "AL",
    paisId: 1,
    op: 0
  },
  {
    id: 25,
    nome: "Amapá",
    sigla: "AP",
    paisId: 1,
    op: 0
  },
  {
    id: 17,
    nome: "Amazonas",
    sigla: "AM",
    paisId: 1,
    op: 0
  },
  {
    id: 9,
    nome: "Bahia",
    sigla: "BA",
    paisId: 1,
    op: 0
  },
  {
    id: 23,
    nome: "Ceará",
    sigla: "CE",
    paisId: 1,
    op: 0
  },
  {
    id: 8,
    nome: "Distrito Federal",
    sigla: "DF",
    paisId: 1,
    op: 0
  },
  {
    id: 10,
    nome: "Espirito Santo",
    sigla: "ES",
    paisId: 1,
    op: 0
  },
  {
    id: 7,
    nome: "Goiás",
    sigla: "GO",
    paisId: 1,
    op: 0
  },
  {
    id: 18,
    nome: "Maranão",
    sigla: "MA",
    paisId: 1,
    op: 0
  },
  {
    id: 1,
    nome: "Mato Grosso",
    sigla: "MT",
    paisId: 1,
    op: 1
  },
  {
    id: 2,
    nome: "Mato Grosso do Sul",
    sigla: "MS",
    paisId: 1,
    op: 0
  },
  {
    id: 3,
    nome: "Minas Gerias",
    sigla: "MG",
    paisId: 1,
    op: 0
  },
  {
    id: 24,
    nome: "Para",
    sigla: "PA",
    paisId: 1,
    op: 0
  },
  {
    id: 21,
    nome: "Paraiba",
    sigla: "PB",
    paisId: 1,
    op: 0
  },
  {
    id: 6,
    nome: "Paraná",
    sigla: "PR",
    paisId: 1,
    op: 0
  },
  {
    id: 20,
    nome: "Pernambuco",
    sigla: "PE",
    paisId: 1,
    op: 0
  },
  {
    id: 19,
    nome: "Piaui",
    sigla: "PI",
    paisId: 1,
    op: 0
  },
  {
    id: 5,
    nome: "Rio de Janeiro",
    sigla: "RJ",
    paisId: 1,
    op: 0
  },
  {
    id: 13,
    nome: "Rio Grande do Norte",
    sigla: "RN",
    paisId: 1,
    op: 0
  },
  {
    id: 12,
    nome: "Rio Grande do Sul",
    sigla: "RS",
    paisId: 1,
    op: 0
  },
  {
    id: 16,
    nome: "Rondonia",
    sigla: "RO",
    paisId: 1,
    op: 0
  },
  {
    id: 15,
    nome: "Roraima",
    sigla: "RR",
    paisId: 1,
    op: 0
  },
  {
    id: 11,
    nome: "Santa Catarina",
    sigla: "SC",
    paisId: 1,
    op: 0
  },
  {
    id: 4,
    nome: "São Paulo",
    sigla: "SP",
    paisId: 1,
    op: 0
  },
  {
    id: 22,
    nome: "Sergipe",
    sigla: "SE",
    paisId: 1,
    op: 0
  },
  {
    id: 26,
    nome: "Tocantis",
    sigla: "TO",
    paisId: 1,
    op: 0
  }
];

export default class InputEstado2 extends React.PureComponent {
  render() {
    const { value, onChange, label, placeholder } = this.props;

    return (
      <Select
        label={label}
        placeholder={placeholder}
        items={items}
        keyText="nome"
        keyValue="id"
        value={value}
        onChange={onChange}
      />
    );
  }
}
