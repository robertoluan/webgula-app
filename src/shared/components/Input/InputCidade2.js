import React from "react";
import { View } from "react-native";
import Select from "./Select";
import EnderecoRemote from "../../../services/remote/estado.remote";

const ApiEstado = new EnderecoRemote();

export default class InputCidade2 extends React.PureComponent {
  state = {
    items: [],
    isLoading: false
  };

  componentDidMount = async () => {
    if (this.props.estadoId) {
      this.loadData(this.props.estadoId);
    }
  };

  componentDidUpdate = async prevProps => {
    if (prevProps.estadoId !== this.props.estadoId) {
      console.log("did update", this.props.estadoId);
      await this.loadData(this.props.estadoId);
    }
  };

  loadData = async estadoId => {
    if (!estadoId) return;

    this.setState({
      isLoading: true
    });

    try {
      const cidades = await this.getCidades(estadoId);

      console.log("cidades", cidades);

      this.setState({ items: cidades || [] });
    } catch (e) {
    } finally {
      this.setState({
        isLoading: false
      });
    }
  };

  getCidades = async estadoId => {
    return await ApiEstado.findCidade(estadoId);
  };

  render() {
    const { value, onChange, label, placeholder } = this.props;
    const { items, isLoading } = this.state;

    return (
      <View>
        <Select
          isLoading={isLoading}
          label={label}
          placeholder={placeholder}
          items={items}
          value={value}
          keyText="nome"
          keyValue="id"
          onChange={onChange}
        />
      </View>
    );
  }
}
