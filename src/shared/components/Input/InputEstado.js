import React from "react";
import Select from "./Select";

const _items = [
  {
    value: "ac",
    text: "Acre"
  },
  {
    value: "al",
    text: "Alagoas"
  },
  {
    value: "ap",
    text: "Amapá"
  },
  {
    value: "ap",
    text: "Amazonas"
  },
  {
    value: "ba",
    text: "Bahia"
  },
  {
    value: "ce",
    text: "Ceará"
  },
  {
    value: "df",
    text: "Distrito Federal"
  },
  {
    value: "es",
    text: "Espírito Santo"
  },
  {
    value: "go",
    text: "Goiás"
  },
  {
    value: "ma",
    text: "Maranhão"
  },
  {
    value: "mt",
    text: "Mato Grosso"
  },
  {
    value: "ms",
    text: "Mato Grosso do Sul"
  },
  {
    value: "mg",
    text: "Minas Gerais"
  },
  {
    value: "pa",
    text: "Pará"
  },
  {
    value: "pb",
    text: "Paraíba"
  },
  {
    value: "pb",
    text: "Paraná"
  },
  {
    value: "pe",
    text: "Pernambuco"
  },
  {
    value: "pi",
    text: "Piauí"
  },
  {
    value: "rj",
    text: "Rio de Janeiro"
  },
  {
    value: "rn",
    text: "Rio Grande do Norte"
  },
  {
    value: "rs",
    text: "Rio Grande do Sul"
  },
  {
    value: "ro",
    text: "Rondônia"
  },
  {
    value: "rr",
    text: "Roraima"
  },
  {
    value: "sc",
    text: "Santa Catarina"
  },
  {
    value: "sp",
    text: "São Paulo"
  },
  {
    value: "se",
    text: "Sergipe"
  },
  {
    value: "to",
    text: "Tocantins"
  }
];

export default class InputEstado extends React.PureComponent {
  render() {
    const { label, value, onChange, disabled, keyValue, items } = this.props;

    return (
      <Select
        label={label}
        disabled={disabled}
        items={items || _items}
        keyValue={keyValue}
        value={value}
        onChange={onChange}
      />
    );
  }
}
