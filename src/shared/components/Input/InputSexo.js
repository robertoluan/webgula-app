import React from "react";
import Select from "./Select";

const items = [
  {
    value: "m",
    text: "Masculino"
  },
  {
    value: "f",
    text: "Feminino"
  },
  {
    value: "o",
    text: "Nenhuma dessas alternativas"
  }
];

export default class InputSexo extends React.PureComponent {
  render() {
    const { value, onChangeText, label } = this.props;

    return (
      <Select
        label={label}
        items={items}
        keyValue="value"
        value={value}
        onChange={onChangeText}
      />
    );
  }
}
