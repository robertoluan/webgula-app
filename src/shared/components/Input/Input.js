import React from "react";
import {
  TextInput,
  View,
  StyleSheet,
  Platform,
  TouchableOpacity,
  Animated
} from "react-native";
import Icon from "../Icon";
import TextCustom from "../TextCustom";
import styled from "styled-components";
import { colors } from "../../../theme";

const Label = styled(View)`
  top: 12;
  left: 8;
  z-index: 1;
  position: absolute;
`;

export default class Input extends React.Component {
  state = {
    focused: false,
    fadeAnim: new Animated.Value(0),
    text: ""
  };

  componentWillMount() {
    this.setState({
      text: this.props.value
    });

    if (this.props.value) {
      this.showAnimationLabel();
    }
  }

  showAnimationLabel() {
    Animated.timing(this.state.fadeAnim, {
      toValue: 1,
      duration: 300
    }).start();
  }

  componentWillUpdate = nextProps => {
    if (nextProps.value != this.props.value) {
      this.setState({
        text: nextProps.value
      });

      if (nextProps.value) {
        this.showAnimationLabel();
      }
    }
  };

  onFocus = e => {
    this.setState({
      focused: true
    });

    this.showAnimationLabel();

    if (typeof this.props.onFocus === "function") {
      this.props.onFocus(e);
    }
  };

  onBlur = e => {
    this.setState({
      focused: false
    });
    if (typeof this.props.onBlur == "function") {
      this.props.onBlur(e);
    }
  };

  onChangeText = text => {
    this.setState({
      text
    });

    if (this.props.onChangeText) this.props.onChangeText(text);
  };

  clear = () => {
    this.setState({ text: "" });
    if (this.props.onChangeText) this.props.onChangeText("");
  };

  render = () => {
    const props = this.props;

    const { focused } = this.state;

    let {
      isSearch,
      canClear,
      transparent,
      onChangeText,
      style,
      textAlign = "left",
      onRef,
      disabled,
      dark,
      label,
      borderless,
      inputStyle,
      ..._props
    } = props;

    borderless = !!borderless;

    const containerStyle = [styles.container];
    let inputStyles = [
      styles.input,
      {
        height: _props.numberOfLines ? null : 40,
        minHeight: _props.numberOfLines ? _props.numberOfLines * 40 : 40,
        textAlign,
        fontFamily: "Montserrat-Regular"
      }
    ];

    if (disabled) {
      inputStyles.push(styles.inputDisabled);
    }

    if (style) {
      [].concat(style).forEach(stl => containerStyle.push(stl));
    }

    if (isSearch) {
      inputStyles.push(styles.hasIcon);
    }

    if (dark) {
      inputStyles.push(styles.inputDark);
    }

    if (focused && !borderless) {
      inputStyles.push(styles.inputFocus);
    }

    if (borderless) {
      inputStyles.push(styles.borderless);
    }

    if (inputStyle) {
      inputStyles.push(inputStyle);
    }

    const placeholder = _props.placeholder;

    let styleLabel = {};

    const displayLabel = label && (focused || this.state.text);

    if (displayLabel) {
      styleLabel.transform = [{ translateY: -30 }];
      styleLabel.left = 0;
      styleLabel.fontSize = 8;
    }

    let inputHeight = inputStyle && inputStyle.height;

    if (inputHeight) {
      styleLabel.top = inputHeight / 2 - 10;
    }

    return (
      <View style={containerStyle}>
        {label ? (
          <Label
            onPress={() => this.setState({ focused: true })}
            style={{ ...styleLabel }}
          >
            <TextCustom style={{ color: dark ? "#FFF" : "#666", fontSize: 13 }}>
              {label}
            </TextCustom>
          </Label>
        ) : null}

        <TextInput
          editable={!disabled}
          selectTextOnFocus={!disabled}
          {..._props}
          placeholder={placeholder}
          value={this.state.text}
          onChangeText={this.onChangeText}
          onFocus={e => this.onFocus(e)}
          onBlur={e => this.onBlur(e)}
          style={inputStyles}
          ref={onRef}
        />
        {canClear && this.state.text ? (
          <TouchableOpacity style={styles.iconButton} onPressIn={this.clear}>
            <Icon
              size={26}
              color={`#ccc`}
              iosSize={22}
              ios="sl-close"
              android="close"
            />
          </TouchableOpacity>
        ) : null}
        {isSearch ? (
          <Icon
            size={20}
            iosSize={16}
            style={styles.icon}
            ios="sl-magnifier"
            android="magnify"
          />
        ) : null}
      </View>
    );
  };
}

const styles = StyleSheet.create({
  container: {
    marginTop: 16,
    marginBottom: 5
  },
  hasIcon: {
    paddingLeft: 40
  },
  iconButton: {
    position: "absolute",
    right: Platform.OS == "ios" ? 10 : 6,
    top: Platform.OS == "ios" ? 10 : 8
  },
  transparent: {
    // backgroundColor: "#FFFFFF22"
  },
  input: {
    fontSize: 16,
    borderRadius: 4,
    height: 40,
    fontSize: 16,
    paddingLeft: 8,
    paddingRight: 8,
    backgroundColor: "#FFFFFF",
    borderColor: "#ddd",
    borderWidth: 1,
    color: "#333"
  },
  inputFocus: {
    borderColor: colors.primaryColor
  },
  inputDark: {
    backgroundColor: "#00000011",
    color: "#FFFFFF"
  },
  borderless: {
    borderWidth: 0
  },
  inputDisabled: {
    paddingLeft: 0,
    paddingRight: 0,
    borderWidth: 0
  },
  icon: {
    position: "absolute",
    color: "#777",
    left: Platform.OS === "ios" ? 12 : 8,
    top: Platform.OS === "ios" ? 12 : 10
  }
});
