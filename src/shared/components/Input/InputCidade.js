import React from "react";
import Select from "./Select";

export default class InputCidade extends React.PureComponent {
  render() {
    const { label, value, onChange, items, keyValue } = this.props;

    return (
      <Select
        label={label}
        items={items}
        keyValue={keyValue}
        value={value}
        onChange={onChange}
      />
    );
  }
}
