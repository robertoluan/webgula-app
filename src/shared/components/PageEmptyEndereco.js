import React from "react";
import { View } from "react-native";
import { connect } from "react-redux";
import Icon from "./Icon";
import TextCustom from "./TextCustom";
import ButtonLoading from "./ButtonLoading";
import { colors } from "../../theme";
import ModalEndereco from "../components/ModalEndereco";

const mapStateToProps = (state, props) => {
  return {};
};

const mapDispatchToProps = (dispatch, props) => ({});

class PageEmptyEndereco extends React.PureComponent {
  state = {
    openModalEndereco: false,
    loading: false
  };

  activeLocationLatLng = async () => {};

  render() {
    const { onClose } = this.props;
    const { openModalEndereco, loading } = this.state;

    return (
      <View style={{ flex: 1, marginTop: 60, marginRight: 32, marginLeft: 32 }}>
        {openModalEndereco ? (
          <ModalEndereco
            onSave={() => {
              this.setState({ openModalEndereco: false });
              onClose();
            }}
            onCancel={() => {
              this.setState({ openModalEndereco: false });
              onClose();
            }}
            open={openModalEndereco}
          />
        ) : null}

        <TextCustom
          style={{
            textAlign: "center",
            marginBottom: 32
          }}
        >
          <Icon name="fa-file-o" style={{ color: "#ccc" }} size={36} />
        </TextCustom>

        <TextCustom
          style={{
            fontSize: 24,
            color: "#ccc",
            textAlign: "center"
          }}
        >
          Você não nos indicou qual a sua localização :/
        </TextCustom>

        <View
          style={{
            marginTop: 66
          }}
        >
          <ButtonLoading
            loading={loading}
            onPress={() =>
              this.setState({
                openModalEndereco: true
              })
            }
            title="Adicionar Endereço"
          />
        </View>

        <View
          style={{
            marginTop: 16
          }}
        >
          <ButtonLoading
            onPress={() => this.activeLocationLatLng()}
            titleColor={colors.primaryColor}
            backgroundColor={"transparent"}
            loading={loading}
            title="Permitir Localização"
          />
        </View>
      </View>
    );
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PageEmptyEndereco);
