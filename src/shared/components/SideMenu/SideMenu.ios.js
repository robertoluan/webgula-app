import React from 'react';
import Drawer from 'react-native-drawer';

export default class SideMenu extends React.PureComponent {
	render() {
		const { children, menu, open, closeMenu } = this.props;
		return (
			<Drawer
				type="static"
				content={menu}
				open={open}
				onClose={closeMenu}
				openDrawerOffset={100}
				tapToClose={true}
				side={'right'}
				styles={drawerStyles}
				tweenHandler={Drawer.tweenPresets.parallax}
			>
				{children}
			</Drawer>
		);
	}
}

const drawerStyles = {
	drawer: { shadowColor: '#000000', shadowOpacity: 0.8, shadowRadius: 3 },
	main: { }
};
