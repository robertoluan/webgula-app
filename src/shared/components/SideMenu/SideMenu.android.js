import React from 'react';
import Drawer from 'react-native-drawer';

export default class SideMenu extends React.PureComponent {
	render() {
		const { children, menu, open, closeMenu } = this.props;
		return (
			<Drawer
				type="overlay"
				content={menu}
				open={open}
				tapToClose={true}
				side={'right'}
				onClose={closeMenu}
				openDrawerOffset={0.2} // 20% gap on the right side of drawer
				panCloseMask={0.2}
				closedDrawerOffset={-3}
				styles={drawerStyles}
				tweenHandler={(ratio) => ({
					main: { opacity: (3 - ratio) / 3 }
				})}
			>
				{children}
			</Drawer>
		);
	}
}

const drawerStyles = {
	drawer: { backgroundColor: 'black', shadowColor: '#000000', shadowOpacity: 0.8, shadowRadius: 3 },
	main: { }
};
