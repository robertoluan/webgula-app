import SideMenu from "./SideMenu";
import { connect } from "react-redux";

const mapStateToProps = (state, props) => {
  return {
    open: state.layout.menu.open
  };
};

const mapDispatchToProps = (dispatch, props) => ({
  closeMenu() {
    dispatch({
      type: "LAYOUT.MENU.CLOSE"
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SideMenu);
