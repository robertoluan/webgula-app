import { connect } from "react-redux";
import component from "./ModalEndereco";
import { setEnderecoAtual } from "../../../services/actions/app/auth.actions";
import AccountsRemote from "../../../services/remote/accounts.remote";

const accountsRemote = new AccountsRemote();

const mapStateToProps = (state, props) => {
  return {};
};

const mapDispatchToProps = (dispatch, props) => ({
  async salvarNoServidor(endereco) {
    // Salva no servidor também
    return accountsRemote.saveEnderecosPessoa(endereco);
  },
  async setEnderecoAtual(endereco) {
    return dispatch(setEnderecoAtual(endereco));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(component);
