import React from 'react';
import {
  Modal,
  StyleSheet,
  View,
  ScrollView,
  Platform,
  Alert
} from 'react-native';
import { colors } from '../../../theme';
import Input from '../Input';
import TextCustom from '../TextCustom';
import { Row, Col } from 'react-native-easy-grid';
import ButtonLoading from '../ButtonLoading';
import { Icon, Button } from 'native-base';
import EmpresaRemote from '../../../services/remote/empresa.remote';
import EstadoRemote from '../../../services/remote/estado.remote';
import ContainerPage from '../ContainerPage';
import { getCompleteAddressToLocation } from '../../../services/actions/app/auth.actions';
import Loader from '../Loader';
import Permissions from 'react-native-permissions';

const empresaRemote = new EmpresaRemote();
const estadoRemote = new EstadoRemote();

export default class ModalEndereco extends React.PureComponent {
  state = {
    cepExistente: false,
    isLoading: false,
    isLoadingLocation: false,
    endereco: {
      cep: ''
    },
    visibleBotaoLocalizacao: true
  };

  static navigationOptions = {
    title: 'Endereço'
  };

  componentDidMount = async () => {
    Permissions.check('location', { type: 'always' }).then(response => {
      if (response === 'undetermined') {
        Permissions.check('location', { type: 'always' }).then(response2 => {
          if (response2 === 'denied' || response2 === 'restricted') {
            this.setState({ visibleBotaoLocalizacao: false });
          }
        });
      } else {
        if (response === 'denied') {
          this.setState({ visibleBotaoLocalizacao: false });
        }
      }
    });

    const endereco = this.props.navigation.getParam('endereco') || {};

    this.setState({
      endereco: endereco,
      cepExistente: !!endereco.cep
    });
  };

  setModalVisible = visible => {
    this.setState({ modalVisible: visible });
  };

  procurarCEP = async () => {
    const { cep } = this.state.endereco;

    if (!cep) {
      Alert.alert('Aviso', 'Informe um CEP!');
      return;
    }

    let cepLimpo = cep
      .replace(/\-/g, '')
      .replace(/\./g, '')
      .replace(/ /g, '');

    if (cepLimpo.toString().length != 8) {
      Alert.alert('Aviso', 'CEP precisa ter 8 caracteres!');
      return;
    }

    // Verifica se nao possui somento numeros
    let regexSomentoNumero = /^[0-9]*$/g;

    if (!regexSomentoNumero.test(cepLimpo)) {
      Alert.alert('Aviso', 'CEP deve possuir somente números!');
      return;
    }

    this.setState({
      isLoading: true
    });

    try {
      let res = await empresaRemote.procurarCep(cepLimpo);

      let endereco = {
        id: this.state.endereco.id || null,
        nome: this.state.endereco.nome || '',
        logradouro: res.logradouro,
        complemento: res.complemento,
        nomeCidade: res.nomeCidade,
        referencia: res.referencia,
        nomeBairro: res.nomeBairro,
        nomeEstado: res.nomeEstado,
        pais: 'Brasil',
        cep: cepLimpo,
        numero: this.state.endereco.numero,
        bairroId: res.bairroId,
        cepId: res.id
      };

      const result = await estadoRemote.getAddressByPostalCodeGoogleMapsApi({
        ...endereco
      });

      endereco.latitude = result.latitude;
      endereco.longitude = result.longitude;

      this.setState({
        endereco: { ...endereco },
        cepExistente: true
      });
    } catch (e) {
      this.setState({
        cepExistente: false
      });

      Alert.alert('Aviso', 'Este CEP não foi localizado!');
    } finally {
      this.setState({
        isLoading: false
      });
    }
  };

  salvar = async () => {
    const { endereco } = this.state;

    if (!endereco.nome) {
      Alert.alert('Aviso', 'Nome do endereço é obrigatório!');
      return;
    }

    if (!endereco.nomeEstado) {
      Alert.alert('Aviso', 'Estado do endereço é obrigatório!');
      return;
    }

    if (!endereco.nomeCidade) {
      Alert.alert('Aviso', 'Cidade do endereço é obrigatório!');
      return;
    }

    if (!endereco.nomeBairro) {
      Alert.alert('Aviso', 'Bairro do endereço é obrigatório!');
      return;
    }

    if (!endereco.logradouro) {
      Alert.alert('Aviso', 'Rua do endereço é obrigatório!');
      return;
    }

    if (!endereco.numero) {
      Alert.alert('Aviso', 'Número do endereço é obrigatório!');
      return;
    }

    this.setState({
      isLoading: true
    });

    try {
      // Salva no servidor
      const { data } = await this.props.salvarNoServidor(endereco);

      // Salva local
      await this.props.setEnderecoAtual({
        ...endereco,
        id: data.id
      });
    } catch (e) {
      Alert.alert('Aviso', 'Houve um problema ao salvar endereço!');
    } finally {
      this.setState({
        isLoading: false
      });
    }

    this.props.navigation.goBack();
  };

  onChange = (prop, value) => {
    this.setState({
      endereco: {
        ...this.state.endereco,
        [prop]: value
      }
    });
  };

  criarEnderecoLocalizacao = async () => {
    this.setState({ isLoadingLocation: true });
    try {
      const endereco = await getCompleteAddressToLocation();

      let cepExistente = !!endereco.cep;

      this.setState({ endereco: { ...endereco }, cepExistente: cepExistente });
    } catch (e) {
      Alert.alert('Aviso', 'Sua localização não está ativa!');
    } finally {
      this.setState({ isLoadingLocation: false });
    }
  };

  get getCompletoEndereco() {
    const { cepExistente, endereco } = this.state;

    if (!cepExistente && !endereco.id) return <View />;

    return (
      <View>
        <Row style={{ marginBottom: 8 }}>
          <Col>
            <Input
              label="Nome"
              style={styles.input}
              transparent
              value={endereco.nome}
              onChangeText={value => this.onChange('nome', value)}
            />
          </Col>
        </Row>

        <Row style={{ marginBottom: 8 }}>
          <Col size={40} style={styles.colRight}>
            <Input
              label="Estado"
              style={{ ...styles.input }}
              transparent
              disabled={endereco.nomeEstado}
              value={endereco.nomeEstado}
              onChange={value => this.onChange('nomeEstado', value)}
            />
          </Col>

          <Col size={60}>
            <Input
              label="Cidade"
              style={{ ...styles.input }}
              transparent
              disabled={endereco.nomeCidade}
              value={endereco.nomeCidade}
              onChangeText={value => this.onChange('nomeCidade', value)}
            />
          </Col>
        </Row>

        <Row style={{ marginBottom: 8 }}>
          <Col>
            <Input
              label="Bairro"
              style={{ ...styles.input }}
              transparent
              disabled={cepExistente && endereco.nomeBairro}
              value={endereco.nomeBairro}
              onChangeText={value => this.onChange('nomeBairro', value)}
            />
          </Col>
        </Row>

        <Row style={{ marginBottom: 8 }}>
          <Col>
            <Input
              label="Rua"
              style={{ ...styles.input }}
              transparent
              disabled={cepExistente && endereco.logradouro}
              value={endereco.logradouro}
              onChangeText={value => this.onChange('logradouro', value)}
            />
          </Col>
        </Row>

        <Row style={{ marginBottom: 8 }}>
          <Col size={25} style={styles.colRight}>
            <Input
              label="Número"
              style={styles.input}
              transparent
              keyboardType="numeric"
              value={endereco.numero}
              onChangeText={value => this.onChange('numero', value)}
            />
          </Col>

          <Col size={75}>
            <Input
              label="Complemento"
              style={styles.input}
              transparent
              value={endereco.complemento}
              onChangeText={value => this.onChange('complemento', value)}
            />
          </Col>
        </Row>

        <Row style={{ marginBottom: 8 }}>
          <Col>
            <Input
              label="Ponto de Referência"
              style={styles.input}
              transparent
              value={endereco.referencia}
              onChangeText={value => this.onChange('referencia', value)}
            />
          </Col>
        </Row>
      </View>
    );
  }

  // TODO implementar isso
  handleKeyPress = e => {
    if (e && e.nativeEvent.key == 'Enter') {
      this.setState(
        {
          cep: e.nativeEvent.text || ''
        },
        () => {
          this.procurarCEP();
        }
      );
    }
  };

  render() {
    const { open, onSave } = this.props;
    const {
      endereco,
      isLoading,
      visibleBotaoLocalizacao,
      isLoadingLocation,
      cepExistente
    } = this.state;

    return (
      <ContainerPage>
        <Loader loading={isLoadingLocation} />

        <ScrollView>
          <View style={styles.formContainer}>
            <View style={{ height: 60 }}>
              <Row style={{ marginBottom: 8, height: 60 }}>
                <Col size={75} style={styles.colRight}>
                  <Input
                    label="CEP"
                    style={styles.input}
                    transparent
                    keyboardType="numeric"
                    value={endereco.cep}
                    maxLength={8}
                    onKeyPress={e => this.handleKeyPress(e)}
                    onChangeText={value => this.onChange('cep', value)}
                  />
                </Col>
                <Col size={25} style={{ paddingTop: 18 }}>
                  <ButtonLoading
                    title={
                      <Icon
                        style={{
                          fontSize: 22,
                          color: colors.primaryColor
                        }}
                        name="search"
                      />
                    }
                    height={40}
                    titleColor={colors.primaryColor}
                    loading={isLoading}
                    backgroundColor="#f7f7f7"
                    onPress={() => this.procurarCEP()}
                  />
                </Col>
              </Row>
            </View>

            {visibleBotaoLocalizacao ? (
              <View style={{ marginTop: 12, marginBottom: 12 }}>
                <Button
                  transparent
                  style={{ backgroundColor: '#f7f7f7' }}
                  onPress={() => this.criarEnderecoLocalizacao()}
                  full
                >
                  <TextCustom>Usar minha localização!</TextCustom>
                </Button>
              </View>
            ) : null}

            {this.getCompletoEndereco}

            <View style={{ marginTop: 32 }}>
              {cepExistente || endereco.id ? (
                <Row style={{ marginBottom: 8 }}>
                  <Col>
                    <ButtonLoading
                      title="Salvar"
                      titleColor={colors.white}
                      backgroundColor={colors.primaryColor}
                      loading={isLoading}
                      onPress={() => this.salvar()}
                    />
                  </Col>
                </Row>
              ) : null}

              <Row style={{ marginBottom: 8 }}>
                <Col>
                  <ButtonLoading
                    title="Cancelar"
                    titleColor={colors.primaryColor}
                    backgroundColor={colors.white}
                    onPress={() => this.props.navigation.goBack()}
                  />
                </Col>
              </Row>
            </View>
          </View>
        </ScrollView>
      </ContainerPage>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {
    flexDirection: 'row',
    backgroundColor: colors.white
  },
  titleWrapper: {
    flex: 1,
    top: 40,
    bottom: 40
  },
  title: {
    fontSize: 18,
    textAlign: 'center',
    color: colors.primaryColor
  },
  container: {
    paddingTop: 50,
    marginRight: 0,
    flex: 1
  },
  colRight: {
    paddingRight: 10
  },
  labelFloat: {
    color: '#333',
    fontSize: 13,
    bottom: -8,
    position: 'relative'
  },
  formContainer: {
    top: 10,
    flex: 1,
    marginBottom: 24,
    marginRight: 16,
    marginLeft: 16
  },
  input: {
    marginBottom: 8
  }
});
