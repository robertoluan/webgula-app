import React from "react";
import Icon from "react-native-vector-icons/dist/MaterialCommunityIcons";
import FontAwesomeIcon from "react-native-vector-icons/dist/FontAwesome";
import MaterialIcon from "react-native-vector-icons/dist/MaterialIcons";
import SimpleLineIcons from "react-native-vector-icons/dist/SimpleLineIcons";
import Ionicons from "react-native-vector-icons/dist/Ionicons";

export default ({ name, android, size, androidSize, ...props }) => {
  let iconName = android || name;
  let iconSize = androidSize || size;

  if (iconName.startsWith("fa-")) {
    iconName = iconName.replace("fa-", "");
    return <FontAwesomeIcon size={iconSize} name={iconName} {...props} />;
  }
  if (iconName.startsWith("sl-")) {
    iconName = iconName.replace("sl-", "");
    return <SimpleLineIcons size={iconSize} name={iconName} {...props} />;
  }

  if (iconName.startsWith("ion-")) {
    iconName = iconName.replace("ion-", "");
    return <Ionicons size={iconSize} name={iconName} {...props} />;
  }

  if (iconName.startsWith("md-")) {
    iconName = iconName.replace("md-", "");
    return <MaterialIcon size={iconSize} name={iconName} {...props} />;
  }

  return <Icon size={iconSize} name={android || name} {...props} />;
};
