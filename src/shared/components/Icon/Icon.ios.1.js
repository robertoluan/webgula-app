import React from "react";
import Icon from "react-native-vector-icons/dist/Ionicons";
import EntypoIcon from "react-native-vector-icons/dist/Entypo";
import FontAwesomeIcon from "react-native-vector-icons/dist/FontAwesome";
import SimpleLineIcons from "react-native-vector-icons/dist/SimpleLineIcons";
import MaterialIcon from "react-native-vector-icons/dist/MaterialIcons";
import Ionicons from "react-native-vector-icons/dist/Ionicons";

export default ({ name, ios, size, iosSize, ...props }) => {
  let iconName = ios || name;
  let iconSize = iosSize || size;

  if (iconName.startsWith("entypo-")) {
    iconName = iconName.replace("entypo-", "");
    return <EntypoIcon size={iconSize} name={iconName} {...props} />;
  }

  if (iconName.startsWith("sl-")) {
    iconName = iconName.replace("sl-", "");
    return <SimpleLineIcons size={iconSize} name={iconName} {...props} />;
  }

  if (iconName.startsWith("ion-")) {
    iconName = iconName.replace("ion-", "");
    return <Ionicons size={iconSize} name={iconName} {...props} />;
  }

  if (iconName.startsWith("fa-")) {
    iconName = iconName.replace("fa-", "");
    return <FontAwesomeIcon size={iconSize} name={iconName} {...props} />;
  }

  if (iconName.startsWith("md-")) {
    iconName = iconName.replace("md-", "");
    return <MaterialIcon size={iconSize} name={"iconName"} {...props} />;
  }

  return <Icon size={iconSize} name={ios || name} {...props} />;
};
