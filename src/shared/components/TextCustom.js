import React from "react";
import { Text } from "react-native";

export default class TextCustom extends React.Component {
  render() {
    let defaultStyle = {
      fontFamily: "Montserrat-Regular"
    };

    let style = {
      ...defaultStyle,
      ...this.props.style
    };
    return (
      <Text {...this.props} style={{ ...style }}>
        {this.props.children}
      </Text>
    );
  }
}
