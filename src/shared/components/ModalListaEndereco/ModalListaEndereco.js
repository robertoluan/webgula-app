import React from "react";
import { Modal, View, ScrollView, StyleSheet } from "react-native";
import TextCustom from "../TextCustom";
import SkeletonLoading from "../SkeletonLoading";
import { Container, ListItem, Left, Right, Radio } from "native-base";
import { colors } from "../../../theme";
import { connect } from "react-redux";
import AccountsRemote from "../../../services/remote/accounts.remote";

const accountRemote = new AccountsRemote();

const mapStateToProps = (state, props) => {
  const app = state.app;
  const delivery = app.delivery;

  const endereco = (delivery && delivery.endereco) || {};

  return {
    endereco
  };
};

const mapDispatchToProps = (dispatch, props) => ({
  async load() {
    return accountRemote.getEnderecosPessoa();
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(
  class ModalListaEndereco extends React.Component {
    state = {
      isLoading: false,
      enderecos: []
    };

    async componentWillMount() {
      this.setState({
        isLoading: true
      });

      const enderecos = await this.props.load();

      this.setState({
        isLoading: false,
        enderecos
      });
    }

    getEndereco(endereco) {
      let frags = [];

      if (endereco.logradouro) {
        frags.push(endereco.logradouro);
      }

      if (endereco.numero) {
        frags.push("Nº " + endereco.numero);
      }

      if (endereco.complemento) {
        frags.push(endereco.complemento);
      }

      if (endereco.bairro) {
        frags.push(endereco.bairro);
      }

      if (endereco.cidade) {
        frags.push(endereco.cidade);
      }

      if (endereco.estado) {
        frags.push(endereco.estado);
      }

      if (endereco.pais) {
        frags.push(endereco.pais);
      }

      return frags.join(" , ");
    }

    selectEndereco(endereco) {
      this.props.onClose(endereco);
    }

    render() {
      const { enderecos, isLoading } = this.state;
      const { open, endereco } = this.props;

      return (
        <Modal
          animationType="slide"
          transparent={false}
          visible={open}
          onRequestClose={() => {}}
        >
          <Container>
            <ScrollView>
              <View style={styles.titleWrapper}>
                <TextCustom style={styles.title}>
                  Selecione um endereço.
                </TextCustom>
              </View>

              <SkeletonLoading show={isLoading}>
                {enderecos.map((item, key) => (
                  <ListItem key={key}>
                    <Left>
                      <TextCustom> {this.getEndereco(item)}</TextCustom>
                    </Left>
                    <Right>
                      <Radio
                        onPress={() => this.selectEndereco(item)}
                        selected={item.id == endereco.id}
                      />
                    </Right>
                  </ListItem>
                ))}
              </SkeletonLoading>
            </ScrollView>
          </Container>
        </Modal>
      );
    }
  }
);

const styles = StyleSheet.create({
  titleWrapper: {
    flex: 1,
    top: 40,
    bottom: 40
  },
  title: {
    fontSize: 18,
    textAlign: "center",
    color: colors.primaryColor
  },
  container: {
    paddingTop: 50,
    marginRight: 0,
    flex: 1
  },
  colRight: {
    paddingRight: 10
  },
  labelFloat: {
    color: "#333", 
    fontSize: 13,
    bottom: -8,
    position: "relative"
  },
  formContainer: {
    top: 80,
    flex: 1,
    minHeight: 750,
    marginRight: 16,
    marginLeft: 16
  },
  input: {
    paddingTop: 2,
    paddingLeft: 0,
    paddingBottom: 0,
    backgroundColor: "transparent",
    borderBottomColor: "#999",
    borderBottomWidth: 2,
    color: "#333",
    marginBottom: 8
  }
});
