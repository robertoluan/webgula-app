import React from 'react';
import { StatusBar, Platform, View } from 'react-native';
import { colors } from '../../../theme';

export default function StatusBarLayout({ networkActivity }) {
  return (
    <StatusBar
      translucent={true}
      barStyle="light-content"
      networkActivityIndicatorVisible={networkActivity}
      backgroundColor={colors.primaryColorDark}
    />
  );
}
