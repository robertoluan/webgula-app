import React from "react";
import Layout from "./StatusBarLayout";

export default class StatusBar extends React.PureComponent {
  render() {
    const { networkActivity } = this.props;

    return <Layout networkActivity={networkActivity} />;
  }
}
