import React from "react";
import { StatusBar } from "react-native";
import { colors } from "../../../theme";

export default function StatusBarLayout({ networkActivity }) {
  return (
    <StatusBar
      barStyle="light-content"
      networkActivityIndicatorVisible={networkActivity}
      backgroundColor={colors.primaryColor}
    />
  );
}
