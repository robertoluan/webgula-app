import React from "react";
import { View, ScrollView, StyleSheet } from "react-native";
import { Title, Body, Right, Content, ListItem } from "native-base";
import Modal from "react-native-modal";
import TextCustom from "../TextCustom";
import { colors } from "../../../theme";

class Dialog extends React.PureComponent {
  render = () => {
    const { open, style, title, children } = this.props;

    return (
      <Modal
        onBackButtonPress={() => this.props.onCancel()}
        animationIn="fadeIn"
        animationOut="fadeOut"
        isVisible={open}
        style={styles.modal}
      >
        <View style={{ flex: 1 }}>
          <ListItem icon noBorder noIndent style={styles.header}>
            <Body>
              <Title>
                <TextCustom style={{ color: "#fff" }}> {title} </TextCustom>
              </Title>
            </Body>
            <Right />
          </ListItem>

          <Content padder>
            <ScrollView>
              <View>{children}</View>
            </ScrollView>
          </Content>
        </View>
      </Modal>
    );
  };
}

const styles = StyleSheet.create({
  modal: {
    borderRadius: 20,
    backgroundColor: "#FFFFFF"
  },
  header: {
    backgroundColor: colors.primaryColor,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    height: 55
  }
});

export default Dialog;
