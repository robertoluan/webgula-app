import React, { PureComponent } from "react";
import { View, TouchableOpacity } from "react-native";
import AnimateLoadingButton from "react-native-animate-loading-button";
import { colors } from "../../theme";
import TextCustom from "./TextCustom";

export default class ButtonLoading extends PureComponent {
  _onPressHandler() {
    if (typeof this.props.onPress === "function") {
      this.props.onPress();
    }
  }

  componentWillUpdate(nextProps) {
    if (nextProps.loading !== this.props.loading) {
      this.loadingButton.showLoading(!!nextProps.loading);
    }
  }

  render() {
    let {
      backgroundColor,
      disabled,
      titleColor,
      title,
      height = 40,
      fontSize = 16,
      style = {}
    } = this.props;

    return (
      <View
        onTouchEnd={this._onPressHandler.bind(this)}
        style={{
          borderRadius: height / 2,
          backgroundColor: backgroundColor || colors.primaryColor,
          justifyContent: "center",
          ...style
        }}
      >
        <AnimateLoadingButton
          ref={c => (this.loadingButton = c)}
          height={height}
          disabled={disabled}
          title={<TextCustom style={{ fontSize }}>{title}</TextCustom>}
          titleFontSize={fontSize}
          titleColor={titleColor || colors.white}
          activityIndicatorColor={titleColor || colors.white}
          backgroundColor={backgroundColor || colors.primaryColor}
          borderRadius={4}
        />
      </View>
    );
  }
}
