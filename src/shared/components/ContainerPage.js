import React from "react";
import { Container } from "native-base";
import StatusBar from "./StatusBar";

function ContainerPage({ padding, children, ...props }) {
  if (padding) {
    props.style = props.style || {};
    props.style.paddingTop = 16;
    props.style.paddingLeft = 16;
    props.style.paddingRight = 16;
  }
  return (
    <React.Fragment>
      <StatusBar />
      <Container {...props}>{children}</Container>
    </React.Fragment>
  );
}

export default ContainerPage;
