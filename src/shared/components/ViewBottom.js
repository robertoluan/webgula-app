import React from "react";
import { View } from "react-native";
class ViewBottom extends React.PureComponent {
  render() {
    const { style } = this.props;

    return (
      <View
        style={{
          margin: 16,
          position: "absolute",
          left: 0,
          bottom: 0,
          right: 0,
          ...style
        }}
      >
        {this.props.children}
      </View>
    );
  }
}

export default ViewBottom;
