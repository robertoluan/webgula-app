import React from "react";
import { View, StyleSheet } from "react-native";
import QRCodeScanner from "react-native-qrcode-scanner";
import TextCustom from "../TextCustom";
import ButtonLoading from "../ButtonLoading";
import { colors } from "../../../theme";

export default class GlobalQRCode extends React.PureComponent {
	state = {
		flashMode: false,
		zoom: 0.2
	};

	onRead = res => {
		if (this.props.onResult) {
			this.props.onResult(res);
		}
	};

	render = () => {
		const { open, onResult } = this.props;

		const onRead = this.onRead.bind(this);

		return (
			<React.Fragment>
				{open && (
					<View style={styles.container}>
						<View
							style={{
								top: 70
							}}
						>
							<TextCustom
								style={{
									textAlign: "center",
									fontSize: 20
								}}
							>
								Escanei seu QR Code
							</TextCustom>
						</View>

						<View
							style={{
								width: "100%",
								height: 350,
								top: -100
							}}
						>
							<QRCodeScanner
								onRead={e => onRead(e.data)}
								showMarker={true}
								checkAndroid6Permissions={true}
								ref={elem => {
									this.scanner = elem;
								}}
							/>
						</View>

						<View
							style={{
								width: "100%",
								bottom: 100,
								paddingRight: 16,
								paddingLeft: 16
							}}
						>
							<ButtonLoading
								title="Voltar"
								titleColor={colors.primaryColor}
								backgroundColor={colors.white}
								onPress={() => onResult(null)}
							/>
						</View>
					</View>
				)}
			</React.Fragment>
		);
	};
}

const styles = StyleSheet.create({
	container: {
		position: "absolute",
		top: 0,
		height: "100%",
		bottom: 0,
		left: 0,
		zIndex: 999,
		right: 0,
		flex: 1,
		backgroundColor: "#f5f5f5",
		alignItems: "center",
		justifyContent: "center"
	}
});
