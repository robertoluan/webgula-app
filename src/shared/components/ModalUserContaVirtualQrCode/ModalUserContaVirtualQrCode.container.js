import { connect } from "react-redux";
import component from "./ModalUserContaVirtualQrCode";

const mapStateToProps = (state, props) => {
  let app = state.app || {};

  const empresaSelecionada = (app.empresa || {}).empresaSelecionada || {};

  return {
    empresaSelecionada: empresaSelecionada
  };
};

const mapDispatchToProps = (dispatch, props) => ({
  init() {}
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(component);
