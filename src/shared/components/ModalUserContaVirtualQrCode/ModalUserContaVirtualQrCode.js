import React from "react";
import { Modal, StyleSheet, View } from "react-native";
import { colors } from "../../../theme";
import TextCustom from "../TextCustom";
import ButtonLoading from "../ButtonLoading";
import QRCode from "react-native-qrcode";
import ContainerPage from "../ContainerPage";

class ModalUserContaVirtualQrCode extends React.PureComponent {
  render() {
    const { open, empresaSelecionada, onClose } = this.props;

    return (
      <Modal animationType="slide" transparent={true} visible={open}>
        <ContainerPage style={{ flex: 1 }}>
          <View
            style={{
              ...styles.center,
              flexDirection: "row",
              marginTop: 150,
              height: 170
            }}
          >
            <QRCode
              value={empresaSelecionada.coinPessoaChaveDigitalCC}
              size={150}
              bgColor="black"
              fgColor="white"
            />
          </View>

          <View
            style={{
              ...styles.center,
              position: "absolute",
              top: 0
            }}
          >
            <View style={{ height: 100 }}>
              <TextCustom style={styles.title}>
                {" " + " Dirija-se ao Caixa para realizar a recarga."}
              </TextCustom>
            </View>
          </View>

          <View>
            <TextCustom style={styles.subtitle}>
              Apresente esse QrCode
            </TextCustom>
          </View>

          <View
            style={{
              margin: 16,
              position: "absolute",
              left: 0,
              bottom: 0,
              right: 0
            }}
          >
            <View>
              <TextCustom
                style={{
                  fontSize: 11,
                  marginBottom: 10,
                  textAlign: "center"
                }}
              >
                Em breve você poderá fazer sua recarga através de pagamento
                Online.
              </TextCustom>
            </View>

            <View style={{ marginBottom: 10 }}>
              <ButtonLoading
                titleColor={colors.white}
                backgroundColor={colors.primaryColor}
                onPress={() => onClose()}
                title="Voltar"
              />
            </View>
          </View>
        </ContainerPage>
      </Modal>
    );
  }
}

export default ModalUserContaVirtualQrCode;

const styles = StyleSheet.create({
  title: {
    marginTop: 22,
    marginBottom: 10,
    fontSize: 18,
    textAlign: "center",
    alignItems: "center",
    justifyContent: "center",
    fontFamily: "Montserrat-Bold",
    color: "#333"
  },
  subtitle: {
    marginTop: 10,
    marginBottom: 20,
    fontSize: 14,
    textAlign: "center",
    alignItems: "center",
    justifyContent: "center",
    color: "#666"
  },
  icon: {
    color: "#56b910"
  },
  center: {
    marginTop: 20,
    textAlign: "center",
    alignItems: "center",
    justifyContent: "center"
  }
});
