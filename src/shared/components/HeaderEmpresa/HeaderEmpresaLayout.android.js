import React from "react";
import { ListItem, Thumbnail, Left, Body, Right } from "native-base";
import { StyleSheet, View, TouchableOpacity } from "react-native";
import { colors } from "../../../theme";
import TextCustom from "../TextCustom";
import Icon from "../../components/Icon";

function HeaderStatus({ status }) {
  return (
    <React.Fragment>
      {!status ? (
        <TextCustom style={{ fontSize: 10, top: -5, color: "red" }}>
          Fechado
        </TextCustom>
      ) : (
        <TextCustom style={{ fontSize: 10, top: -5, color: "green" }}>
          Aberto
        </TextCustom>
      )}
    </React.Fragment>
  );
}

export default function HeaderEmpresa({
  title,
  aberto,
  image,
  extras,
  endereco,
  favoritar,
  navigation
}) {
  endereco = endereco || [];

  return (
    <View style={styles.container}>
      <ListItem noBorder thumbnail style={{ borderColor: "transparent" }}>
        <Left>
          <TouchableOpacity
            onPress={() =>
              navigation && navigation.navigate("EstabelecimentoSobre")
            }
          >
            <Thumbnail resizeMode={"contain"} source={image} />
          </TouchableOpacity>
        </Left>

        <Body style={{ borderColor: "transparent" }}>
          <TouchableOpacity
            onPress={() =>
              navigation && navigation.navigate("EstabelecimentoSobre")
            }
          >
            <TextCustom style={{ fontSize: 14, top: -5, color: "#111" }}>
              {title}
            </TextCustom>

            {aberto !== undefined ? <HeaderStatus status={aberto} /> : null}

            {extras.telefone ? (
              <TextCustom style={{ fontSize: 12, color: colors.grayColor }}>
                {extras.telefone}
              </TextCustom>
            ) : null}

            {extras.tipoFavoritoNome ? (
              <TextCustom style={{ fontSize: 12, color: colors.grayColor }}>
                VIP - {extras.tipoFavoritoNome}
              </TextCustom>
            ) : null}

            <TextCustom style={{ fontSize: 10, color: colors.grayColor }}>
              {endereco.join(", ")}
            </TextCustom>
          </TouchableOpacity>
        </Body>
        {extras.flgFavorito !== undefined ? (
          <Right style={{ borderWidth: 0 }}>
            <Icon
              size={24}
              name={extras.flgFavorito ? "fa-heart" : "fa-heart-o"}
              color={
                extras.flgFavorito
                  ? colors.primaryColor
                  : colors.grayDarkBackground
              }
              onPress={() => favoritar()}
            />
          </Right>
        ) : null}
      </ListItem>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white,
    minHeight: 75, 
    borderBottomColor: "#eee",
    borderBottomWidth: 1
  }
});
