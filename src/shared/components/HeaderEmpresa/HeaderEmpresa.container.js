import component from './HeaderEmpresa';
import { connect } from 'react-redux';
import { Alert } from 'react-native';
import { toggleMenu } from '../../../services/actions/layout/menu.actions';
import EmpresaRemote from '../../../services/remote/empresa.remote';
import { checkAlertAuth } from '../../../services/actions/app/auth.actions';

const empresaRemoteApi = new EmpresaRemote();

const mapStateToProps = (state, props) => {
  const app = state.app || {};

  const empresaSelecionada = (app.empresa || {}).empresaSelecionada || {};

  return {
    open: state.layout.menu.open,
    empresaSelecionada,
    back: props.back,
    title: props.title,
    status: props.status,
    isClientCredentials: app.auth && app.auth.isClientCredentials,
    userInfo: (app.auth && app.auth.userInfo) || {},
    networkActivity: !!state.layout.global.networkActivityCount
  };
};

const mapDispatchToProps = (dispatch, props) => ({
  closeMenu() {
    dispatch(toggleMenu());
  },

  async findInformacaoCompletaEmpresa(empresaSelecionada) {
    const empresa = await empresaRemoteApi.findInformacaoCompletaEmpresa();

    console.log('empresa', empresa);

    dispatch({
      type: 'APP.EMPRESA.EMPRESA_SELECTED',
      payload: {
        empresaSelecionada: {
          ...empresa,
          flgFidelidade: empresaSelecionada.flgFidelidade,
          aberto: empresaSelecionada.aberto
        }
      }
    });

    return empresa;
  },

  async favoritar(empresaSelecionada, isClientCredentials) {
    if (isClientCredentials) {
      return checkAlertAuth(props.navigation);
    }

    empresaSelecionada.flgFavorito = !empresaSelecionada.flgFavorito;

    dispatch({
      type: 'APP.EMPRESA.EMPRESA_SELECTED',
      payload: {
        empresaSelecionada: { ...empresaSelecionada }
      }
    });

    try {
      await empresaRemoteApi.favoritar({ ...empresaSelecionada });
    } catch (e) {
      Alert.alert('Aviso', 'Houve um problema ao realizar a a operação');
      // Caso der erro volta como tava
      empresaSelecionada.flgFavorito = !empresaSelecionada.flgFavorito;

      dispatch({
        type: 'APP.EMPRESA.EMPRESA_SELECTED',
        payload: {
          empresaSelecionada: { ...empresaSelecionada }
        }
      });
    }
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(component);
