import React from 'react';
import Layout from './HeaderEmpresaLayout';
import { View } from 'react-native';
import SkeletonLoading from '../SkeletonLoading';

const image = require('../../../../assets/images/empty.jpeg');

export default class HeaderEmpresa extends React.PureComponent {
	state = {
		isLoading: false
	};

	componentDidMount = async () => {
		let empresa = this.props.empresaSelecionada || {};

		this.setState({
			isLoading: !empresa.nome
		});

		try {
			await this.props.findInformacaoCompletaEmpresa(empresa);
		} catch (e) {
		} finally {
			this.setState({
				isLoading: false
			});
		}
	};

	render = () => {
		const { isLoading } = this.state;
		const empresa = this.props.empresaSelecionada || {};
		const { navigation, favoritar, userInfo, isClientCredentials } = this.props;

		let wrapperImage = image;

		if (empresa.logoUrl) {
			wrapperImage = {
				uri: empresa.logoUrl
			};
		}

		const endereco = [];

		if (empresa.endBairroNome) {
			endereco.push(empresa.endBairroNome);
		}

		if (empresa.endCidadeNome) {
			endereco.push(empresa.endCidadeNome);
		}

		if (empresa.endUFSigla) {
			endereco.push(empresa.endUFSigla);
		}

		return (
			<View style={{ padding: isLoading ? 16 : 0 }}>
				<SkeletonLoading show={isLoading}>
					<Layout
						title={empresa.nome}
						navigation={navigation}
						image={wrapperImage}
						userInfo={userInfo || {}}
						aberto={empresa.aberto}
						avaliacao={empresa.avaliacao}
						extras={{ ...empresa }}
						endereco={endereco}
						favoritar={() => favoritar(empresa, isClientCredentials)}
					/>
				</SkeletonLoading>
			</View>
		);
	};
}
