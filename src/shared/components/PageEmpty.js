import React from "react";
import Icon from "./Icon";
import TextCustom from "./TextCustom";
import { View, Dimensions } from "react-native";

const width = Dimensions.get("window").width;

export default class PageEmpty extends React.PureComponent {
  render() {
    let { content, icon } = this.props;

    if (!React.isValidElement(content)) {
      content = (
        <TextCustom
          style={{
            fontSize: 18,
            color: "#aaa",
            textAlign: "center"
          }}
        >
          {content}
        </TextCustom>
      );
    }

    return (
      <View
        style={{
          position: "absolute",
          top: "40%",
          alignItems: "center",
          justifyContent: "center",
          paddingLeft: 16,
          paddingRight: 16,
          width: width
        }}
      >
        {content}

        <View>
          {icon ? (
            <Icon
              style={{ color: "#aaa", marginTop: 20 }}
              size={56}
              name={icon}
            />
          ) : null}
        </View>
      </View>
    );
  }
}
