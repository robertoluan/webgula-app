import componente from "./DrawerMenu";
import { connect } from "react-redux";
import { clearAuth } from "../../../services/actions/app/auth.actions";
import authContext from "../../../infra/auth";

const mapStateToProps = (state, props) => {
  const menu = (state.layout || {}).menu || {};
  let app = state.app || {};
  let empresaSelecionada = {};

  if (app.empresa && app.empresa.empresaSelecionada) {
    empresaSelecionada = app.empresa.empresaSelecionada;
  }

  return {
    isDelivery: !!menu.isDelivery,
    isAuth: app.auth && app.auth.isAuth,
    empresaSelecionada,
    isClientCredentials: app.auth && app.auth.isClientCredentials,
    userInfo: (app.auth && app.auth.userInfo) || {}
  };
};

const mapDispatchToProps = (dispatch, props) => ({
  async logout() {
    await authContext.logout();

    dispatch({
      type: "APP.EMPRESA.EMPRESA_SELECTED",
      payload: {
        empresaSelecionada: {}
      }
    });

    return dispatch(clearAuth());
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(componente);
