import React, { Component } from "react";
import {
  View,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Image,
  Alert
} from "react-native";
import { colors } from "../../../theme";
import { ListItem, Body, Left } from "native-base";
import TextCustom from "../TextCustom";
import logoImg from "../../../../assets/images/logo-secondary.png";
import Icon from "../Icon";
import { checkAlertAuth } from "../../../services/actions/app/auth.actions";

let imagemUsuarioFake = require("../../../../assets/images/profile.png");

export default class DrawerMenu extends Component {
  state = {
    imagemUsuario: imagemUsuarioFake
  };

  navigateToScreen = item => {
    if (typeof item.action === "function") {
      return item.action();
    }

    this.props.navigation.navigate(item.route);
    this.props.navigation.closeDrawer();
  };

  logout = async () => {
    await this.props.logout();

    setTimeout(() => {
      this.props.navigation.navigate("Login");
      this.props.navigation.closeDrawer();
    }, 100);
  };

  componentWillUpdate = nextProps => {
    if (this.props.userInfo !== nextProps.userInfo) {
      if (nextProps.userInfo.imagemUrl) {
        this.setState({
          imagemUsuario: { uri: nextProps.userInfo.imagemUrl }
        });
      }
    }
  };

  componentDidMount = () => {
    if (this.props.userInfo) {
      if (this.props.userInfo.imagemUrl) {
        this.setState({
          imagemUsuario: { uri: this.props.userInfo.imagemUrl }
        });
      }
    }
  };

  render() {
    const {
      empresaSelecionada,
      isDelivery,
      isClientCredentials,
      userInfo,
      isAuth
    } = this.props;

    let { imagemUsuario } = this.state;

    let menuData = [
      {
        icon: "sl-home",
        name: "Inicio",
        route: "Home",
        visible: true
      },
      {
        icon: "sl-user",
        name: "Meu Perfil",
        route: "Perfil",
        visible: isAuth && !isClientCredentials
      }
    ];

    menuData.push({
      icon: "fa-file-text-o",
      name: "Pedidos",
      route: "DeliveryPedidos",
      visible: isDelivery
    });

    menuData.push({
      icon: "fa-envelope-o",
      name: "Sobre o Web Gula",
      route: "Sobre",
      visible: true
    });

    let isMenuAndEmpresaSelecionada =
      empresaSelecionada && empresaSelecionada.id && !isDelivery;

    menuData.push({
      icon: "fa-info",
      name: "Sobre a Empresa",
      route: "EstabelecimentoSobre",
      visible: isMenuAndEmpresaSelecionada
    });

    menuData.push({
      icon: "fa-file",
      name: "Cardápio",
      action: () => {
        if (!empresaSelecionada.flgMostraCardapioMenu) {
          return Alert.alert(
            "Aviso",
            "O Cardápio desta empresa ainda não está disponível para consulta."
          );
        }

        return this.navigateToScreen({ route: "EstabelecimentoCardapio" });
      },
      visible: isMenuAndEmpresaSelecionada
    });

    menuData.push({
      icon: "fa-file-o",
      name: "Acompanhe sua conta",
      visible: isMenuAndEmpresaSelecionada,
      action: () => {
        if (isClientCredentials) {
          return checkAlertAuth(this.props.navigation);
        }

        return this.navigateToScreen({
          route: "EstabelecimentoAcompanheConta"
        });
      }
    });

    menuData.push({
      icon: "fa-star-half-o",
      name: "Avalie",
      visible: isMenuAndEmpresaSelecionada,
      action: () => {
        if (!isAuth) {
          return checkAlertAuth(this.props.navigation);
        }

        return this.navigateToScreen({
          route: "EstabelecimentoAvalie"
        });
      }
    });

    menuData.push({
      icon: "fa-power-off",
      name: "Sair",
      color: colors.primaryColor,
      action: () => this.logout(),
      visible: isAuth
    });

    menuData.push({
      icon: "fa-sign-in",
      name: "Fazer Login",
      route: "Login",
      visible: !isAuth
    });

    const items = menuData.filter(item => item.visible);

    return (
      <View style={styles.container}>
        <View style={styles.headerContainer}>
          <TouchableOpacity style={styles.imgContainer}>
            <Image source={logoImg} style={styles.logo} />
          </TouchableOpacity>

          {isAuth ? (
            <ListItem
              noBorder
              avatar
              noIndent
              style={{
                top: 35
              }}
            >
              <Left>
                <TouchableOpacity
                  onPress={() => {
                    this.navigateToScreen({ route: "Perfil" });
                  }}
                >
                  <Image
                    style={{
                      borderWidth: 4,
                      borderColor: "#fafafa",
                      width: 60,
                      height: 60,
                      borderRadius: 30
                    }}
                    onError={() => {
                      this.setState({
                        imagemUsuario: imagemUsuarioFake
                      });
                    }}
                    source={imagemUsuario}
                  />
                </TouchableOpacity>
              </Left>
              <Body>
                <TouchableOpacity
                  onPress={() => {
                    this.navigateToScreen({ route: "Perfil" });
                  }}
                >
                  <TextCustom style={styles.headerText}>
                    {userInfo.nome}
                  </TextCustom>
                </TouchableOpacity>
              </Body>
            </ListItem>
          ) : (
            <View style={{ height: 40 }} />
          )}
        </View>
        <View style={styles.screenContainer}>
          <ScrollView>
            {items.map((item, key) => (
              <ListItem
                noBorder
                noIndent
                icon
                onPress={() => this.navigateToScreen(item)}
              >
                <Left style={{ width: 50 }}>
                  <Icon
                    style={[styles.icon, { color: item.color }]}
                    name={item.icon}
                  />
                </Left>
                <Body>
                  <TextCustom
                    style={{
                      color: item.color || "#333"
                    }}
                  >
                    {item.name}
                  </TextCustom>
                </Body>
              </ListItem>
            ))}
          </ScrollView>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  imgContainer: {
    flex: 1,
    top: 50,
    margin: 10,
    justifyContent: "center",
    alignItems: "center"
  },
  logo: {
    alignSelf: "center",
    resizeMode: "contain",
    width: 130
  },
  container: {
    alignItems: "center"
  },
  headerContainer: {
    width: "100%",
    height: 120,
    backgroundColor: colors.primaryColor
  },
  headerText: {
    color: "#fff"
  },
  screenContainer: {
    top: 40,
    width: "100%"
  },
  screenTextStyle: {
    fontSize: 20,
    marginLeft: 20
  },
  icon: {
    color: "#666",
    fontSize: 16,
    top: 0
  }
});
