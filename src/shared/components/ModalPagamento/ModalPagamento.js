import React from "react";
import { Modal, StyleSheet, View } from "react-native";
import { TextInputMask } from "react-native-masked-text";
import { colors } from "../../../theme";
import TextCustom from "../TextCustom";
import Icon from "../Icon";
import { Container } from "native-base";
import ButtonLoading from "../ButtonLoading";
import { cleanNumber } from "../../../infra/utils/number.helpers";

class ModalPagamento extends React.PureComponent {
  state = {
    value: 0
  };

  onChange = value => {
    this.setState({ value });
  };

  onClose = () => {
    this.props.onClose(cleanNumber(this.state.value));
  };

  render() {
    return (
      <Modal
        animationType="slide"
        transparent={false}
        visible={this.props.open}
        onRequestClose={() => this.onClose()}
      >
        <Container>
          <View style={styles.center}>
            <Icon size={56} style={styles.icon} name="fa-check-circle" />
          </View>

          <View style={styles.title}>
            <TextCustom>Pedido realizado com sucesso!</TextCustom>
          </View>

          <View style={styles.formContainer}>
            <View style={{ height: 60 }}>
              <Row style={{ marginBottom: 8, height: 60 }}>
                <Col style={styles.colRight}>
                  <TextCustom style={styles.labelFloat}>
                    Você precisa de troco para:
                  </TextCustom>
                  <TextInputMask
                    type={"money"}
                    style={styles.input}
                    includeRawValueInChangeText={true}
                    value={this.state.value}
                    onChangeText={maskValue => {
                      this.onChange(maskValue);
                    }}
                  />
                </Col>
              </Row>
            </View>
          </View>

          <View
            style={{
              margin: 16,
              position: "absolute",
              left: 0,
              bottom: 0,
              right: 0
            }}
          >
            <View style={{ marginBottom: 10 }}>
              <ButtonLoading
                disabled={!this.state.value}
                onPress={() => this.onClose()}
                title="Salvar"
              />
            </View>
          </View>
        </Container>
      </Modal>
    );
  }
}

export default ModalPagamento;

const styles = StyleSheet.create({
  title: {
    marginTop: 20,
    fontSize: 20,
    textAlign: "center",
    alignItems: "center",
    justifyContent: "center",
    color: colors.primaryColor
  },
  icon: {
    color: "#56b910"
  },
  center: {
    marginTop: 80,
    textAlign: "center",
    alignItems: "center",
    justifyContent: "center"
  },
  input: {
    paddingTop: 2,
    paddingLeft: 0,
    height: 40,
    paddingBottom: 0,
    backgroundColor: "transparent",
    borderBottomColor: "#ccc",
    borderBottomWidth: 2,
    marginBottom: 10,
    fontSize: 18,
    color: "white"
  }
});
