import React from "react";
import { Modal, StyleSheet, View } from "react-native";
import { colors } from "../../../theme";
import TextCustom from "../TextCustom";
import Icon from "../Icon";
import { Container } from "native-base";
import ButtonLoading from "../ButtonLoading";

class ModalSuccess extends React.PureComponent {
  navigateTo(screen) {
    const { pedido } = this.props;

    this.props.navigation.replace(screen, { pedido });
  }

  render() {
    const navigateTo = this.navigateTo.bind(this);

    return (
      <Modal
        animationType="slide"
        transparent={false}
        visible={this.props.open}
      >
        <Container>
          <View style={styles.center}>
            <Icon size={56} style={styles.icon} name="fa-check-circle" />
          </View>

          <View style={styles.title}>
            <TextCustom>Pedido realizado com sucesso!</TextCustom>
          </View>

          <View
            style={{
              margin: 16,
              position: "absolute",
              left: 0,
              bottom: 0,
              right: 0
            }}
          >
            <View style={{ marginBottom: 10 }}>
              <ButtonLoading
                titleColor={colors.white}
                backgroundColor={colors.primaryColor}
                onPress={() => navigateTo("DeliveryAcompanhamento")}
                title="Acompanhar pedido"
              />
            </View>
            <View>
              <ButtonLoading
                titleColor={colors.primaryColor}
                backgroundColor={colors.white}
                onPress={() => navigateTo("Home")}
                title="Voltar ao inicio"
              />
            </View>
          </View>
        </Container>
      </Modal>
    );
  }
}

export default ModalSuccess;

const styles = StyleSheet.create({
  title: {
    marginTop: 20,
    fontSize: 20,
    textAlign: "center",
    alignItems: "center",
    justifyContent: "center",
    color: colors.primaryColor
  },
  icon: {
    color: "#56b910"
  },
  center: {
    marginTop: 80,
    textAlign: "center",
    alignItems: "center",
    justifyContent: "center"
  }
});
