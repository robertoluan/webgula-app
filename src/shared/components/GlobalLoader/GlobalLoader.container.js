import GlobalLoader from "./GlobalLoader";
import { connect } from "react-redux";

const mapStateToProps = (state, props) => {
  const loading = state.layout.global.loading;
  const loadingMessage = state.layout.global.loadingMessage;

  return {
    loading,
    loadingMessage
  };
};

export default connect(mapStateToProps, null)(GlobalLoader);
