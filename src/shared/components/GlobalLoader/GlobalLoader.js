import React from "react";

import {
  View,
  Text,
  StyleSheet,
  Platform,
  ActivityIndicator
} from "react-native";
import { Col } from "native-base";

export default class GlobalLoader extends React.Component {
  render() {
    const { loading } = this.props;

    if (!loading) {
      return null;
    }

    return (
      <View
        style={[
          styles.background,
          {
            zIndex: 99999,
            // backgroundColor: '#00000033',
            flexDirection: "row"
          }
        ]}
      >
        <View style={{ flexDirection: "row", alignSelf: "center" }}>
          <Col style={styles.offset} />
          <View style={styles.loadingContainer}>
            {/* <Text style={{ textAlign: 'center', marginTop: 16 }}>{loadingMessage || 'Carregando...'}</Text> */}
            {/* <Spinner color="#633" /> */}
            <ActivityIndicator size="large" />
          </View>
          <Col style={styles.offset} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  background: {
    position: "absolute",
    top: 0,
    left: 0,
    width: "100%",
    height: "100%"
  },
  loadingContainer: {
    borderRadius: Platform.OS == "ios" ? 8 : 0,
    // backgroundColor: '#eee',
    flex: 2
  },
  offset: {
    flex: 1
  }
});
