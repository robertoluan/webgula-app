import React from "react";
import { View, StyleSheet, Platform, ActivityIndicator } from "react-native";
import { Col } from "native-base";

export default function GlobalLoader({ loading, message }) {
  if (!loading) {
    return null;
  }

  return (
    <View
      style={[
        styles.background,
        {
          zIndex: 99999,
          backgroundColor: "transparent",
          flexDirection: "row"
        }
      ]}
    >
      <View style={{ flexDirection: "row", alignSelf: "center" }}>
        <Col style={styles.offset} />
        <View style={styles.loadingContainer}>
          {/* <Text style={{ textAlign: "center", marginTop: 8 }}>
            {message || "Carregando..."}
          </Text> */}
          {/* <Spinner color="#633" /> */}
          <ActivityIndicator size="large" />
        </View>
        <Col style={styles.offset} />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  background: {
    position: "absolute",
    top: 0,
    left: 0,
    width: "100%",
    height: "100%"
  },
  loadingContainer: {
    borderRadius: Platform.OS == "ios" ? 8 : 0,
    // backgroundColor: "#eee",
    flex: 2,
    paddingBottom: 8
  },
  offset: {
    flex: 1
  }
});
