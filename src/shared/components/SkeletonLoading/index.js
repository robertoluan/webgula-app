import React from "react";
import Placeholder from "rn-placeholder";
import { View } from "react-native";

function SkeletonLoading({ show, lines = 1, children }) {
  let items = [];

  for (i = 0; i < lines; i++) {
    items.push(i);
  }

  if (!show) return <React.Fragment>{children}</React.Fragment>;

  return (
    <React.Fragment>
      {items.map(item => (
        <View style={{ marginBottom: show ? 20 : 0 }}>
          {[0, 1, 2, 3].map(item2 => (
            <View style={{ marginBottom: 6 }}>
              <Placeholder.Line
                size={60}
                animate="fade"
                lineNumber={4}
                lineSpacing={5}
                // lastLineWidth="30%"
                style={{borderRadius: 0}}
                onReady={!show}
              />
            </View>
          ))}
        </View>
      ))}
    </React.Fragment>
  );
}

export default SkeletonLoading;
