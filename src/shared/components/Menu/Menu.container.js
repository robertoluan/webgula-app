import componente from "./Menu";
import { connect } from "react-redux";
import { toggleMenu } from "../../../services/actions/layout/menu.actions";
import { clearAuth } from "../../../services/actions/app/auth.actions";
import {
  NavigationActions,
  DrawerActions,
  NavigationEvents
} from "react-navigation";
import authContext from "../../../infra/auth";

const navigateToScreen = route => () => {
  const navigateAction = NavigationActions.navigate({
    routeName: route
  });

  window.navigation.dispatch(navigateAction);
  // window.navigation.dispatch(DrawerActions.closeDrawer());
};

const mapStateToProps = (state, props) => {
  let menu = (state.layout || {}).menu || {};
  let app = state.app || {};
  let empresaSelecionada = null;

  if (app.empresa && app.empresa.empresaSelecionada) {
    empresaSelecionada = app.empresa.empresaSelecionada;
  }

  return {
    isDelivery: !!menu.isDelivery,
    empresaSelecionada,
    userInfo: (app.auth && app.auth.userInfo) || {}
  };
};

const mapDispatchToProps = (dispatch, props) => ({
  navigateTo(route) {
    dispatch(toggleMenu());
    setTimeout(() => dispatch(navigateToScreen(route)), 200);
  },
  async logout() {
    dispatch(navigateToScreen("Login"));

    await authContext.logout();

    dispatch(clearAuth());
    dispatch(toggleMenu());
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(componente);
