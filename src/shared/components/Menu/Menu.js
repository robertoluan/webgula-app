import React from "react";
import { TouchableOpacity, Image, ScrollView, FlatList, View } from "react-native";
import styled from "styled-components/native";
import Icon from "../Icon/Icon.ios";
import {
  Thumbnail,
  List,
  ListItem as ListItemNativeBase,
  Left,
  Body
} from "native-base";
import TextCustom from "../TextCustom";
import ContainerPage from "../ContainerPage";

const logo = require("../../../../assets/images/logo-primary.png");

const femalePlaceholder = require("../../../../assets/placeholder/female-placeholder.jpg");

const ContainerImage = styled.View`
  flex: 1;
`;

const ContainerBody = styled.View`
  flex: 3;
  top: 35;
  /* padding-bottom: 16; */
  /* flex-direction: column; */
`;

const ListItem = styled.TouchableOpacity`
  flex-direction: row;
  padding: 7px 5px;
`;

const IconItem = styled(Icon)`
  color: #848484;
  flex: 1;
  text-align: center;
  padding-vertical: 6;
  font-size: 18;
`;

const MenuText = styled.Text`
  color: #848484;
  flex: 3;
  text-align: left;
  padding-vertical: 6;
  font-size: 14;
  font-family: "Montserrat-Regular";
`;

const MenuList = ({ navigateTo, isDelivery, empresaSelecionada }) => {
  let menuData = [
    {
      icon: "sl-home",
      name: "Inicio",
      route: "Home"
    },
    {
      icon: "fa-user-circle",
      name: "Meu Perfil",
      route: "Perfil",
      auth: true
    }
  ];

  if (isDelivery) {
    menuData.push({
      icon: "fa-file-text-o",
      empresaSelecionada: true,
      name: "Pedidos",
      route: "DeliveryPedidos"
    });
  }

  menuData.push({
    icon: "fa-envelope-o",
    name: "Sobre o Web Gula",
    route: "Sobre"
  });

  if (empresaSelecionada && empresaSelecionada.id && !isDelivery) {
    menuData.push({
      icon: "fa-info",
      empresaSelecionada: true,
      name: "Sobre a Empresa",
      route: "EstabelecimentoSobre"
    });
    menuData.push({
      icon: "fa-file",
      empresaSelecionada: true,
      name: "Cardápio",
      route: "EstabelecimentoCardapio"
    });
    menuData.push({
      icon: "fa-file-o",
      empresaSelecionada: true,
      name: "Acompanhe sua conta",
      route: "EstabelecimentoAcompanheConta"
    });
    menuData.push({
      icon: "fa-star-half-o",
      empresaSelecionada: true,
      name: "Avalie",
      route: "EstabelecimentoAvalie"
    });
  }

  menuData.push({
    icon: "fa-power-off",
    name: "Sair",
    action: () => logout()
  });

  let items = menuData.filter(item => item);

  return (
    <FlatList
      data={items}
      extraData={[]}
      renderItem={({ item }) => {
        const { icon, name, route } = item;

        return (
          <ListItem
            style={{ height: 50 }}
            onPress={() => (route.action ? route.action() : navigateTo(route))}
          >
            <IconItem name={icon} size={20} />
            <MenuText>{name}</MenuText>
          </ListItem>
        );
      }}
    />
  );
};

class Menu extends React.PureComponent {
  render() {
    let {
      navigateTo,
      isDelivery,
      logout,
      userInfo,
      empresaSelecionada
    } = this.props;

    return (
      <ContainerPage>
        <ScrollView>
          <View>
            <ContainerImage>
              <TouchableOpacity>
                <Image
                  source={logo}
                  style={{
                    width: 120,
                    alignSelf: "center",
                    resizeMode: "contain",
                    top: 20
                  }}
                />
                <List
                  style={{
                    marginTop: 25
                  }}
                >
                  <ListItemNativeBase noBorder noIndent avatar>
                    <Left>
                      <Thumbnail
                        style={{
                          borderWidth: 1,
                          borderColor: "#ddd"
                        }}
                        source={femalePlaceholder}
                      />
                    </Left>
                    <Body>
                      <TextCustom
                        style={{
                          fontSize: 12,
                        }}
                      >
                        {(userInfo && userInfo.nome) || "--"}
                      </TextCustom>

                      <TextCustom
                        style={{
                          fontSize: 10,
                          color: "#bbb",
                          top: 10
                        }}
                      >
                        {(userInfo && userInfo.email) || "--"}
                      </TextCustom>
                    </Body>
                  </ListItemNativeBase>
                </List>
              </TouchableOpacity>
            </ContainerImage>
            <ContainerBody>
              <MenuList
                logout={logout}
                isDelivery={isDelivery}
                empresaSelecionada={empresaSelecionada}
                navigateTo={navigateTo}
              />
            </ContainerBody>
          </View>
        </ScrollView>
      </ContainerPage>
    );
  }
}

export default Menu;
