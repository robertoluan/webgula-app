import React from "react";
import { Modal, StyleSheet, View, ScrollView, Alert } from "react-native";
import { colors } from "../../../theme";
import Input from "../Input";
import TextCustom from "../TextCustom";
import { Row, Col } from "react-native-easy-grid";
import ButtonLoading from "../ButtonLoading";
import { Container } from "native-base";
import { connect } from "react-redux";
import AccountsRemote from "../../../services/remote/accounts.remote";
import { cleanNumber, isNumber } from "../../../infra/utils/number.helpers";

const accountsRemote = new AccountsRemote();

const mapStateToProps = (state, props) => {
  return {};
};

const mapDispatchToProps = (dispatch, props) => ({
  async salvar(telefone) {
    // Salva no servidor também
    return await accountsRemote.saveTelefonePessoa(telefone);
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(
  class ModalContato extends React.PureComponent {
    state = {
      isLoading: false,
      telefone: {
        codDdd: "",
        telefone: ""
      }
    };

    componentWillUpdate(nextProps) {
      if (nextProps.open != this.props.open) {
        if (nextProps.open == true) {
          const telefone = { ...(nextProps.telefone || {}) };

          this.setState({
            telefone: telefone
          });
        }
      }
    }

    async salvar() {
      const { telefone } = this.state;

      // telefone
      if (!telefone.codDdd) {
        Alert.alert("Aviso", "Informe o DDD!");
        return;
      }

      if (telefone.codDdd.length != 2) {
        Alert.alert("Aviso", "DDD precisa ter 2 digitos!");
        return;
      }

      if (!telefone.telefone) {
        Alert.alert("Aviso", "Informe o Telefone!");
        return;
      }

      let phoneNumberLimpo = cleanNumber(telefone.telefone);

      if (!isNumber(phoneNumberLimpo)) {
        Alert.alert("Aviso", "Telefone está em formato incorreto!");
        return;
      }

      if (phoneNumberLimpo.toString().length < 9) {
        Alert.alert("Aviso", "Telefone deve possuir 9 digitos!");
        return;
      }

      if (phoneNumberLimpo.toString().length > 9) {
        Alert.alert(
          "Aviso",
          "Telefone é maior do que o tamanho permitido de 9 digitos!"
        );
        return;
      }

      this.setState({
        isLoading: true
      });

      try {
        // Salva no servidor
        await this.props.salvar(telefone);

        if (this.props.onSave) {
          this.props.onSave(telefone);
        }
      } catch (e) {
        Alert.alert("Aviso", "Houve um problema ao criar o contato!");
        return;
      } finally {
        this.setState({
          isLoading: false
        });
      }
    }

    onChange(prop, value) {
      this.setState({
        telefone: {
          ...this.state.telefone,
          [prop]: value
        }
      });
    }

    get getForm() {
      const { telefone } = this.state;

      return (
        <View>
          <Row style={{ marginBottom: 8 }}>
            <Col size={30} style={styles.colRight}>
              <Input
                label="DDD"
                style={styles.input}
                transparent
                keyboardType="phone-pad"
                value={telefone.codDdd}
                onChangeText={value => this.onChange("codDdd", value)}
              />
            </Col>

            <Col size={70}>
              <Input
                label="Telefone"
                style={styles.input}
                transparent
                keyboardType="phone-pad"
                value={telefone.telefone}
                onChangeText={value => this.onChange("telefone", value)}
              />
            </Col>
          </Row>
        </View>
      );
    }

    render() {
      const { open, onSave } = this.props;
      const { telefone } = this.state;

      return (
        <Modal
          animationType="slide"
          transparent={false}
          visible={open}
          onRequestClose={() => onSave()}
        >
          <Container>
            <ScrollView>
              <View style={styles.titleWrapper}>
                <TextCustom style={styles.title}>
                  {telefone.id ? "Editar " : "Adicionar "} Telefone
                </TextCustom>
              </View>

              <View style={styles.formContainer}>{this.getForm}</View>
            </ScrollView>

            <View
              style={{
                margin: 16,
                position: "absolute",
                left: 0,
                bottom: 0,
                right: 0
              }}
            >
              <Row style={{ marginBottom: 8 }}>
                <Col>
                  <ButtonLoading
                    title="Salvar"
                    titleColor={colors.white}
                    backgroundColor={colors.primaryColor}
                    loading={this.state.isLoading}
                    onPress={() => this.salvar()}
                  />
                </Col>
              </Row>

              <Row style={{ marginBottom: 8 }}>
                <Col>
                  <ButtonLoading
                    title="Cancelar"
                    titleColor={colors.primaryColor}
                    backgroundColor={colors.white}
                    onPress={() => this.props.onCancel()}
                  />
                </Col>
              </Row>
            </View>
          </Container>
        </Modal>
      );
    }
  }
);

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    flexDirection: "row",
    backgroundColor: colors.white
  },
  titleWrapper: {
    flex: 1,
    top: 40,
    bottom: 40
  },
  title: {
    fontSize: 18,
    textAlign: "center",
    color: colors.primaryColor
  },
  container: {
    paddingTop: 50,
    marginRight: 0,
    flex: 1
  },
  colRight: {
    paddingRight: 10
  },
  labelFloat: {
    color: "#333", 
    fontSize: 13,
    bottom: -8,
    position: "relative"
  },
  formContainer: {
    top: 80,
    flex: 1,
    minHeight: 750,
    marginRight: 16,
    marginLeft: 16
  },
  input: {
    marginBottom: 8
  }
});
