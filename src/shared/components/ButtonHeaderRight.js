import React from "react";
import { Platform, TouchableOpacity } from "react-native";
import Icon from "./Icon";

let iconMenu = Platform.select({
  android: "ion-md-menu",
  ios: "ion-ios-menu"
});

function ButtonHeaderRight({ navigation }) {
  return (
    <TouchableOpacity
      style={{
        paddingRight: 16,
        paddingTop: 4
      }}
      onPress={() => navigation.openDrawer()}
    >
      <Icon size={26} style={{ color: "white" }} name={iconMenu} />
    </TouchableOpacity>
  );
}

export default ButtonHeaderRight;
