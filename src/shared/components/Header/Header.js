import React from 'react';
import { Platform } from 'react-native';
import { Header as HeaderNB, Left, Right, Body, Title, Subtitle, Text } from 'native-base';
import HeaderButton from './Button';
// import LinearGradient from 'react-native-linear-gradient';
import { colors } from '../../../theme'; 

export default class Header extends React.Component {

    back = () => {
        this.props.onBack();
    }

    renderTransparent() {
        const { title, subtitle, transparent, back, right } = this.props;

        return (
            <HeaderNB
                androidStatusBarColor={colors.primaryColorDark}
                iosBarStyle="light-content"
                hasTabs
                noShadow
                style={{ zIndex: 1200, transform: [{ "translate": [0, 0, 1] }], position: 'absolute', top: 0, left: 0, right: 0, backgroundColor: 'transparent' }}>

                {/* <LinearGradient
                    style={{
                        position: 'absolute',
                        top: 0,
                        left: 0,
                        right: 0,
                        height: Platform.OS == "ios" ? 80 : 50
                    }}
                    colors={gradientColors} /> */}

                {back && <Left>
                    <HeaderButton onPress={this.back} icon="arrow-back" />
                </Left>}
                <Body >
                    <Title style={{ color: 'white' }} >{title}</Title>
                    {subtitle && <Subtitle style={{ color: 'white' }} > {subtitle}</Subtitle>}
                </Body>
                {right && <Right>
                    {right}
                </Right>}
            </HeaderNB>
        );
    }

    render() {
        const { title, transparent, subtitle, back, right } = this.props;

        if (transparent) return this.renderTransparent();

        return (
            <HeaderNB iosBarStyle="light-content" style={{ backgroundColor: colors.primaryColorTransparent }} androidStatusBarColor={colors.primaryColorDark}  >
                <Left>
                    {back && <HeaderButton onPress={this.back} icon="arrow-back" />}
                </Left>
                <Body>
                    <Title style={{ fontSize: 14, color: colors.primaryTextColor }}>{title}</Title>
                    {subtitle ? <Subtitle style={{ color: 'white' }} > {subtitle}</Subtitle> : null}
                </Body>
                {right && <Right>
                    {right}
                </Right>}
            </HeaderNB>
        );

    }
}