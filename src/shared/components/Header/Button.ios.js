import React from 'react';
import { Button as ButtonNB } from 'native-base';
import { Icon } from 'native-base';


export default function Button({ icon, ...props }) {
    return <ButtonNB transparent {...props } >
        <Icon name={icon} style={{ color: "white" }} />
    </ButtonNB>;
}