import React from 'react';
import { TouchableOpacity } from 'react-native';
import { Icon } from 'native-base';

export default function Button({ icon, ...props }) {
    return (
        <TouchableOpacity  {...props } >
            <Icon style={{ color: 'white' }} name={icon} />
        </TouchableOpacity>
    );
}