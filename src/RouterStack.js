import React from 'react';
import HomeScreen from './screens/Home';
import EstabelecimentosScreen from './screens/Menu/Estabelecimentos';
import EstabelecimentoScreen from './screens/Menu/Estabelecimento';
import EstabelecimentoSobreScreen from './screens/Menu/EstabelecimentoSobre';
import EstabelecimentoCardapioProdutoItemScreen from './screens/Menu/EstabelecimentoCardapioProdutoItem';
import EstabelecimentoCardapioProdutoScreen from './screens/Menu/EstabelecimentoCardapioProduto';
import EstabelecimentoCardapioScreen from './screens/Menu/EstabelecimentoCardapio';
import EstabelecimentoAvalieScreen from './screens/Menu/EstabelecimentoAvalie';
import EstabelecimentoAcompanheContaScreen from './screens/Menu/EstabelecimentoAcompanheConta';
import EstabelecimentoAcompanheContaVendaScreen from './screens/Menu/EstabelecimentoAcompanheContaVenda';
import EstabelecimentoAcompanheContaVendaRateioScreen from './screens/Menu/EstabelecimentoAcompanheContaVendaRateio';
import EstabelecimentoAcompanheContaVendaRateioNovoScreen from './screens/Menu/EstabelecimentoAcompanheContaVendaRateioNovo';
import EstabelecimentoAcompanheContaVendaUsuariosScreen from './screens/Menu/EstabelecimentoAcompanheContaVendaUsuarios';
import EstabelecimentoAcompanheContaVendaProdutoScreen from './screens/Menu/EstabelecimentoAcompanheContaVendaProduto';
import EnderecoMenuScreen from './screens/Menu/EnderecoMenu';
import VipsScreen from './screens/Fidelidade/Vips';
import FidelidadeScreen from './screens/Fidelidade/Fidelidade';
import TutorialScreen from './screens/Tutorial';
import SobreScreen from './screens/Sobre';
import {
  createDrawerNavigator,
  StackNavigator,
  createMaterialTopTabNavigator
} from 'react-navigation';
import { colors } from './theme';
import ButtonHeaderRight from './shared/components/ButtonHeaderRight';
import DeliveryEstabelecimentosScreen from './screens/Delivery/Estabelecimentos';
import CarrinhoScreen from './screens/Delivery/Carrinho';
import DeliveryPedidosScreen from './screens/Delivery/Pedidos';
import DeliveryProdutosScreen from './screens/Delivery/Produtos';
import DeliveryProdutosGruposScreen from './screens/Delivery/ProdutosGrupos';
import DeliveryProdutosGruposItemScreen from './screens/Delivery/ProdutosGruposItem';
import DeliveryAcompanhamentoScreen from './screens/Delivery/Acompanhamento';
import DrawerMenu from './shared/components/DrawerMenu';
import Constants from './infra/constants';
import EstabelecimentoControleCreditoScreen from './screens/Menu/EstabelecimentoControleCredito';
import EstabelecimentoCreditoInformacaoScreen from './screens/Menu/EstabelecimentoCreditoInformacao';
import EstabelecimentoCreditoExtratoScreen from './screens/Menu/EstabelecimentoCreditoExtrato';
import EstabelecimentoCreditoResumoPedidoScreen from './screens/Menu/EstabelecimentoCreditoResumoPedido';
import LancamentosScreen from './screens/Fidelidade/Lancamentos';
import FidelidadePremioScreen from './screens/Fidelidade/FidelidadePremio';
import CarregandoConfiguracaoScreen from './screens/CarregandoConfiguracao';
import AuthRoutes from './screens/Auth';
import EstabelecimentoCreditoDetalhesPedidoScreen from './screens/Menu/EstabelecimentoCreditoDetalhesPedido';
import EstabelecimentoCreditoDetalhesPedidoProdutosScreen from './screens/Menu/EstabelecimentoCreditoDetalhesPedidoProdutos';
import ModalEnderecoScreen from './shared/components/ModalEndereco';

const TabsPerfil = createMaterialTopTabNavigator(
  {
    Perfil: AuthRoutes.Perfil,
    Endereco: AuthRoutes.Endereco,
    Contato: AuthRoutes.Contato
  },
  {
    tabBarOptions: {
      activeTintColor: '#fff',
      inactiveTintColor: 'rgba(255,255,255,.8)',
      style: {
        backgroundColor: colors.primaryColor
      },
      headerTitleStyle: {
        fontFamily: 'Montserrat-Regular'
      },

      indicatorStyle: {
        backgroundColor: '#fff'
      }
    }
  }
);

const TabsFidelidade = createMaterialTopTabNavigator(
  {
    Fidelidade: FidelidadeScreen,
    Vips: VipsScreen
  },
  {
    tabBarOptions: {
      activeTintColor: '#fff',
      inactiveTintColor: 'rgba(255,255,255,.8)',
      style: {
        backgroundColor: colors.primaryColor
      },
      headerTitleStyle: {
        fontFamily: 'Montserrat-Regular'
      },
      indicatorStyle: {
        backgroundColor: '#fff'
      }
    }
  }
);

const AppNavigator = new StackNavigator(
  {
    CarregandoConfiguracao: CarregandoConfiguracaoScreen,
    Home: {
      screen: HomeScreen
      /*navigationOptions: {
        gesturesEnabled: false
        // swipeEnabled: false
      }*/
    },
    Perfil: {
      screen: TabsPerfil,
      navigationOptions: {
        title: 'Perfil',
        headerTitleStyle: {
          fontWeight: 'normal',
          fontSize: 18,
          fontFamily: 'Montserrat-Regular'
        },
        headerStyle: {
          marginTop: Constants.statusBarHeight,
          elevation: 0, // remove shadow on Android
          shadowOpacity: 0,
          backgroundColor: colors.primaryColor
        }
      }
    },
    Endereco: AuthRoutes.Endereco,
    Fidelidade: {
      screen: TabsFidelidade,
      navigationOptions: {
        title: 'Fidelidade',
        headerTitleStyle: {
          fontWeight: 'normal',
          fontSize: 18,
          fontFamily: 'Montserrat-Regular'
        },
        headerStyle: {
          marginTop: Constants.statusBarHeight,
          elevation: 0, // remove shadow on Android
          shadowOpacity: 0,
          backgroundColor: colors.primaryColor
        }
      }
    },
    DeliveryCarrinho: CarrinhoScreen,
    EnderecoMenu: EnderecoMenuScreen,
    Estabelecimentos: EstabelecimentosScreen,
    Estabelecimento: EstabelecimentoScreen,
    EstabelecimentoControleCredito: EstabelecimentoControleCreditoScreen,
    EstabelecimentoCreditoInformacao: EstabelecimentoCreditoInformacaoScreen,
    EstabelecimentoCreditoExtrato: EstabelecimentoCreditoExtratoScreen,
    EstabelecimentoCreditoResumoPedido: EstabelecimentoCreditoResumoPedidoScreen,
    EstabelecimentoCardapio: EstabelecimentoCardapioScreen,
    EstabelecimentoCardapioProduto: EstabelecimentoCardapioProdutoScreen,
    EstabelecimentoCardapioProdutoItem: EstabelecimentoCardapioProdutoItemScreen,
    EstabelecimentoAvalie: EstabelecimentoAvalieScreen,
    EstabelecimentoSobre: EstabelecimentoSobreScreen,
    EstabelecimentoAcompanheConta: EstabelecimentoAcompanheContaScreen,
    EstabelecimentoAcompanheContaVenda: EstabelecimentoAcompanheContaVendaScreen,
    EstabelecimentoAcompanheContaVendaRateio: EstabelecimentoAcompanheContaVendaRateioScreen,
    EstabelecimentoAcompanheContaVendaRateioNovo: EstabelecimentoAcompanheContaVendaRateioNovoScreen,
    EstabelecimentoAcompanheContaVendaUsuarios: EstabelecimentoAcompanheContaVendaUsuariosScreen,
    EstabelecimentoAcompanheContaVendaProduto: EstabelecimentoAcompanheContaVendaProdutoScreen,
    EstabelecimentoCreditoDetalhesPedido: EstabelecimentoCreditoDetalhesPedidoScreen,
    EstabelecimentoCreditoDetalhesPedidoProdutos: EstabelecimentoCreditoDetalhesPedidoProdutosScreen,
    Cadastro: AuthRoutes.Cadastro,
    Senha: AuthRoutes.Senha,
    Sobre: SobreScreen,
    ModalEndereco: ModalEnderecoScreen,
    RecuperarSenha: AuthRoutes.RecuperarSenha,
    Login: {
      screen: AuthRoutes.LoginEmailSenha,
      navigationOptions: {
        headerRight: null,
        gesturesEnabled: false,
        disableOpenGesture: true,
        headerStyle: {
          marginTop: Constants.statusBarHeight,
          elevation: 0, // remove shadow on Android
          shadowOpacity: 0,
          backgroundColor: colors.primaryColor
        }
      }
    },
    Tutorial: {
      screen: TutorialScreen,
      navigationOptions: {
        header: null
      }
    },
    LoginSegundaEtapa: {
      screen: AuthRoutes.LoginSegundaEtapa,
      navigationOptions: {
        headerRight: null,
        gesturesEnabled: false,
        disableOpenGesture: true,
        headerStyle: {
          marginTop: Constants.statusBarHeight,
          elevation: 0, // remove shadow on Android
          shadowOpacity: 0,
          backgroundColor: colors.primaryColor
        }
      }
    },
    DeliveryEstabelecimentos: DeliveryEstabelecimentosScreen,
    DeliveryProdutos: DeliveryProdutosScreen,
    DeliveryProdutosGrupos: DeliveryProdutosGruposScreen,
    DeliveryProdutosGruposItem: DeliveryProdutosGruposItemScreen,
    DeliveryPedidos: DeliveryPedidosScreen,
    DeliveryAcompanhamento: {
      screen: DeliveryAcompanhamentoScreen
    },
    Lancamentos: LancamentosScreen,
    FidelidadePremio: FidelidadePremioScreen
  },
  {
    initialRouteName: 'CarregandoConfiguracao',
    navigationOptions: ({ navigation }) => {
      return {
        headerBackTitle: null,
        headerStyle: {
          backgroundColor: colors.primaryColor,
          marginTop: Constants.statusBarHeight,
          elevation: 0, // remove shadow on Android
          shadowOpacity: 0,
          backgroundColor: colors.primaryColor
        },
        headerTintColor: colors.white,
        headerTitleStyle: {
          fontWeight: 'normal',
          fontSize: 18,
          fontFamily: 'Montserrat-Regular'
        },
        headerRight: <ButtonHeaderRight navigation={navigation} />
      };
    }
  }
);

let MainStack = createDrawerNavigator(
  {
    App: AppNavigator
  },
  {
    drawerPosition: 'right',
    drawerBackgroundColor: colors.white,
    contentComponent: DrawerMenu
  }
);

export default MainStack;
