const colors = {
  // primaryColor: "#DC0C17",
  primaryColor: "#c52f33",
  primaryColorDark: "#b11d21",
  primaryColorOriginal: "#df0101",
  primaryColorTransparent: "rgba(220, 12, 23, 0.82)",
  primaryColorTransparentDark: "#343434f2",
  primaryTextColor: "#F4F4F5",
  primaryTitleColor: "#555555",
  accentColor1: "#C52429",
  accentColor2: "#0079A9",
  accentColor3: "#4EA2F9",
  accentColor4: "#01AA7B",
  grayColor: "#777",
  arrowColor: "#4EA2F9",
  loveColor: "#FF3074",
  goldColor: "#FFCC00",
  defaultBackground: "#F3F3F3",
  grayBackground: "#EEE",
  grayDarkBackground: "#CCC",
  grayBorder: "#aaa",
  white: "#fff",
  grayButton: "#777777",
  blueButton: "#0085B2",
  greenButton: "#25B6A9",
  yellowButton: "#FFA54D",
  organgeButton: "#f78520",
  buttonColor: "#04B3B3",
  buttonColorAccent: "#007979"
};

export default colors;
