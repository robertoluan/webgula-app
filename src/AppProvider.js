import React from "react";
import store from "./infra/store";
import { Provider } from "react-redux";
import { Root } from "native-base"; 
import AppComponent from "./App.container";

class AppProvider extends React.Component {
  render = () => {
    return (
      <React.Fragment>
        <Provider store={store}>
          <Root>
            <AppComponent />
          </Root>
        </Provider>
      </React.Fragment>
    );
  };
}
export default AppProvider;
