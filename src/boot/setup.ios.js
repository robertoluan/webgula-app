import React, { Component } from "react";
import AppProvider from "../AppProvider";

export default class Setup extends Component {
  render() {
    return <AppProvider />;
  }
}
