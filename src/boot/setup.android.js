import React, { Component } from "react";

import AppProvider from "../AppProvider";

export default class Setup extends Component {
  state = {
    fontsLoaded: false
  };

  async componentWillMount() {
    this.setState({ fontsLoaded: true });
  }

  render() {
    if (!this.state.fontsLoaded) return null;

    return <AppProvider />;
  }
}
