import { connect } from "react-redux";
import component from "./EstabelecimentoCreditoInformacaoScreen";

const mapStateToProps = (state, props) => {
  const app = state.app;
  const empresaSelecionada = (app.empresa || {}).empresaSelecionada || {};

  return { empresaSelecionada };
};

const mapDispatchToProps = (dispatch, props) => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(component);
