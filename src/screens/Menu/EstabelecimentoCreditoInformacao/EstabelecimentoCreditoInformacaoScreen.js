import React from 'react';
import HeaderEmpresa from '../../../shared/components/HeaderEmpresa';
import { View, ScrollView } from 'react-native';
import TextCustom from '../../../shared/components/TextCustom';
import ContainerPage from '../../../shared/components/ContainerPage';

export default class EstabelecimentoCreditoInformacaoScreen extends React.PureComponent {
  static navigationOptions = {
    title: 'Crédito Informações'
  };

  render() {
    const { empresaSelecionada } = this.props;
    return (
      <ContainerPage>
        <View>
          <HeaderEmpresa navigation={this.props.navigation} />
        </View>

        <ScrollView>
          <TextCustom
            style={{
              padding: 16,
              fontSize: 16
            }}
          >
            {empresaSelecionada.coinAgenciaTexto}
          </TextCustom>
        </ScrollView>
      </ContainerPage>
    );
  }
}
