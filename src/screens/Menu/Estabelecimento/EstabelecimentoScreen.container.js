import { connect } from "react-redux";
import component from "./EstabelecimentoScreen";
import EmpresaRemote from "../../../services/remote/empresa.remote";
import FidelidadeRemote from "../../../services/remote/fidelidade.remote";

const empresaRemote = new EmpresaRemote();
const fidelidadeRemote = new FidelidadeRemote();

const mapStateToProps = (state, props) => {
  let app = state.app || {};

  const empresaSelecionada = (app.empresa || {}).empresaSelecionada || {};

  return {
    isAuth: app.auth && app.auth.isAuth,
    isClientCredentials: app.auth && app.auth.isClientCredentials,
    empresaSelecionada,
    auth: (app.auth && app.auth.userInfo) || {}
  };
};

const mapDispatchToProps = (dispatch, props) => ({
  async findFidelidadeCampanhaAtiva() {
    return fidelidadeRemote.findFidelidadeCampanhaAtiva();
  },
  async selectedEmpresa(empresa) {
    await empresaRemote.setEmpresaDefault(empresa);

    dispatch({
      type: "APP.EMPRESA.EMPRESA_SELECTED",
      payload: {
        empresaSelecionada: { ...empresa }
      }
    });

    // registra visita na empresa
    try {
      await empresaRemote.logVisita();
    } catch (e) {}

    return true;
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(component);
