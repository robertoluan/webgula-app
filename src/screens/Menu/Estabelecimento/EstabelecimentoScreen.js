import React from 'react';
import HeaderEmpresa from '../../../shared/components/HeaderEmpresa';
import {
  View,
  ScrollView,
  Linking,
  TouchableOpacity,
  Platform,
  Alert
} from 'react-native';
import { colors } from '../../../theme';
import ContainerPage from '../../../shared/components/ContainerPage';
import InformacaoSemanal from '../EstabelecimentoSobre/components/InformacaoSemanal';
import SaldoCredito from '../EstabelecimentoControleCredito/components/SaldoCredito';
import SkeletonLoading from '../../../shared/components/SkeletonLoading';
import TextCustom from '../../../shared/components/TextCustom';
import ModalDetalhesFidelidade from '../EstabelecimentoControleCredito/components/SaldoCredito/ModalDetalhesFidelidade';
import { withNavigationFocus } from 'react-navigation';
import SaldoContaVirtual from '../EstabelecimentoControleCredito/components/SaldoContaVirtual';
import ModalDetalhesContaVirtual from '../EstabelecimentoControleCredito/components/SaldoContaVirtual/ModalDetalhesContaVirtual';
import GridMenu from './components/GridMenu';
import Icon from '../../../shared/components/Icon';
import ActionButton from 'react-native-action-button';

class EstabelecimentoScreen extends React.PureComponent {
  state = {
    isLoading: false,
    openModalFidelidadeAdesao: false,
    openModalAbrirConta: false,
    fidelidade: null
  };

  static navigationOptions = {
    title: 'Estabelecimento'
  };

  navigateTo = async screen => {
    this.props.navigation.navigate(screen);
  };

  componentDidMount = async (noLoading = false) => {
    const { empresaSelecionada } = this.props;

    this.setState({
      isLoading: !noLoading ? true : false,
      fidelidade: null // por causa do refresh
    });

    try {
      let data = await this.props.findFidelidadeCampanhaAtiva();

      if (Array.isArray(data)) {
        data = null;
      }

      this.setState({
        fidelidade: data
      });

      await this.props.selectedEmpresa({
        ...empresaSelecionada,
        flgFidelidade: !!data
      });
    } catch (e) {
      Alert.alert(
        'Aviso',
        'Houve um problema ao atualizar as informaçōes da empresa!'
      );
    } finally {
      setTimeout(
        () =>
          this.setState({
            isLoading: false
          }),
        200
      );
    }
  };

  reloadApp = async () => {
    return this.componentDidMount();
  };

  componentDidUpdate = async prevProps => {
    if (prevProps.isFocused !== this.props.isFocused && this.props.isFocused) {
      await this.componentDidMount(true);
    }
  };

  verLocalizacao = () => {
    const { empresaSelecionada } = this.props;

    const lat = empresaSelecionada.endLatitude;
    const lng = empresaSelecionada.endLongitude;
    const scheme = Platform.select({
      ios: `maps: ${lat},${lng}?q=`,
      android: `geo:${lat},${lng}?q=`
    });
    const latLng = `${lat},${lng}`;
    const label = empresaSelecionada.nome;
    const url = Platform.select({
      ios: `${scheme}${label}@${latLng}`,
      android: `${scheme}${latLng}(${label})`
    });

    Linking.openURL(url);
  };
  render() {
    const { empresaSelecionada } = this.props;
    const {
      isLoading,
      fidelidade,
      openModalFidelidadeAdesao,
      openModalAbrirConta
    } = this.state;

    return (
      <ContainerPage
        style={{
          backgroundColor: '#f5f5f5'
        }}
      >
        <View>
          {!isLoading ? (
            <HeaderEmpresa navigation={this.props.navigation} />
          ) : null}
        </View>

        <ScrollView>
          {!isLoading ? (
            <HeaderValores
              navigation={this.props.navigation}
              isAuth={this.props.isAuth}
              empresaSelecionada={empresaSelecionada}
              fidelidade={fidelidade}
              openModalFidelidadeAdesao={openModalFidelidadeAdesao}
              openModalAbrirConta={openModalAbrirConta}
              onPressPontosFidelidade={status =>
                this.setState({ openModalFidelidadeAdesao: status })
              }
              onPressContaVirtual={status =>
                this.setState({ openModalAbrirConta: status })
              }
              onReloadApp={() => this.reloadApp()}
            />
          ) : null}

          <SkeletonLoading lines={4} show={isLoading}>
            <GridMenu
              empresaSelecionada={empresaSelecionada}
              navigation={this.props.navigation}
            />
          </SkeletonLoading>

          <View style={{ marginTop: 16, marginBottom: 16 }}>
            <InformacaoSemanal />
          </View>
        </ScrollView>

        {empresaSelecionada.endLatitude ? (
          <ActionButton
            offsetY={30}
            offsetX={16}
            buttonColor={colors.blueButton}
            renderIcon={() => (
              <Icon
                name="ion-md-locate"
                style={{
                  fontSize: 20,
                  height: 22,
                  color: 'white'
                }}
              />
            )}
            onPress={() => this.verLocalizacao()}
          />
        ) : null}
      </ContainerPage>
    );
  }
}

function HeaderValores({
  empresaSelecionada,
  fidelidade,
  onPressContaVirtual,
  onPressPontosFidelidade,
  openModalFidelidadeAdesao,
  openModalAbrirConta,
  onReloadApp,
  isAuth,
  navigation
}) {
  if (empresaSelecionada.coinAgenciaId <= 0 && !fidelidade) return <View />;
  if (!isAuth) return <View />;

  return (
    <View
      style={{
        flexDirection: 'row',
        backgroundColor: 'white',
        paddingVertical: 16,
        paddingHorizontal: 16
      }}
    >
      <ConfigSaldoFidelidade
        onPress={status => onPressPontosFidelidade(status)}
        empresaSelecionada={empresaSelecionada}
        open={openModalFidelidadeAdesao}
        fidelidade={fidelidade}
        isAuth={isAuth}
        navigation={navigation}
        onReloadApp={onReloadApp}
      />

      <ConfigSaldoContaVirtual
        onPress={status => onPressContaVirtual(status)}
        empresaSelecionada={empresaSelecionada}
        open={openModalAbrirConta}
        navigation={navigation}
        fidelidade={fidelidade}
        onReloadApp={onReloadApp}
      />
    </View>
  );
}

function ConfigSaldoContaVirtual({
  empresaSelecionada,
  navigation,
  onPress,
  open,
  onReloadApp,
  fidelidade
}) {
  if (empresaSelecionada.coinAgenciaId <= 0) return null;

  return (
    <View
      style={{
        flex: 1,
        borderLeftColor: fidelidade ? '#efefef' : undefined,
        paddingHorizontal: fidelidade ? 8 : 0,
        borderLeftWidth: fidelidade ? 1 : 0
      }}
    >
      {empresaSelecionada.coinPessoaContaId > 0 ? (
        <SaldoContaVirtual
          onPress={() => navigation.navigate('EstabelecimentoControleCredito')}
        />
      ) : (
        <View>
          <TouchableOpacity onPress={() => onPress(true)}>
            <View
              style={{
                paddingHorizontal: 8,
                alignItems: 'center',
                alignContent: 'center',
                justifyContent: 'center',
                alignSelf: 'center'
              }}
            >
              <TextCustom
                style={{
                  fontSize: 14,
                  textAlign: 'center',
                  color: colors.primaryColor
                }}
              >
                Criar uma conta virtual de consumo
              </TextCustom>
            </View>
          </TouchableOpacity>

          <ModalDetalhesContaVirtual
            open={open}
            navigation={navigation}
            onOk={async () => {
              onPress(false);
              await onReloadApp();
            }}
            onCancel={() => onPress(false)}
            empresaSelecionada={empresaSelecionada}
          />
        </View>
      )}
    </View>
  );
}

function ConfigSaldoFidelidade({
  fidelidade,
  navigation,
  onPress,
  open,
  isAuth,
  onReloadApp
}) {
  if (!fidelidade) return null;

  return (
    <View style={{ flex: 1 }}>
      {fidelidade.flgParticipa ? (
        <SaldoCredito
          pontos={fidelidade.saldoAtualConta}
          onPress={() =>
            navigation.navigate('Lancamentos', {
              item: {
                ...fidelidade,
                fidelidadeId: fidelidade.id
              }
            })
          }
        />
      ) : (
        <View>
          <TouchableOpacity onPress={() => onPress(true)}>
            <View
              style={{
                paddingHorizontal: 8,
                alignItems: 'center',
                alignContent: 'center',
                justifyContent: 'center',
                alignSelf: 'center'
              }}
            >
              <TextCustom
                style={{
                  fontSize: 14,
                  textAlign: 'center',
                  color: colors.primaryColor
                }}
              >
                Conheça nosso plano de fidelidade
              </TextCustom>
            </View>
          </TouchableOpacity>

          <ModalDetalhesFidelidade
            open={open}
            isAuth={isAuth}
            navigation={navigation}
            onOk={async () => {
              onPress(false);
              await onReloadApp();
            }}
            onCancel={() => onPress(false)}
            fidelidade={fidelidade}
          />
        </View>
      )}
    </View>
  );
}

export default withNavigationFocus(EstabelecimentoScreen);
