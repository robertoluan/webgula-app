import React from "react";
import styled from "styled-components/native";
import Icon from "../../../../shared/components/Icon";
import { View, FlatList, TouchableOpacity, Alert } from "react-native";
import { colors } from "../../../../theme";
import { checkAlertAuth } from "../../../../services/actions/app/auth.actions";

class GridMenu extends React.PureComponent {
  state = {};

  navigateTo = async screen => {
    this.props.navigation.navigate(screen);
  };

  render() {
    const { empresaSelecionada, isClientCredentials } = this.props;

    const menus = [
      {
        title: "Cardápio",
        action: "EstabelecimentoCardapio",
        icon: "fa-file",
        iconSize: 22,
        color: "#2e7189",
        borderColor: "#a4bbc3",
        visible: empresaSelecionada.flgMostraCardapioMenu,
        onPress: screen => {
          if (!empresaSelecionada.flgMostraCardapioMenu) {
            return Alert.alert(
              "Aviso",
              "O Cardápio desta empresa ainda não está disponível para consulta."
            );
          }

          return this.navigateTo(screen);
        }
      },
      {
        title: "Acompanhe sua conta",
        action: "EstabelecimentoAcompanheConta",
        icon: "fa-file-o",
        iconSize: 22,
        color: "#2e7189",
        borderColor: "#a4bbc3",
        visible: true,
        onPress: screen => {
          if (isClientCredentials) {
            return checkAlertAuth(this.props.navigation);
          }

          this.navigateTo(screen);
        }
      },
      {
        title: "Avalie",
        action: "EstabelecimentoAvalie",
        icon: "fa-star-half-o",
        iconSize: 25,
        color: "#2e7189",
        borderColor: "#a4bbc3",
        visible: true,
        onPress: screen => {
          if (isClientCredentials) {
            return checkAlertAuth(this.props.navigation);
          }
          this.navigateTo(screen);
        }
      }
    ];

    return (
      <View
        style={{
          paddingTop: 16,
          paddingLeft: 8,
          paddingRight: 8
        }}
      >
        <FlatList
          data={menus.filter(x => x.visible)}
          keyExtractor={menu => menu.title}
          numColumns={3}
          renderItem={({ item }) => {
            return (
              <TouchableOpacity
                style={{
                  flexGrow: 1,
                  flexBasis: 0,
                  paddingHorizontal: 16,
                  paddingBottom: 8,
                  alignItems: "center"
                }}
                onPress={() => item.onPress(item.action)}
              >
                <View
                  style={{
                    backgroundColor: colors.white,
                    paddingTop: 16,
                    paddingBottom: 16,
                    paddingLeft: 4,
                    paddingRight: 4,
                    borderRadius: 70,
                    // borderWidth: 3,
                    // borderColor: item.borderColor,
                    textAlign: "center",
                    alignItems: "center",
                    justifyContent: "center",
                    width: 75,
                    height: 75
                  }}
                >
                  <Icon
                    style={{
                      color: item.color
                    }}
                    name={item.icon}
                    size={item.iconSize}
                  />
                </View>

                <MenuText>{item.title}</MenuText>
              </TouchableOpacity>
            );
          }}
        />
      </View>
    );
  }
}

export default GridMenu;

const MenuText = styled.Text`
  font-family: "Montserrat-Bold";
  color: #666;
  text-align: center;
  padding-top: 6;
  font-size: 13;
`;
