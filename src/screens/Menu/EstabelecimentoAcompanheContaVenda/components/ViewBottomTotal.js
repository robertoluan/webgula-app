import React from "react";
import { Col, Row, Badge } from "native-base";
import { View } from "react-native";
import { colors } from "../../../../theme";
import { formatMoney } from "../../../../infra/helpers";
import TextCustom from "../../../../shared/components/TextCustom";
import ViewBottom from "../../../../shared/components/ViewBottom";
import ButtonLoading from "../../../../shared/components/ButtonLoading";

export default class ViewBottomTotal extends React.PureComponent {
  navigateTo = () => {
    const { venda, saida } = this.props;

    this.props.navigation.navigate(
      "EstabelecimentoAcompanheContaVendaUsuarios",
      { venda, saida }
    );
  };

  render() {
    const { venda = {} } = this.props;

    return (
      <ViewBottom
        style={{
          margin: 0,
          paddingLeft: 16,
          paddingRight: 16,
          paddingTop: 8,
          paddingBottom: 8,
          backgroundColor: "#efefef"
        }}
      >
        <Row>
          <Col size={50}>
            <ButtonLoading
              height={30}
              style={{ marginTop: 0 }}
              fontSize={13}
              titleColor={colors.grayButton}
              backgroundColor={colors.white}
              onPress={() => this.navigateTo()}
              title="Ver Usuários"
            />

            <View
              style={{
                position: "absolute",
                fontSize: 10,
                left: 0,
                top: -5,
                backgroundColor: colors.primaryColor,
                justifyContent:"center",
                alignItems: "center",
                borderRadius: 25,
                height: 25,
                width: 25
              }}
            > 
              <TextCustom style={{ color: "white", textAlign: "center" }}>
                {venda.qtdeObservadores}
              </TextCustom> 
            </View>
          </Col>
          <Col size={50}>
            <TextCustom
              style={{
                fontFamily: "Montserrat-Bold",
                fontSize: 20,
                textAlign: "right",
                paddingRight: 8,
                color: colors.primaryColor
              }}
            >
              R$ {formatMoney(venda.valorTotal || 0)}
            </TextCustom>
          </Col>
        </Row>
      </ViewBottom>
    );
  }
}
