import React from 'react';
import { Body, Right, ListItem, Col, Row, Toast } from 'native-base';
import HeaderEmpresa from '../../../shared/components/HeaderEmpresa';
import {
  StyleSheet,
  FlatList,
  ScrollView,
  RefreshControl,
  Alert,
  View
} from 'react-native';
import { colors } from '../../../theme';
import { formatMoney } from '../../../infra/helpers';
import ButtonLoading from '../../../shared/components/ButtonLoading';
import moment from 'moment';
import TextCustom from '../../../shared/components/TextCustom';
import ContainerPage from '../../../shared/components/ContainerPage';
import ViewBottomTotal from './components/ViewBottomTotal';
import EmpresaRemote from '../../../services/remote/empresa.remote';
import ActionButton from 'react-native-action-button';
import Icon from '../../../shared/components/Icon';

const empresaRemote = new EmpresaRemote();

class EstabelecimentoAcompanheContaVendaScreen extends React.PureComponent {
  state = {
    isLoading: false,
    venda: {},
    items: [],
    empresa: empresaRemote.getEmpresaDefault()
  };

  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Conta: ' + navigation.getParam('saida').saidaId
    };
  };

  componentDidMount = async () => {
    const saida = this.props.navigation.getParam('saida');

    this.setState({
      isLoading: true
    });

    try {
      const venda = await this.props.loadVenda(saida.saidaId);

      console.log('venda', venda);

      this.setState(
        {
          venda
        },
        () => {
          this.updateData();
        }
      );
    } catch (e) {
      Alert.alert('Aviso', 'Não houve resultado para sua chave!');

      this.props.navigation.navigate('EstabelecimentoAcompanheConta');
    } finally {
      this.setState({ isLoading: false });
    }
  };

  navigateTo = () => {
    const { venda } = this.state;

    const saida = this.props.navigation.getParam('saida');

    this.props.navigation.navigate(
      'EstabelecimentoAcompanheContaVendaProduto',
      { venda, saida }
    );
  };

  calcularRateio = () => {
    const { venda } = this.state;
    const { navigation } = this.props;

    const saida = navigation.getParam('saida');

    navigation.navigate('EstabelecimentoAcompanheContaVendaRateio', {
      venda,
      saida
    });
  };

  updateData = () => {
    const { venda } = this.state;

    if (!venda.id) return [];

    const items = [
      {
        id: 1,
        nome: 'Produtos',
        valor: venda.valorProdutos
      },
      {
        id: 2,
        nome: 'Gorjeta',
        valor: venda.valorComissao
      },
      {
        id: 3,
        nome: 'Couvert',
        valor: venda.valorIngressoTotal
      },
      {
        id: 4,
        nome: 'Desconto',
        valor: venda.valorDescontoTotal
      }
    ];

    this.setState({
      items: [...items]
    });
  };

  getMesa = () => {
    const venda = this.state.venda || {};

    if (venda.id) {
      return venda.numIdentificadorCompleto.replace('Mesa ', '');
    }

    return '--';
  };

  resgatarPontos = async () => {
    let { venda } = this.state;

    const saida = this.props.navigation.getParam('saida');

    this.setState({
      isLoading: true
    });

    try {
      const res = await this.props.resgatarPontos(saida.saidaId);

      if (res && res.type === 'exception') {
        let mensagem = res.error;

        if (res.exception && res.exception.response) {
          mensagem = res.exception.response.data.message;
        }

        Alert.alert('Aviso', mensagem);
      } else {
        venda.flgFidelidadeProcessada = true;

        this.setState({
          venda: { ...venda }
        });

        Toast.show({ text: 'Pontos resgatados com sucesso!' });
      }
    } catch (e) {
      Alert.alert('Aviso', 'Houve um problema ao realizar a operação.');
    } finally {
      this.setState({
        isLoading: false
      });
    }
  };

  render = () => {
    const { venda = {}, items = [], isLoading } = this.state;
    const saida = this.props.navigation.getParam('saida');

    return (
      <ContainerPage style={[styles.wrapper]}>
        <HeaderEmpresa navigation={this.props.navigation} />

        <View style={{ flex: 1, paddingBottom: 60 }}>
          <ScrollView
            refreshControl={
              <RefreshControl
                refreshing={isLoading}
                onRefresh={() => this.componentDidMount()}
              />
            }
          >
            <View
              style={{
                borderBottomColor: '#eee',
                borderBottomWidth: 1,
                top: -10,
                paddingTop: 8,
                paddingBottom: 8,
                paddingLeft: 8,
                paddingRight: 8
              }}
            >
              <Row>
                <Col size={50}>
                  <TextCustom style={styles.labelTop}> Mesa </TextCustom>
                  <TextCustom style={styles.labelBottom}>
                    {this.getMesa()}
                  </TextCustom>
                </Col>

                <Col size={50}>
                  <TextCustom style={styles.labelTop}> Data</TextCustom>
                  <TextCustom style={styles.labelBottom}>
                    {venda.dataSaida
                      ? moment(venda.dataSaida).format('DD/MM/YYYY HH:mm')
                      : '--'}
                  </TextCustom>
                </Col>
              </Row>

              <Row>
                <Col size={50}>
                  <TextCustom style={styles.labelTop}> Garçon</TextCustom>
                  <TextCustom style={styles.labelBottom}>
                    {venda.vendedorApelido || '--'}
                  </TextCustom>
                </Col>

                <Col size={50}>
                  <TextCustom style={styles.labelTop}>Atualizado em</TextCustom>
                  <TextCustom style={styles.labelBottom}>
                    {venda.dataUltimaSync
                      ? moment(venda.dataUltimaSync).format('DD/MM/YYYY HH:mm')
                      : '--'}
                  </TextCustom>
                </Col>
              </Row>
            </View>

            <View>
              <TextCustom style={styles.title}>Valores</TextCustom>

              <FlatList
                data={items}
                extraData={[]}
                renderItem={({ item }) => (
                  <ListItem
                    noBorder
                    noIndent
                    style={{
                      backgroundColor: 'transparent'
                      // padding: 0
                    }}
                  >
                    <Body>
                      <TextCustom
                        style={{
                          fontSize: 14,
                          color: '#333'
                        }}
                      >
                        {item.nome}
                      </TextCustom>
                    </Body>

                    <Right>
                      <TextCustom
                        style={{
                          fontSize: 14,
                          fontFamily: 'Montserrat-Bold'
                        }}
                      >
                        {formatMoney(item.valor)}
                      </TextCustom>
                    </Right>
                  </ListItem>
                )}
              />

              <ButtonLoading
                titleColor={colors.primaryColor}
                backgroundColor={colors.white}
                title="Ver Produtos"
                onPress={() => this.navigateTo()}
              />

              {venda.flgStatusFidelidade === 'P' ? (
                <ButtonLoading
                  loading={isLoading}
                  backgroundColor={
                    !venda.flgFidelidadeProcessada
                      ? colors.primaryColor
                      : colors.grayButton
                  }
                  style={{ marginTop: 8, marginLeft: 16, marginRight: 16 }}
                  title={
                    !venda.flgFidelidadeProcessada
                      ? 'Resgatar ' +
                        (venda.usuarioFidelidade
                          ? venda.fidelidadePontos
                          : '') +
                        ' Pontos'
                      : 'Fidelidade já foi processada!'
                  }
                  onPress={() =>
                    !venda.flgFidelidadeProcessada && this.resgatarPontos()
                  }
                />
              ) : null}
            </View>
          </ScrollView>
        </View>

        {!venda.flgFidelidadeProcessada ? (
          <ActionButton
            offsetY={70}
            offsetX={16}
            buttonColor={colors.primaryColor}
            renderIcon={() => (
              <Icon name="ion-md-calculator" style={styles.actionButtonIcon} />
            )}
            onPress={() => this.calcularRateio()}
          />
        ) : null}

        <ViewBottomTotal
          saida={saida}
          navigation={this.props.navigation}
          venda={venda}
        />
      </ContainerPage>
    );
  };
}

export default EstabelecimentoAcompanheContaVendaScreen;

const styles = StyleSheet.create({
  wrapper: {
    backgroundColor: '#fff'
  },
  title: {
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
    paddingLeft: 16,
    paddingTop: 0,
    paddingRight: 16,
    marginBottom: 6
  },
  labelTop: {
    fontSize: 12,
    paddingTop: 8,
    color: colors.grayColor
  },
  labelBottom: {
    fontSize: 14,
    paddingTop: 0,
    paddingLeft: 2
  },

  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: 'white'
  }
});
