import React from 'react';
import HeaderEmpresa from '../../../shared/components/HeaderEmpresa';
import { View, ScrollView, ActivityIndicator } from 'react-native';
import EmpresaRemote from '../../../services/remote/empresa.remote';
import TextCustom from '../../../shared/components/TextCustom';
import ContainerPage from '../../../shared/components/ContainerPage';
import InformacaoSemanal from './components/InformacaoSemanal';

const empresaRemote = new EmpresaRemote();

export default class EstabelecimentoSobreScreen extends React.Component {
  state = {
    item: {},
    items: [],
    loading: false
  };

  static navigationOptions = {
    title: 'Sobre a Empresa'
  };

  componentDidMount = async () => {
    this.setState({
      loading: true
    });

    try {
      const items = await this.props.load();

      this.setState({
        items: items.map(item => {
          return {
            title: item.diaSemanaNome,
            caption: item.texto,
            flgMusicaAoVivo: item.flgMusicaAoVivo,
            url: item.imagemUrl || 'http://placeimg.com/640/480/any'
          };
        })
      });
    } catch (e) {
    } finally {
      this.setState({
        loading: false
      });
    }
  };

  render() {
    const { loading } = this.state;
    const { empresaSelecionada } = this.props;

    return (
      <ContainerPage>
        {loading && <ActivityIndicator />}

        <View>
          <HeaderEmpresa navigation={this.props.navigation} />
        </View>

        <ScrollView>
          <TextCustom
            style={{
              padding: 16,
              fontSize: 16
            }}
          >
            {(empresaSelecionada && empresaSelecionada.mensagemPadrao) + ''}
          </TextCustom>

          <InformacaoSemanal />
        </ScrollView>
      </ContainerPage>
    );
  }
}
