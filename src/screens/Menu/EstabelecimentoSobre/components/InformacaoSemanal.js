import React from 'react';
import {
  View,
  ScrollView,
  Image,
  ActivityIndicator,
  FlatList
} from 'react-native';
import TextCustom from '../../../../shared/components/TextCustom';
import { Card, CardItem, Button, Body, Left } from 'native-base';
import Icon from '../../../../shared/components/Icon';
import EmpresaApi from '../../../../services/remote/empresa.remote';

const empresaApi = new EmpresaApi();

export default class InformacaoSemanal extends React.Component {
  state = {
    item: {},
    items: [],
    loading: false
  };

  componentDidMount = async () => {
    try {
      const items = await empresaApi.findInfoSemana();

      this.setState({
        items: items.map(item => {
          return {
            title: item.diaSemanaNome,
            caption: item.texto,
            flgMusicaAoVivo: item.flgMusicaAoVivo,
            url: item.imagemUrl || 'http://placeimg.com/640/480/any'
          };
        })
      });
    } catch (e) {
    } finally {
      this.setState({
        loading: false
      });
    }
  };

  render() {
    const { loading, items } = this.state;

    return (
      <View>
        {loading && <ActivityIndicator />}

        <ScrollView>
          {items.length ? (
            <TextCustom
              style={{
                margin: 16,
                marginBottom: 6,
                fontSize: 16
              }}
            >
              Programação da Semana
            </TextCustom>
          ) : null}

          <FlatList
            ref={ref => {
              this.flatListRef = ref;
            }}
            style={{ flex: 1 }}
            pagingEnabled={true}
            data={items}
            showsHorizontalScrollIndicator={false}
            legacyImplementation={false}
            automaticallyAdjustContentInsets={true}
            horizontal
            renderItem={({ item }) => (
              <View
                style={{
                  width: 260,
                  marginLeft: 16,
                  marginRight: 16
                }}
              >
                <Card>
                  <CardItem>
                    <Left>
                      <Body>
                        <TextCustom
                          style={{
                            fontFamily: 'Montserrat-Bold'
                          }}
                        >
                          {item.title}
                        </TextCustom>
                      </Body>
                    </Left>
                  </CardItem>

                  <CardItem cardBody>
                    <Image
                      source={{ uri: item.url }}
                      style={{ height: 200, width: null, flex: 1 }}
                    />
                  </CardItem>

                  <CardItem>
                    <Left>
                      {item.flgMusicaAoVivo ? (
                        <Button transparent>
                          <Icon name="fa-music" />
                          <TextCustom> Música ao vivo </TextCustom>
                        </Button>
                      ) : null}
                    </Left>
                  </CardItem>

                  <CardItem>
                    <TextCustom>{item.caption}</TextCustom>
                  </CardItem>
                </Card>
              </View>
            )}
          />
        </ScrollView>
      </View>
    );
  }
}
