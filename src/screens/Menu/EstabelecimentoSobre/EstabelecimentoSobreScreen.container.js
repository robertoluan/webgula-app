import { connect } from 'react-redux';
import component from './EstabelecimentoSobreScreen';
import EmpresaApi from '../../../services/remote/empresa.remote';

const empresaApi = new EmpresaApi();

const mapStateToProps = (state, props) => {
  let app = state.app || {};

  const empresaSelecionada = (app.empresa || {}).empresaSelecionada || {};

  return {
    empresaSelecionada
  };
};

const mapDispatchToProps = (dispatch, props) => ({
  async load() {
    return empresaApi.findInfoSemana();
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(component);
