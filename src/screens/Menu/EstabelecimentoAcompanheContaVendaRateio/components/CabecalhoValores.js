import React from 'react';
import { Col, Row } from 'native-base';
import { StyleSheet, View } from 'react-native';
import { formatMoney } from '../../../../infra/helpers';
import TextCustom from '../../../../shared/components/TextCustom';
import { colors } from '../../../../theme';

function CabecalhoValores({ venda }) {
  const valorProdutos = venda.valorProdutos;
  const valorComissao = venda.valorComissao;
  const valorCouvert = venda.valorIngressoTotal;
  const valorTotal = valorProdutos + valorComissao + valorCouvert;

  return (
    <View style={{ flex: 1, marginBottom: 20 }}>
      <View
        style={{
          borderBottomColor: '#eee',
          borderBottomWidth: 1,
          top: -10,
          paddingTop: 8,
          paddingBottom: 8,
          paddingLeft: 8,
          paddingRight: 8
        }}
      >
        <Row>
          <Col size={50}>
            <TextCustom style={styles.labelTop}> Produtos </TextCustom>
            <TextCustom style={styles.labelBottom}>
              R$ {formatMoney(valorProdutos)}
            </TextCustom>
          </Col>

          <Col size={50}>
            <TextCustom style={styles.labelTop}> Serviços</TextCustom>
            <TextCustom style={styles.labelBottom}>
              R$ {formatMoney(valorComissao)}
            </TextCustom>
          </Col>
        </Row>

        <Row>
          <Col size={50}>
            <TextCustom style={styles.labelTop}> Couvert</TextCustom>
            <TextCustom style={styles.labelBottom}>
              R$ {formatMoney(valorCouvert)}
            </TextCustom>
          </Col>

          <Col size={50}>
            <TextCustom style={styles.labelTop}> Total</TextCustom>
            <TextCustom style={styles.bigText}>
              R$ {formatMoney(valorTotal)}
            </TextCustom>
          </Col>
        </Row>
      </View>
    </View>
  );
}

export default CabecalhoValores;

const styles = StyleSheet.create({
  title: {
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
    paddingLeft: 16,
    paddingTop: 0,
    paddingRight: 16,
    marginBottom: 6
  },
  labelTop: {
    fontSize: 12,
    paddingTop: 8,
    color: colors.grayColor,
    paddingLeft: 0
  },
  labelBottom: {
    fontSize: 14,
    paddingTop: 0,
    paddingLeft: 2
  },
  bigText: {
    fontSize: 18,
    fontWeight: 'bold',
    paddingTop: 0,
    color: 'green',
    paddingLeft: 2
  }
});
