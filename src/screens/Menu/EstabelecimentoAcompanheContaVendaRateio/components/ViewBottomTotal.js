import React from 'react';
import { Col, Row } from 'native-base';
import { StyleSheet } from 'react-native';
import { colors } from '../../../../theme';
import { formatMoney } from '../../../../infra/helpers';
import TextCustom from '../../../../shared/components/TextCustom';
import ViewBottom from '../../../../shared/components/ViewBottom';

export default class ViewBottomTotal extends React.PureComponent {
	render() {
		const { valorRateado, valorRestante } = this.props;

		return (
			<ViewBottom
				style={{
					margin: 0,
					height: 70,
					paddingLeft: 16,
					paddingRight: 16,
					paddingTop: 8,
					paddingBottom: 8,
					backgroundColor: '#efefef'
				}}
			>
				<Row>
					<Col size={50}>
						<TextCustom style={styles.labelTop}> Valor Rateado</TextCustom>
						<TextCustom style={styles.labelBottom}>
							R$ {formatMoney(valorRateado)}
						</TextCustom>
					</Col>
					<Col size={50}>
						<TextCustom style={styles.labelTop}> Valor Restante</TextCustom>
						<TextCustom style={styles.labelBottom}>
							R$ {formatMoney(valorRestante)}
						</TextCustom>
					</Col>
				</Row>
			</ViewBottom>
		);
	}
}

const styles = StyleSheet.create({
	labelTop: {
		fontSize: 12,
		color: colors.grayColor
	},
	labelBottom: {
		fontSize: 14,
		paddingTop: 0,
		paddingLeft: 2
	}
});
