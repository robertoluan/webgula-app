import React from 'react';
import { View, StyleSheet, Alert } from 'react-native';
import { colors } from '../../../../theme';
import ButtonLoading from '../../../../shared/components/ButtonLoading';
import Dialog from '../../../../shared/components/Dialog';
import Input from '../../../../shared/components/Input';
import InputMask from '../../../../shared/components/Input/InputMask';
import { Content, Toast } from 'native-base';
import AsyncStorageRemote from '../../../../services/remote/asyncStorage.remote';
import { formatMoney } from '../../../../infra/helpers';

const asyncStorage = new AsyncStorageRemote();

class ModalRateioAvulso extends React.PureComponent {
  state = {
    form: {}
  };

  componentDidMount = () => {
    const { form } = this.props;

    if (form && form.id) {
      this.setState({
        form: {
          nome: form.usuario.nomeUsuario,
          valor: form.valor,
          id: form.id
        }
      });
    }
  };

  onChange = (prop, value) => {
    this.setState({
      form: {
        ...this.state.form,
        [prop]: value
      }
    });
  };

  onSalvar = async () => {
    const { saida, venda } = this.props;
    const { form } = this.state;

    if (!form.nome) return Alert.alert('Aviso', 'Nome é obrigatório!');
    if (!form.valor) return Alert.alert('Aviso', 'Valor é obrigatório!');

    const usuariosRateio =
      (await asyncStorage.get('usuariosRateio' + saida.saidaId)) || [];

    let valorVendaAtual = 0;

    usuariosRateio.forEach(item => {
      valorVendaAtual += item.valor;
    });

    if (venda.valorTotal + valorVendaAtual > valorVendaAtual) {
      return Alert.alert(
        'Aviso',
        `O Valor do rateio ultrapassou o valor restante da venda que é R$ ${formatMoney(
          venda.valorTotal - valorVendaAtual
        )}. Por favor remova algum item da sua cota.`
      );
    }

    if (!form.id) {
      usuariosRateio.push({
        id: +new Date(),
        usuario: {
          nomeUsuario: form.nome
        },
        valor: Number(form.valor),
        isAvulso: true
      });
    } else {
      usuariosRateio.forEach(x => {
        if (x.id === form.id) {
          x.valor = Number(form.valor);
          x.usuario = {
            nomeUsuario: form.nome
          };
        }
      });
    }

    await asyncStorage.save('usuariosRateio' + saida.saidaId, usuariosRateio);

    this.props.onOk();
  };

  onApagar = async () => {
    Alert.alert('Aviso', 'Deseja realmente remover do rateio?', [
      {
        text: 'Remover',
        onPress: async () => {
          const saida = this.props.saida;

          let usuariosRateio =
            (await asyncStorage.get('usuariosRateio' + saida.saidaId)) || [];

          usuariosRateio = usuariosRateio.filter(
            x => x.id !== this.state.form.id
          );

          await asyncStorage.save(
            'usuariosRateio' + saida.saidaId,
            usuariosRateio
          );

          this.props.onCancel();
        }
      },
      {
        text: 'Cancelar'
      }
    ]);
  };

  render = () => {
    const { onCancel, onOk } = this.props;
    const { form } = this.state;

    const onApagar = this.onApagar.bind(this);
    return (
      <Dialog
        open={true}
        title={'Rateio Avulso'}
        onCancel={() => onCancel()}
        onOk={() => onOk()}
      >
        <Content
          style={{
            top: -30,
            borderBottomLeftRadius: 20,
            borderBottomRightRadius: 20
          }}
        >
          <View style={styles.bottom}>
            <View>
              <Input
                label="Nome"
                transparent
                value={form.nome}
                onChangeText={value => this.onChange('nome', value)}
              />
            </View>

            <View>
              <InputMask
                label={'Valor'}
                type={'money'}
                unit=""
                value={form.valor}
                onChangeText={value => this.onChange('valor', value)}
              />
            </View>

            <View style={{ height: 15 }} />
            <ButtonLoading onPress={() => this.onSalvar()} title="Adicionar" />

            {form.id ? <View style={{ height: 15 }} /> : null}
            {form.id ? (
              <ButtonLoading
                onPress={() => onApagar()}
                backgroundColor={'#f7f7f7'}
                titleColor={colors.grayColor}
                title="Remover"
              />
            ) : null}

            <View style={{ height: 15 }} />
            <ButtonLoading
              onPress={() => onCancel()}
              backgroundColor={'white'}
              titleColor={colors.grayColor}
              title="Cancelar"
            />
          </View>
        </Content>
      </Dialog>
    );
  };
}

const styles = StyleSheet.create({
  bottom: {
    marginTop: 30
  }
});

export default ModalRateioAvulso;
