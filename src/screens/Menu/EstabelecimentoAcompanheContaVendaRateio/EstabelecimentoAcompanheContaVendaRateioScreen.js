import React from 'react';
import HeaderEmpresa from '../../../shared/components/HeaderEmpresa';
import {
  StyleSheet,
  ScrollView,
  RefreshControl,
  Alert,
  View
} from 'react-native';
import ContainerPage from '../../../shared/components/ContainerPage';
import ViewBottomTotal from './components/ViewBottomTotal';
import CabecalhoValores from './components/CabecalhoValores';
import ListaRateados from './components/ListaRateados';
import ActionButton from 'react-native-action-button';
import Icon from '../../../shared/components/Icon';
import { colors } from '../../../theme';
// import saida from './saida.json';
// import venda from './venda.json';
import AsyncStorageRemote from '../../../services/remote/asyncStorage.remote';
import PageEmpty from '../../../shared/components/PageEmpty';
import TextCustom from '../../../shared/components/TextCustom';
import { withNavigationFocus } from 'react-navigation';
import ModalRateioAvulso from './components/ModalRateioAvulso';

const asyncStorage = new AsyncStorageRemote();

class EstabelecimentoAcompanheContaVendaRateioScreen extends React.PureComponent {
  state = {
    isLoading: false,
    openModalRateioAvulso: false,
    formAvulso: {},
    venda: {},
    items: [],
    usuariosRateio: []
  };

  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Rateio da Conta: ' + navigation.getParam('saida').saidaId
    };
  };

  componentDidMount = async () => {
    const { navigation } = this.props;

    const saida = navigation.getParam('saida');

    const usuariosRateio =
      (await asyncStorage.get('usuariosRateio' + saida.saidaId)) || [];

    this.setState({
      usuariosRateio
    });
  };

  componentDidUpdate = async prevProps => {
    if (prevProps.isFocused !== this.props.isFocused && this.props.isFocused) {
      this.componentDidMount();
    }
  };

  criarRateioAvulso = (rateio = null) => {
    this.setState({
      openModalRateioAvulso: true,
      formAvulso: rateio
    });
  };

  closeRateioAvulso = () => {
    this.setState(
      {
        openModalRateioAvulso: false
      },
      () => {
        this.componentDidMount();
      }
    );
  };

  irParaRateio = (rateio = null) => {
    if (rateio && rateio.isAvulso) return this.criarRateioAvulso(rateio);

    const { navigation } = this.props;
    const venda = navigation.getParam('venda');
    const saida = navigation.getParam('saida');

    navigation.navigate('EstabelecimentoAcompanheContaVendaRateioNovo', {
      venda,
      saida,
      rateio
    });
  };

  render = () => {
    const {
      isLoading,
      usuariosRateio = [],
      openModalRateioAvulso
    } = this.state;
    const saida = this.props.navigation.getParam('saida') || {};
    const venda = this.props.navigation.getParam('venda') || {};

    let valorRateado = 0;
    let valorRestante = 0;

    usuariosRateio.forEach(item => {
      valorRateado += item.valor;
    });

    valorRestante = venda.valorTotal - valorRateado;

    return (
      <ContainerPage style={styles.wrapper}>
        <HeaderEmpresa navigation={this.props.navigation} />

        <View style={{ flex: 1, paddingBottom: 60 }}>
          <ScrollView
            refreshControl={
              <RefreshControl
                refreshing={isLoading}
                onRefresh={() => this.componentDidMount()}
              />
            }
          >
            <CabecalhoValores venda={venda} />

            {openModalRateioAvulso ? (
              <ModalRateioAvulso
                open={openModalRateioAvulso}
                form={this.state.formAvulso}
                saida={saida}
                venda={venda}
                onCancel={() => this.closeRateioAvulso()}
                onOk={() => this.closeRateioAvulso()}
              />
            ) : null}

            <ListaRateados
              items={usuariosRateio}
              onPress={rateio => this.irParaRateio({ ...rateio })}
            />

            {!usuariosRateio.length ? (
              <View style={{ marginTop: 70 }}>
                <PageEmpty
                  icon="fa-frown-o"
                  content={
                    <View>
                      <TextCustom>Nenhum usuário ainda no rateio</TextCustom>
                    </View>
                  }
                />
              </View>
            ) : null}
          </ScrollView>
        </View>

        <ActionButton
          renderIcon={() => (
            <Icon name="ion-md-add" style={styles.actionButtonIcon} />
          )}
          offsetY={80}
          offsetX={16}
          buttonColor={colors.blueButton}
        >
          <ActionButton.Item
            buttonColor={colors.primaryColor}
            title="Usar Calculadora"
            onPress={() => {
              if (valorRestante < 0) {
                return Alert.alert(
                  'Aviso',
                  'Não é possível mais ratear a conta!'
                );
              }

              return this.irParaRateio();
            }}
          >
            <Icon name="ion-md-calculator" style={styles.actionButtonIcon} />
          </ActionButton.Item>
          <ActionButton.Item
            buttonColor="#3498db"
            title="Criar Avulso"
            onPress={() => {
              if (valorRestante < 0) {
                return Alert.alert(
                  'Aviso',
                  'Não é possível mais ratear a conta!'
                );
              }

              return this.criarRateioAvulso();
            }}
          >
            <Icon name="ion-md-person-add" style={styles.actionButtonIcon} />
          </ActionButton.Item>
        </ActionButton>

        <ViewBottomTotal
          saida={saida}
          valorRateado={valorRateado}
          valorRestante={valorRestante}
          navigation={this.props.navigation}
          venda={venda}
        />
      </ContainerPage>
    );
  };
}

export default withNavigationFocus(
  EstabelecimentoAcompanheContaVendaRateioScreen
);

const styles = StyleSheet.create({
  wrapper: {
    backgroundColor: '#fff'
  },
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: 'white'
  }
});
