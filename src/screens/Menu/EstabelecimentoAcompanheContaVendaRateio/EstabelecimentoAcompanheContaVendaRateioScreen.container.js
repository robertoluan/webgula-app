import { connect } from 'react-redux';
import component from './EstabelecimentoAcompanheContaVendaRateioScreen';

const mapStateToProps = (state, props) => {
	return {};
};

const mapDispatchToProps = (dispatch, props) => ({});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(component);
