import React from "react";
import HeaderEmpresa from "../../../shared/components/HeaderEmpresa";
import styled from "styled-components/native";
import { FlatList, ScrollView } from "react-native";
import EmpresaRemote from "../../../services/remote/empresa.remote";
import CardapioGrupoProduto from "./CardapioGrupoProduto";
import SkeletonLoading from "../../../shared/components/SkeletonLoading";
import ContainerPage from "../../../shared/components/ContainerPage";
import { Content } from "native-base";

const empresaRemote = new EmpresaRemote();

export default class EstabelecimentoCardapioScreen extends React.Component {
  state = {
    items: [],
    empresa: {},
    loading: false,
    item: {}
  };

  static navigationOptions = {
    title: "Produtos"
  };

  componentDidMount = async () => {
    let item = this.props.navigation.getParam("grupo") || {};

    this.setState({
      item,
      loading: true
    });

    let { id } = item;

    let empresa = await empresaRemote.getEmpresaDefault();

    let produtos = await this.props.load(id); // GRUPO 0 porque é o unico

    this.setState({
      items: produtos.filter(x => !!x.mostrarCardapioCliente),
      empresa,
      loading: false
    });
  };

  navigateTo = item => {
    this.props.navigation.navigate(`EstabelecimentoCardapioProdutoItem`, {
      item
    });
  };

  renderItem = ({ item }) => {
    return (
      <CardapioGrupoProduto
        item={item}
        onPress={produto => this.navigateTo(produto)}
      />
    );
  };

  render = () => {
    const { items, item, loading } = this.state;

    return (
      <ContainerPage
        style={{
          paddingBottom: 32
        }}
      >
        <Content>
          <HeaderEmpresa navigation={this.props.navigation} />

          <H3>{item.nome}</H3>

          <SkeletonLoading show={loading}>
            <ScrollView>
              <FlatList data={items} renderItem={this.renderItem} />
            </ScrollView>
          </SkeletonLoading>
        </Content>
      </ContainerPage>
    );
  };
}

const H3 = styled.Text`
  font-size: 20px;
  margin-left: 16px;
  margin-right: 16px;
  font-family: Montserrat-Bold;
  margin-bottom: 12px;
  margin-top: 16px;
  color: #111;
`;
