import React from "react";
import { Thumbnail, Left, Body, ListItem } from "native-base";
import { TouchableOpacity } from "react-native";
import colors from "../../../theme/colors";
import { formatMoney } from "../../../infra/helpers";
import TextCustom from "../../../shared/components/TextCustom";

const imagem = require("../../../../assets/images/noimage.png");

export default class CardapioGrupoProduto extends React.Component {
  render = () => {
    const { item, onPress } = this.props;

    let wrapperImagem = {};

    if (item && item.imagem && item.imagem.length) {
      wrapperImagem = {
        uri: item.imagem[0].url
      };
    } else {
      wrapperImagem = imagem;
    }

    return (
      <TouchableOpacity>
        <ListItem
          style={{
            marginTop: -8,
            marginBottom: -8,
            padding: 0,
            backgroundColor: "white"
          }}
          noIndent={true}
          thumbnail
          noBorder
          itemDivider
          onPress={() => onPress(item)}
        >
          <Left>
            <Thumbnail
              // square
              borderRadius={5}
              resizeMode={"contain"}
              source={wrapperImagem}
            />
          </Left>

          <Body>
            <TextCustom style={{ fontSize: 14, color: "#333" }}>
              {item.nomeCurto}
            </TextCustom>

            <TextCustom
              note
              style={{
                fontSize: 12,
                textAlign: "left",
                color: colors.primaryColor
              }}
            >
              R$ {formatMoney(item.preco)}
            </TextCustom>
          </Body>
        </ListItem>
      </TouchableOpacity>
    );
  };
}
