import { connect } from "react-redux";
import component from "./EstabelecimentoCardapioProdutoScreen";
import EmpresaApi from "../../../services/remote/empresa.remote";

const empresaApi = new EmpresaApi();

const mapStateToProps = (state, props) => {
  return {};
};

const mapDispatchToProps = (dispatch, props) => ({
  async load(grupoId) {
    return empresaApi.findCardapio(grupoId);
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(component);
