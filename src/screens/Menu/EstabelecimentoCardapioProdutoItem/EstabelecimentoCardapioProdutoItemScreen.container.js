import { connect } from 'react-redux';
import component from './EstabelecimentoCardapioProdutoItemScreen';   
import EmpresaApi from '../../../services/remote/empresa.remote';

const empresaApi = new EmpresaApi;

const mapStateToProps = (state, props) => {
    return {  
    }
}

const mapDispatchToProps = (dispatch, props) => ({
    
    async load(itemId) {
        return empresaApi.findCardapioItem(itemId);
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(component);
