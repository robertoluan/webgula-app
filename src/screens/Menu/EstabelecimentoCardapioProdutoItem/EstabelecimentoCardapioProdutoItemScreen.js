import React from "react";
import { List } from "native-base";
import { View, Image, Dimensions, Alert, TouchableOpacity } from "react-native";
import colors from "../../../theme/colors";
import EmpresaRemote from "../../../services/remote/empresa.remote";
import GlobalLoader from "../../../shared/components/Loader/Loader";
import TextCustom from "../../../shared/components/TextCustom";
import RenderPrecoUnico from "../../Delivery/ProdutosGruposItem/components/RenderPrecoUnico";
import RenderPrecoItem from "../../Delivery/ProdutosGruposItem/components/RenderPrecoItem";
import ContainerPage from "../../../shared/components/ContainerPage";
import ImageView from "react-native-image-view";

const empresaRemote = new EmpresaRemote();

const imagem = require("../../../../assets/images/noimage.png");

export default class EstabelecimentoCardapioProdutoItemScreen extends React.Component {
  state = {
    produto: {
      precos: []
    },
    empresa: {},
    isLoading: false,
    visible: false
  };

  static navigationOptions = {
    title: "Detalhes do Item"
  };

  componentDidMount = async () => {
    let empresa = await empresaRemote.getEmpresaDefault();

    let item = this.props.navigation.getParam("item") || {};

    const produto = { precos: [], ...item };

    this.setState({
      isLoading: true,
      empresa,
      produto: { ...produto }
    });

    try {
      let response = await this.props.load(item.id);

      response = response || {};

      if (response) {
        produto.precos = response.valorPrecoItem || [];

        produto.precos.sort((a, b) => a.valorPreco - b.valorPreco);
      } else {
        produto.precos = [{ valorPreco: produto.preco, selected: true }];
      }

      this.setState({
        produto: { ...produto }
      });
    } catch (e) {
      Alert.alert("Aviso", "Houve um problema ao trazer o item selecionado.");
    } finally {
      this.setState({
        isLoading: false
      });
    }
  };

  renderLista(items) {
    items = items || [];

    return (
      <List>
        {items.map((preco, key) => (
          <RenderPrecoItem item={preco} key={key} />
        ))}
      </List>
    );
  }

  abrirPreview() {
    this.setState({
      visible: true
    });
  }

  render() {
    let { produto, isLoading, visible } = this.state;

    let wrapperImagem = {};

    if (produto && produto.imagem && produto.imagem.length) {
      wrapperImagem = {
        uri: produto.imagem[0].url
      };
    } else {
      wrapperImagem = imagem;
    }

    return (
      <ContainerPage
        style={{
          backgroundColor: "#f5f5f5"
        }}
      >
        <ImageView
          images={[
            {
              source: wrapperImagem,
              title: "",
              width: 806,
              height: 720
            }
          ]}
          imageIndex={0}
          onClose={() => this.setState({ visible: false })}
          isVisible={visible}
        />

        <GlobalLoader loading={isLoading} />

        <View
          style={{
            marginTop: 0,
            left: 0,
            top: -10,
            right: 0
          }}
        >
          <TouchableOpacity
            activeOpacity={0.8}
            onPress={() => this.abrirPreview()}
          >
            <Image
              style={{
                width: Dimensions.get("window").width,
                height: Dimensions.get("window").width / 2
              }}
              source={wrapperImagem}
            />
          </TouchableOpacity>
        </View>

        <View
          style={{
            top: -40,
            paddingRight: 16,
            paddingLeft: 16,
            paddingBottom: 12,
            marginRight: 16,
            marginLeft: 16,
            backgroundColor: "white",
            borderRadius: 4
          }}
        >
          <TextCustom
            style={{
              fontSize: 18,
              textAlign: "center",
              fontFamily: "Montserrat-Bold",
              marginBottom: 26,
              marginTop: 16,
              color: colors.primaryColor
            }}
          >
            {produto.nomeCompleto}
          </TextCustom>

          <View style={{ minHeight: 50 }}>
            {produto.precos.length > 1 ? (
              this.renderLista(produto.precos)
            ) : (
              <RenderPrecoUnico item={produto} />
            )}
          </View>

          {produto.textoInformativo ? (
            <View style={{ marginTop: 32 }}>
              <TextCustom>{produto.textoInformativo}</TextCustom>
            </View>
          ) : null}
        </View>
      </ContainerPage>
    );
  }
}
