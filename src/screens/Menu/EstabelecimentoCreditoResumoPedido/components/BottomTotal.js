import React from "react";
import { View } from "react-native";
import TextCustom from "../../../../shared/components/TextCustom";
import ViewBottom from "../../../../shared/components/ViewBottom";
import { formatMoney } from "../../../../infra/helpers";
import ButtonLoading from "../../../../shared/components/ButtonLoading";

export default class BottomTotal extends React.PureComponent {
  state = {
    items: [],
    total: 0
  };

  navigateTo = async screen => {
    this.props.navigation.navigate(screen);
  };

  render() {
    const { items = [] } = this.props;

    let valorTotal = 0;

    items.forEach(item => (valorTotal += item.valorTotal));

    return (
      <ViewBottom style={{ margin: 0 }}>
        <View
          style={{ flexDirection: "row" }}
          style={{
            backgroundColor: "#f7f7f7",
            flexDirection: "row",
            paddingTop: 10,
            paddingBottom: 10,
            paddingLeft: 16,
            paddingRight: 16
          }}
        >
          <View style={{ flex: 1, paddingTop: 6 }}>
            <View style={{ flexDirection: "row" }}>
              <TextCustom
                style={{
                  fontSize: 16
                }}
              >
                Total:
              </TextCustom>
              <TextCustom
                style={{
                  fontSize: 18,
                  marginLeft: 10,
                  textAlign: "right"
                }}
              >
                R$ {formatMoney(valorTotal)}
              </TextCustom>
            </View>
          </View>

          <View>
            {items && items.length ? (
              <ButtonLoading
                style={{ paddingHorizontal: 16 }}
                fontSize={13}
                loading={this.props.isLoading}
                onPress={() => this.props.onEnviarPedido()}
                title="Enviar Pedido"
              />
            ) : null}
          </View>
        </View>
      </ViewBottom>
    );
  }
}
