import React from "react";
import { View, StyleSheet } from "react-native";
import TextCustom from "../../../../shared/components/TextCustom";
import { formatMoney } from "../../../../infra/helpers";
import ButtonLoading from "../../../../shared/components/ButtonLoading";

export default class HeaderResumo extends React.Component {
  state = {
    loading: false
  };

  onAdicionar = () => {
    this.props.setOrigemPedido();

    this.props.navigation.navigate("DeliveryProdutos");
  };

  render() {
    const { empresaSelecionada } = this.props;

    return (
      <View style={styles.container}>
        <View style={{ flex: 2 }}>
          <TextCustom style={styles.labelTop}> Meu saldo </TextCustom>
          <TextCustom style={styles.labelBottom}>
            R$ {formatMoney(empresaSelecionada.coinPessoaSaldo, 2)}
          </TextCustom>
        </View>

        <View style={{ marginTop: 10 }}>
          <ButtonLoading
            style={{ paddingHorizontal: 16 }}
            fontSize={13}
            onPress={() => this.onAdicionar()}
            title="Adicionar"
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "white",
    paddingHorizontal: 8,
    paddingVertical: 8,
    flexDirection: "row"
  },
  labelTop: {
    fontSize: 12,
    color: "#666",
    paddingTop: 8
  },
  labelBottom: {
    fontSize: 16,
    paddingTop: 0,
    paddingLeft: 2
  }
});
