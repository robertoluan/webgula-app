import { connect } from "react-redux";
import component from "./BottomTotal";

const mapStateToProps = (state, props) => {
  const app = state.app;
  const delivery = app.delivery;
  const items = (delivery && delivery.items) || [];

  return {
    items
  };
};

const mapDispatchToProps = (dispatch, props) => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(component);
