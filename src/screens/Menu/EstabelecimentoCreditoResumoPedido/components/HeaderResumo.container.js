import { connect } from "react-redux";
import component from "./HeaderResumo";

const mapStateToProps = (state, props) => {
  const app = state.app;
  const delivery = app.delivery;
  const items = (delivery && delivery.items) || [];
  const empresaSelecionada = (app.empresa || {}).empresaSelecionada || {};

  return {
    items,
    empresaSelecionada
  };
};

const mapDispatchToProps = (dispatch, props) => ({
  setOrigemPedido() {
    dispatch({
      type: "APP.DELIVERY.SET_ORIGIN_PEDIDO",
      payload: {
        origem: "conta-credito"
      }
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(component);
