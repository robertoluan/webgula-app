import { connect } from "react-redux";
import component from "./EstabelecimentoCreditoResumoPedidoScreen";
import { removeAll } from "../../../services/actions/app/delivery.actions";

const mapStateToProps = (state, props) => {
  const app = state.app;
  const delivery = app.delivery;
  const items = (delivery && delivery.items) || [];

  return {
    items,
    empresaSelecionada: (app.empresa || {}).empresaSelecionada || {}
  };
};

const mapDispatchToProps = (dispatch, props) => ({
  async clearPedido() {
    return dispatch(removeAll());
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(component);
