import React from "react";
import { withNavigationFocus } from "react-navigation";
import { Alert, View } from "react-native";
import HeaderEmpresa from "../../../shared/components/HeaderEmpresa";
import ContainerPage from "../../../shared/components/ContainerPage";
import ProdutosBody from "../../Delivery/Carrinho/screens/Produtos/ProdutosBody.container";
import HeaderResumo from "./components/HeaderResumo.container";
import BottomTotal from "./components/BottomTotal.container";
import ContaVirtualRemote from "../../../services/remote/contavirtual.remote";


class EstabelecimentoCreditoResumoPedidoScreen extends React.Component {
  state = {
    isLoading: false
  };

  static navigationOptions = {
    title: "Resumo do Pedido"
  };

  componentDidUpdate = prevProps => {
    // Hack para atualizar o redux
    if (
      prevProps.isFocused !== this.props.isFocused &&
      this.props.isFocused
    ) {
      if (this.refProdutos) {
        this.refProdutos.reload();
      }
      this.forceUpdate();
    }
  };

  onEnviarPedido = async () => {
    let valorTotal = 0;

    this.props.items.forEach(item => (valorTotal += item.valorTotal));

    if (valorTotal > this.props.empresaSelecionada.coinPessoaSaldo) {
      return Alert.alert("Aviso", "Você não possui saldo suficiente para concluir este pedido.");
    }

    this.setState({
      isLoading: true
    });

    try {
      const res = await new ContaVirtualRemote().fazerPedido();

      if (res.success) {
        this.props.clearPedido();

        Alert.alert("Aviso", "Pedido Feito com sucesso!", [
          {
            text: "Ok",
            onPress: () => {
              this.props.navigation.navigate(
                "EstabelecimentoControleCredito"
              );
            }
          }
        ]);
      } else {
        Alert.alert("Aviso", "Houve um problema ao realizar o pedido.");
      }
    } catch (e) {
      Alert.alert("Aviso", "Ops. Houve um problema");
    } finally {
      this.setState({
        isLoading: false
      });
    }
  };

  render() {
    return (
      <ContainerPage>
        <View>
          <HeaderEmpresa navigation={this.props.navigation} />
        </View>

        <HeaderResumo navigation={this.props.navigation} />

        <ProdutosBody
          onRef={ref => (this.refProdutos = ref)}
          navigation={this.props.navigation}
        />

        <BottomTotal
          isLoading={this.state.isLoading}
          navigation={this.props.navigation}
          onEnviarPedido={() => this.onEnviarPedido()}
        />
      </ContainerPage>
    );
  }
}
export default withNavigationFocus(
  EstabelecimentoCreditoResumoPedidoScreen
);
