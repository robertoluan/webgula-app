import React from "react";
import { ListItem, Thumbnail, Left, Body } from "native-base";
import styled from "styled-components/native";
import { TouchableOpacity, Text } from "react-native";
import TextCustom from "../../../shared/components/TextCustom";

const image = require("../../../../assets/images/noimage.png");

export default class Cardapioitem extends React.Component {
  render() {
    let { item, onPress } = this.props;

    const stylesImg = {
      style: {
        // borderColor: "#efefef",
        // borderWidth: 4
      }
    };

    return (
      <ViewStyle>
        <TouchableOpacity>
          <ListItem noBorder>
            <TextCustom
              style={{
                fontSize: 16,
                fontFamily: "Montserrat-Bold"
              }}
            >
              {item.nome}
            </TextCustom>
          </ListItem>

          {item.items.map((grupo, key) => (
            <ListItem
              thumbnail
              noBorder
              key={key}
              style={{ marginBottom: 4 }}
              onPress={() => onPress(grupo)}
            >
              <Left>
                {!!grupo.imagemUrl ? (
                  <Thumbnail
                    borderRadius={5}
                    resizeMode="contain"
                    {...stylesImg}
                    source={{ uri: grupo.imagemUrl }}
                  />
                ) : (
                  <Thumbnail
                    borderRadius={5}
                    resizeMode="contain"
                    {...stylesImg}
                    source={image}
                  />
                )}
              </Left>

              <Body>
                <TextCustom
                  style={{
                    fontSize: 14,
                    // fontStyle: "italic",
                    paddingTop: 2,
                    color: "#333"
                  }}
                >
                  {grupo.nome}
                </TextCustom>

                <Text
                  note
                  style={{
                    color: "#bbb",
                    fontSize: 12,
                    marginBottom: 0
                  }}
                >
                  {grupo.total} produto(s)
                </Text>
              </Body>
            </ListItem>
          ))}
        </TouchableOpacity>
      </ViewStyle>
    );
  }
}

const ViewStyle = styled.View`
  padding: 8px 0;
`;
