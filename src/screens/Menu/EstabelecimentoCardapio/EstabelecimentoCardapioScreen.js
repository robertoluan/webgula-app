import React from "react";
import ProdutosBody from "../../Delivery/Produtos/ProdutosBody";

export default class EstabelecimentoCardapioScreen extends React.PureComponent {
  static navigationOptions = {
    title: "Cardápio"
  };

  render = () => {
    return <ProdutosBody isMenu={true} {...this.props} />;
  };
}
