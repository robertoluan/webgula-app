import { connect } from 'react-redux';
import component from './EstabelecimentoCardapioScreen'; 
import EmpresaApi from '../../../services/remote/empresa.remote';


const empresaApi = new EmpresaApi;

const mapStateToProps = (state, props) => {
    return {
    }
}

const mapDispatchToProps = (dispatch, props) => ({

    async load(grupoPaiId = null) {
        return empresaApi.findGrupos(grupoPaiId);
    },
    async loadCardapio() {
        return empresaApi.findCardapio();
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(component);
