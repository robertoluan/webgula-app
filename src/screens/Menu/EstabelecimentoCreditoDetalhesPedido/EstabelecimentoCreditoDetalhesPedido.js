import React from 'react';
import { Body, Right, ListItem, Col, Row } from 'native-base';
import HeaderEmpresa from '../../../shared/components/HeaderEmpresa';
import {
	StyleSheet,
	FlatList,
	ScrollView,
	Alert,
	View,
	ActivityIndicator
} from 'react-native';
import { colors } from '../../../theme';
import { formatMoney } from '../../../infra/helpers';
import ButtonLoading from '../../../shared/components/ButtonLoading';
import moment from 'moment';
import TextCustom from '../../../shared/components/TextCustom';
import ContainerPage from '../../../shared/components/ContainerPage';
import ViewBottomTotal from './components/ViewBottomTotal';
import ContaVirtualRemote from '../../../services/remote/contavirtual.remote';
import QRCode from 'react-native-qrcode';

const contaVirtualRemote = new ContaVirtualRemote();

class EstabelecimentoCreditoDetalhesPedido extends React.PureComponent {
	state = {
		isLoading: false,
		pedido: {},
		items: []
	};

	static navigationOptions = ({ navigation }) => {
		return {
			title: 'Pedido: ' + navigation.getParam('item').id
		};
	};

	componentDidMount = async () => {
		const item = this.props.navigation.getParam('item');

		this.setState({
			isLoading: true
		});

		try {
			const pedido = await contaVirtualRemote.getPedidoDetalhado(item.id);

			this.setState(
				{
					pedido
				},
				() => {
					this.updateData();
				}
			);
		} catch (e) {
			Alert.alert('Aviso', 'Não houve resultado!');
		} finally {
			this.setState({ isLoading: false });
		}
	};

	updateData = () => {
		const { pedido } = this.state;

		if (!pedido.id) return [];

		const items = [
			{
				id: 1,
				nome: 'Produtos',
				valor: pedido.valorProduto
			},
			{
				id: 2,
				nome: 'Taxa de Serviço',
				valor: pedido.valorTaxaServico || 0
			},
			{
				id: 4,
				nome: 'Desconto',
				valor: pedido.valorDesconto || 0
			},
			{
				id: 4,
				nome: 'Total',
				valor: pedido.valorTotal
			}
		];

		this.setState({
			items: [...items]
		});
	};

	getMesa = () => {
		const { pedido = {} } = this.state;

		if (pedido.id) {
			return pedido.mesaNumero || '--';
		}

		return '--';
	};

	navigateToProdutos = () => {
		this.props.navigation.navigate(
			'EstabelecimentoCreditoDetalhesPedidoProdutos',
			{
				item: this.state.pedido
			}
		);
	};

	render = () => {
		const { pedido = {}, items, isLoading } = this.state;

		return (
			<ContainerPage style={[styles.wrapper]}>
				<HeaderEmpresa navigation={this.props.navigation} />

				{isLoading ? <ActivityIndicator /> : null}

				{pedido && pedido.chave ? (
					<View
						style={{
							justifyContent: 'center',
							flexDirection: 'row',
							height: 130,
							top: -10,
							marginTop: 0,
							marginBottom: 10
						}}
					>
						<QRCode
							value={pedido.chave}
							size={130}
							bgColor="black"
							fgColor="white"
						/>
					</View>
				) : null}

				<View style={{ flex: 1, paddingBottom: 60 }}>
					<ScrollView>
						<View
							style={{
								borderBottomColor: '#eee',
								borderBottomWidth: 1,
								top: -10,
								paddingTop: 8,
								paddingBottom: 8,
								paddingLeft: 8,
								paddingRight: 8
							}}
						>
							<View style={styles.row}>
								<View
									style={{
										flex: 1
									}}
								>
									<TextCustom style={styles.labelTop}> Situação</TextCustom>
									<TextCustom
										style={[styles.labelBottom, { color: colors.primaryColor }]}
									>
										{pedido.situacaoNome || '--'}
									</TextCustom>
								</View>

								<View
									style={{
										flex: 1
									}}
								>
									<TextCustom style={styles.labelTop}> Data</TextCustom>
									<TextCustom style={styles.labelBottom}>
										{pedido.dataHoraPedido
											? moment(pedido.dataHoraPedido).format('DD/MM/YYYY HH:mm')
											: '--'}
									</TextCustom>
								</View>
							</View>
						</View>

						<View>
							<TextCustom style={styles.title}>Valores</TextCustom>

							<FlatList
								data={items}
								extraData={[]}
								renderItem={({ item }) => (
									<ListItem
										noBorder
										noIndent
										style={{
											backgroundColor: 'transparent'
										}}
									>
										<Body>
											<TextCustom
												style={{
													fontSize: 14,
													color: '#333'
												}}
											>
												{item.nome}
											</TextCustom>
										</Body>

										<Right>
											<TextCustom
												style={{
													fontSize: 14,
													fontFamily: 'Montserrat-Bold'
												}}
											>
												{formatMoney(item.valor)}
											</TextCustom>
										</Right>
									</ListItem>
								)}
							/>

							<ButtonLoading
								title="Ver Produtos"
								onPress={() => this.navigateToProdutos()}
							/>
						</View>
					</ScrollView>
				</View>

				<ViewBottomTotal navigation={this.props.navigation} pedido={pedido} />
			</ContainerPage>
		);
	};
}

export default EstabelecimentoCreditoDetalhesPedido;

const styles = StyleSheet.create({
	wrapper: {
		backgroundColor: '#fff'
	},
	row: {
		flexDirection: 'row'
	},
	title: {
		fontFamily: 'Montserrat-Bold',
		fontSize: 18,
		paddingLeft: 16,
		paddingTop: 0,
		paddingRight: 16,
		marginBottom: 6
	},
	labelTop: {
		fontSize: 12,
		color: '#bbb',
		paddingTop: 8
	},
	labelBottom: {
		fontSize: 14,
		paddingLeft: 3,
		paddingTop: 0
	}
});
