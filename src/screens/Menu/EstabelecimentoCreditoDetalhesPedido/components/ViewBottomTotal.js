import React from "react";
import { Col, Row } from "native-base";
import { colors } from "../../../../theme";
import { formatMoney } from "../../../../infra/helpers";
import TextCustom from "../../../../shared/components/TextCustom";
import ViewBottom from "../../../../shared/components/ViewBottom";

export default function ViewBottomTotal({ pedido = {} }) {
  return (
    <ViewBottom
      style={{
        margin: 0,
        paddingLeft: 16,
        paddingRight: 16,
        paddingTop: 8,
        paddingBottom: 8,
        backgroundColor: "#efefef"
      }}
    >
      <Row>
        <Col size={100}>
          <TextCustom
            style={{
              fontFamily: "Montserrat-Bold",
              fontSize: 20,
              textAlign: "right",
              paddingRight: 8,
              color: colors.primaryColor
            }}
          >
            R$ {formatMoney(pedido.valorTotal || 0)}
          </TextCustom>
        </Col>
      </Row>
    </ViewBottom>
  );
}
