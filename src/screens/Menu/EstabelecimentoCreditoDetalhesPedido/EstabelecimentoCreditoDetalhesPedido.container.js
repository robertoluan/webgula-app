import { connect } from "react-redux";
import component from "./EstabelecimentoCreditoDetalhesPedido";
import EmpresaApi from "../../../services/remote/empresa.remote";
import FidelidadeApi from "../../../services/remote/fidelidade.remote";

const empresaApi = new EmpresaApi();
const fidelidadeApi = new FidelidadeApi();

const mapStateToProps = (state, props) => {
  return {};
};

const mapDispatchToProps = (dispatch, props) => ({
  async loadVenda(saidaId) {
    return empresaApi.findVenda(saidaId);
  },
  async resgatarPontos(saidaId) {
    return fidelidadeApi.resgatarPontosSaida(saidaId);
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(component);
