import React from "react";
import { ListItem, Left, Text, Body } from "native-base";
import { StyleSheet } from "react-native";
import TextCustom from "../../shared/components/TextCustom";
import Icon from "../../shared/components/Icon";
 

class EnderecoItem extends React.PureComponent {
  render() {
    const { item, onPress } = this.props;
    return (
      <ListItem
        style={{
          paddingBottom: 8,
          backgroundColor: "white"
        }}
        noIndent={true}
        onPress={() => onPress()}
        avatar
      >
        <Left>
          <Icon size={22} name="fa-compass" />
        </Left>

        <Body>
          <Text>
            <TextCustom>{item.nome}</TextCustom>
          </Text>

          <Text note>
            <TextCustom>
              {item.logradouro},{item.nomeBairro},{item.nomeCidade} -{" "}
              {item.nomeEstado}
            </TextCustom>
          </Text>
        </Body>
      </ListItem>
    );
  }
}

const styles = StyleSheet.create({
  containerView: {
    flex: 1,
    marginTop: 16,
    marginRight: 16,
    marginLeft: 16
  },

  viewEmpty: {
    marginTop: 25,
    marginBottom: 25,
    textAlign: "center",
    alignSelf: "center",
    justifyContent: "center",
    paddingTop: 70
  },
  textEmpty: {
    fontSize: 24,
    flex: 1,
    marginTop: -30,
    paddingRight: 32,
    paddingLeft: 32,
    textAlign: "center",
    opacity: 0.6
  },
  icon: {
    flex: 1,
    textAlign: "center",
    alignSelf: "center",
    fontSize: 60,
    opacity: 0.4,
    marginBottom: 0
  }
});

export default EnderecoItem;
