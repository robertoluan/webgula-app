import EnderecoMenuScreen from "./EnderecoMenuScreen";
import { connect } from "react-redux";
import { setEnderecoMenu } from "../../../services/actions/app/auth.actions";
import EnderecoRemote from "../../../services/remote/estado.remote";

const ApiEstado = new EnderecoRemote();

const mapStateProps = (state, props) => {
  let app = state.app || {};

  const enderecoMenu = state.app.endereco.menu || {};

  return {
    enderecoMenu,
    auth: (app.auth && app.auth.userInfo) || {}
  };
};

const mapDispatchToProps = (dispatch, props) => ({
  findEstado() {
    return ApiEstado.findAll();
  },
  async findCidade(uf) {
    return await ApiEstado.findCidade(uf);
  },
  async setEnderecoMenu(endereco) {
    return dispatch(setEnderecoMenu(endereco));
  }
});

export default connect(
  mapStateProps,
  mapDispatchToProps
)(EnderecoMenuScreen);
