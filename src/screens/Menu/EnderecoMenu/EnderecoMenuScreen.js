import React from "react";
import { Container } from "native-base";
import {
  View,
  StyleSheet,
  ActivityIndicator,
  Alert,
  Platform
} from "react-native";
import TextCustom from "../../../shared/components/TextCustom";
import { colors } from "../../../theme";
import ButtonLoading from "../../../shared/components/ButtonLoading";
import ViewBottom from "../../../shared/components/ViewBottom";
import SelectPicker from "react-native-picker-select";
import InputEstado from "../../../shared/components/Input/InputEstado";
import InputCidade from "../../../shared/components/Input/InputCidade";

class EnderecoScreen extends React.PureComponent {
  state = {
    estados: [],
    cidades: [],
    endereco: {},
    visibleEstado: false,
    visibleCidade: false,
    isLoading: false
  };

  static navigationOptions = {
    title: "Endereço",
    headerRight: null
  };

  constructor(props) {
    super(props);

    this.onChangeEstado = this.onChangeEstado.bind(this);
    this.onChangeCidade = this.onChangeCidade.bind(this);
  }

  async componentDidMount() {
    try {
      let estados = this.props.findEstado();
      let { enderecoMenu } = this.props;

      let cidades = [];

      if (enderecoMenu.estadoId) {
        cidades = await this.getCidades(enderecoMenu.estadoId);
      }

      this.setState({
        endereco: {
          cidadeId: enderecoMenu.cidadeId,
          estadoId: enderecoMenu.estadoId
        },
        estados: [...estados],
        cidades: [...cidades]
      });
    } catch (e) {
      console.log("error", e);
    }
  }

  async getCidades(estadoId) {
    return await this.props.findCidade(estadoId);
  }

  async onChangeEstado(item) {
    if (!item) return;

    let endereco = this.state.endereco;

    endereco.estadoId = item;

    this.setState({ isLoading: true, endereco: { ...endereco } });

    try {
      const cidades = await this.getCidades(endereco.estadoId);

      this.setState({
        cidades: [...cidades]
      });
    } catch (e) {
      Alert.alert("Aviso", "Houve um problema ao carregar as cidades!");
    } finally {
      this.setState({ isLoading: false });
    }
  }

  async onChangeCidade(item) {
    const endereco = this.state.endereco;

    endereco.cidadeId = item;

    this.setState({
      endereco: {
        ...endereco
      }
    });
  }

  async onSalvar() {
    if (!this.state.endereco.estadoId) {
      return Alert.alert("Aviso", "Selecione um estado antes de continuar!");
    }

    if (!this.state.endereco.cidadeId) {
      return Alert.alert("Aviso", "Selecione uma cidade antes de continuar!");
    }

    await this.props.setEnderecoMenu(this.state.endereco);

    this.props.navigation.replace("Estabelecimentos");
  }

  render() {
    const { endereco, cidades, estados, isLoading } = this.state;

    return (
      <Container>
        {isLoading ? <ActivityIndicator size="large" /> : null}

        <View style={styles.containerView}>
          <TextCustom>Estado: </TextCustom>

          <View style={styles.selectDropdown}>
            {Platform.OS === "ios" ? (
              <SelectPicker
                doneText="Ok"
                placeholder="Selecione um estado"
                value={endereco.estadoId}
                onValueChange={value => this.onChangeEstado(value)}
                items={estados.map(item => ({
                  label: item.nome,
                  value: item.id
                }))}
              />
            ) : (
              <InputEstado
                keyValue="value"
                value={endereco.estadoId}
                onChange={value => this.onChangeEstado(value)}
                items={estados.map(item => ({
                  text: item.nome,
                  value: item.id
                }))}
              />
            )}
          </View>

          <TextCustom>Cidade:</TextCustom>
          <View style={styles.selectDropdown}>
            {Platform.OS === "ios" ? (
              <SelectPicker
                doneText="Ok"
                placeholder="Selecione uma cidade"
                value={endereco.cidadeId}
                disabled={!this.state.endereco.estadoId}
                onValueChange={value => this.onChangeCidade(value)}
                items={cidades.map(item => ({
                  label: item.nome,
                  value: item.id
                }))}
              />
            ) : (
              <InputCidade
                keyValue="value"
                value={endereco.cidadeId}
                onChange={value => this.onChangeCidade(value)}
                items={cidades.map(item => ({
                  text: item.nome,
                  value: item.id
                }))}
              />
            )}
          </View>
        </View>

        <ViewBottom>
          <ButtonLoading onPress={() => this.onSalvar()} title="Continuar" />
        </ViewBottom>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  containerView: {
    flex: 1,
    marginTop: 16,
    marginRight: 16,
    marginLeft: 16
  },

  selectDropdown: {
    borderBottomWidth: 2,
    borderBottomColor: colors.grayBackground,
    height: 40,
    marginBottom: 20,
    paddingTop: Platform.OS == "ios" ? 10 : 0,
    paddingLeft: 6,
    borderRadius: 5
  }
});

export default EnderecoScreen;
