import React from 'react';
import HeaderEmpresa from '../../../shared/components/HeaderEmpresa';
import {
  StyleSheet,
  ScrollView,
  Alert,
  RefreshControl,
  View
} from 'react-native';
import ContainerPage from '../../../shared/components/ContainerPage';
import ViewBottomTotal from './components/ViewBottomTotal';
import CabecalhoPessoa from './components/CabecalhoPessoa';
import ValoresRateio from './components/ValoresRateio';
import EmpresaRemote from '../../../services/remote/empresa.remote';
import AsyncStorageRemote from '../../../services/remote/asyncStorage.remote';
import Modal from './components/Modal';
import CouvertRateio from './components/CouvertRateio';
import ButtonLoading from '../../../shared/components/ButtonLoading';
import { colors } from '../../../theme';
import ButtonRatear from './components/ButtonRatear';
import { parseDoubleCustom, formatMoney } from '../../../infra/helpers';

const asyncStorage = new AsyncStorageRemote();
const empresaRemote = new EmpresaRemote();

class EstabelecimentoAcompanheContaVendaRateioNovoScreen extends React.PureComponent {
  state = {
    isLoading: false,
    openModal: false,
    form: {
      itemsRateados: []
    },
    usuariosRateio: [],
    usuarios: [],
    venda: {}
  };

  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Novo Rateio da Conta: ' + navigation.getParam('saida').saidaId
    };
  };

  componentDidMount = async () => {
    const venda = { ...this.props.navigation.getParam('venda') };
    const saida = { ...this.props.navigation.getParam('saida') };

    const usuarioParametro = this.props.navigation.getParam('rateio');

    const usuariosRateio =
      (await asyncStorage.get('usuariosRateio' + saida.saidaId)) || [];

    if (usuarioParametro) {
      const user = usuariosRateio.filter(
        item => item.id === usuarioParametro.id
      )[0];

      if (user) {
        venda.saidaItem.forEach(item => {
          item.qtdeUsuario = 0;

          user.itemsRateados.forEach(item2 => {
            if (item.id == item2.id) {
              item.qtdeUsuario = parseDoubleCustom(item2.qtdeUsuario);
            }
          });
        });

        this.onChange('usuario', usuarioParametro.usuario.usuarioACCId, false);
        this.onChange('qtdeCouvert', user.qtdeCouvert);
        this.onChange('itemsRateados', user.itemsRateados);
        this.onChange('id', usuarioParametro.id);
      }
    }

    try {
      const usuarios = await empresaRemote.findUsuariosVenda(venda.id);

      this.setState({
        usuarios,
        venda: { ...venda },
        usuariosRateio: usuariosRateio.filter(item => !item.isAvulso)
      });
    } catch (e) {
      Alert.alert('Aviso', 'Houve um problema ao carregar a informação!');
    } finally {
      this.setState({ isLoading: false });
    }
  };

  removerDoRateio = () => {
    Alert.alert('Aviso', 'Deseja realmente remover do rateio?', [
      {
        text: 'Remover',
        onPress: async () => {
          const saida = this.props.navigation.getParam('saida');

          let usuariosRateio =
            (await asyncStorage.get('usuariosRateio' + saida.saidaId)) || [];

          usuariosRateio = usuariosRateio.filter(
            x => x.id !== this.state.form.id
          );

          await asyncStorage.save(
            'usuariosRateio' + saida.saidaId,
            usuariosRateio
          );

          this.props.navigation.navigate(
            'EstabelecimentoAcompanheContaVendaRateio'
          );
        }
      },
      {
        text: 'Cancelar'
      }
    ]);
  };

  confirmarAdicaoRateio = async () => {
    const { venda } = this.state;

    const valorVendaAtual = await this.calcularValorTotalDaVendaAtual();

    if (venda.valorTotal + valorVendaAtual > valorVendaAtual) {
      return Alert.alert(
        'Aviso',
        `O Valor do rateio ultrapassou o valor restante da venda que é R$ ${formatMoney(
          venda.valorTotal - valorVendaAtual
        )}. Por favor remova algum item da sua cota.`
      );
    }

    if (!this.state.form.usuario) {
      return Alert.alert('Aviso', 'Selecione uma pessoa para o rateio!');
    }

    const { navigation } = this.props;

    const saida = navigation.getParam('saida');

    const { form, usuarios } = this.state;

    const usuariosRateio =
      (await asyncStorage.get('usuariosRateio' + saida.saidaId)) || [];

    const valorServicosPago = this.calcularValorServicoPago();
    const valorTotalCouvert = this.calcularCouvertPago();
    const valorTotalProdutos = this.calcularValorProdutosPago();

    const usuario = usuarios.filter(x => x.usuarioACCId === form.usuario)[0];

    // Calcula o Total
    let valorTotalRateio = 0;

    valorTotalRateio += valorTotalCouvert;
    valorTotalRateio += valorServicosPago;
    valorTotalRateio += valorTotalProdutos;

    if (!form.id) {
      usuariosRateio.push({
        id: +new Date(),
        usuario: usuario,
        valor: parseDoubleCustom(valorTotalRateio),
        valorTotalCouvert: parseDoubleCustom(valorTotalCouvert),
        qtdeCouvert: form.qtdeCouvert,
        itemsRateados: form.itemsRateados
      });
    } else {
      usuariosRateio.forEach(x => {
        if (x.id === form.id) {
          x.qtdeCouvert = form.qtdeCouvert;
          x.usuario = usuario;
          x.valor = parseDoubleCustom(valorTotalRateio);
          x.itemsRateados = form.itemsRateados;
          x.valorTotalCouvert = parseDoubleCustom(valorTotalCouvert);
        }
      });
    }

    await asyncStorage.save('usuariosRateio' + saida.saidaId, usuariosRateio);

    this.props.navigation.navigate('EstabelecimentoAcompanheContaVendaRateio');
  };

  onChange = (prop, value) => {
    let { form, venda } = this.state;

    this.setState({
      venda: { ...venda },
      form: {
        ...form,
        [prop]: value
      }
    });
  };

  onOk = items => {
    const itemsRateados = items;

    /// qtdeUsuario
    this.setState({
      openModal: false,
      form: {
        ...this.state.form,
        itemsRateados
      }
    });
  };

  calcularQuantidadeCouvertJaRateada() {
    const { usuariosRateio } = this.state;

    let quantidadeCouvertJaRateada = 0;

    usuariosRateio.forEach(usuarioRateio => {
      quantidadeCouvertJaRateada += usuarioRateio.qtdeCouvert;
    });

    return quantidadeCouvertJaRateada;
  }

  calcularValorTotalDaVendaAtual = async () => {
    const { form } = this.state;
    const saida = this.props.navigation.getParam('saida');

    const usuariosRateio =
      (await asyncStorage.get('usuariosRateio' + saida.saidaId)) || [];

    let valorJaRateado = 0;

    usuariosRateio
      .filter(x => x.id !== form.id)
      .forEach(item => {
        valorJaRateado += item.valor;
      });

    valorJaRateado += this.calcularCouvertPago();
    valorJaRateado += this.calcularValorProdutosPago();
    valorJaRateado += this.calcularValorServicoPago();

    return valorJaRateado;
  };

  /*
   * Base de calculo
   *
   * @valorComissaoRateio = (valorComissao / valorTotal) * ( quantidade * valorUnitario )
   */
  calcularValorServicoPago() {
    const { form } = this.state;

    let valorServicosPago = 0;

    form.itemsRateados.forEach(item => {
      valorServicosPago += parseDoubleCustom(
        (item.valorComissao / item.valorTotal) *
          (item.qtdeUsuario * item.valorUnitario)
      );
    });

    return valorServicosPago;
  }

  calcularCouvertPago() {
    const { venda, form } = this.state;

    return venda.valorIngressoUnt * (form.qtdeCouvert || 0);
  }

  /*
   * Base de calculo
   *
   * @valorComissaoRateio =  ( quantidade * valorUnitario )
   */
  calcularValorProdutosPago = () => {
    const { form } = this.state;

    let valorProdutosPago = 0;

    form.itemsRateados.forEach(item => {
      valorProdutosPago += item.valorUnitario * item.qtdeUsuario;
    });

    return valorProdutosPago;
  };

  render = () => {
    const {
      usuarios,
      form,
      venda,
      usuariosRateio,
      isLoading,
      openModal
    } = this.state;
    const { navigation } = this.props;

    let valorTotalRateio = 0;

    let valorProdutosPago = this.calcularValorProdutosPago();
    let valorCouvertPago = this.calcularCouvertPago();
    let valorServicosPago = this.calcularValorServicoPago();

    valorTotalRateio += valorProdutosPago;
    valorTotalRateio += valorCouvertPago;
    valorTotalRateio += valorServicosPago;

    let valorTotalCouvertRateadoSemConsiderarAtual = 0;

    usuariosRateio
      .filter(item => item.id !== form.id)
      .forEach(item => {
        valorTotalCouvertRateadoSemConsiderarAtual += item.valorTotalCouvert;
      });

    let quantidadeUsuariosRestanteCouvert =
      (venda.valorIngressoTotal - valorTotalCouvertRateadoSemConsiderarAtual) /
      venda.valorIngressoUnt;

    console.log('usuariosRateio', usuariosRateio);
    console.log(
      'valorTotalCouvertRateadoSemConsiderarAtual',
      valorTotalCouvertRateadoSemConsiderarAtual
    );
    console.log(
      'quantidadeUsuariosRestanteCouvert',
      quantidadeUsuariosRestanteCouvert
    );

    const isFormEdit = !!form.id;

    // TRATA ERRO QUANDO FOR MENOR QUE ZERO TODO BUG
    quantidadeUsuariosRestanteCouvert =
      !quantidadeUsuariosRestanteCouvert ||
      quantidadeUsuariosRestanteCouvert < 0
        ? 0
        : quantidadeUsuariosRestanteCouvert;

    return (
      <ContainerPage style={[styles.wrapper]}>
        <HeaderEmpresa navigation={navigation} />

        <View style={{ flex: 1, paddingBottom: 60 }}>
          <ScrollView
            refreshControl={
              <RefreshControl
                refreshing={isLoading}
                onRefresh={() => this.componentDidMount()}
              />
            }
          >
            <CabecalhoPessoa
              onChange={value => this.onChange('usuario', value)}
              usuario={form.usuario}
              usuarios={usuarios}
            />

            <ButtonRatear
              onPress={() =>
                this.setState({
                  openModal: true
                })
              }
            />

            {openModal ? (
              <Modal
                open={openModal}
                items={venda.saidaItem}
                onCancel={() => this.setState({ openModal: false })}
                onOk={items => this.onOk(items)}
              />
            ) : null}

            {venda.valorIngressoTotal ? (
              <CouvertRateio
                valorUnidadeCouvert={venda.valorIngressoUnt}
                qtde={form.qtdeCouvert}
                quantidadeUsuariosRestante={quantidadeUsuariosRestanteCouvert}
                onChange={value => {
                  this.onChange('qtdeCouvert', value);
                }}
              />
            ) : null}

            <ValoresRateio
              valorProdutosPago={valorProdutosPago}
              valorServicosPago={valorServicosPago}
              valorCouvertPago={valorCouvertPago}
              venda={venda}
            />

            <View style={{ marginHorizontal: 8, marginBottom: 16 }}>
              <ButtonLoading
                onPress={() => this.confirmarAdicaoRateio()}
                title="Confirmar Rateio"
              />
            </View>

            {isFormEdit ? (
              <View style={{ marginHorizontal: 8, marginBottom: 16 }}>
                <ButtonLoading
                  onPress={() => this.removerDoRateio()}
                  backgroundColor={'transparent'}
                  titleColor={colors.yellowButton}
                  title="Remover do Rateio"
                />
              </View>
            ) : null}
          </ScrollView>
        </View>

        <ViewBottomTotal
          venda={venda}
          usuario={form.usuario}
          valorRateio={valorTotalRateio}
        />
      </ContainerPage>
    );
  };
}

export default EstabelecimentoAcompanheContaVendaRateioNovoScreen;

const styles = StyleSheet.create({
  wrapper: {
    backgroundColor: '#fff'
  },
  title: {
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
    paddingLeft: 16,
    paddingTop: 0,
    paddingRight: 16,
    marginBottom: 6
  },
  labelTop: {
    fontSize: 12,
    paddingTop: 8,
    // color: colors.grayColor,
    paddingLeft: 0
  },
  labelBottom: {
    fontSize: 14,
    paddingTop: 0,
    paddingLeft: 2
  }
});
