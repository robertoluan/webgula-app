import React from 'react';
import { Col, Row } from 'native-base';
import { StyleSheet, View } from 'react-native';
import { formatMoney } from '../../../../infra/helpers';
import TextCustom from '../../../../shared/components/TextCustom';
import { colors } from '../../../../theme';

class ValoresRateio extends React.PureComponent {
  render = () => {
    const {
      venda,
      valorProdutosPago = 0,
      valorServicosPago = 0,
      valorCouvertPago = 0
    } = this.props;

    const valorProdutos = venda.valorProdutos;

    const valorCouvert = venda.valorIngressoTotal;

    return (
      <View style={{ flex: 1, marginBottom: 20 }}>
        <View
          style={{
            paddingTop: 8,
            paddingBottom: 8,
            paddingLeft: 8,
            paddingRight: 8
          }}
        >
          <Row style={{ marginVertical: 8 }}>
            <Col size={100}>
              <TextCustom style={styles.labelTop}> Produtos </TextCustom>
              <TextCustom style={styles.labelBottom}>
                R$ {formatMoney(valorProdutosPago)} / Restante: R${' '}
                {formatMoney(valorProdutos)} (
                {formatMoney((valorProdutosPago * 100) / valorProdutos, 2, '.')}
                %)
              </TextCustom>
            </Col>
          </Row>

          <Row style={{ marginVertical: 8 }}>
            <Col size={100}>
              <TextCustom style={styles.labelTop}> Serviço/Gorjeta</TextCustom>
              <TextCustom style={styles.labelBottom}>
                R$ {formatMoney(valorServicosPago)}
              </TextCustom>
            </Col>
          </Row>

          <Row style={{ marginVertical: 8 }}>
            <Col size={100}>
              <TextCustom style={styles.labelTop}> Couvert</TextCustom>
              <TextCustom style={styles.labelBottom}>
                R$ {formatMoney(valorCouvertPago)} / Restante: R${' '}
                {formatMoney(valorCouvert)} (
                {formatMoney((valorCouvertPago * 100) / valorCouvert, 2, '.')}
                %)
              </TextCustom>
            </Col>
          </Row>
        </View>
      </View>
    );
  };
}

export default ValoresRateio;

const styles = StyleSheet.create({
  title: {
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
    paddingLeft: 16,
    paddingTop: 0,
    paddingRight: 16,
    marginBottom: 6
  },
  labelTop: {
    fontSize: 12,
    paddingTop: 8,
    color: colors.grayColor,
    paddingLeft: 0
  },
  labelBottom: {
    fontSize: 14,
    paddingTop: 0,
    paddingLeft: 4
  },
  bigText: {
    fontSize: 18,
    fontWeight: 'bold',
    paddingTop: 0,
    color: 'green',
    paddingLeft: 2
  }
});
