import React from 'react';
import { StyleSheet, FlatList, View } from 'react-native';
import { formatMoney } from '../../../../infra/helpers';
import TextCustom from '../../../../shared/components/TextCustom';
import { colors } from '../../../../theme';
import { Body, Right, ListItem, Left } from 'native-base';
import Icon from '../../../../shared/components/Icon';

class ListaRateados extends React.PureComponent {
	render = () => {
		const items = [
			{
				nome: 'João Silva',
				valor: 40
			},
			{
				nome: 'Roberto da Cruz',
				valor: 20
			}
		];

		return (
			<View>
				<TextCustom style={styles.title}>Rateados</TextCustom>

				<FlatList
					data={items}
					extraData={[]}
					renderItem={({ item }) => (
						<ListItem
							style={{ backgroundColor: '#f7f7f7', marginBottom: 6 }}
							icon
							noBorder
							noIndent
						>
							<Left>
								<Icon
									size={20}
									color={colors.grayColor}
									name="ion-md-contact"
								/>
							</Left>
							<Body>
								<TextCustom
									style={{
										color: '#333'
									}}
								>
									{item.nome}
								</TextCustom>
							</Body>

							<Right>
								<TextCustom style={{ top: -8 }}>
									R$ {formatMoney(item.valor)}
								</TextCustom>
							</Right>
						</ListItem>
					)}
				/>
			</View>
		);
	};
}

export default ListaRateados;

const styles = StyleSheet.create({
	title: {
		fontFamily: 'Montserrat-Bold',
		fontSize: 18,
		paddingLeft: 16,
		paddingTop: 0,
		paddingRight: 16,
		marginBottom: 16
	}
});
