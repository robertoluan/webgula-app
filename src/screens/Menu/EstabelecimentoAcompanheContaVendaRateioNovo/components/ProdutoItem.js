import React from 'react';
import { ListItem, Right, Body } from 'native-base';
import { View } from 'react-native';
import TextCustom from '../../../../shared/components/TextCustom';
import RangeQuantidade from '../../../Delivery/ProdutosGruposItem/components/RangeQuantidade';
import { formatMoney, parseDoubleCustom } from '../../../../infra/helpers';

export default function ProdutoItem({ item, onChange }) {
  return (
    <View>
      <ListItem
        style={{
          marginLeft: 0,
          marginRight: 0,
          paddingRight: 0
        }}
      >
        <Body>
          <View style={{ flexDirection: 'column' }}>
            <View>
              <TextCustom
                style={{
                  fontSize: 14
                }}
              >
                {parseDoubleCustom(item.qtde, 2)} x {item.produtoNomeCompleto}
              </TextCustom>
            </View>

            <View style={{ flexDirection: 'row' }}>
              <TextCustom
                style={{
                  fontSize: 14,
                  marginTop: 5
                }}
              >
                R$ {formatMoney(parseDoubleCustom(item.valorTotal))}
              </TextCustom>

              <TextCustom
                style={{
                  fontSize: 14,
                  marginTop: 5,
                  marginLeft: 10,
                  marginRight: 10
                }}
              >
                /
              </TextCustom>

              <TextCustom
                style={{
                  fontSize: 14,
                  marginTop: 5,
                  marginLeft: 0,
                  fontFamily: 'Montserrat-Bold'
                }}
              >
                R${' '}
                {formatMoney(
                  parseDoubleCustom(item.valorUnitario) *
                    parseDoubleCustom(item.qtdeUsuario)
                )}
              </TextCustom>
            </View>
          </View>
        </Body>

        <Right>
          <View style={{ textAlign: 'right' }}>
            <RangeQuantidade
              min={0}
              step={0.25}
              max={item.qtde}
              textSize={12}
              value={item.qtdeUsuario || 0}
              onChange={value => onChange(value)}
            />
          </View>
        </Right>
      </ListItem>
    </View>
  );
}
