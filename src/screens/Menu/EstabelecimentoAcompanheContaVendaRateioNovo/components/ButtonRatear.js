import React from 'react';
import { View } from 'react-native';
import ButtonLoading from '../../../../shared/components/ButtonLoading';
import { colors } from '../../../../theme';

function ButtonRatear({ onPress }) {
  return (
    <View style={{ marginHorizontal: 8, marginBottom: 16 }}>
      <ButtonLoading
        onPress={onPress}
        backgroundColor={'transparent'}
        titleColor={colors.primaryColorDark}
        title="Ratear Produtos"
        style={{
          borderWidth: 1,
          borderColor: colors.primaryColorDark
        }}
      />
    </View>
  );
}

export default ButtonRatear;
