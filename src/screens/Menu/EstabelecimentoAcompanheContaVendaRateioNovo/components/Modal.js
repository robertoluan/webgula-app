import React from 'react';
import { View, StyleSheet, FlatList } from 'react-native';
import { colors } from '../../../../theme';
import ButtonLoading from '../../../../shared/components/ButtonLoading';
import Dialog from '../../../../shared/components/Dialog';
import { Content } from 'native-base';
import ProdutoItem from './ProdutoItem';

class Modal extends React.PureComponent {
  state = {
    items: []
  };

  componentDidMount = async () => {
    this.setState({ form: { ...this.props.form }, items: this.props.items });
  };

  onChangeQuantidade = (value, item) => {
    const { items } = this.state;

    items.forEach(itemFor => {
      if (itemFor.id === item.id) {
        itemFor.qtdeUsuario = value;
      }
    });

    this.setState({
      items: [...items]
    });
  };

  onSalvar = async () => {
    this.props.onOk(this.state.items.filter(item => item.qtdeUsuario));
  };

  render = () => {
    const { onCancel, onOk } = this.props;
    const { items } = this.state;

    return (
      <Dialog
        open={true}
        title={'Rateio'}
        onCancel={() => onCancel()}
        onOk={() => onOk()}
      >
        <Content
          style={{
            top: -30,
            borderBottomLeftRadius: 20,
            borderBottomRightRadius: 20
          }}
        >
          <View style={styles.bottom}>
            <View>
              <FlatList
                data={items}
                extraData={[]}
                renderItem={({ item }) => (
                  <ProdutoItem
                    onChange={value => this.onChangeQuantidade(value, item)}
                    item={item}
                  />
                )}
              />
            </View>

            <View style={{ height: 15 }} />
            <ButtonLoading onPress={() => this.onSalvar()} title="Adicionar" />

            <View style={{ height: 15 }} />
            <ButtonLoading
              onPress={() => onCancel()}
              backgroundColor={'white'}
              titleColor={colors.grayColor}
              title="Cancelar"
            />
          </View>
        </Content>
      </Dialog>
    );
  };
}

const styles = StyleSheet.create({
  bottom: {
    marginTop: 30
  }
});

export default Modal;
