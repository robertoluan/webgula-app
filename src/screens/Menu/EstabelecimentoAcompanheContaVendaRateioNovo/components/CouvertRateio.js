import React from 'react';
import { View } from 'react-native';
import TextCustom from '../../../../shared/components/TextCustom';
import RangeQuantidade from '../../../Delivery/ProdutosGruposItem/components/RangeQuantidade';
import { formatMoney, parseDoubleCustom } from '../../../../infra/helpers';
import { colors } from '../../../../theme';

export default class CouvertRateio extends React.PureComponent {
  render = () => {
    let {
      qtde,
      valorUnidadeCouvert,
      quantidadeUsuariosRestante,
      onChange
    } = this.props;

    return (
      <View
        style={{
          marginBottom: 0
        }}
      >
        <View
          noBorder
          style={{
            flexDirection: 'row',
            marginLeft: 8,
            marginRight: 8,
            padding: 8,
            paddingRight: 0,
            backgroundColor: '#f8f8f8'
          }}
        >
          <View style={{ marginRight: 48 }}>
            <TextCustom
              style={{
                color: colors.grayColor,
                marginBottom: 8,
                fontSize: 14
              }}
            >
              Couvert
            </TextCustom>
            <View style={{ flexDirection: 'row' }}>
              <RangeQuantidade
                min={0}
                max={quantidadeUsuariosRestante}
                textSize={14}
                value={qtde || 0}
                onChange={value => onChange(value)}
              />
              <TextCustom style={{ marginLeft: 10 }}>
                / {parseDoubleCustom(quantidadeUsuariosRestante - (qtde || 0))}{' '}
                (Restante)
              </TextCustom>
            </View>
          </View>

          <View style={{ flexDirection: 'column' }}>
            <TextCustom
              style={{
                color: colors.grayColor,
                fontSize: 14
              }}
            >
              Valor
            </TextCustom>

            <TextCustom
              style={{
                fontSize: 18,
                marginTop: 5
              }}
            >
              R$
              {formatMoney(valorUnidadeCouvert * qtde)}
            </TextCustom>
          </View>
        </View>
      </View>
    );
  };
}
