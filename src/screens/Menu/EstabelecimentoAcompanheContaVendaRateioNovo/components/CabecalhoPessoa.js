import React from 'react';
import { Col, Row } from 'native-base';
import { View } from 'react-native';
import Select from '../../../../shared/components/Input/Select';

class CabecalhoPessoa extends React.PureComponent {
  render = () => {
    const { usuarios, usuario, onChange } = this.props;

    return (
      <View style={{ flex: 1, marginBottom: 10 }}>
        <View
          style={{
            borderBottomColor: '#eee',
            borderBottomWidth: 1,
            top: -10,
            paddingTop: 8,
            paddingBottom: 8,
            paddingLeft: 8,
            paddingRight: 8
          }}
        >
          <Row>
            <Col size={50}>
              <Select
                label={'Selecione uma Pessoa'}
                placeholder={'--selecione--'}
                items={usuarios}
                keyText="nomeUsuario"
                keyValue="usuarioACCId"
                value={usuario}
                onChange={value => onChange(value)}
              />
            </Col>
          </Row>
        </View>
      </View>
    );
  };
}

export default CabecalhoPessoa;
