import React from 'react';
import { Col, Row } from 'native-base';
import { StyleSheet } from 'react-native';
import { colors } from '../../../../theme';
import { formatMoney } from '../../../../infra/helpers';
import TextCustom from '../../../../shared/components/TextCustom';
import ViewBottom from '../../../../shared/components/ViewBottom';

export default function ViewBottomTotal({ valorRateio }) {
  return (
    <ViewBottom
      style={{
        margin: 0,
        height: 70,
        paddingLeft: 16,
        paddingRight: 16,
        paddingTop: 8,
        paddingBottom: 8,
        backgroundColor: '#efefef'
      }}
    >
      <Row>
        <Col size={50}>
          <TextCustom style={styles.labelTop}> Total</TextCustom>
          <TextCustom style={styles.labelBottom}>
            R$ {formatMoney(valorRateio)}
          </TextCustom>
        </Col>
      </Row>
    </ViewBottom>
  );
}

const styles = StyleSheet.create({
  labelTop: {
    fontSize: 11,
    color: colors.grayColor
  },
  labelBottom: {
    fontSize: 18,
    paddingTop: 0,
    paddingLeft: 2
  }
});
