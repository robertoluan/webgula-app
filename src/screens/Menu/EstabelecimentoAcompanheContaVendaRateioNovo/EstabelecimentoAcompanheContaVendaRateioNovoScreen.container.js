import { connect } from 'react-redux';
import component from './EstabelecimentoAcompanheContaVendaRateioNovoScreen';

const mapStateToProps = (state, props) => {
	return {};
};

const mapDispatchToProps = (dispatch, props) => ({});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(component);
