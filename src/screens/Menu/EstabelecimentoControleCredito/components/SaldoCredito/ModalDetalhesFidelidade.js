import React from "react";
import { View, StyleSheet, Image } from "react-native";
import TextCustom from "../../../../../shared/components/TextCustom";
import { colors } from "../../../../../theme";
import ButtonLoading from "../../../../../shared/components/ButtonLoading";
import Dialog from "../../../../../shared/components/Dialog";
import FidelidadeRemote from "../../../../../services/remote/fidelidade.remote";
import { Content, Toast } from "native-base";

const fidelidadeRemote = new FidelidadeRemote();

class ModalDetalhesFidelidade extends React.PureComponent {
  state = {
    isLoading: false
  };

  onAceitar = async () => {
    const { onOk, fidelidade } = this.props;

    this.setState({
      isLoading: true
    });

    try {
      const res = await fidelidadeRemote.cadastrarContaFidelidade(
        fidelidade.id
      );

      if (res.type === "exception") {
        Toast.show({
          text: "Não foi possível aceitar a campanha " + fidelidade.nome
        });
      } else {
        Toast.show({
          text: "Parabéns! Você se fidelizou a " + fidelidade.nome
        });

        onOk();
      }
    } catch (e) {
      Toast.show({
        text: "Ops. Houve um problema ao realizar está operação!"
      });
    } finally {
      this.setState({
        isLoading: false
      });
    }
  };

  onFazerLogin = async () => {
    this.onCancel();

    this.props.navigation.navigate("Login");
  };

  displayTexto = texto => {
    let splites = texto.split("*");

    return (
      <View>
        {splites.map((item, index) => (
          <TextCustom>{item}</TextCustom>
        ))}
      </View>
    );
  };

  render = () => {
    const { isLoading } = this.state;
    const { open, onCancel, onOk, fidelidade, isAuth } = this.props;

    if (!fidelidade) return null;

    return (
      <Dialog
        title={"Fidelidade"}
        open={open}
        onCancel={() => onCancel()}
        onOk={() => onOk()}
      >
        <Content
          style={{ borderBottomLeftRadius: 20, borderBottomRightRadius: 20 }}
        >
          <View style={{ marginBottom: 16 }}>
            {fidelidade.imagemUrl ? (
              <Image
                style={{ flex: 1, width: null, height: 200 }}
                source={{ uri: fidelidade.imagemUrl }}
              />
            ) : null}
          </View>
          <View style={{ ...styles.containerView, marginBottom: 20 }}>
            {this.displayTexto(fidelidade.textoCliente)}
          </View>

          <View style={{ ...styles.containerView, marginBottom: 20 }}>
            {fidelidade.pontosAdesao ? (
              <TextCustom>
                Pontos para Adesão: {fidelidade.pontosAdesao} pontos
              </TextCustom>
            ) : null}
          </View>

          <View style={{ ...styles.containerView, marginBottom: 20 }}>
            <TextCustom>Serviço Gratuito</TextCustom>
          </View>

          <View style={styles.bottom}>
            {isAuth ? (
              <ButtonLoading
                loading={isLoading}
                onPress={() => this.onAceitar()}
                title="Aceitar"
              />
            ) : (
              <ButtonLoading
                onPress={() => this.onFazerLogin()}
                title="Fazer Login para continuar"
              />
            )}

            <View style={{ height: 10 }} />

            <ButtonLoading
              onPress={() => onCancel()}
              backgroundColor={"#efefef"}
              titleColor={colors.grayColor}
              title="Fechar"
            />
          </View>
        </Content>
      </Dialog>
    );
  };
}

const styles = StyleSheet.create({
  containerView: {
    marginTop: 16,
    flexWrap: "wrap",
    justifyContent: "center"
  },
  bottom: {
    marginTop: 16
  }
});

export default ModalDetalhesFidelidade;
