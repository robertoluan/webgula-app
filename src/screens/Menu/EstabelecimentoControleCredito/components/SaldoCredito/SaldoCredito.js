import React from "react";
import { View, TouchableOpacity } from "react-native";
import { colors } from "../../../../../theme";
import { formatMoney } from "../../../../../infra/helpers";
import TextCustom from "../../../../../shared/components/TextCustom";

export default function SaldoCredito({ pontos, onPress }) {
  return (
    <TouchableOpacity activeOpacity={0.6} onPress={onPress}>
      <View>
        <View style={{ flexDirection: "column" }}>
          <TextCustom
            style={{
              color: colors.grayColor,
              fontSize: 9,
              marginTop: 0
            }}
          >
            Fidelidade
          </TextCustom>

          <TextCustom
            style={{
              color: "#11b719",
              fontSize: 18,
              fontFamily: "Montserrat-Bold",
              marginTop: 0
            }}
          >
            {formatMoney(pontos, 0)} ponto(s)
          </TextCustom>
        </View>
      </View>
    </TouchableOpacity>
  );
}
