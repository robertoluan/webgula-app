import React from "react";
import { View } from "react-native";
import { ListItem, Left, Body } from "native-base";
import TextCustom from "../../../../shared/components/TextCustom";
import { formatMoney } from "../../../../infra/helpers";
import moment from "moment";
import { colors } from "../../../../theme";

export default class PedidoItem extends React.PureComponent {
  render() {
    let { item, onPress } = this.props;

    return (
      <ListItem
        style={{
          marginBottom: 10,
          padding: 8,
          paddingTop: 0,
          backgroundColor: "white"
        }}
        noIndent={true}
        avatar
        noBorder={true}
        itemDivider={false}
        onPress={onPress}
      >
        <Left>
          <View
            style={{
              flexDirection: "column",
              backgroundColor: "#f7f7f7",
              borderRadius: 4,
              paddingLeft: 4,
              paddingRight: 4,
              paddingTop: 8,
              paddingBottom: 8
            }}
          >
            <View>
              <TextCustom style={{ textAlign: "center", fontSize: 20 }}>
                {moment(item.dataHoraPedido).format("DD")}
              </TextCustom>
            </View>
            <View>
              <TextCustom style={{ textAlign: "center", fontSize: 11 }}>
                {moment(item.dataHoraPedido).format("MM/YYYY")}
              </TextCustom>
            </View>
          </View>
        </Left>

        <Body>
          <View style={{ flexDirection: "column" }}>
            <TextCustom style={{ fontSize: 13, color: "#333" }}>
              #{item.id} - {item.situacaoNome}
            </TextCustom>
            <TextCustom style={{ fontSize: 14, color: colors.primaryColor }}>
              R$ {formatMoney(item.valorTotal)}
            </TextCustom>
            <TextCustom style={{ fontSize: 10 }}>
              {item.tipoPedidoNome}
            </TextCustom>
          </View>
        </Body>
      </ListItem>
    );
  }
}
