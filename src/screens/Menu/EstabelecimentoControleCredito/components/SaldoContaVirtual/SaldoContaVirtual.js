import React from "react";
import { View, TouchableOpacity } from "react-native";
import { colors } from "../../../../../theme";
import { formatMoney } from "../../../../../infra/helpers";
import TextCustom from "../../../../../shared/components/TextCustom";

export default function SaldoContaVirtual({ onPress, empresaSelecionada }) {
  return (
    <TouchableOpacity activeOpacity={0.6} onPress={onPress}>
      <View>
        <View style={{ flexDirection: "column" }}>
          <TextCustom
            style={{
              color: colors.grayColor,
              fontSize: 9,
              marginTop: 0
            }}
          >
            Saldo conta virtual
          </TextCustom>

          <TextCustom
            style={{
              color: "#11b719",
              fontSize: 18,
              fontFamily: "Montserrat-Bold",
              marginTop: 0
            }}
          >
            <TextCustom style={{ fontSize: 10 }}> R$ </TextCustom>
            {formatMoney(empresaSelecionada.coinPessoaSaldo, 2)}
          </TextCustom>
        </View>
      </View>
    </TouchableOpacity>
  );
}
