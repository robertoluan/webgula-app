import { connect } from "react-redux";
import component from "./SaldoContaVirtual";

const mapStateToProps = (state, props) => {
  let app = state.app || {};

  const empresaSelecionada = (app.empresa || {}).empresaSelecionada || {};

  return {
    empresaSelecionada
  };
};

const mapDispatchToProps = (dispatch, props) => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(component);
