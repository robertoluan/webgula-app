import React from "react";
import { View, StyleSheet, ScrollView, Dimensions } from "react-native";
import TextCustom from "../../../../../shared/components/TextCustom";
import { colors } from "../../../../../theme";
import ButtonLoading from "../../../../../shared/components/ButtonLoading";
import Dialog from "../../../../../shared/components/Dialog";
import { Content, Toast } from "native-base";
import ContaVirtualRemote from "../../../../../services/remote/contavirtual.remote";

class ModalDetalhesContaVirtual extends React.PureComponent {
  state = {
    isLoading: false
  };

  onCriarConta = async () => {
    const { onOk, empresaSelecionada } = this.props;

    this.setState({
      isLoading: true
    });

    try {
      const res = await new ContaVirtualRemote().abrirContaVirtual(
        empresaSelecionada.coinAgenciaId
      );

      if (res.type === "exception") {
        Toast.show({
          text: "Não foi possível criar sua conta virtual."
        });
      } else {
        Toast.show({
          text: "Parabéns! Você criou sua conta virtual."
        });

        onOk();
      }
    } catch (e) {
      Toast.show({
        text: "Ops. Houve um problema ao realizar está operação!"
      });
    } finally {
      this.setState({
        isLoading: false
      });
    }
  };

  onFazerLogin = async () => {
    this.onCancel();

    this.props.navigation.navigate("Login");
  };

  render = () => {
    const { isLoading } = this.state;
    const { open, onCancel, onOk, empresaSelecionada } = this.props;

    return (
      <Dialog
        title={"Conta Virtual"}
        open={open}
        onCancel={() => onCancel()}
        onOk={() => onOk()}
      >
        <Content
          style={{
            borderBottomLeftRadius: 20,
            borderBottomRightRadius: 20
          }}
        >
          <View style={{ ...styles.containerView }}>
            <ScrollView>
              <TextCustom>TERMO DE USO: </TextCustom>
              <TextCustom>
                A Conta Corrente Virtual do WebGula foi criada com objetivo de
                facilitar o consumo entre o Usuário e o Estabelecimento que
                trabalha com produtos pré-pagos. O Funcionamento inicia com a
                abertura de uma conta virtual exclusiva para aquele determinado
                estabelecimento e finaliza a utilização do crédito através de
                consumos realizados dentro do mesmo estabelecimento. Todo
                Crédito comprado pelo usuário é feito através de uma negociação
                direta entre o Usuário e o Estabelecimento, portanto o saldo do
                Usuário estará sempre em posse do Estabelecimento em questão. O
                WebGula é responsável apenas por gerenciar o consumo desse saldo
                na medida que o usuário vai realizando suas compras através da
                modalidade pré-paga. As regras de consumo, qualidade dos
                produtos, tempo de produção, disponibilidade e até mesmo a
                possibilidade de devolução do crédito são responsabilidades
                exclusivas do Estabelecimento.
              </TextCustom>
            </ScrollView>
          </View>

          <View style={{ ...styles.containerView }}>
            <ScrollView>
              <TextCustom>{empresaSelecionada.coinAgenciaTexto}</TextCustom>
            </ScrollView>
          </View>

          <View style={{ marginTop: 8, marginBottom: 8 }}>
            <TextCustom style={styles.textInfo}>
              Ao clicar em aceitar estará concordando com o termo de uso e as
              Regras acima e sua conta será criada.
            </TextCustom>
          </View>

          <View>
            <ButtonLoading
              loading={isLoading}
              backgroundColor={colors.primaryColorDark}
              onPress={() => this.onCriarConta()}
              title="Aceitar e criar minha Conta Virtual"
            />

            <View style={{ height: 10 }} />

            <ButtonLoading
              onPress={() => onCancel()}
              backgroundColor={"#efefef"}
              titleColor={colors.grayColor}
              title="Fechar"
            />
          </View>
        </Content>
      </Dialog>
    );
  };
}

const styles = StyleSheet.create({
  containerView: {
    height: Dimensions.get("window").height / 2 - 200,
    marginTop: 16,
    marginBottom: 8,
    paddingHorizontal: 8,
    paddingVertical: 8,
    flexWrap: "wrap",
    backgroundColor: "#f7f7f7",
    justifyContent: "center"
  },
  textInfo: {
    fontSize: 11,
    color: "#111",
    textAlign: "center"
  }
});

export default ModalDetalhesContaVirtual;
