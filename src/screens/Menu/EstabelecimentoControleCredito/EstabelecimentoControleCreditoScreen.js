import React from "react";
import { Card } from "native-base";
import HeaderEmpresa from "../../../shared/components/HeaderEmpresa";
import styled from "styled-components/native";
import Icon from "../../../shared/components/Icon";
import { View, ScrollView, FlatList, TouchableOpacity } from "react-native";
import { Grid } from "react-native-easy-grid";
import { colors } from "../../../theme";
import ContainerPage from "../../../shared/components/ContainerPage";
import ModalUserContaVirtualQrCode from "../../../shared/components/ModalUserContaVirtualQrCode";
import SaldoContaVirtual from "./components/SaldoContaVirtual";
import PedidoItem from "./components/PedidoItem";
import SkeletonLoading from "../../../shared/components/SkeletonLoading";
import ContaVirtualRemote from "../../../services/remote/contavirtual.remote";
import TextCustom from "../../../shared/components/TextCustom";
import { withNavigationFocus } from "react-navigation";

const contaVirtualRemote = new ContaVirtualRemote();

export default withNavigationFocus(
  class EstabelecimentoControleCreditoScreen extends React.PureComponent {
    state = {
      openQrCode: false,
      isLoading: false,
      ultimosPedidos: []
    };

    navigateTo = async screen => {
      this.props.navigation.navigate(screen);
    };

    static navigationOptions = {
      title: "Controle de Crédito"
    };

    componentWillMount = async () => {
      this.setState({
        isLoading: true
      });

      try {
        const data = await contaVirtualRemote.getUltimosPedidos();

        this.setState({
          ultimosPedidos: data
        });
      } catch (e) {
      } finally {
        this.setState({
          isLoading: false
        });
      }
    };

    componentDidUpdate = async prevProps => {
      if (
        prevProps.isFocused !== this.props.isFocused &&
        this.props.isFocused
      ) {
        await this.componentWillMount();
      }
    };

    render() {
      const { empresaSelecionada } = this.props;
      const { openQrCode, ultimosPedidos, isLoading } = this.state;

      const menus = [
        {
          title: "Extrato",
          action: "EstabelecimentoCreditoExtrato",
          icon: "fa-file-o",
          color: "#06b0a6",
          visible: true,
          onPress: screen => this.navigateTo(screen)
        },
        {
          title: "Recarregar",
          icon: "fa-file",
          color: "#ff9f1c",
          visible: true,
          onPress: () => this.setState({ openQrCode: true })
        },
        {
          title: "Comprar",
          action: "EstabelecimentoCreditoResumoPedido",
          icon: "ion-md-cash",
          color: "#2e7189",
          visible: true,
          onPress: screen => this.navigateTo(screen)
        },
        {
          title: "Informação",
          action: "EstabelecimentoCreditoInformacao",
          icon: "fa-info-circle",
          color: "#f0223f",
          visible: true,
          onPress: screen => this.navigateTo(screen)
        }
      ];

      return (
        <ContainerPage
          style={{
            backgroundColor: "#f5f5f5"
          }}
        >
          <ModalUserContaVirtualQrCode
            open={openQrCode}
            onClose={() => this.setState({ openQrCode: false })}
            item={empresaSelecionada || {}}
          />
          <View>
            <HeaderEmpresa navigation={this.props.navigation} />
          </View>

          <ScrollView style={{ paddingTop: 0 }}>
            <View
              style={{
                flexDirection: "row",
                backgroundColor: "white",
                paddingVertical: 16,
                paddingHorizontal: 16
              }}
            > 
              <SaldoContaVirtual />
            </View>

            <Grid
              style={{
                paddingLeft: 8,
                paddingTop: 16,
                paddingRight: 8
              }}
            >
              <FlatList
                data={menus.filter(x => x.visible)}
                numColumns={2}
                keyExtractor={menu => menu.title}
                renderItem={({ item }) => {
                  return (
                    <View
                      style={{
                        flexGrow: 1,
                        flexBasis: 0,
                        paddingLeft: 8,
                        paddingBottom: 8,
                        paddingRight: 12
                      }}
                    >
                      <TouchableOpacity
                        onPress={() => item.onPress(item.action)}
                      >
                        <Card
                          noShadow
                          transparent
                          style={{
                            alignItems: "center",
                            backgroundColor: colors.white,
                            paddingTop: 16,
                            paddingBottom: 8,
                            borderWidth: 0
                          }}
                        >
                          <IconItem
                            style={{
                              textAlign: "center",
                              margin: "auto",
                              color: item.color
                            }}
                            name={item.icon}
                            size={25}
                          />
                          <MenuText>{item.title}</MenuText>
                        </Card>
                      </TouchableOpacity>
                    </View>
                  );
                }}
              />
            </Grid>

            {ultimosPedidos.length ? (
              <View style={{ marginTop: 16, paddingHorizontal: 16 }}>
                <View style={{ marginBottom: 10 }}>
                  <TextCustom>Ultimos Pedidos</TextCustom>
                </View>
                <SkeletonLoading lines={3} show={isLoading}>
                  <FlatList
                    data={ultimosPedidos}
                    renderItem={({ item }) => (
                      <PedidoItem
                        onPress={() =>
                          this.props.navigation.navigate(
                            "EstabelecimentoCreditoDetalhesPedido",
                            {
                              item: item
                            }
                          )
                        }
                        item={item}
                      />
                    )}
                  />
                </SkeletonLoading>
              </View>
            ) : null}
          </ScrollView>
        </ContainerPage>
      );
    }
  }
);

const MenuText = styled.Text`
  color: #222;
  flex: 3;
  text-align: center;
  padding-left: 6;
  padding-right: 6;
  padding-top: 6;
  font-family: Montserrat-Regular;
  font-weight: 400;
  font-size: 12;
`;

const IconItem = styled(Icon)`
  border-radius: 50px;
  text-align: center;
  padding-left: 6;
  padding-right: 6;
  padding-top: 6;
  font-size: 32;
  margin: 0 15px;
`;
