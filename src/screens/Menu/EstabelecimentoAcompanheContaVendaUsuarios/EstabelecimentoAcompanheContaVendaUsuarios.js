import React from 'react';
import { Body, ListItem, Left, Right, Radio } from 'native-base';
import {
  ScrollView,
  View,
  FlatList,
  Alert,
  TouchableOpacity
} from 'react-native';
import TextCustom from '../../../shared/components/TextCustom';
import Icon from '../../../shared/components/Icon';
import ContainerPage from '../../../shared/components/ContainerPage';
import EmpresaRemote from '../../../services/remote/empresa.remote';
import FidelidadeRemote from '../../../services/remote/fidelidade.remote';
import SkeletonLoading from '../../../shared/components/SkeletonLoading';
import PageEmpty from '../../../shared/components/PageEmpty';
import CabecalhoUsuario from './components/CabecalhoUsuario';

const empresaRemote = new EmpresaRemote();
const fidelidadeRemote = new FidelidadeRemote();

export default class EstabelecimentoAcompanheContaVendaUsuarios extends React.PureComponent {
  static navigationOptions = {
    title: 'Usuários'
  };

  state = {
    items: [],
    usuariosRateio: [],
    lider: {},
    isLoading: false,
    isChangeLider: false,
    isChangeFidelidade: false
  };

  componentDidMount = async () => {
    const venda = this.props.navigation.getParam('venda');

    this.setState({
      isLoading: true
    });

    try {
      const usuarios = await empresaRemote.findUsuariosVenda(venda.id);

      const lider = usuarios.filter(item => item.flgLider)[0] || {};

      this.setState({
        items: usuarios,
        lider: lider
      });
    } catch (e) {
      Alert.alert('Aviso', 'Houve um problema ao carregar a informação!');
    } finally {
      this.setState({ isLoading: false });
    }
  };

  onTrocarLider = () => {
    this.setState({
      isChangeLider: true,
      isChangeFidelidade: false
    });
  };

  onTrocarFidelidade = () => {
    this.setState({
      isChangeFidelidade: true,
      isChangeLider: false
    });
  };

  onChange = async item => {
    const { isChangeLider, isChangeFidelidade } = this.state;
    const saida = this.props.navigation.getParam('saida');

    this.setState({
      isLoading: true
    });

    try {
      if (isChangeLider) {
        await fidelidadeRemote.alterarLiderFidelidade(
          saida.saidaId,
          item.usuarioACCId
        );
      }

      if (isChangeFidelidade) {
        await fidelidadeRemote.alterarGanhadorFidelidade(
          saida.saidaId,
          item.usuarioACCId
        );
      }

      this.setState({
        isChangeFidelidade: false,
        isChangeLider: false
      });

      this.componentDidMount();
    } catch (e) {
      Alert.alert('Aviso', 'Houve um problema ao realizar a operação!');
    } finally {
      this.setState({
        isLoading: false
      });
    }
  };

  renderItem = ({ item }) => {
    const { isChangeLider, isChangeFidelidade } = this.state;
    const isEdicao = isChangeLider || isChangeFidelidade;

    return (
      <ListItem noBorder avatar>
        <Left>
          {!item.flgBloqueado ? (
            <Icon color={'#666'} name="ion-md-eye" size={30} />
          ) : (
            <Icon color={'#666'} name="ion-md-eye-off" size={30} />
          )}
        </Left>
        <Body>
          <TouchableOpacity onPress={() => isEdicao && this.onChange(item)}>
            <TextCustom style={{ fontSize: 16 }}>{item.nomeUsuario}</TextCustom>
            {item.flgFidelidade ? (
              <TextCustom note style={{ fontSize: 10 }}>
                (Ganhador dos pontos desta Compra)
              </TextCustom>
            ) : null}
          </TouchableOpacity>
        </Body>
        <Right>
          {isEdicao ? (
            <Radio
              onPress={() => this.onChange(item)}
              selected={
                isChangeLider
                  ? item.flgLider
                  : isChangeFidelidade
                  ? item.flgFidelidade
                  : false
              }
            />
          ) : null}
        </Right>
      </ListItem>
    );
  };

  render = () => {
    let { items, isLoading, lider } = this.state;
    const { empresaSelecionada, auth } = this.props;

    return (
      <ContainerPage>
        {!isLoading && !items.length ? (
          <PageEmpty
            icon="ion-md-contact"
            content="Nenhuma pessoa visualizando está venda."
          />
        ) : null}

        <CabecalhoUsuario
          items={items}
          auth={auth}
          onTrocarLider={() => this.onTrocarLider()}
          onTrocarFidelidade={() => this.onTrocarFidelidade()}
          onChange={item => this.onChange(item)}
          lider={lider}
          empresaSelecionada={empresaSelecionada}
        />

        <View>
          <ScrollView>
            <SkeletonLoading show={isLoading} lines={4}>
              <FlatList
                extraData={[]}
                data={items}
                renderItem={this.renderItem}
              />
            </SkeletonLoading>
          </ScrollView>
        </View>
      </ContainerPage>
    );
  };
}
