import React from "react";
import { View } from "react-native";
import { ListItem, Thumbnail, Left, Body, Right } from "native-base";
import { colors } from "../../../theme";
import Stars from "react-native-stars-rating";
import TextCustom from "../../../shared/components/TextCustom";
import { formatMoney } from "../../../infra/helpers";

const image = require("../../../../assets/images/empty.jpeg");

export default class EstabelecimentoItem extends React.Component {
  render() {
    let { item, isDelivery, disableDisplayDistance = false } = this.props;

    let wrapperImage = image;

    if (item.logoUrl) {
      wrapperImage = {
        uri: item.logoUrl
      };
    }

    return (
      <ListItem
        style={{
          opacity: !item.aberto ? 0.8 : 1,
          borderRadius: 6,
          backgroundColor: "#F7F7F7",
          padding: 0,
          marginLeft: 16,
          marginRight: 16,
          marginTop: 5,
          marginBottom: 5
        }}
        noIndent
        thumbnail
        noBorder
        onPress={this.props.onPress}
      >
        <Left>
          <Thumbnail
            borderRadius={5}
            resizeMode={"contain"}
            source={wrapperImage}
          />
        </Left>

        <Body>
          <TextCustom style={{ fontSize: 16, color: "#222" }}>
            {item.nome}
          </TextCustom>

          <View style={{ flexDirection: "column" }}>
            <TextCustom
              style={{
                color: item.aberto ? colors.greenButton : colors.grayColor,
                fontSize: 10,
                marginTop: 0
              }}
            >
              {item.aberto ? "Aberto" : "Fechado"}
            </TextCustom>

            {!disableDisplayDistance && item.distancia ? (
              <TextCustom
                style={{
                  color: colors.grayColor,
                  fontSize: 11,
                  marginTop: 0
                }}
              >
                {formatMoney(item.distancia)} km
              </TextCustom>
            ) : null}
          </View>

          {isDelivery && item.tempoMedioEntrega ? (
            <View style={{ flexDirection: "row" }}>
              <TextCustom
                style={{
                  color: "#222",
                  backgroundColor: "#FFFFFF",
                  borderWidth: 1,
                  marginTop: 4,
                  borderColor: "#ccc",
                  borderRadius: 4,
                  fontSize: 11,
                  padding: 4
                }}
              >
                Tempo de Entrega {item.tempoMedioEntrega}
              </TextCustom>
            </View>
          ) : null}
        </Body>

        {item.avaliacao ? (
          <Right>
            <View
              style={{
                flexDirection: "row",
                flex: 1,
                textAlign: "right"
              }}
            >
              <Stars
                rateMax={1}
                isHalfStarEnabled={true}
                rate={1}
                size={10}
                rounding={null}
              />

              <TextCustom
                style={{
                  fontSize: 12,
                  top: -3
                }}
              >
                {" "}
                {item.avaliacao.toString().replace(".", ",")}
              </TextCustom>
            </View>
          </Right>
        ) : null}
      </ListItem>
    );
  }
}
