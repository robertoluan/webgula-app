import React from "react";
import {
  View,
  StyleSheet,
  ActivityIndicator,
  Alert,
  Slider
} from "react-native";
import TextCustom from "../../../../shared/components/TextCustom";
import { colors } from "../../../../theme";
import ButtonLoading from "../../../../shared/components/ButtonLoading";
import InputSelect from "../../../../shared/components/Input/Select";
import InputTipoConceito from "../../../../shared/components/Input/InputTipoConceito";
import Dialog from "../../../../shared/components/Dialog";
import { Content } from "native-base";
import InputEstado2 from "../../../../shared/components/Input/InputEstado2";
import InputCidade2 from "../../../../shared/components/Input/InputCidade2";

class Filtro extends React.Component {
  state = {
    filtros: {
      cidadeId: null,
      estadoId: null,
      isFavorito: -1,
      isAberto: -1,
      distancia: 10
    },
    visibleEstado: false,
    visibleCidade: false,
    isLoading: false
  };

  constructor(props) {
    super(props);

    this.onChange = this.onChange.bind(this);
  }

  componentDidMount = async () => {
    this.setState({ filtros: { ...this.props.filtros } });
  };

  onChange = async (prop, value) => {
    const { filtros } = this.state;

    filtros[prop] = value;

    this.setState({
      filtros: {
        ...filtros
      }
    });
  };

  onSalvar = async () => {
    const { filtros } = this.props;

    if (!filtros.latitude) {
      if (!this.state.filtros.estadoId) {
        return Alert.alert("Aviso", "Selecione um estado antes de continuar!");
      }

      if (!this.state.filtros.cidadeId) {
        return Alert.alert("Aviso", "Selecione uma cidade antes de continuar!");
      }
    }

    this.props.onOk({ ...this.state.filtros });
  };

  get possuiLatitude() {
    return !!this.state.filtros.latitude;
  }

  render = () => {
    const { isLoading, filtros } = this.state;
    const { open, onCancel, onOk } = this.props;

    return (
      <Dialog
        open={open}
        title={"Filtros"}
        onCancel={() => onCancel()}
        onOk={() => onOk({ ...filtros })}
      >
        <Content
          style={{ borderBottomLeftRadius: 20, borderBottomRightRadius: 20 }}
        >
          {isLoading && <ActivityIndicator size="large" />}

          {!this.possuiLatitude ? (
            <TextCustom style={{ ...styles.containerView, marginBottom: 20 }}>
              Sua localização está desativada! Você pode mudar o acesso à sua
              localização nas configurações de aplicativos do seu aparelho.
            </TextCustom>
          ) : null}

          <View style={styles.containerView}>
            {!this.possuiLatitude ? (
              <View style={{ flexDirection: "row", top: -15 }}>
                <View style={{ flex: 1, paddingRight: 4 }}>
                  <InputEstado2
                    label="Estado"
                    value={filtros.estadoId}
                    onChange={value => this.onChange("estadoId", value)}
                  />
                </View>
                <View style={{ flex: 1, paddingLeft: 4 }}>
                  <InputCidade2
                    label="Cidade"
                    estadoId={filtros.estadoId}
                    value={filtros.cidadeId}
                    onChange={value => this.onChange("cidadeId", value)}
                  />
                </View>
              </View>
            ) : (
              <View>
                <TextCustom>Distancia (km):</TextCustom>
                <View
                  style={{
                    flex: 1,
                    marginBottom: 20,
                    marginTop: 20
                  }}
                >
                  <View
                    style={{
                      marginTop: 0,
                      textAlign: "center",
                      alignItems: "center",
                      marginBottom: 10
                    }}
                  >
                    <TextCustom>{filtros.distancia} km</TextCustom>
                  </View>

                  <Slider
                    step={1}
                    maximumValue={30}
                    minimumValue={1}
                    onValueChange={value => this.onChange("distancia", value)}
                    value={filtros.distancia}
                  />
                </View>
              </View>
            )}

            <View style={{ flexDirection: "row", top: -15 }}>
              <View style={{ flex: 1, paddingRight: 4 }}>
                <InputSelect
                  label={"Status"}
                  items={[
                    { text: "Todos", value: -1 },
                    { text: "Apenas Abertos", value: 1 },
                    { text: "Fechados", value: 0 }
                  ]}
                  keyValue="value"
                  value={filtros.isAberto}
                  onChange={value => this.onChange("isAberto", value)}
                />
              </View>

              <View style={{ flex: 1, paddingLeft: 4 }}>
                <InputSelect
                  label={"Favorito"}
                  items={[
                    { text: "Todos", value: -1 },
                    { text: "Apenas Favoritos", value: 1 },
                    { text: "Não Favoritos", value: 0 }
                  ]}
                  keyValue="value"
                  value={filtros.isFavorito}
                  onChange={value => this.onChange("isFavorito", value)}
                />
              </View>
            </View>
            <View style={{ top: -15 }}>
              <InputTipoConceito
                label="Conceito de Empresa"
                placeholder="Todos"
                value={filtros.wgTipoConceitoId}
                onChange={value => this.onChange("wgTipoConceitoId", value)}
              />
            </View>
          </View>

          <View style={styles.bottom}>
            <ButtonLoading onPress={() => this.onSalvar()} title="Filtrar" />

            <View style={{ height: 10 }} />

            <ButtonLoading
              onPress={() => onCancel()}
              backgroundColor={"white"}
              titleColor={colors.grayColor}
              title="Cancelar"
            />
          </View>
        </Content>
      </Dialog>
    );
  };
}

const styles = StyleSheet.create({
  containerView: {
    marginTop: 16,
    flexWrap: "wrap",
    justifyContent: "center"
  },
  bottom: {
    marginTop: 30
  }
});

export default Filtro;
