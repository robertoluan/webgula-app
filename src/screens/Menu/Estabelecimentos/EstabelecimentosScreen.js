import React from 'react';
import {
  StyleSheet,
  View,
  ScrollView,
  FlatList,
  RefreshControl,
  Image,
  TouchableOpacity,
  Platform
} from 'react-native';
import Input from '../../../shared/components/Input';
import EstabelecimentoItem from './EstabelecimentosItem';
import PageEmpty from '../../../shared/components/PageEmpty';
import ContainerPage from '../../../shared/components/ContainerPage';
import SkeletonLoading from '../../../shared/components/SkeletonLoading';
import { requestLocationPermissionAndroid } from '../../../services/resolvers/shared/gps';
import Filtro from './components/Filtro';
import { ListItem, Body, Right } from 'native-base';
import moment from 'moment';
import { diffMinutes } from '../../../services/resolvers/shared/cache';
import ButtonLoading from '../../../shared/components/ButtonLoading';
import TextCustom from '../../../shared/components/TextCustom';

import Permissions from 'react-native-permissions';

const imageFilter = require('../../../../assets/images/icon-filter.png');

export default class EstabelecimentoScreen extends React.PureComponent {
  state = {
    filtered: [],
    isSearching: true,
    possuiAcessoLocalizacao: false,
    openFilter: false,
    filtros: {
      dataUltimaLocalizacao: null,
      isFavorito: -1,
      isAberto: -1,
      wgTipoConceitoId: null,
      distancia: 10
    }
  };

  static navigationOptions = {
    title: 'Estabelecimentos'
  };

  componentDidMount = async () => {
    await this.props.getEnderecoMenu();

    setTimeout(() => {
      this.setState(
        {
          filtros: {
            ...this.props.enderecoMenu,
            distancia: this.props.enderecoMenu.distancia || 10
          }
        },
        () => this.onLoad()
      );
    }, 100);
  };

  componentDidUpdate = prevProps => {
    if (prevProps.enderecoMenu !== this.props.enderecoMenu) {
      this.setState({
        filtros: {
          ...this.props.enderecoMenu,
          distancia: this.props.enderecoMenu.distancia || 10
        }
      });
    }
  };

  resolveLocation = async () => {
    return new Promise(async (resolve, reject) => {
      const response = await Permissions.check('location', { type: 'always' });

      if (response === 'denied') {
        this.setState({ possuiAcessoLocalizacao: false });

        return resolve();
      }

      try {
        let { filtros } = this.state;

        if (filtros.dataUltimaLocalizacao) {
          if (this.isTimeLimitCache) {
            filtros.dataUltimaLocalizacao = null;
          } else {
            if (filtros.latitude) {
              this.setState({
                possuiAcessoLocalizacao: true
              });
              return resolve();
            } else {
              // POSSUI DATA DE ULTIMA LOCALIZACAO POREM NAO TEM LATITUDE ENTAO EU RETIRO A DATA
              filtros.dataUltimaLocalizacao = null;
            }
          }
        }

        if (!filtros.dataUltimaLocalizacao) {
          navigator.geolocation.getCurrentPosition(
            position => {
              const filtroState = {
                ...filtros,
                latitude: position.coords.latitude,
                longitude: position.coords.longitude,
                dataUltimaLocalizacao: moment().format()
              };

              this.setState(
                {
                  filtros: filtroState,
                  possuiAcessoLocalizacao: true
                },
                () => resolve()
              );
            },
            err => {
              const filtroState = {
                ...filtros,
                possuiAcessoLocalizacao: false
              };

              this.setState(
                {
                  filtros: filtroState
                },
                () => reject(err)
              );
            },
            { enableHighAccuracy: false, timeout: 20000, maximumAge: 10000 }
          );
        } else {
          resolve();
        }
      } catch (e) {}
    });
  };

  get isTimeLimitCache() {
    return diffMinutes(this.state.filtros.dataUltimaLocalizacao) * -1 > 5;
  }

  onLoad = async () => {
    this.setState({
      isSearching: true
    });

    try {
      await this.resolveLocation();
    } catch (e) {}

    setTimeout(async () => {
      let { filtros, possuiAcessoLocalizacao } = this.state;

      if (filtros.latitude) {
        delete filtros.estadoId;
        delete filtros.cidadeId;
      }

      let isOpenModalFiltro = false;

      if (!possuiAcessoLocalizacao && !filtros.cidadeId) {
        isOpenModalFiltro = true;
        filtros.latitude = null;
        filtros.longitude = null;
      }

      // Atualiza o endereço do menu
      await this.props.setEnderecoMenu({ ...filtros });

      if (isOpenModalFiltro) {
        this.setState({
          openFilter: true,
          isSearching: false
        });
        return;
      }

      try {
        await this.props.init({ ...filtros });
      } catch (e) {
      } finally {
        this.setState({
          isSearching: false
        });
      }
    }, 100);
  };

  navigateToItem = async item => {
    await this.props.selectedEmpresa(item);

    this.props.navigation.navigate('Estabelecimento');
  };

  filterData = () => {
    const { filtros } = this.state;

    let data = this.props.items || [];

    if (filtros.search) {
      data = data.filter(filtered => {
        return (
          filtered.nome.toLowerCase().indexOf(filtros.search.toLowerCase()) > -1
        );
      });
    }

    if (filtros.isFavorito === 0) {
      data = data.filter(item => !item.flgFavorito);
    }

    if (filtros.isFavorito === 1) {
      data = data.filter(item => item.flgFavorito);
    }

    if (filtros.isAberto === 0) {
      data = data.filter(item => !item.aberto);
    }

    if (filtros.isAberto === 1) {
      data = data.filter(item => item.aberto);
    }

    return data;
  };

  renderItem = ({ item }) => {
    return (
      <EstabelecimentoItem
        onPress={() => this.navigateToItem(item)}
        key={item.id}
        disableDisplayDistance={!this.state.possuiAcessoLocalizacao}
        {...this.props}
        item={item}
      />
    );
  };

  openFiltro = () => {
    this.setState({
      openFilter: true
    });
  };

  closeFiltro = () => {
    this.setState({
      openFilter: false
    });
  };

  onChangeFiltro = (prop, value) => {
    const filtros = this.state.filtros;

    filtros[prop] = value;

    this.setState({ filtros: { ...filtros } });
  };

  render = () => {
    const { isSearching, filtros = {}, openFilter } = this.state;

    const filtered = this.filterData();

    return (
      <ContainerPage>
        {openFilter ? (
          <Filtro
            open={openFilter}
            filtros={{
              ...filtros
            }}
            onCancel={() => this.closeFiltro()}
            onOk={value =>
              this.setState(
                {
                  openFilter: false,
                  filtros: { ...value }
                },
                async () => this.onLoad()
              )
            }
          />
        ) : null}

        <View
          style={{
            marginTop: -10
          }}
        >
          <ListItem noBorder noIndent>
            <Body>
              <Input
                style={styles.searchArea}
                isSearch
                canClear
                onChangeText={search => this.onChangeFiltro('search', search)}
                placeholder={'Buscar '}
              />
            </Body>

            <Right>
              <TouchableOpacity onPress={() => this.openFiltro()}>
                <Image
                  style={{ height: 30, top: 5, width: 45 }}
                  resizeMode="contain"
                  source={imageFilter}
                />
              </TouchableOpacity>
            </Right>
          </ListItem>
        </View>

        <View
          style={{
            paddingBottom: 80,
            paddingLeft: isSearching ? 16 : 0,
            paddingRight: isSearching ? 16 : 0
          }}
        >
          <SkeletonLoading lines={6} show={isSearching}>
            <ScrollView
              keyboardShouldPersistTaps="always"
              refreshControl={
                <RefreshControl
                  refreshing={isSearching}
                  onRefresh={() => this.onLoad()}
                />
              }
            >
              <FlatList
                extraData={[]}
                data={filtered}
                renderItem={this.renderItem}
              />
            </ScrollView>
          </SkeletonLoading>

          {!isSearching && !filtered.length ? (
            <PageEmpty
              icon="fa-frown-o"
              content={
                <React.Fragment>
                  <TextCustom>
                    Não houve resultado para a busca de empresas.
                  </TextCustom>
                  <ButtonLoading
                    title="Atualizar"
                    onPress={() => this.onLoad()}
                    titleColor="#666"
                    backgroundColor="transparent"
                  />
                </React.Fragment>
              }
            />
          ) : null}
        </View>
      </ContainerPage>
    );
  };
}

const styles = StyleSheet.create({
  searchArea: {
    borderRadius: 20,
    fontSize: 14,
    paddingTop: 2
  }
});
