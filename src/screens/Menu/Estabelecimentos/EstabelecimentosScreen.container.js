import { connect } from "react-redux";
import component from "./EstabelecimentosScreen";
import { loadEmpresas } from "../../../services/actions/app/empresa.actions";
import authContext from "../../../infra/auth";
import EmpresaRemote from "../../../services/remote/empresa.remote";
import {
  getEndereco,
  setEnderecoMenu
} from "../../../services/actions/app/auth.actions";

const empresaRemote = new EmpresaRemote();

const mapStateToProps = (state, props) => {
  let empresas = [];
  let loading = false;

  if (state.app && state.app.empresa && state.app.empresa) {
    empresas = state.app.empresa.empresas || [];
    loading = state.app.empresa.loading;
  }

  const enderecoMenu = state.app.endereco.menu || {};

  return {
    enderecoMenu,
    loading,
    items: empresas
  };
};

const mapDispatchToProps = (dispatch, props) => ({
  async init(params = {}) {
    const _params = { ...params };

    const userData = await authContext.userDataAsync();

    _params.pessoaId = userData.userInfo.pessoaId;

    // pessoaID 14 evandro
    // if (_params.pessoaId === 14 || (__DEV__ && !_params.cidadeId)) {
    //   _params.latitude = "-15.5919269";
    //   _params.longitude = "-56.1025646";
    // }

    if (!_params.latitude) {
      _params.latitude = 0;
      _params.longitude = 0;
    }

    _params.distancia = _params.distancia || 10;

    delete _params.dataUltimaLocalizacao;

    return dispatch(loadEmpresas(_params));
  },

  async getEnderecoMenu() {
    return dispatch(getEndereco());
  },

  async setEnderecoMenu(endereco) {
    dispatch(setEnderecoMenu(endereco));
  },

  async selectedEmpresa(empresa) {
    await empresaRemote.setEmpresaDefault(empresa);

    dispatch({
      type: "APP.EMPRESA.EMPRESA_SELECTED",
      payload: {
        empresaSelecionada: { ...empresa }
      }
    });
  }
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(component);
