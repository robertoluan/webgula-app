import { connect } from "react-redux";
import component from "./EstabelecimentoCreditoDetalhesPedidoProdutos";

const mapStateToProps = (state, props) => {
  let app = state.app || {};

  const empresaSelecionada = (app.empresa || {}).empresaSelecionada || {};

  return {
    auth: (app.auth && app.auth.userInfo) || {},
    empresaSelecionada
  };
};

const mapDispatchToProps = (dispatch, props) => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(component);
