import React from "react";
import { Body, ListItem } from "native-base";
import { ScrollView, View, FlatList } from "react-native";
import TextCustom from "../../../shared/components/TextCustom";
import ContainerPage from "../../../shared/components/ContainerPage";
import { formatMoney } from "../../../infra/helpers";

export default class EstabelecimentoCreditoDetalhesPedidoProdutos extends React.PureComponent {
  static navigationOptions = {
    title: "Produtos"
  };

  state = {
    items: []
  };

  componentDidMount = async () => {
    const item = this.props.navigation.getParam("item");

    this.setState({
      items: item.consumoItems
    });
  };

  renderItem = ({ item }) => {
    return (
      <ListItem>
        <Body>
          <TextCustom
            style={{
              fontSize: 16,
              fontFamily: "Montserrat-Bold",
              marginBottom: 6
            }}
          >
            {item.produtoNome}
          </TextCustom>
          <TextCustom style={{ marginBottom: 6 }}>
            {item.qtde} x R$ {formatMoney(item.valorUnitario)} ={" "}
            <TextCustom style={{ fontFamily: "Montserrat-Bold" }}>
              R$ {formatMoney(item.valorTotal)}
            </TextCustom>
          </TextCustom>
        </Body>
      </ListItem>
    );
  };

  render = () => {
    const { items = [] } = this.state;

    return (
      <ContainerPage>
        <View style={{ flex: 1, paddingBottom: 60 }}>
          <ScrollView>
            <FlatList data={items} renderItem={this.renderItem} />
          </ScrollView>
        </View>
      </ContainerPage>
    );
  };
}
