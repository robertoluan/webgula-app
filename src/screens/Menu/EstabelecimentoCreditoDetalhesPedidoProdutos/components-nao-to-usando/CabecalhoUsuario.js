import React from "react";
import { View, Alert } from "react-native";
import TextCustom from "../../../../shared/components/TextCustom";
import Icon from "../../../../shared/components/Icon";
import { colors } from "../../../../theme";
import ButtonLoading from "../../../../shared/components/ButtonLoading";

export default class CabecalhoUsuario extends React.Component {
  render() {
    const { empresaSelecionada, items, lider, auth } = this.props;

    return (
      <View
        style={{
          marginBottom: 36,
          paddingTop: 36
        }}
      >
        <View
          style={{
            alignItems: "center"
          }}
        >
          <Icon name="ion-md-ribbon" color={colors.yellowButton} size={50} />
        </View>
        <View>
          <TextCustom style={{ textAlign: "center", fontSize: 12 }}>
            Líder
          </TextCustom>
          <TextCustom style={{ fontSize: 24, textAlign: "center" }}>
            {lider.nomeUsuario}
          </TextCustom>
          <TextCustom style={{ textAlign: "center" }}>
            {lider.flgFidelidade ? "(Ganhador dos pontos desta compra)" : null}
          </TextCustom>
        </View>
        {items.length > 1 && auth.id == lider.usuarioACCId ? (
          <View
            style={{
              marginTop: 16,
              marginLeft: 16,
              marginRight: 16,
              flexDirection: "row"
            }}
          >
           <View
              style={{
                flex: 1,
                marginRight: empresaSelecionada.flgFidelidade ? 8 : 0
              }}
            >
              <ButtonLoading
                fontSize={13}
                onPress={() => this.props.onTrocarLider()}
                title="Alterar Lider"
              />
            </View>  
            {empresaSelecionada.flgFidelidade ? (
              <View style={{ flex: 1, marginLeft: 8 }}>
                <ButtonLoading
                  fontSize={13}
                  onPress={() => this.props.onTrocarFidelidade()}
                  title="Indicar Fidelidade"
                />
              </View>
            ) : null}
          </View>
        ) : null}
      </View>
    );
  }
}
