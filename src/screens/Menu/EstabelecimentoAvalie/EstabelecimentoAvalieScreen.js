import React from "react";
import { Form } from "native-base";
import HeaderEmpresa from "../../../shared/components/HeaderEmpresa";
import { StyleSheet, View, TextInput, Alert } from "react-native";
import Rating from "../../../shared/components/rating";
import ButtonLoading from "../../../shared/components/ButtonLoading";
import TextCustom from "../../../shared/components/TextCustom";
import ContainerPage from "../../../shared/components/ContainerPage";

export default class EstabelecimentoAvalieScreen extends React.Component {
  state = {
    avaliacaoTexto: "",
    avaliacaoNumero: 3,
    loading: false
  };

  static navigationOptions = {
    title: "Avalie"
  };

  enviar = async () => {
    if (!this.state.avaliacaoNumero) {
      return Alert.alert(
        "Aviso",
        "Por favor selecione a quantidade de estrelas para a avaliação!"
      );
    }

    this.setState({
      loading: true
    });

    try {
      const { type } = await this.props.enviar(
        this.state.avaliacaoNumero,
        this.state.avaliacaoTexto
      );

      this.setState({
        loading: false
      });

      if (type == "exception") {
        return Alert.alert(
          "Aviso",
          "Você já realizou uma avalição nas ultimas 12 horas!",
          [
            {
              text: "OK",
              onPress: () => this.props.navigation.navigate("Estabelecimento")
            }
          ]
        );
      }

      Alert.alert("Aviso", "Avaliação realizada com sucesso!", [
        {
          text: "OK",
          onPress: () => this.props.navigation.navigate("Estabelecimento")
        }
      ]);
    } catch (e) {
      this.setState({
        loading: false
      });

      this.props.navigation.navigate("Estabelecimento");
    }
  };

  render() {
    let { loading } = this.state;

    return (
      <ContainerPage style={[styles.wrapper]}>
        <View>
          <HeaderEmpresa navigation={this.props.navigation} />
        </View>

        <View style={styles.container} keyboardShouldPersistTaps="always">
          <View style={{ alignItems: "center", height: 150, top: 20 }}>
            <Rating
              onChange={value => {
                this.setState({
                  avaliacaoNumero: value
                });
              }}
            />
          </View>

          <TextCustom style={styles.title2}>
            Informe sua Critica, Sugestão ou Elogio!
          </TextCustom>

          <Form>
            <TextInput
              multiline={true}
              style={styles.textArea}
              onChangeText={text => this.setState({ avaliacaoTexto: text })}
              value={this.state.avaliacaoTexto}
            />

            <View style={styles.buttonContainer}>
              <ButtonLoading
                loading={loading}
                title="Enviar"
                onPress={() => this.enviar()}
              />
            </View>
          </Form>
        </View>
      </ContainerPage>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {},
  container: {
    marginRight: 0
  },
  buttonContainer: {
    margin: 16
  },
  form: {
    // paddingLeft: 16,
    // paddingBottom: 16
  },
  textArea: {
    backgroundColor: "#efefef",
    marginRight: 16,
    marginLeft: 16,
    marginBottom: 16
  },
  title: {
    padding: 16,
    marginTop: 32,
    fontSize: 18,
    textAlign: "center"
  },
  title2: {
    padding: 16,
    marginTop: 0,
    fontSize: 16,
    textAlign: "center"
  },
  btnAvaliar: {
    paddingBottom: 4,
    paddingTop: 4,
    marginRight: 16,
    marginLeft: 16
  },
  txtAvaliar: {
    color: "#FFF",
    fontSize: 22,
    top: -10,
    position: "absolute"
  }
});
