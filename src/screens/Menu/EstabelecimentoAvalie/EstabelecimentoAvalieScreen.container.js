import { connect } from "react-redux";
import component from "./EstabelecimentoAvalieScreen";
import EmpresaApi from "../../../services/remote/empresa.remote";
import moment from "moment";
import authContext from "../../../infra/auth";

const empresaApi = new EmpresaApi();

const mapStateToProps = (state, props) => {
  return {};
};

const mapDispatchToProps = (dispatch, props) => ({
  async enviar(avaliacao, mensagem) {
    return await empresaApi.avaliar({
      dataHoraAvaliacao: moment().toDate(),
      mensagem,
      avaliacao,
      flgStatus: "A",
      usuarioClienteEmail: authContext.userInfo.email || "avaliar@tacto.com.br"
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(component);
