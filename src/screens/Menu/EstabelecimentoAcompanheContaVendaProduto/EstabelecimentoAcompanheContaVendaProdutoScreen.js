import React from "react";
import { Body, ListItem } from "native-base";
import { ScrollView, View, FlatList, RefreshControl } from "react-native";
import { formatMoney } from "../../../infra/helpers";
import TextCustom from "../../../shared/components/TextCustom";
import ContainerPage from "../../../shared/components/ContainerPage";
import ViewBottomTotal from "../EstabelecimentoAcompanheContaVenda/components/ViewBottomTotal";
import SkeletonLoading from "../../../shared/components/SkeletonLoading";

export default class EstabelecimentoAcompanheContaVendaProdutoScreen extends React.Component {
  static navigationOptions = {
    title: "Itens da Conta"
  };

  state = { venda: {}, isLoading: false };

  componentDidMount = () => {
    const venda = this.props.navigation.getParam("venda");

    this.setState({
      venda: { ...venda }
    });
  };

  loadData = async () => {
    const saida = this.props.navigation.getParam("saida");

    this.setState({
      isLoading: true
    });

    try {
      const venda = await this.props.loadVenda(saida.saidaId);

      this.setState({
        venda: venda
      });
    } catch (e) {
      Alert.alert("Aviso", "Houve um problema ao realizar a operação!");
    } finally {
      this.setState({ isLoading: false });
    }
  };

  renderItem = ({ item }) => {
    return (
      <ListItem>
        <Body>
          <TextCustom
            style={{
              fontSize: 16,
              fontFamily: "Montserrat-Bold",
              marginBottom: 6
            }}
          >
            {item.nome}
          </TextCustom>
          {/* <TextCustom note style={{ color: colors.grayColor, marginBottom: 6 }}>
            {moment(item.data).format("DD/MM/YYYY HH:mm")}
          </TextCustom> */}
          <TextCustom style={{ marginBottom: 6 }}>
            {item.qtd} x R$ {formatMoney(item.valor)} ={" "}
            <TextCustom style={{ fontFamily: "Montserrat-Bold" }}>
              R$ {formatMoney(item.total)}
            </TextCustom>
          </TextCustom>
        </Body>
      </ListItem>
    );
  };

  render() {
    const { venda, isLoading } = this.state;

    const items = (venda.saidaItem || []).map(x => {
      return {
        id: x.id,
        nome: x.produtoNomeCompleto,
        qtd: x.qtde,
        data: venda.dataSaida,
        valor: x.valorUnitario,
        total: x.valorTotal
      };
    });

    const saida = this.props.navigation.getParam("saida");

    return (
      <ContainerPage>
        <View style={{ flex: 1, paddingBottom: 60 }}>
          <SkeletonLoading show={isLoading} lines={5}>
            <ScrollView
              refreshControl={
                <RefreshControl
                  refreshing={isLoading}
                  onRefresh={() => this.loadData()}
                />
              }
            >
              <FlatList data={items} renderItem={this.renderItem} />
            </ScrollView>
          </SkeletonLoading>
        </View>

        <ViewBottomTotal
          saida={saida}
          navigation={this.props.navigation}
          venda={venda}
        />
      </ContainerPage>
    );
  }
}
