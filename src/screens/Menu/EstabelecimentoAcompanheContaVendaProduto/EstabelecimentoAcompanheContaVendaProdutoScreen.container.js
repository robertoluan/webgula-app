import { connect } from "react-redux";
import component from "./EstabelecimentoAcompanheContaVendaProdutoScreen";
import EmpresaApi from "../../../services/remote/empresa.remote";

const empresaApi = new EmpresaApi();

const mapStateToProps = (state, props) => {
  return {};
};

const mapDispatchToProps = (dispatch, props) => ({
  async loadVenda(saidaId) {
    return empresaApi.findVenda(saidaId);
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(component);
