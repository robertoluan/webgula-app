import React from "react";
import { Input } from "native-base";
import HeaderEmpresa from "../../../shared/components/HeaderEmpresa";
import {
  View,
  StyleSheet,
  ScrollView,
  Alert,
  Image,
  RefreshControl
} from "react-native";
import ButtonLoading from "../../../shared/components/ButtonLoading";
import TextCustom from "../../../shared/components/TextCustom";
import imageQRCode from "../../../../assets/images/qrcode.png";
import { TouchableOpacity } from "react-native";
import { colors } from "../../../theme";
import GlobalQRCode from "../../../shared/components/GlobalQRCode";
import ContainerPage from "../../../shared/components/ContainerPage";
import ListaAcompanhamento from "./components/ListaAcompanhamento";
import { withNavigationFocus } from "react-navigation";

class EstabelecimentoAcompanheContaScreen extends React.PureComponent {
  state = {
    chave: "", //8750002
    isLoading: false,
    openQrCode: false,
    isReload: false
  };

  static navigationOptions = {
    title: "Acompanhe sua Conta"
  };

  buscarPedido = async () => {
    const { chave } = this.state;

    if (!chave) {
      return Alert.alert("Aviso", "Informe a chave!");
    }

    this.setState({
      isLoading: true
    });

    try {
      const saida = await this.props.load(chave);

      if (saida.saidaId == 0) {
        Alert.alert("Aviso", saida.mensagem);
      } else {
        this.props.navigation.navigate("EstabelecimentoAcompanheContaVenda", {
          saida
        });
      }
    } catch (e) {
      Alert.alert("Aviso", "Conta não encontrada!");
    } finally {
      this.setState({
        isLoading: false
      });
    }
  };

  componentDidUpdate = async prevProps => {
    if (prevProps.isFocused !== this.props.isFocused && this.props.isFocused) {
      await this.reload();
    }
  };

  reload = async () => {
    if (this.listaAcompanhamento) {
      this.setState({
        isReload: true
      });

      await this.listaAcompanhamento.componentDidMount();

      this.setState({
        isReload: false
      });
    }
  };

  render = () => {
    const { isLoading, isReload } = this.state;

    return (
      <ContainerPage style={styles.wrapper}>
        <GlobalQRCode
          open={this.state.openQrCode}
          onResult={result => {
            if (result) {
              if (isNaN(Number(result))) {
                Alert.alert(
                  "Aviso",
                  "A informação contida no QR Code não é valida para o WebGula"
                );
              } else {
                if (result) {
                  this.setState(
                    {
                      chave: result
                    },
                    () => {
                      this.buscarPedido();
                    }
                  );
                }
              }
            }

            this.setState({ openQrCode: false });
          }}
        />

        <View>
          <HeaderEmpresa navigation={this.props.navigation} />
        </View>

        <ScrollView
          refreshControl={
            <RefreshControl
              refreshing={isReload}
              onRefresh={() => this.reload()}
            />
          }
          style={styles.container}
          keyboardShouldPersistTaps="always"
        >
          <TextCustom style={styles.title}> Informe a Chave </TextCustom>
          <View>
            <Input
              disabled={isLoading}
              keyboardType="numeric"
              style={styles.input}
              value={this.state.chave}
              onChangeText={text =>
                this.setState({
                  chave: (text || "").trim()
                })
              }
            />
          </View>
          <View style={styles.buttonContainer}>
            <ButtonLoading
              loading={isLoading}
              title="Buscar"
              onPress={() => this.buscarPedido()}
            />
          </View>
          <View style={{ height: 150 }}>
            <TouchableOpacity
              onPress={() => this.setState({ openQrCode: true })}
              activeOpacity={0.8}
            >
              <View>
                <Image style={styles.qrcodeImg} source={imageQRCode} />
              </View>

              <View style={{ marginTop: 16 }}>
                <TextCustom
                  style={{
                    textAlign: "center",
                    fontSize: 16
                  }}
                >
                  Clique aqui para ler o QrCode
                </TextCustom>
              </View>
            </TouchableOpacity>
          </View>
          <ListaAcompanhamento
            onRef={ref => (this.listaAcompanhamento = ref)}
            navigation={this.props.navigation}
          />
        </ScrollView>
      </ContainerPage>
    );
  };
}

export default withNavigationFocus(EstabelecimentoAcompanheContaScreen);

const styles = StyleSheet.create({
  wrapper: {
    height: "100%",
    backgroundColor: colors.white
  },
  container: {
    height: "100%",
    paddingBottom: 30,
    marginRight: 0
  },
  qrcodeImg: {
    width: 80,
    height: 80,
    top: 16,
    alignSelf: "center"
  },
  buttonContainer: {
    margin: 16,
    marginTop: 8
  },
  input: {
    backgroundColor: "#efefef",
    margin: 16
  },
  title: {
    padding: 16,
    marginTop: 32,
    paddingTop: 8,
    paddingBottom: 8,
    fontSize: 18,
    textAlign: "center"
  },
  title2: {
    padding: 16,
    marginTop: 16,
    fontSize: 16,
    textAlign: "center"
  },
  btnAvaliar: {
    paddingBottom: 4,
    paddingTop: 4,
    marginRight: 16,
    marginLeft: 16
  },
  txtAvaliar: {
    color: "#FFF",
    fontSize: 22
  }
});
