import React from 'react';
import { View, FlatList, ScrollView, Alert } from 'react-native';
import { Body, Right, ListItem } from 'native-base';
import TextCustom from '../../../../shared/components/TextCustom';
import moment from 'moment';
import EmpresaRemote from '../../../../services/remote/empresa.remote';
import SkeletonLoading from '../../../../shared/components/SkeletonLoading';
import { formatMoney } from '../../../../infra/helpers';

const Api = new EmpresaRemote();

export function ListItemHistorico({ value, onPress }) {
  const data = moment(value.dataSaida).format('DD/MM/YYYY hh:mm');

  return (
    <ListItem
      onPress={onPress}
      style={{
        borderBottomColor: '#f7f7f7',
        borderBottomWidth: 1,
        backgroundColor: '#f4f4f4',
        marginBottom: 10
      }}
      noIndent
      noBorder
    >
      <Body>
        <View>
          <TextCustom note style={{ fontSize: 10 }}>
            {data} - {value.chaveMobile}
          </TextCustom>
        </View>
        <TextCustom
          style={{
            fontSize: 14
          }}
        >
          chave: #{value.chaveMobile}{' '}
          {value.flgLider ? '(Líder da Mesa)' : null}
        </TextCustom>
      </Body>
      <Right>
        <TextCustom
          style={{
            fontSize: 14,
            fontFamily: 'Montserrat-Bold'
          }}
        >
          R$ {formatMoney(value.valorTotal)}
        </TextCustom>
      </Right>
    </ListItem>
  );
}

export default class ListaAcompanhamento extends React.PureComponent {
  state = {
    items: [],
    isLoading: false
  };

  constructor(props) {
    super(props);

    if (typeof this.props.onRef === 'function') {
      this.props.onRef(this);
    }
  }

  componentDidMount = async () => {
    this.setState({
      isLoading: true
    });

    try {
      const data = await Api.findContasAcompanhamento();

      console.log('data', data);

      this.setState({
        items: data
      });
    } catch (e) {
    } finally {
      this.setState({
        isLoading: false
      });
    }
  };

  navigateTo = async item => {
    if (item.flgBloqueado) {
      return Alert.alert(
        'Aviso',
        'Está venda ja foi fechada! Não é possível mais visualiza-la.'
      );
    }

    this.props.navigation.navigate('EstabelecimentoAcompanheContaVenda', {
      saida: { saidaId: item.saidaId }
    });
  };

  render() {
    const { items, isLoading } = this.state;

    return (
      <View style={{ paddingHorizontal: isLoading ? 16 : 0 }}>
        <SkeletonLoading lines={1} show={isLoading}>
          <ScrollView>
            <FlatList
              data={items}
              renderItem={({ item }) => (
                <ListItemHistorico
                  onPress={() => this.navigateTo(item)}
                  value={item}
                />
              )}
            />
          </ScrollView>
        </SkeletonLoading>

        {!isLoading && !items.length ? (
          <TextCustom
            style={{
              textAlign: 'center',
              fontSize: 16,
              padding: 16
            }}
          >
            Solicite o cupom de acompanhamento de conta para o Garçom ou Caixa
          </TextCustom>
        ) : null}
      </View>
    );
  }
}
