import { connect } from "react-redux";
import component from "./EstabelecimentoAcompanheContaScreen";
import EmpresaApi from "../../../services/remote/empresa.remote";

const empresaApi = new EmpresaApi();

const mapStateToProps = (state, props) => {
  return {};
};

const mapDispatchToProps = (dispatch, props) => ({
  async load(chave) {
    return empresaApi.findSaida(chave);
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(component);
