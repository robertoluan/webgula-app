import React from "react";
import { StyleSheet, View, TouchableOpacity, Image } from "react-native";
import logoImg from "../../../assets/images/logo-secondary.png";
import { colors } from "../../theme";
import TextCustom from "../../shared/components/TextCustom";
import ContainerPage from "../../shared/components/ContainerPage";

import BouncingPreloader from "react-native-bouncing-preloader";

const icons = [
  require("../../../assets/images/759946_bar_512x512.png"),
  require("../../../assets/images/759908_food_512x512.png"),
  require("../../../assets/images/759956_food_512x512.png"),
  require("../../../assets/images/759954_food_512x512.png"),
  require("../../../assets/images/759906_food_512x512.png"),
  require("../../../assets/images/759921_food_512x512.png")
];

class Offline extends React.PureComponent {
  static navigationOptions = {
    header: null
  };

  componentDidMount = async () => {
    try {
      await this.props.init();
    } catch (e) {
      alert("Houve um problema ao iniciar o Webgula! Feche e abra novamente.");
      return;
    }

    setTimeout(() => {
      const { isAuth, isClientCredentials, dados } = this.props;

      if (isAuth && !dados.pessoaId) {
        return this.props.navigation.replace("Tutorial");
      }

      if (isAuth) {
        return this.props.navigation.replace("Home");
      }

      if (isClientCredentials || !isAuth) {
        return this.props.navigation.replace("Login");
      }
    }, 300);
  };

  render = () => {
    return (
      <ContainerPage>
        <View style={styles.wrapper}>
          <TouchableOpacity style={styles.imgContainer}>
            <Image source={logoImg} style={styles.logo} />
          </TouchableOpacity>

          <View style={styles.containerLoader}>
            <BouncingPreloader
              icons={icons}
              leftDistance={-100}
              rightDistance={-150}
              speed={1000}
            />
          </View>

          <TextCustom style={{ textAlign: "center", margin: 16 }}>
            Ops! Você está desconectado da internet.
          </TextCustom>
        </View>
      </ContainerPage>
    );
  };
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1
  },
  containerLoader: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#fff"
  },
  container: {
    marginBottom: 60
  },
  imgContainer: {
    alignItems: "center",
    top: 120
  },
  logo: {
    alignSelf: "center",
    resizeMode: "contain",
    width: 172
  },
  btnMenu: {
    marginTop: 10,
    marginBottom: 10,
    marginLeft: 16,
    marginRight: 16
  },
  btnMenuClick: {
    justifyContent: "center",
    padding: 14,
    borderRadius: 4,
    backgroundColor: "rgba(0, 0, 0, .4)",
    flexDirection: "row"
  },
  textMenu: {
    color: colors.white,
    fontFamily: "Montserrat-Regular",
    fontSize: 16
  },
  icon: {
    fontSize: 20,
    marginRight: 10,
    color: colors.white
  }
});

export default Offline;

