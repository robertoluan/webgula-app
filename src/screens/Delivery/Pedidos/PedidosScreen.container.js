import { connect } from "react-redux";
import component from "./PedidosScreen";
import { getPedidos } from "../../../services/actions/app/delivery.actions";

const mapStateToProps = (state, props) => {
  const app = state.app;

  return {
    pedidos: (app.delivery && app.delivery.pedidos) || []
  };
};

const mapDispatchToProps = (dispatch, props) => ({
  async getPedidos(params = {}) {
    return dispatch(getPedidos(params));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(component);
