import React from "react";
import { Image, TouchableOpacity, View } from "react-native";
import TextCustom from "../../../../shared/components/TextCustom";
import { Card, CardItem } from "native-base";
import { formatMoney } from "../../../../infra/helpers";
import { colors } from "../../../../theme";

const imagem = require("../../../../../assets/images/noimage.png");

export default class ProdutoItemCard extends React.PureComponent {
  render() {
    const { item, onPress } = this.props;

    let wrapperImagem = {};

    if (item && item.imagem && item.imagem.length) {
      wrapperImagem = {
        uri: item.imagem[0].url
      };
    } else {
      wrapperImagem = imagem;
    }

    return (
      <TouchableOpacity
        style={{ paddingRight: 10 }}
        activeOpacity={0.8}
        onPress={() => onPress(item)}
      >
        <Card style={{ position: "relative", borderRadius: 6 }}>
          <CardItem cardBody>
            <Image
              source={wrapperImagem}
              borderRadius={6}
              style={{ height: 200, width: 200, flex: 1 }}
            />
          </CardItem>

          <View
            style={{
              position: "absolute",
              bottom: 0,
              left: 0,
              right: 0,
              height: 70,
              padding: 8,
              backgroundColor: "rgba(0, 0, 0, 0.3)"
            }}
          >
            <TextCustom
              style={{
                fontSize: 12,
                marginBottom: 6,
                fontFamily: "Montserrat-Bold",
                color: "white"
              }}
            >
              {item.nomeCurto}
            </TextCustom>

            <TextCustom
              style={{
                fontFamily: "Montserrat-Bold",
                fontSize: 20,
                color: colors.primaryColor,
                position: "absolute",
                bottom: 8,
                left: 8
              }}
            >
              R$ {formatMoney(item.preco)}
            </TextCustom>
          </View>
        </Card>
      </TouchableOpacity>
    );
  }
}
