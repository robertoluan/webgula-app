import React from "react";
import ButtonHeaderDeliveryRight from "../../../shared/components/ButtonHeaderDeliveryRight";
import ProdutosBody from "./ProdutosBody";

export default class ProdutosScreen extends React.PureComponent {
  static navigationOptions = ({ navigation }) => {
    return {
      title: "Produtos",
      headerRight: <ButtonHeaderDeliveryRight navigation={navigation} />
    };
  };

  render = () => {
    return <ProdutosBody {...this.props} />;
  };
}
