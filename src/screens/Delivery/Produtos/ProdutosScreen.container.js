import { connect } from "react-redux";
import component from "./ProdutosScreen";
import EmpresaApi from "../../../services/remote/empresa.remote";

const empresaApi = new EmpresaApi();

const mapStateToProps = (state, props) => {
  return {};
};

const mapDispatchToProps = (dispatch, props) => ({
  async load(grupoPaiId = null) {
    return empresaApi.findGrupos(grupoPaiId);
  },
  async loadCardapio() {
    return empresaApi.findCardapio(null, 3);
  }, 
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(component);
