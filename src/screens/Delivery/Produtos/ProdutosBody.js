import React from 'react';
import HeaderEmpresa from '../../../shared/components/HeaderEmpresa';
import {
  FlatList,
  ScrollView,
  RefreshControl,
  View,
  TouchableOpacity
} from 'react-native';
import SkeletonLoading from '../../../shared/components/SkeletonLoading';
import PageEmpty from '../../../shared/components/PageEmpty';
import TextCustom from '../../../shared/components/TextCustom';
import ProdutoItemCard from './componentes/ProdutoItemCard';
import ContainerPage from '../../../shared/components/ContainerPage';
import { colors } from '../../../theme';

export default class ProdutosBody extends React.PureComponent {
  state = {
    items: [],
    loading: false,
    grupos: []
  };

  componentDidMount = async () => {
    this.setState({
      loading: true
    });

    let grupoPai = this.props.navigation.getParam('grupoPaiId');

    let result = await this.props.load(grupoPai > 0 ? grupoPai : null);

    if (result.length == 0) {
      this.setState({
        loading: false
      });

      return;
    }

    let cardapio = {};

    try {
      cardapio = await this.props.loadCardapio();
    } catch (e) {
      this.setState({
        loading: false
      });
      return;
    }

    let groupedItems = {};

    result.forEach(grupo => {
      if (!groupedItems[grupo.categoriaNome]) {
        groupedItems[grupo.categoriaNome] = [];
      }

      grupo.produtos = cardapio.produtos.filter(
        produto => produto.grupoId == grupo.id
      );

      groupedItems[grupo.categoriaNome].push(grupo);
    });

    let items = [];

    for (let prop in groupedItems) {
      items.push({
        nome: prop,
        items: groupedItems[prop]
      });
    }

    this.setState({
      grupos: items,
      loading: false
    });
  };

  navigateTo = (screen, params = {}) => {
    if (params.grupoPaiId > 0) {
      let view = 'DeliveryProdutos';

      if (this.props.isMenu) {
        view = 'EstabelecimentoCardapio';
      }

      return this.props.navigation.navigate(view, params);
    }

    return this.props.navigation.navigate(screen, params);
  };

  navigateToProduto = item => {
    let view = 'DeliveryProdutosGruposItem';

    if (this.props.isMenu) {
      view = 'EstabelecimentoCardapioProdutoItem';
    }

    this.props.navigation.navigate(view, { item });
  };

  renderItem = ({ item }) => {
    return (
      <View>
        <View style={{ marginTop: 10, marginBottom: 10 }}>
          <TextCustom
            style={{
              fontFamily: 'Montserrat-Bold',
              fontSize: 18
            }}
          >
            {item.nome}
          </TextCustom>
        </View>
        <View>
          <FlatList
            data={item.items}
            renderItem={categoria => (
              <View>
                <View style={{ marginVertical: 8, flexDirection: 'row' }}>
                  <TextCustom
                    style={{
                      fontSize: 14,
                      flex: 1
                    }}
                  >
                    {categoria.item.nome}
                  </TextCustom>

                  <TouchableOpacity
                    onPress={() => {
                      let view = 'DeliveryProdutosGrupos';

                      if (this.props.isMenu) {
                        view = 'EstabelecimentoCardapioProduto';
                      }

                      this.navigateTo(view, {
                        grupo: categoria.item
                      });
                    }}
                  >
                    <TextCustom
                      style={{
                        fontSize: 13,
                        color: colors.primaryColor
                      }}
                    >
                      Ver mais
                    </TextCustom>
                  </TouchableOpacity>
                </View>
                <View style={{ marginBottom: 16 }}>
                  <FlatList
                    data={categoria.item.produtos}
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                    renderItem={recordProduto => (
                      <ProdutoItemCard
                        onPress={produto => this.navigateToProduto(produto)}
                        item={recordProduto.item}
                      />
                    )}
                  />
                </View>
              </View>
            )}
          />
        </View>
      </View>
    );
  };

  render = () => {
    const { grupos, loading } = this.state;

    return (
      <ContainerPage>
        <HeaderEmpresa navigation={this.props.navigation} />

        <SkeletonLoading lines={5} show={loading}>
          <ScrollView
            refreshControl={
              <RefreshControl
                refreshing={this.state.loading}
                onRefresh={() => this.componentDidMount()}
              />
            }
          >
            <View
              style={{
                paddingLeft: 16,
                paddingRight: 16
              }}
            >
              <FlatList data={grupos} renderItem={this.renderItem} />
            </View>
          </ScrollView>
        </SkeletonLoading>

        {!loading && !grupos.length && (
          <PageEmpty icon="fa-frown-o" content="Nenhum produto encontrado!" />
        )}
      </ContainerPage>
    );
  };
}
