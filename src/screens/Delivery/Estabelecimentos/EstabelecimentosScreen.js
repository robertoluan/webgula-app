import React from "react";
import {
  Container,
  Content,
  Card,
  CardItem,
  Thumbnail,
  Toast
} from "native-base";
import {
  View,
  ScrollView,
  Dimensions,
  StyleSheet,
  RefreshControl,
  FlatList,
  TouchableOpacity,
  Alert
} from "react-native";
import { Grid } from "react-native-easy-grid";
import { colors } from "../../../theme";
import TextCustom from "../../../shared/components/TextCustom";
import EstabelecimentoItem from "../../Menu/Estabelecimentos/EstabelecimentosItem";
import ButtonHeaderDeliveryRight from "../../../shared/components/ButtonHeaderDeliveryRight";
import store from "../../../infra/store";
import EmpresaRemote from "../../../services/remote/empresa.remote";
import DeliveryRemote from "../../../services/remote/delivery.remote";
import SkeletonLoading from "../../../shared/components/SkeletonLoading";
import ContainerPage from "../../../shared/components/ContainerPage";
import PageEmpty from "../../../shared/components/PageEmpty";
import authContext from "../../../infra/auth";

const { width } = Dimensions.get("window");
const itemWidth = (width - 15) / 3.4;

const empresaRemote = new EmpresaRemote();
const deliveryRemote = new DeliveryRemote();

export default class EstabelecimentoScreen extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: "Delivery",
      headerRight: <ButtonHeaderDeliveryRight navigation={navigation} />
    };
  };

  state = {
    itemSelected: {
      id: null
    },
    empresas: [],
    isSearching: false
  };

  componentWillMount = async () => {
    await this.props.resetEmpresaSelecionada();
  };

  // quando for pro ar desabilitar isso
  componentDidMount = async () => {
    const userData = await authContext.userDataAsync();

    const pessoaId = userData.userInfo.pessoaId;

    if (pessoaId !== 14) return;

    this.setState({
      isSearching: true,
      pessoaId
    });

    await this.props.getEndereco();

    try {
      let especialidades = await deliveryRemote.especialidades();

      this.setState({ especialidades: [...especialidades] });
    } catch (e) {
    } finally {
      this.getData({});
    }
  };

  getData = async params => {
    this.setState({
      isSearching: true
    });

    const { endereco } = this.props;

    params = {
      ...params,
      cidadeId: endereco.cidadeId,
      bairroId: endereco.bairroId,
      cepId: endereco.cepId,
      latitude: endereco.latitude,
      longitude: endereco.longitude
    };

    try {
      const empresas = await this.props.getEmpresas(params);

      this.setState({
        empresas: empresas
      });
    } catch (e) {
      Toast.show({
        text: "Houve um problema ao carregar os restaurantes!"
      });
    } finally {
      this.setState({
        isSearching: false
      });
    }
  };

  navigateTo = async item => {
    const { itemsDelivery, empresaSelecionada } = this.props;

    if (
      empresaSelecionada.nome != item.nome &&
      itemsDelivery &&
      itemsDelivery.length
    ) {
      Alert.alert(
        "Aviso",
        "Existe um pedido em andamento para a empresa " +
          empresaSelecionada.nome +
          ", deseja continuar? Caso continue o pedido será descartado.",
        [
          {
            text: "Continuar",
            onPress: () => {
              this.props.clearPedido();
              this.irParaProdutos(item);
            }
          },
          {
            text: "Voltar",
            onPress: () => {}
          }
        ]
      );
    } else {
      this.irParaProdutos(item);
    }
  };

  irParaProdutos = async item => {
    item.chave = item.chaveGrupoEmpresarial;

    await empresaRemote.setEmpresaDefault(item);

    store.dispatch({
      type: "APP.EMPRESA.EMPRESA_SELECTED",
      payload: {
        empresaSelecionada: { ...item }
      }
    });

    this.props.navigation.navigate("DeliveryProdutos");
  };

  selecionarEspecialidade = async item => {
    if (this.state.itemSelected.id == item.id) {
      item = { id: "" };

      this.setState({
        itemSelected: { id: null }
      });
    } else {
      this.setState({
        itemSelected: { ...item }
      });
    }

    await this.getData({ especialiadeId: item.id });
  };

  renderItem = ({ item }) => {
    return (
      <EstabelecimentoItem
        onPress={() => this.navigateTo(item)}
        isDelivery={true}
        key={item.id}
        {...this.props}
        item={item}
      />
    );
  };

  render() {
    let {
      itemSelected,
      isSearching,
      empresas,
      especialidades,
      pessoaId
    } = this.state;

    if (pessoaId !== 14) {
      return (
        <ContainerPage>
          <PageEmpty content="Desculpe! Ainda estamos preparando as empresas da sua região. Em breve você poderá utilizar o nosso delivery =)" />
        </ContainerPage>
      );
    }

    return (
      <ContainerPage>
        <View>
          <Grid
            style={{
              paddingLeft: 8,
              paddingRight: 8
            }}
          >
            <FlatList
              data={especialidades}
              extraData={[]}
              showsHorizontalScrollIndicator={false}
              horizontal
              keyExtractor={menu => menu.id}
              renderItem={({ item }) => {
                return (
                  <TouchableOpacity
                    onPress={() => this.selecionarEspecialidade(item)}
                  >
                    <Card
                      noShadow
                      transparent
                      style={{
                        alignItems: "center",
                        paddingTop: 16,
                        paddingBottom: 8,
                        borderWidth: 0,
                        width: itemWidth
                      }}
                    >
                      <CardItem style={{ backgroundColor: "transparent" }}>
                        {!!item.imagemUrl && (
                          <Thumbnail source={item.imagemUrl} />
                        )}
                      </CardItem>

                      <View
                        style={[
                          styles.subtitleCategoria,
                          itemSelected.id == item.id
                            ? styles.selectedItem
                            : null
                        ]}
                      >
                        <TextCustom
                          style={{
                            textAlign: "center",
                            fontSize: 12,
                            color: itemSelected.id == item.id ? "white" : "#333"
                          }}
                        >
                          {item.nome}
                        </TextCustom>
                      </View>
                    </Card>
                  </TouchableOpacity>
                );
              }}
            />
          </Grid>
        </View>

        <View
          style={{
            padding: 12,
            top: -10,
            textAlign: "center",
            color: "#333"
          }}
        >
          <TextCustom style={{ fontSize: 18 }}>Restaurantes</TextCustom>
        </View>

        <View>
          <View
            style={{
              paddingLeft: isSearching ? 16 : 0,
              paddingRight: isSearching ? 16 : 0
            }}
          >
            <SkeletonLoading show={isSearching} />
            <SkeletonLoading show={isSearching} />
            <SkeletonLoading show={isSearching} />
            <SkeletonLoading show={isSearching} />
            <SkeletonLoading show={isSearching} />
            <SkeletonLoading show={isSearching}>
              <ScrollView
                refreshControl={
                  <RefreshControl onRefresh={() => this.componentDidMount()} />
                }
              >
                {!isSearching && !empresas.length ? (
                  <TextCustom
                    style={{
                      fontSize: 16,
                      paddingRight: 16,
                      paddingLeft: 16,
                      color: "#ccc",
                      textAlign: "center"
                    }}
                  >
                    Nenhuma empresa disponível no momento.
                  </TextCustom>
                ) : null}
                <FlatList data={empresas} renderItem={this.renderItem} />
              </ScrollView>
            </SkeletonLoading>
          </View>
        </View>
      </ContainerPage>
    );
  }
}

const styles = StyleSheet.create({
  subtitleCategoria: {
    top: -20,
    width: "90%",
    alignSelf: "center",
    borderRadius: 4,
    padding: 4,
    borderColor: colors.grayBorder,
    backgroundColor: "#efefef"
  },
  selectedItem: {
    backgroundColor: colors.primaryColor
  },
  selectedItemText: {
    color: colors.white
  }
});
