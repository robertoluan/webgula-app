import { connect } from "react-redux";
import component from "./EstabelecimentosScreen";
import { getEndereco } from "../../../services/actions/app/auth.actions";
import { removeAll } from "../../../services/actions/app/delivery.actions";
import authContext from "../../../infra/auth";
import EmpresaRemote from "../../../services/remote/empresa.remote";
import { INITIAL_STATE } from "../../../services/reducers/shared/list.reducer";

const empresaRemoteApi = new EmpresaRemote();

const mapStateToProps = (state, props) => {
  let loading = false;
  let endereco = {};

  const app = state.app || {};
  const delivery = app.delivery;
  const itemsDelivery = (delivery && delivery.items) || [];

  if (state.app && state.app.empresa) {
    loading = state.app.empresa.loading;
  }

  endereco = state.app.endereco.atual || {};

  return {
    loading,
    endereco,
    itemsDelivery,
    empresaSelecionada: app.empresa && app.empresa.empresaSelecionada
  };
};

const mapDispatchToProps = (dispatch, props) => ({
  async resetEmpresaSelecionada() {
    dispatch({
      type: "APP.EMPRESA.EMPRESA_SELECTED",
      payload: {
        empresaSelecionada: {}
      }
    });
  },
  async getEmpresas(params) {
    
    if (__DEV__) {
      params.latitude = "-15.5919269";
      params.longitude = "-56.1025646";
    }

    params.distancia = 10;

    const userData = await authContext.userDataAsync();

    params.pessoaId = userData.userInfo.pessoaId;

    return await empresaRemoteApi.findEmpresasDelivery(params);
  },
  async getEndereco() {
    return dispatch(getEndereco());
  },
  async clearPedido() {
    return dispatch(removeAll());
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(component);
