import React from "react";
import { View } from "react-native";
import { Container } from "native-base";
import EnderecoEntrega from "../../components/EnderecoEntrega";
import ViewBottom from "../../../../../shared/components/ViewBottom";
import { colors } from "../../../../../theme";
import ButtonLoading from "../../../../../shared/components/ButtonLoading";

export default class EnderecoEntregaScreen extends React.PureComponent {
  static navigationOptions = {
    title: "Endereço de Entrega"
  };

  render() {
    return (
      <Container style={{ paddingBottom: 40 }}>
        <View
          style={{
            paddingLeft: 8,
            paddingRight: 8
          }}
        >
          <EnderecoEntrega navigation={this.props.navigation} />
        </View>

        <ViewBottom>
          <ButtonLoading
            titleColor={colors.primaryColor}
            backgroundColor={"#efefef"}
            borderRadius={20}
            onPress={() => this.props.navigation.navigate("")}
            title="Continuar"
          />
        </ViewBottom>
      </Container>
    );
  }
}
