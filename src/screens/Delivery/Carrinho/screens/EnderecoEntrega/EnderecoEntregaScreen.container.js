import { connect } from "react-redux";
import component from "./EnderecoEntregaScreen";
import { removeAll } from "../../../../../services/actions/app/delivery.actions";

const mapStateToProps = (state, props) => {
  let app = state.app;
  const delivery = app.delivery;
  let items = (delivery && delivery.items) || [];

  return {
    items,
    endereco: (app.endereco && app.endereco.atual) || {}
  };
};

const mapDispatchToProps = (dispatch, props) => ({
  async clearPedido() {
    return dispatch(removeAll());
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(component);
