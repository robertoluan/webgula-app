import { connect } from "react-redux";
import component from "./FormaPagamentoScreen";

const mapStateToProps = (state, props) => {
  const app = state.app;
  const delivery = app.delivery;
  const items = (delivery && delivery.items) || [];

  return {
    items,
    endereco: (app.endereco && app.endereco.atual) || {}
  };
};

const mapDispatchToProps = (dispatch, props) => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(component);
