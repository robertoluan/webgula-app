import React from "react";
import { ListItem, Body, Right } from "native-base";
import { View, FlatList } from "react-native";
import ModalidadePagamento from "../../components/ModalidadePagamento";
import TextCustom from "../../../../../shared/components/TextCustom";
import ContainerPage from "../../../../../shared/components/ContainerPage";
import { formatMoney } from "../../../../../infra/helpers";

export default class FormaPagamentoScreen extends React.PureComponent {
  static navigationOptions = {
    title: <TextCustom> Forma Pagamento </TextCustom>
  };

  renderItem = ({ item }) => {
    return (
      <ListItem noIndent>
        <Body>
          <TextCustom style={{ fontSize: 14, marginBottom: 6 }}>
            {item.nome}
          </TextCustom>
        </Body>
        <Right>
          <TextCustom
            style={{
              fontSize: 14,
              marginBottom: 6,
              fontFamily: item.bold ? "Montserrat-Bold" : "Montserrat-Regular"
            }}
          >
            R$ {formatMoney(item.total)}
          </TextCustom>
        </Right>
      </ListItem>
    );
  };

  render = () => {
    const { endereco, items } = this.props;

    let valorTotal = 0;

    const itemsValores = [];

    items.forEach(item => (valorTotal += item.valorTotal));

    itemsValores.push({
      nome: "Valor dos Produtos",
      total: valorTotal
    });

    itemsValores.push({
      nome: "Taxa de Entrega",
      total: endereco.taxaEntrega
    });

    itemsValores.push({
      nome: "Valor Total",
      bold: true,
      total: valorTotal + endereco.taxaEntrega
    });

    return (
      <ContainerPage>
        <View style={{ marginTop: 16, marginBottom: 16 }}>
          <FlatList
            extraData={[]}
            data={itemsValores}
            renderItem={this.renderItem}
          />
        </View>

        <View style={{ marginTop: 0 }}>
          <ModalidadePagamento />
        </View>
      </ContainerPage>
    );
  };
}
