import React from "react";
import { Row, Col } from "native-base";
import { View, FlatList, StyleSheet, Alert } from "react-native";
import TextCustom from "../../../../../shared/components/TextCustom";
import Icon from "../../../../../shared/components/Icon";
import ProdutoItem from "../../components/ProdutoItem";
import Input from "../../../../../shared/components/Input";
import ContainerPage from "../../../../../shared/components/ContainerPage";

export default class ProdutosBody extends React.PureComponent {
  componentDidMount() {
    if (typeof this.props.onRef === "function") {
      this.props.onRef(this);
    }
  }
  renderItem = ({ item }) => {
    return (
      <ProdutoItem
        onRemove={() => this.onRemoveItem(item)}
        onPress={produto => this.navigateTo(produto)}
        item={item}
      />
    );
  };

  onRemoveItem = async item => {
    Alert.alert("Aviso", "Deseja remover o item do pedido ?", [
      {
        text: "Cancelar",
        onPress: () => {}
      },
      {
        text: "Confirmar",
        onPress: async () => this.props.removeItem(item.uuid)
      }
    ]);
  };

  navigateTo = produto => {
    this.props.navigation.navigate(`DeliveryProdutosGruposItem`, {
      item: produto
    });
  };

  get isCartEmpty() {
    const { items } = this.props;
    return !items || (items && !items.length);
  }

  reload = () => {
    this.forceUpdate();
  };

  render() {
    let { items, observacao, onChangeObservacao } = this.props;

    return (
      <ContainerPage style={{ paddingBottom: 40 }}>
        <FlatList data={items} extraData={[]} renderItem={this.renderItem} />

        {items.length ? (
          <View style={{ marginTop: 26, paddingLeft: 16, paddingRight: 16 }}>
            <Row>
              <Col size={100}>
                <TextCustom>Observação (máximo 50 caracteres)</TextCustom>
                <Input
                  color="#333"
                  placeholder="Escreva uma observação!"
                  style={styles.input}
                  transparent
                  maxLength={50}
                  multiline={true}
                  value={observacao}
                  onChangeText={value => onChangeObservacao(value)}
                />
              </Col>
            </Row>
          </View>
        ) : null}

        {this.isCartEmpty ? (
          <View style={styles.viewEmpty}>
            <Icon style={styles.icon} name="fa-shopping-basket" />
            <TextCustom style={styles.textEmpty}>
              Você não possui nenhum item no seu carrinho.
            </TextCustom>
          </View>
        ) : null}
      </ContainerPage>
    );
  }
}

const styles = StyleSheet.create({
  viewEmpty: {
    marginTop: 25,
    marginBottom: 25,
    textAlign: "center",
    alignSelf: "center",
    justifyContent: "center",
    paddingTop: 70
  },
  textEmpty: {
    fontSize: 18,
    flex: 1,
    paddingRight: 32,
    paddingLeft: 32,
    textAlign: "center",
    opacity: 0.6
  },
  icon: {
    flex: 1,
    textAlign: "center",
    alignSelf: "center",
    fontSize: 60,
    opacity: 0.4,
    marginBottom: 24
  },
  input: {
    paddingTop: 2,
    paddingLeft: 0,
    paddingBottom: 0,
    backgroundColor: "transparent",
    borderRadius: 4,
    color: "#333",
    marginBottom: 8
  }
});
