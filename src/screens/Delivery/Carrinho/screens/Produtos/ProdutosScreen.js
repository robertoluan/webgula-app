import React from "react";
import ContainerPage from "../../../../../shared/components/ContainerPage";
import ProdutosBody from "./ProdutosBody.container";

export default class ProdutosScreen extends React.PureComponent {
  static navigationOptions = {
    title: "Produtos"
  };

  render() {
    return (
      <ContainerPage style={{ paddingBottom: 40 }}>
        <ProdutosBody />
      </ContainerPage>
    );
  }
}
