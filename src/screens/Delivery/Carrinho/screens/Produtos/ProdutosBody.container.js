import { connect } from "react-redux";
import component from "./ProdutosBody";
import { removeItem } from "../../../../../services/actions/app/delivery.actions";

const mapStateToProps = (state, props) => {
  const app = state.app;
  const delivery = app.delivery;
  const items = (delivery && delivery.items) || [];
  const modalidadeId = (delivery && delivery.modalidadeId) || null;
  const observacao = (delivery && delivery.observacao) || null;

  return {
    items,
    modalidadeId,
    observacao,
    endereco: (app.endereco && app.endereco.atual) || {}
  };
};

const mapDispatchToProps = (dispatch, props) => ({
  async onChangeObservacao(value) {
    dispatch({
      type: "APP.DELIVERY.UPDATE_ITEM",
      payload: {
        observacao: value
      }
    });
  },
  async removeItem(uuid) {
    return dispatch(removeItem(uuid));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(component);
