import React from "react";
import Steps from "react-native-steps";
import { colors } from "../../../theme";
import { Container } from "native-base";
import ProdutosScreen from "./screens/Produtos";
import FormaPagamentoScreen from "./screens/FormaPagamento";
import EnderecoEntregaScreen from "./screens/EnderecoEntrega";
import ButtonFinalizar from "./components/ButtonFinalizar";
import ButtonLoading from "../../../shared/components/ButtonLoading";
import ViewBottom from "../../../shared/components/ViewBottom";
import ButtonHeaderDeliveryRight from "../../../shared/components/ButtonHeaderDeliveryRight";
import TextCustom from "../../../shared/components/TextCustom";

const configs = {
  stepIndicatorSize: 25,
  currentStepIndicatorSize: 30,
  separatorStrokeWidth: 2,
  currentStepStrokeWidth: 3,
  stepStrokeCurrentColor: colors.primaryColor,
  stepStrokeWidth: 3,
  stepStrokeFinishedColor: colors.primaryColor,
  stepStrokeUnFinishedColor: "#aaaaaa",
  separatorFinishedColor: colors.primaryColor,
  separatorUnFinishedColor: "#aaaaaa",
  stepIndicatorFinishedColor: colors.primaryColor,
  stepIndicatorUnFinishedColor: "#ffffff",
  stepIndicatorCurrentColor: "#ffffff",
  stepIndicatorLabelFontSize: 13,
  currentStepIndicatorLabelFontSize: 13,
  stepIndicatorLabelCurrentColor: colors.primaryColor,
  stepIndicatorLabelFinishedColor: "#ffffff",
  stepIndicatorLabelUnFinishedColor: "#aaaaaa",
  labelColor: "#999999",
  labelSize: 13,
  currentStepLabelColor: colors.primaryColor
};

export default class CarrinhoScreen extends React.PureComponent {
  state = {
    current: 0
  };
 
  static navigationOptions = ({ navigation }) => {
    return {
      title: " Pedido",
      headerRight: <ButtonHeaderDeliveryRight navigation={navigation} />
    };
  };

  onPageChange = position => this.setState({ current: position });

  render() {
    let { current } = this.state;
    const { items } = this.props;

    const labels = ["Produtos", "Endereço para Entrega", "Forma de Pagamento"];

    return (
      <Container style={{ paddingTop: 16 }}>
        <Steps
          count={3}
          configs={configs}
          current={current}
          labels={labels.map(label => (
            <TextCustom>{label}</TextCustom>
          ))}
          onPress={position => items.length && this.onPageChange(position)}
        />

        {current == 0 && <ProdutosScreen {...this.props} />}
        {current == 1 && <EnderecoEntregaScreen {...this.props} />}
        {current == 2 && <FormaPagamentoScreen {...this.props} />}

        {items.length ? (
          <ViewBottom>
            {current >= 0 && current <= 1 ? (
              <ButtonLoading
                titleColor={colors.primaryColor}
                backgroundColor={"#efefef"}
                borderRadius={20}
                onPress={() => this.onPageChange(current + 1)}
                title="Continuar"
              />
            ) : null}

            {current == 2 && (
              <ButtonFinalizar navigation={this.props.navigation} />
            )}
          </ViewBottom>
        ) : null}
      </Container>
    );
  }
}
