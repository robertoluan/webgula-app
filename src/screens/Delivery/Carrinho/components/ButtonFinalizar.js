import React from "react";
import { View, Alert } from "react-native";
import { connect } from "react-redux";
import { removeAll } from "../../../../services/actions/app/delivery.actions";
import DeliveryRemote from "../../../../services/remote/delivery.remote";
import ButtonLoading from "../../../../shared/components/ButtonLoading";
import { colors } from "../../../../theme";
import ModalSuccess from "../../../../shared/components/ModalSuccess/ModalSuccess";
import ModalPagamento from "../../../../shared/components/ModalPagamento/ModalPagamento";
import PageEmpty from "../../../../shared/components/PageEmpty";

const deliveryRemote = new DeliveryRemote();

const mapStateToProps = (state, props) => {
  const app = state.app;
  const delivery = app.delivery;

  const endereco = state.app.endereco.atual || {};
  const items = (delivery && delivery.items) || [];
  let modalidadeId = (delivery && delivery.modalidadeId) || null;
  let modalidadeNome = (delivery && delivery.modalidadeNome) || null;
  let valorPagamento = (delivery && delivery.valorPagamento) || null;

  let valorTotal = 0;

  items.forEach(x => (valorTotal += x.valorTotal));

  valorTotal += endereco.taxaEntrega || 0;

  return {
    valorTotal,
    items,
    modalidadeId,
    modalidadeNome,
    valorPagamento,
    endereco
  };
};

const mapDispatchToProps = (dispatch, props) => ({
  async finalizarPedido() {
    return deliveryRemote.checkout();
  },
  async clearPedido() {
    return dispatch(removeAll());
  },
  async setValorPagamento(value) {
    dispatch({
      type: "APP.DELIVERY.UPDATE_ITEM",
      payload: {
        valorPagamento: value
      }
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(
  class ButtonFinalizar extends React.PureComponent {
    state = {
      isLoading: false,
      success: false,
      pedido: {},
      openPagamento: false
    };

    navigateTo = pedido =>
      this.props.navigation.navigate(`DeliveryAcompanhamento`, { pedido });

    finalizarPedido = async () => {
      const {
        finalizarPedido,
        clearPedido,
        modalidadeId,
        modalidadeNome,
        valorPagamento,
        endereco,
        valorTotal
      } = this.props;

      if (!modalidadeId) {
        return Alert.alert("Aviso", "Selecione uma forma de pagamento!");
      }

      if (!endereco.nome) {
        return Alert.alert("Aviso", "Informa um endereço para entrega!");
      }

      if (modalidadeNome.toLowerCase().indexOf("dinheiro") > -1) {
        if (!valorPagamento) {
          Alert.alert("Aviso", "Informe o valor do pagamento.");
          return;
        }

        if (valorPagamento < valorTotal) {
          Alert.alert(
            "Aviso",
            "O Valor do pagamento não pode ser menor que o valor total do pedido."
          );
          return;
        }
      }

      this.setState({
        isLoading: true
      });

      try {
        const pedido = await finalizarPedido();

        this.setState({ pedido });

        clearPedido();

        this.props.navigation.navigate("DeliveryAcompanhamento", { pedido });
      } catch (e) {
        Alert.alert("Aviso", "Houve um problema ao finalizar o pedido!");
      } finally {
        this.setState({
          isLoading: false
        });
      }
    };

    componentWillUpdate = nextProps => {
      if (nextProps.modalidadeNome != this.props.modalidadeNome) {
        if (
          nextProps.modalidadeNome.toLowerCase().indexOf("dinheiro") > -1 &&
          !nextProps.valorPagamento
        ) {
          this.setState({
            openPagamento: true
          });
        }
      }
    };
    get isCartEmpty() {
      const { items } = this.props;
      return !items || (items && !items.length);
    }

    render() {
      const { pedido, endereco } = this.props;
      const { isLoading } = this.state;

      if (this.isCartEmpty) return <View />;

      if (!endereco || !endereco.nome)
        return (
          <PageEmpty content="Selecione o endereço para entrega antes de continuar!" />
        );

      return (
        <View>
          <ModalSuccess
            navigation={this.props.navigation}
            open={this.state.success}
            pedido={this.state.pedido}
          />

          <ModalPagamento
            open={this.state.openPagamento}
            onClose={value => {
              set;
              this.setState({ openPagamento: false });
            }}
          />

          {!pedido ? (
            <ButtonLoading
              titleColor={colors.primaryColor}
              backgroundColor={"#efefef"}
              borderRadius={20}
              loading={isLoading}
              onPress={() => this.finalizarPedido()}
              title="Realizar Pedido"
            />
          ) : (
            <ButtonLoading
              titleColor={colors.primaryColor}
              backgroundColor={"#efefef"}
              borderRadius={20}
              onPress={() => this.navigateTo(pedido)}
              title="Acompanhe o Pedido"
            />
          )}
        </View>
      );
    }
  }
);
