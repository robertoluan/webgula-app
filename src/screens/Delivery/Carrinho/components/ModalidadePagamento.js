import React from "react";
import { Left, ListItem, Toast, Radio, Right } from "native-base";
import { View, TouchableOpacity, FlatList } from "react-native";
import { connect } from "react-redux";
import TextCustom from "../../../../shared/components/TextCustom";
import SkeletonLoading from "../../../../shared/components/SkeletonLoading";
import DeliveryRemote from "../../../../services/remote/delivery.remote";

const deliveryRemove = new DeliveryRemote();

const mapStateToProps = (state, props) => {
  let app = state.app;
  const delivery = app.delivery;
  let modalidadeId = (delivery && delivery.modalidadeId) || null;

  return {
    modalidadeId: modalidadeId
  };
};

const mapDispatchToProps = (dispatch, props) => ({
  async load() {
    return deliveryRemove.getModalidadesPagamento();
  },
  async setModalidadePagamento(modalidadeId, modalidadeNome) {
    dispatch({
      type: "APP.DELIVERY.UPDATE_ITEM",
      payload: {
        modalidadeId,
        modalidadeNome
      }
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(
  class ModalidadePagamento extends React.PureComponent {
    state = {
      items: [],
      loading: false
    };

    async componentDidMount() {
      this.setState({
        loading: true
      });

      try {
        const data = await this.props.load();

        this.setState({
          items: data
        });
      } catch (e) {
        Toast.show({
          text: "Não foi possível carregar as modalidades de pagamento!"
        });
      } finally {
        this.setState({
          loading: false
        });
      }
    }

    render() {
      const { modalidadeId, setModalidadePagamento } = this.props;
      const { items, loading } = this.state;

      return (
        <View style={{ marginBottom: 10, marginTop: 10 }}>
          <SkeletonLoading show={loading}>
            <View>
              <FlatList
                extraData={[]}
                data={items}
                renderItem={({ item, key }) => (
                  <ListItem
                    noIndent
                    noBorder={key + 1 == items.length}
                    key={key}
                  >
                    <Left>
                      <TouchableOpacity
                        onPress={() =>
                          setModalidadePagamento(item.id, item.nome)
                        }
                      >
                        <TextCustom>{item.nome}</TextCustom>
                      </TouchableOpacity>
                    </Left>
                    <Right>
                      <Radio
                        onPress={() => setModalidadePagamento(item.id)}
                        selected={item.id == modalidadeId}
                      />
                    </Right>
                  </ListItem>
                )}
              />
            </View>
          </SkeletonLoading>
        </View>
      );
    }
  }
);
