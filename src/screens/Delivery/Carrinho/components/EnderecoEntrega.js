import React from "react";
import { View } from "react-native";
import { connect } from "react-redux";

import TextCustom from "../../../../shared/components/TextCustom";
import SkeletonLoading from "../../../../shared/components/SkeletonLoading";
import EnderecoItem from "../../../Auth/Endereco/EnderecoItem";
import { formatMoney } from "../../../../infra/helpers";

const mapStateToProps = (state, props) => {
  const endereco = state.app.endereco.atual || {};

  return {
    endereco
  };
};

const mapDispatchToProps = (dispatch, props) => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(
  class EnderecoEntrega extends React.Component {
    state = {
      open: false,
      openListaEndereco: false
    };
    trocarEndereco() {
      this.props.navigation.navigate("Endereco", {
        VeioExterno: true
      });
    }

    render() {
      const { endereco } = this.props;

      return (
        <SkeletonLoading>
          <View
            style={{
              backgroundColor: "white",
              padding: 8
            }}
          >
            <View style={{ flexDirection: "column", flex: 2 }}>
              <TextCustom
                style={{
                  fontSize: 11,
                  alignSelf: "flex-start",
                  flex: 1,
                  fontFamily: "Montserrat-Bold",
                  color: "#000"
                }}
              >
                Entregar em
              </TextCustom>
              <TextCustom
                style={{
                  fontSize: 11,
                  flex: 1,
                  alignSelf: "flex-end",
                  fontFamily: "Montserrat-Bold",
                  color: "#000"
                }}
              >
                Taxa de Entrega R$ {formatMoney(endereco.taxaEntrega)}
              </TextCustom>
            </View>

            <EnderecoItem
              item={this.props.endereco}
              onPress={() => this.trocarEndereco()}
            />
          </View>
        </SkeletonLoading>
      );
    }
  }
);
