import React from "react";
import { Thumbnail, Left, Body, ListItem, Right } from "native-base";
import styled from "styled-components/native";
import { TouchableOpacity } from "react-native";
import TextCustom from "../../../../shared/components/TextCustom";
import { colors } from "../../../../theme";
import { formatMoney } from "../../../../infra/helpers";
import Icon from "../../../../shared/components/Icon";

const imagem = require("../../../../../assets/images/noimage.png");

export default class ProdutoItem extends React.PureComponent {
  render = () => {
    let { item, onPress, onRemove } = this.props;
    let wrapperImagem = {};

    if (item.imagem && item.imagem.length) {
      wrapperImagem = {
        uri: item.imagem[0].url
      };
    } else {
      wrapperImagem = imagem;
    }

    return (
      <ViewStyle>
        <ListItem style={{ marginBottom: 0 }} noIndent noBorder thumbnail>
          <Left>
            <TouchableOpacity onPress={() => onPress && onPress(item)}>
              <Thumbnail
                borderRadius={5}
                resizeMode="contain"
                source={wrapperImagem}
              />
            </TouchableOpacity>
          </Left>

          <Body>
            <TouchableOpacity onPress={() => onPress && onPress(item)}>
              <TextCustom style={{ fontSize: 14, color: "#333" }}>
                {item.nomeCompleto}
              </TextCustom>
              <TextCustom
                note
                style={{
                  fontSize: 12,
                  textAlign: "left",
                  color: colors.primaryColor
                }}
              >
                {item.quantidade} x R$ {formatMoney(item.valorTotal / item.quantidade)} ={" R$ "}
                {formatMoney(item.valorTotal)}
              </TextCustom>
            </TouchableOpacity>
          </Body>
          {typeof onRemove === "function" && (
            <Right>
              <TouchableOpacity onPress={() => onRemove(item)}>
                <Icon
                  size={18}
                  style={{
                    opacity: 0.5
                  }}
                  name="fa-trash"
                />
              </TouchableOpacity>
            </Right>
          )}
        </ListItem>
      </ViewStyle>
    );
  };
}

const ViewStyle = styled.View`
  padding: 8px 0;
`;
