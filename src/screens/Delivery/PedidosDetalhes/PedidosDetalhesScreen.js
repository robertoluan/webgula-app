import React from "react";
import { Container, Content } from "native-base";
import {
  View,
  ScrollView,
  RefreshControl,
  FlatList,
  Alert
} from "react-native";
import TextCustom from "../../../shared/components/TextCustom";
import ButtonHeaderDeliveryRight from "../../../shared/components/ButtonHeaderDeliveryRight";
import SkeletonLoading from "../../../shared/components/SkeletonLoading";
import PedidoItem from "./components/PedidoItem";

export default class PedidosDetalhesScreen extends React.Component {
  state = {
    isSearching: false
  };

  static navigationOptions = ({ navigation }) => {
    return {
      title: "Pedidos",
      headerRight: <ButtonHeaderDeliveryRight navigation={navigation} />
    };
  };

  componentDidMount = async () => {
    this.setState({
      isSearching: true
    });

    try {
      await this.props.getPedidos();
    } catch (e) {
      Alert.alert("Aviso", "Houve um problema ao trazer a lista de pedidos.");
    } finally {
      this.setState({
        isSearching: false
      });
    }
  };

  navigateTo = async pedido => {
    this.props.navigation.navigate("DeliveryCarrinho", { pedido });
  };

  renderItem = ({ item }) => {
    return (
      <PedidoItem
        onPress={() => this.navigateTo(item)}
        key={item.id}
        {...this.props}
        item={item}
      />
    );
  };

  render() {
    let { isSearching } = this.state;
    const items = this.props.pedidos;

    return (
      <Container
        style={{
          backgroundColor: "#f5f5f5"
        }}
      >
        <Content>
          <View
            style={{
              padding: 12,
              textAlign: "center",
              color: "#333"
            }}
          >
            <TextCustom>Pedidos</TextCustom>
          </View>

          <View>
            <View
              style={{
                paddingLeft: isSearching ? 16 : 0,
                paddingRight: isSearching ? 16 : 0
              }}
            >
              <SkeletonLoading show={isSearching} />
              <SkeletonLoading show={isSearching} />
              <SkeletonLoading show={isSearching} />
              <SkeletonLoading show={isSearching} />
              <SkeletonLoading show={isSearching}>
                <ScrollView
                  refreshControl={
                    <RefreshControl
                      onRefresh={() => this.componentDidMount()}
                    />
                  }
                >
                  <FlatList data={items} renderItem={this.renderItem} />
                </ScrollView>
              </SkeletonLoading>
            </View>
          </View>
        </Content>
      </Container>
    );
  }
}
