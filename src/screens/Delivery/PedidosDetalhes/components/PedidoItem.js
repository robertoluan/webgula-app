import React from "react";
import { View } from "react-native";
import { ListItem, Left, Body, Card } from "native-base";
import TextCustom from "../../../../shared/components/TextCustom";
import { formatMoney } from "../../../../infra/helpers";
import moment from "moment";

export default class PedidoItem extends React.PureComponent {
  render() {
    let { item, onPress } = this.props;

    return (
      //
      <ListItem
        style={{
          paddingBottom: 8
        }}
        noIndent={true}
        avatar
        noBorder={true}
        itemDivider={false}
        onPress={onPress}
      >
        <Left>
          <View
            style={{
              flexDirection: "column",
              backgroundColor: "white",
              borderRadius: 4,
              paddingLeft: 4,
              paddingRight: 4,
              paddingTop: 8,
              paddingBottom: 8
            }}
          >
            <View>
              <TextCustom style={{ textAlign: "center" }}>
                {moment(item.dataHoraPedido).format("DD")}
              </TextCustom>
            </View>
            <View>
              <TextCustom style={{ textAlign: "center", fontSize: 11 }}>
                {moment(item.dataHoraPedido).format("MM/YYYY")}
              </TextCustom>
            </View>
          </View>
        </Left>

        <Body>
          <TextCustom style={{ fontSize: 13, color: "#333" }}>
            {item.empresa.nome} R$ {formatMoney(item.valorProduto)}
          </TextCustom>
        </Body>
      </ListItem>
    );
  }
}
