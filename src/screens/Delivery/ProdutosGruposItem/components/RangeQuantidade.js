import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import TextCustom from '../../../../shared/components/TextCustom';
import Icon from '../../../../shared/components/Icon';
import { parseDoubleCustom } from '../../../../infra/helpers';

class RangeQuantidade extends React.PureComponent {
  onChange = (add = true) => {
    let { value, step = 1, onChange, min = 0, max = 9999 } = this.props;

    if (add) {
      value = value - step;
    } else {
      value = value + step;
    }

    if (value < min) {
      value = min;
    }
    if (value > max) {
      value = max;
    }

    value = parseDoubleCustom(value);

    onChange(value);
  };
  render = () => {
    let { value, min = 0, max = 9999, textSize = 16 } = this.props;

    value = value || 0;

    return (
      <View
        style={{
          flexDirection: 'row'
        }}
      >
        <View style={{ width: 25, textAlign: 'center' }}>
          <TouchableOpacity onPress={() => this.onChange(true)}>
            <Icon
              name="fa-minus"
              size={18}
              style={{
                color: value === min ? '#ddd' : '#333',
                textAlign: 'center'
              }}
            />
          </TouchableOpacity>
        </View>
        <View style={{ width: 25 }}>
          <TextCustom
            style={{ fontSize: textSize, textAlign: 'center', color: '#333' }}
          >
            {parseDoubleCustom(value, 2)}
          </TextCustom>
        </View>
        <View style={{ width: 25 }}>
          <TouchableOpacity onPress={() => this.onChange(false)}>
            <Icon
              name="fa-plus"
              size={18}
              style={{
                color: value === max ? '#ddd' : '#333',
                textAlign: 'center'
              }}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  };
}

export default RangeQuantidade;
