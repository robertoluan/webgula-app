import React from 'react';
import { ListItem, Right, Radio, Body } from 'native-base';
import { View } from 'react-native';
import TextCustom from '../../../../shared/components/TextCustom';
import RangeQuantidade from './RangeQuantidade';
import { formatMoney } from '../../../../infra/helpers';

export default class RenderVariacaoItem extends React.PureComponent {
  state = {
    maximoDisponivelParaVariacao: this.props.item.qtdeMax
  };

  componentDidMount = () => {
    this.updateMaximoDisponivelParaVariacao();
  };

  calculeQuantidadeMaxima = variacao => {
    let quantidadeJaUsada = 0;

    variacao.items.forEach(item => {
      quantidadeJaUsada += item.quantidade || 0;
    });

    return variacao.qtdeMax - quantidadeJaUsada;
  };

  onChangeQuantidade = (value, item) => {
    const variacao = this.props.item;

    let quantidadeJaSelecionada = 0;

    variacao.items.forEach(itemFor => {
      quantidadeJaSelecionada += itemFor.quantidade;

      if (itemFor.id === item.id) {
        itemFor.quantidade = value;
      }
    });

    variacao.quantidadeJaSelecionada = quantidadeJaSelecionada;

    this.props.onChange(variacao);

    this.updateMaximoDisponivelParaVariacao();
  };

  updateMaximoDisponivelParaVariacao = () => {
    const variacao = this.props.item;

    this.setState({
      maximoDisponivelParaVariacao: this.calculeQuantidadeMaxima(variacao)
    });
  };

  onChangeQuantidadeRadio = item => {
    const variacao = this.props.item;

    let quantidadeJaSelecionada = 0;

    variacao.items.forEach(itemFor => {
      quantidadeJaSelecionada += itemFor.quantidade;

      itemFor.quantidade = 0;

      if (itemFor.id == item.id) {
        itemFor.quantidade = 1;
      }
    });

    variacao.quantidadeJaSelecionada = quantidadeJaSelecionada;

    this.props.onChange(variacao);
  };

  renderMultiple = item => {
    const { maximoDisponivelParaVariacao } = this.state;

    item.quantidade = item.quantidade || 0;

    return (
      <RangeQuantidade
        min={0}
        max={item.quantidade + maximoDisponivelParaVariacao}
        value={item.quantidade}
        onChange={value => this.onChangeQuantidade(value, item)}
      />
    );
  };

  render = () => {
    const { item, tamanhoId } = this.props;

    const items = (item.items || []).filter(x => x.tamanhoId === tamanhoId);

    if (items.length === 0) return <View />;

    return (
      <View
        style={{
          backgroundColor: 'white',
          marginTop: 20,
          marginLeft: 16,
          marginRight: 16,
          paddingLeft: 16,
          paddingRight: 16,
          paddingTop: 16,
          borderRadius: 4
        }}
      >
        <View
          style={{
            flexDirection: 'column',
            marginBottom: 6,
            marginTop: 6,
            paddingLeft: 0,
            paddingRight: 16
          }}
        >
          <View>
            <TextCustom
              style={{
                fontSize: 16,
                fontFamily: 'Montserrat-Bold'
              }}
            >
              {item.nomeMostra}
            </TextCustom>
          </View>
          <View>
            {item.qtdeMax > 1 ? (
              <TextCustom style={{ fontSize: 12, color: '#999' }}>
                Escolha as {item.qtdeMax} partes deste produto.
              </TextCustom>
            ) : null}
          </View>
        </View>

        {items.map((itemFor, key) => (
          <ListItem
            style={{ marginLeft: 0, marginRight: 0, paddingRight: 0 }}
            key={key}
          >
            <Body>
              <View style={{ flexDirection: 'column' }}>
                <View>
                  <TextCustom
                    onPress={() =>
                      item.qtdeMax === 1 &&
                      this.onChangeQuantidadeRadio(itemFor)
                    }
                    style={{
                      fontSize: 14
                    }}
                  >
                    {itemFor.nomeCompleto}
                  </TextCustom>
                </View>

                {!!itemFor.textoInformativo ? (
                  <View>
                    <TextCustom
                      style={{
                        fontSize: 12,
                        color: '#999'
                      }}
                      onPress={() =>
                        item.qtdeMax == 1 &&
                        this.onChangeQuantidadeRadio(itemFor)
                      }
                    >
                      {itemFor.textoInformativo}
                    </TextCustom>
                  </View>
                ) : null}

                <View>
                  <TextCustom
                    style={{
                      fontSize: 14,
                      marginTop: 5,
                      fontFamily: item.selected
                        ? 'Montserrat-Bold'
                        : 'Montserrat-Regular'
                    }}
                    onPress={() =>
                      item.qtdeMax == 1 && this.onChangeQuantidadeRadio(itemFor)
                    }
                  >
                    R$ {formatMoney(itemFor.preco)}
                  </TextCustom>
                </View>
              </View>
            </Body>

            <Right>
              <View style={{ textAlign: 'right' }}>
                {item.qtdeMax === 1 ? (
                  <Radio
                    onPress={() => this.onChangeQuantidadeRadio(itemFor)}
                    selected={itemFor.quantidade === 1}
                  />
                ) : (
                  this.renderMultiple(itemFor)
                )}
              </View>
            </Right>
          </ListItem>
        ))}
      </View>
    );
  };
}
