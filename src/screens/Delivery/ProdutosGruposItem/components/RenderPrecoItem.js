import React from "react";
import { Left, ListItem, Right, Radio } from "native-base";
import { View } from "react-native";
import { formatMoney } from "../../../../infra/helpers";
import TextCustom from "../../../../shared/components/TextCustom";

export default class RenderPrecoItem extends React.Component {
  render() {
    const { item, key, onPress } = this.props;

    return (
      <ListItem style={{ marginLeft: 0 }} key={key}>
        <Left>
          <View style={{ flexDirection: "column" }}>
            <View>
              <TextCustom
                style={{
                  fontSize: 14
                }}
                onPress={() => (onPress ? onPress(item) : null)}
              >
                {item.tamanhoNome}
              </TextCustom>
            </View>

            <View>
              <TextCustom
                style={{
                  fontSize: 14
                }}
                onPress={() => (onPress ? onPress(item) : null)}
              >
                R$ {formatMoney(item.valorPreco)}
              </TextCustom>
            </View>
          </View>
        </Left>

        {onPress && (
          <Right>
            <Radio
              onPress={() => onPress(item)}
              selected={item.selected}
            />
          </Right>
        )}
      </ListItem>
    );
  }
}
