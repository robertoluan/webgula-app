import React from "react"; 
import { View } from "react-native";
import { formatMoney } from "../../../../infra/helpers";
import TextCustom from "../../../../shared/components/TextCustom";

export default class RenderPrecoUnico extends React.Component {
  render() {
    const { item } = this.props;

    if (!item) return <View />;
    if (!item.precos) return <View />;
    if (!Array.isArray(item.precos)) return <View />;

    let preco = 0;

    if (item.precos.length === 0) {
      preco = 0;
    }

    if (item.precos.length === 1) {
      preco = item.precos[0].valorPreco;
    }

    return (
      <View
        style={{
          flex: 1,
          flexDirection: "row",
          justifyContent: "center",
          textAlign: "center"
        }}
      >
        <TextCustom
          style={{
            top: 17,
            fontSize: 18,
            right: 10
          }}
        >
          R$
        </TextCustom>

        <TextCustom
          style={{
            fontSize: 36,
            fontFamily: "Montserrat-Bold",
            color: "#222"
          }}
        >
          {formatMoney(preco)}
        </TextCustom>
      </View>
    );
  }
}
