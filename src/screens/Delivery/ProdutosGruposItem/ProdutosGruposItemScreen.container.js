import { connect } from "react-redux";
import component from "./ProdutosGruposItemScreen";
import EmpresaApi from "../../../services/remote/empresa.remote";
import { addItem } from "../../../services/actions/app/delivery.actions";

const empresaApi = new EmpresaApi();

const mapStateToProps = (state, props) => {
  const app = state && state.app;
  const delivery = app.delivery;
  const origem = (delivery && delivery.origem) || null;
  return {
    origem
  };
};

const mapDispatchToProps = (dispatch, props) => ({
  async load(itemId) {
    return empresaApi.findCardapioItem(itemId);
  },
  addCart(item) {
    return dispatch(addItem(item));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(component);
