import React from 'react';
import { Container, List, Toast, Row, Col } from 'native-base';
import {
  View,
  Image,
  Dimensions,
  ScrollView,
  Alert,
  FlatList,
  StyleSheet
} from 'react-native';
import colors from '../../../theme/colors';
import TextCustom from '../../../shared/components/TextCustom';
import ViewBottom from '../../../shared/components/ViewBottom';
import ButtonLoading from '../../../shared/components/ButtonLoading';
import ButtonHeaderDeliveryRight from '../../../shared/components/ButtonHeaderDeliveryRight';
import RenderPrecoItem from './components/RenderPrecoItem';
import RenderPrecoUnico from './components/RenderPrecoUnico';
import SkeletonLoading from '../../../shared/components/SkeletonLoading';
import RangeQuantidade from './components/RangeQuantidade';
import RenderVariacaoItem from './components/RenderVariacaoItem';
import Input from '../../../shared/components/Input';
import { formatMoney } from '../../../infra/helpers';

const imagem = require('../../../../assets/images/noimage.png');

export default class ProdutosGruposItemScreen extends React.Component {
  state = {
    produto: {
      precos: [],
      variacoes: []
    },
    empresa: {},
    isLoading: false,
    adicionado: false
  };

  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Detalhe do Item',
      headerRight: <ButtonHeaderDeliveryRight navigation={navigation} />
    };
  };

  componentDidMount = async () => {
    const item = this.props.navigation.getParam('item') || {};

    const produto = { precos: [], variacoes: [], quantidade: 1, ...item };

    produto.variacoes = produto.variacoes || [];

    produto.variacoes = produto.variacoes.sort((a, b) =>
      a.flgTipo > b.flgTipo ? 1 : -1
    );

    this.setState({
      isLoading: true,
      adicionado: false,
      produto: { ...produto }
    });

    try {
      if (!produto.precos.length) {
        const response = await this.props.load(item.id);

        if (response) {
          produto.precos = response.valorPrecoItem || [];

          produto.precos = produto.precos.sort(
            (a, b) => a.valorPreco - b.valorPreco
          );
        } else {
          produto.precos = [{ valorPreco: produto.preco, selected: true }];
        }
      }

      let precoJaSelecionado = !!produto.precos.filter(x => x.selected)[0];

      if (!precoJaSelecionado && produto.precos.length) {
        produto.precos[0].selected = true;
      }

      if (!produto.jaEstaNoCarrinho) {
        produto.variacoes.forEach(variacao =>
          variacao.items.forEach(item => (item.quantidade = 0))
        );
      }

      this.setState({
        produto: { ...produto }
      });
    } catch (e) {
      Alert.alert('Aviso', 'Houve um problema ao trazer o item selecionado.');
    } finally {
      this.setState({
        isLoading: false
      });
    }
  };

  adicionar = () => {
    const { produto } = this.state;

    let precoSelecionado = null;

    if (produto.precos && produto.precos.length) {
      precoSelecionado = produto.precos.filter(x => x.selected)[0] || {};

      produto.tamanhoId = precoSelecionado.tamanhoId;
    }

    if (!produto.tamanhoId) {
      return Alert.alert('Aviso', 'Informe o tamanho do produto!');
    }

    let possuiAlgumDadoInvalido = false;

    let variacoesUsadas = produto.variacoes.filter(variacao => {
      return !!variacao.items.filter(
        item => item.tamanhoId === precoSelecionado.tamanhoId
      )[0];
    });

    variacoesUsadas.forEach(variacao => {
      if (possuiAlgumDadoInvalido) return;

      let quantidadeTotalSelecionada = 0;

      variacao.items.forEach(item => {
        quantidadeTotalSelecionada += item.quantidade;
      });

      if (variacao.qtdeMax === 1) {
        if (quantidadeTotalSelecionada === 0) {
          possuiAlgumDadoInvalido = true;

          return Alert.alert(
            'Aviso',
            'Ops. Você precisa informar algum(a) ' + variacao.nomeMostra
          );
        }
      } else {
        if (quantidadeTotalSelecionada < variacao.qtdeMax) {
          possuiAlgumDadoInvalido = true;

          return Alert.alert(
            'Aviso',
            'Ops. Você precisa informar ' +
              variacao.qtdeMax +
              ' quantidade(s) de ' +
              variacao.nomeMostra
          );
        }
      }
    });

    if (possuiAlgumDadoInvalido) return;

    this.props.addCart({
      ...produto,
      valorPreco: precoSelecionado.valorPreco,
      valorDesconto: produto.valorDesconto || 0,
      jaEstaNoCarrinho: true,
      valorTotal: this.getValorTotalProduto(produto),
      variacoesUsadas: variacoesUsadas
    });

    if (produto.jaEstaNoCarrinho) {
      if (this.props.origem === 'conta-credito') {
        return this.props.navigation.navigate(
          'EstabelecimentoCreditoResumoPedido'
        );
      } else {
        return this.props.navigation.navigate('DeliveryCarrinho');
      }
    }

    Toast.show({
      text: 'Produto adicionado ao carrinho com sucesso!'
    });

    this.props.navigation.navigate('DeliveryProdutos');
  };

  continuar = () => this.props.navigation.navigate('DeliveryProdutos');

  changeQuantidade = value => {
    const { produto } = this.state;

    this.setState({
      produto: { ...produto, quantidade: value }
    });
  };

  changeVariacao = variacao => {
    let { produto } = this.state;

    produto.variacoes.forEach(item => {
      if (item.id === variacao.id) {
        item = {
          ...variacao
        };
      }
    });

    this.setState({
      produto: { ...produto }
    });
  };

  changeObservacao = value => {
    const { produto } = this.state;

    produto.observacao = value;

    this.setState({
      produto: { ...produto }
    });
  };

  selecionarPreco = preco => {
    const { produto } = this.state;

    produto.precos.forEach(p => (p.selected = p == preco));

    produto.quantidade = 1;

    produto.variacoes.forEach(variacao => {
      variacao.quantidadeJaSelecionada = 0;

      variacao.items.forEach(item => {
        item.quantidade = 0;
      });
    });

    this.setState({ produto: { ...produto } });
  };

  renderLista = items => {
    items = items || [];

    return (
      <List>
        {items.map((preco, key) => (
          <RenderPrecoItem
            onPress={() => this.selecionarPreco(preco)}
            item={preco}
            key={key}
          />
        ))}
      </List>
    );
  };

  getTamanhoSelecionado = () => {
    const { produto } = this.state;

    const tamanho = produto.precos.filter(x => x.selected)[0] || {};

    return tamanho.tamanhoId;
  };

  getValorTotalProduto = item => {
    let valorTotal = 0;

    let precoSelecionado = item.precos.filter(x => x.selected)[0];

    if (!precoSelecionado) return 0;

    let possuiVariacaoMultiplaSelecionada = false;

    item.variacoes.forEach(variacao => {
      const divisaoMultipla = variacao.flgTipo === 'M';

      const itemsSelecionados = variacao.items.filter(x => x.quantidade);

      let quantidadesSelecionada = 0;

      itemsSelecionados.forEach(
        item => (quantidadesSelecionada += item.quantidade)
      );

      if (divisaoMultipla && quantidadesSelecionada > 0) {
        possuiVariacaoMultiplaSelecionada = true;
      }

      let totalItemsVariacao = 0;

      itemsSelecionados.forEach(variacaoitem => {
        let quantidade = variacaoitem.quantidade;

        if (divisaoMultipla) {
          if (itemsSelecionados.length > 1) {
            quantidade = quantidade / quantidadesSelecionada;
          } else {
            quantidade = 1;
          }
        }

        totalItemsVariacao += quantidade * variacaoitem.preco;
      });

      valorTotal += totalItemsVariacao * (item.quantidade || 1);
    });

    if (!possuiVariacaoMultiplaSelecionada) {
      valorTotal += precoSelecionado.valorPreco * item.quantidade;
    }

    valorTotal -= item.valorDesconto || 0;

    return valorTotal;
  };

  render = () => {
    let { produto, isLoading } = this.state;

    let wrapperImagem = {};

    if (produto && produto.imagem && produto.imagem.length) {
      wrapperImagem = {
        uri: produto.imagem[0].url
      };
    } else {
      wrapperImagem = imagem;
    }

    return (
      <Container
        style={{
          backgroundColor: '#f5f5f5',
          flex: 1,
          paddingBottom: 80
        }}
      >
        <View style={{ marginBottom: 16 }}>
          <ScrollView>
            <View
              style={{
                marginTop: 0,
                left: 0,
                top: -10,
                right: 0
              }}
            >
              <Image
                style={{
                  width: Dimensions.get('window').width,
                  height: Dimensions.get('window').width / 2
                }}
                source={wrapperImagem}
              />
            </View>

            <View
              style={{
                top: -40,
                paddingRight: 16,
                paddingLeft: 16,
                paddingBottom: 12,
                marginRight: 16,
                marginLeft: 16,
                backgroundColor: 'white',
                borderRadius: 4
              }}
            >
              <TextCustom
                style={{
                  fontSize: 18,
                  textAlign: 'center',
                  fontFamily: 'Montserrat-Bold',
                  marginBottom: 26,
                  marginTop: 16,
                  color: colors.primaryColor
                }}
              >
                {produto.nomeCompleto}
              </TextCustom>

              <SkeletonLoading show={isLoading}>
                <View>
                  {produto.precos.length > 1 ? (
                    this.renderLista(produto.precos)
                  ) : (
                    <RenderPrecoUnico item={produto} />
                  )}
                </View>

                <View
                  style={{
                    alignItems: 'center',
                    marginTop: 20,
                    marginBottom: 10
                  }}
                >
                  <RangeQuantidade
                    min={1}
                    max={10}
                    onChange={value => this.changeQuantidade(value)}
                    value={produto.quantidade}
                  />
                </View>
              </SkeletonLoading>
            </View>

            <SkeletonLoading show={isLoading}>
              <View style={{ marginTop: -10 }}>
                <FlatList
                  data={produto.variacoes}
                  extraData={produto}
                  renderItem={({ item }) => (
                    <RenderVariacaoItem
                      tamanhoId={this.getTamanhoSelecionado()}
                      onChange={variacao => this.changeVariacao(variacao)}
                      item={item}
                    />
                  )}
                />
              </View>

              <View
                style={{
                  marginTop: 26,
                  marginLeft: 16,
                  marginRight: 16,
                  paddingLeft: 16,
                  paddingRight: 16,
                  paddingTop: 16,
                  borderRadius: 4,
                  backgroundColor: 'white'
                }}
              >
                <Row>
                  <Col>
                    <TextCustom>Observação</TextCustom>
                    <Input
                      color="#333"
                      placeholder="Escreva uma observação!"
                      style={styles.input}
                      transparent
                      numberOfLines={3}
                      multiline={true}
                      value={produto.observacao}
                      onChangeText={value => this.changeObservacao(value)}
                    />
                  </Col>
                </Row>
              </View>
            </SkeletonLoading>
          </ScrollView>
        </View>

        <ViewBottom
          style={{
            paddingTop: 12,
            paddingBottom: 12,
            marginLeft: 0,
            marginRight: 0,
            paddingLeft: 16,
            paddingRight: 16,
            marginBottom: 0,
            backgroundColor: '#FFF'
          }}
        >
          <Row>
            <Col size={35}>
              <View
                style={{
                  marginRight: 6,
                  height: 45,
                  alignItems: 'center',
                  justifyContent: 'center'
                }}
              >
                <TextCustom style={{ fontSize: 18 }}>
                  R$ {formatMoney(this.getValorTotalProduto(produto))}{' '}
                </TextCustom>
              </View>
            </Col>

            <Col size={65}>
              <ButtonLoading
                titleColor="yellow"
                disabled={isLoading}
                onPress={() => this.adicionar()}
                full
                borderRadius={20}
                title={
                  produto.jaEstaNoCarrinho
                    ? 'Atualizar Produto'
                    : 'Adicionar no Carrinho'
                }
              />
            </Col>
          </Row>
        </ViewBottom>
      </Container>
    );
  };
}

const styles = StyleSheet.create({
  input: {
    paddingTop: 2,
    paddingLeft: 0,
    paddingBottom: 0,
    backgroundColor: 'transparent',
    borderColor: '#ccc',
    borderWidth: 2,
    borderRadius: 4,
    color: '#333',
    marginBottom: 8
  }
});
