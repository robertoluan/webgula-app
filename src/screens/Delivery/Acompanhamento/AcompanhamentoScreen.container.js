import { connect } from "react-redux";
import component from "./AcompanhamentoScreen"; 
import DeliveryRemote from "../../../services/remote/delivery.remote";

const deliveryRemote = new DeliveryRemote();

const mapStateToProps = (state, props) => {
  const pedidos = state.app.delivery.pedidos || [];

  return {
    pedidos
  };
};

const mapDispatchToProps = (dispatch, props) => ({
  async getAcompanhamento(pedidoId) {
    return deliveryRemote.getAcompanhamento(pedidoId);
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(component);
