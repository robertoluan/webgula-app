import React from "react";
import { ScrollView, View } from "react-native";
import { Container, Content } from "native-base";
import Timeline from "react-native-timeline-listview";
import ButtonHeaderDeliveryRight from "../../../shared/components/ButtonHeaderDeliveryRight";
import ButtonLoading from "../../../shared/components/ButtonLoading";
import moment from "moment";
import SkeletonLoading from "../../../shared/components/SkeletonLoading";
import ContainerPage from "../../../shared/components/ContainerPage";

export default class AcompanhamentoScreen extends React.Component {
  interval = null;

  state = {
    pedido: {},
    data: [],
    loading: false
  };
 

  static navigationOptions = ({ navigation }) => {
    return {
      title: "Acompanhe o Pedido",
      headerRight: <ButtonHeaderDeliveryRight navigation={navigation} />
    };
  };

  navigateTo = () => {
    this.props.navigation.replace(`Home`);
  };

  componentDidMount = async () => this.listenerNotification();

  listenerNotification = async () => {
    const pedido = this.props.navigation.getParam("pedido");

    this.setState({
      pedido,
      loading: true
    });

    const acompanhamentos = await this.props.getAcompanhamento(pedido.id);

    if (!Array.isArray(acompanhamentos)) {
      this.setState({
        loading: false
      });
      return;
    }

    let pedidoFinalizadoOuCancelado = false;

    this.setState(
      {
        loading: false,
        data: acompanhamentos.map(item => {
          if (item.situacaoPedidoid == 5 || item.situacaoPedidoid == 2) {
            pedidoFinalizadoOuCancelado = true;
          }
          return {
            time: moment(item.data.substr(0, 19)).format("DD/MM/YYYY h:mm"),
            title: item.situacaoPedido,
            description: ""
          };
        })
      },
      () => this.forceUpdate()
    );

    if (pedidoFinalizadoOuCancelado) {
      clearTimeout(this.interval);
      return;
    }

    this.interval = setTimeout(
      async () => await this.listenerNotification(),
      20000
    );
  };

  componentWillUnmount = () => {
    if (this.interval) {
      clearTimeout(this.interval);
    }
  };

  render = () => {
    const { data } = this.state;

    return (
      <ContainerPage>
        <Content>
          <View style={{ padding: 16, flex: 1 }}>
            <ScrollView>
              <SkeletonLoading show={this.state.loading}>
                <Timeline
                  data={data}
                  circleSize={10}
                  circleColor="rgba(0,0,0, .3)"
                  lineColor="rgb(45, 156, 219)"
                  timeContainerStyle={{ minWidth: 52, marginTop: -5 }}
                  timeStyle={{
                    textAlign: "center",
                    backgroundColor: "#ff9797",
                    color: "white",
                    padding: 5,
                    borderRadius: 13
                  }}
                  descriptionStyle={{ color: "gray" }}
                  options={{
                    style: { paddingTop: 5 }
                  }}
                />
              </SkeletonLoading>
            </ScrollView>
          </View>
        </Content>
        <View
          style={{
            margin: 16,
            position: "absolute",
            left: 0,
            bottom: 0,
            right: 0
          }}
        >
          <ButtonLoading
            title="Voltar ao Inicio"
            block
            full
            onPress={() => this.navigateTo()}
          />
        </View>
      </ContainerPage>
    );
  };
}
