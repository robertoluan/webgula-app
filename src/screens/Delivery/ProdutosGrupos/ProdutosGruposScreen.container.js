import { connect } from "react-redux";
import component from "./ProdutosGruposScreen";
import EmpresaApi from "../../../services/remote/empresa.remote";

const empresaApi = new EmpresaApi();

const mapStateToProps = (state, props) => {
  return {};
};

const mapDispatchToProps = (dispatch, props) => ({
  async load(grupoId) {
    return empresaApi.findCardapio(grupoId, 3);
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(component);
