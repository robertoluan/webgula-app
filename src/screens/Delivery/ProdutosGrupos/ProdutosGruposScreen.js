import React from "react";
import HeaderEmpresa from "../../../shared/components/HeaderEmpresa";
import styled from "styled-components/native";
import { FlatList, ScrollView } from "react-native";
import EmpresaRemote from "../../../services/remote/empresa.remote";
import SkeletonLoading from "../../../shared/components/SkeletonLoading";
import ButtonHeaderDeliveryRight from "../../../shared/components/ButtonHeaderDeliveryRight";
import CardapioGrupoProduto from "../../Menu/EstabelecimentoCardapioProduto/CardapioGrupoProduto";
import ContainerPage from "../../../shared/components/ContainerPage";

const empresaRemote = new EmpresaRemote();

export default class ProdutosGruposScreen extends React.Component {
  state = {
    items: [],
    empresa: {},
    loading: false,
    item: {}
  };

  static navigationOptions = ({ navigation }) => {
    return {
      title: "Produtos",
      headerRight: <ButtonHeaderDeliveryRight navigation={navigation} />
    };
  };

  componentDidMount = async () => {
    const item = this.props.navigation.getParam("grupo") || {};

    this.setState({
      item: { ...item },
      loading: true
    });

    const { id } = item;

    const empresa = await empresaRemote.getEmpresaDefault();

    const cardapio = await this.props.load(id); // GRUPO 0 porque é o unico

    this.setState({
      items: cardapio,
      empresa,
      loading: false
    });
  };

  navigateTo = item => {
    this.props.navigation.navigate(`DeliveryProdutosGruposItem`, { item });
  };

  renderItem = ({ item }) => {
    return (
      <CardapioGrupoProduto
        item={item}
        onPress={produto => this.navigateTo(produto)}
      />
    );
  };

  render() {
    let { items, item, loading } = this.state;

    return (
      <ContainerPage>
        <HeaderEmpresa navigation={this.props.navigation} />

        <H3>{item.nome}</H3>

        <SkeletonLoading show={loading}>
          <ScrollView>
            <FlatList data={items} renderItem={this.renderItem} />
          </ScrollView>
        </SkeletonLoading>
      </ContainerPage>
    );
  }
}

const H3 = styled.Text`
  font-size: 20px;
  margin-left: 16px;
  margin-right: 16px;
  font-family: Montserrat-Bold;
  margin-bottom: 12px;
  margin-top: 16px;
  color: #111;
`;
