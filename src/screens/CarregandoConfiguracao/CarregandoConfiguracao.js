import React from 'react';
import {
  StyleSheet,
  View,
  ActivityIndicator,
  TouchableOpacity,
  Image,
  Alert,
  Text
} from 'react-native';
import logoImg from '../../../assets/images/logo-secondary.png';
import TextCustom from '../../shared/components/TextCustom';
import ContainerPage from '../../shared/components/ContainerPage';
import { colors } from '../../theme';

class CarregandoConfiguracao extends React.PureComponent {
  static navigationOptions = {
    header: null
  };

  state = { visible: true };

  componentDidMount = async () => {
    try {
      await this.props.init();
    } catch (e) {
      Alert.alert(
        'Aviso',
        'Houve um problema ao iniciar o Webgula! Feche e abra novamente.'
      );
      return;
    }

    this.handleUpdateRedirect();
  };

  componentDidUpdate = async prevProps => {
    if (prevProps.dados.id !== this.props.dados.id && this.props.dados.id) {
      await this.handleUpdateRedirect();
    }
  };

  handleUpdateRedirect = async () => {
    const { isAuth, isClientCredentials, dados, navigation } = this.props;

    await this.props.updateClientDispositivo();

    if (isAuth && dados['phone-confirmed'] !== 'true') {
      return navigation.replace('LoginSegundaEtapa');
    }

    if (isAuth) {
      return navigation.replace('Home');
    }

    if (isClientCredentials || !isAuth) {
      return navigation.replace('Login');
    }
  };

  render = () => {
    return (
      <ContainerPage>
        <View style={styles.wrapper}>
          <View style={{ flex: 1 }}>
            <TouchableOpacity activeOpacity={0.8} style={styles.imgContainer}>
              <Image source={logoImg} style={styles.logo} />
            </TouchableOpacity>
          </View>

          <View style={{ flex: 1, top: 50 }}>
            <ActivityIndicator size={'large'} color={'white'} />
          </View>

          <View style={{ flex: 2 }}>
            <TextCustom style={styles.textInfo}>
              Configurando o Webgula para você =)
            </TextCustom>
          </View>
        </View>
      </ContainerPage>
    );
  };
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: colors.primaryColorOriginal
  },
  textInfo: {
    textAlign: 'center',
    fontSize: 16,
    color: 'white',
    margin: 16
  },
  container: {
    marginBottom: 60
  },
  imgContainer: {
    alignItems: 'center',
    top: 120
  },
  logo: {
    alignSelf: 'center',
    resizeMode: 'contain',
    width: 172
  }
});

export default CarregandoConfiguracao;
