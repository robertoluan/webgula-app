import { connect } from "react-redux";
import component from "./CarregandoConfiguracao";
import authContext from "../../infra/auth";
import AccountsRemote from "../../services/remote/accounts.remote";

const mapStateToProps = (state, props) => {
  const app = state.app || {};

  return {
    isAuth: app.auth && app.auth.isAuth,
    isClientCredentials: app.auth && app.auth.isClientCredentials,
    dados: (app.auth && app.auth.userInfo) || {}
  };
};

const mapDispatchToProps = (dispatch, props) => ({
  async init() {
    //await authContext.setup();
    // await authContext.logout();
    const isAuth = await authContext.isAuthAsync();

    let isClientCredentials = false;

    let authData = {};

    if (!isAuth) {
      isClientCredentials = await authContext.isClientCredentialsAsync();

      if (!isClientCredentials) {
        await authContext.logout();
        await authContext.loginClientCredentials();
        isClientCredentials = await authContext.isClientCredentialsAsync();
      }
    } else {

      const isAuthExpiresTime = await authContext.isExpiresAuth();

      if (isAuthExpiresTime) {
        await authContext.refreshToken();
      }

 
    }

    authData = (await authContext.userDataAsync()) || {};

    return dispatch({
      type: "APP.AUTH.LOAD",
      userInfo: authData.userInfo || {},
      isAuth,
      isClientCredentials,
      token: authContext.token,
      refresh_token: authData.refresh_token
    });
  },

  async updateClientDispositivo() {

    setTimeout(async () => {
      try {
        await new AccountsRemote().atualizarToken();
      } catch (e) {
        Toast.show("Houve um problema no webgula!");
      }
    }, 500);
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(component);
