import React from "react";
import { View, ActivityIndicator, TouchableOpacity } from "react-native";
import { ListItem, Thumbnail, Left, Body } from "native-base";
import { colors } from "../../../theme";
import TextCustom from "../../../shared/components/TextCustom";
import EmpresaRemote from "../../../services/remote/empresa.remote";
import { formatMoney } from "../../../infra/helpers";

const empresaRemoteApi = new EmpresaRemote();

const image = require("../../../../assets/images/empty.jpeg");

export default class EstabelecimentoItem extends React.Component {
  state = {
    loading: false
  };

  setVip = async () => {
    const { item, onSavedVip } = this.props;

    this.setState({
      loading: true
    });

    try {
      await empresaRemoteApi.setVip(item);

      onSavedVip();
    } catch (e) {
    } finally {
      this.setState({
        loading: false
      });
    }
  };

  render() {
    let { item, onPressItem } = this.props;
    let { loading } = this.state;

    let wrapperImage = image;

    if (item.logoUrl) {
      wrapperImage = {
        uri: item.logoUrl
      };
    }

    return (
      <TouchableOpacity>
        {loading ? <ActivityIndicator size="large" /> : null}
        <ListItem
          style={{
            paddingBottom: 0,
            borderRadius: 6,
            backgroundColor: "#F7F7F7",
            marginTop: 5,
            marginBottom: 5
          }}
          noIndent={true}
          thumbnail
          noBorder
          onPress={() => onPressItem(item)}
        >
          <Left>
            <Thumbnail resizeMode={"contain"} source={wrapperImage} />
          </Left>
          <Body>
            <TextCustom style={{ fontSize: 16, color: "#222" }}>
              {/* {item.nomeEmpresaResponsavel} */}
              {item.nomeCampanha}
            </TextCustom>

            <View style={{ flexDirection: "row" }}>
              <TextCustom
                style={{
                  color: colors.greenButton,
                  fontSize: 13,
                  marginTop: 0
                }}
              >
                {formatMoney(item.saldoAtualConta, 0)} ponto
                {item.saldoAtualConta != 1 ? "s" : ""}
              </TextCustom>
            </View>
          </Body>
        </ListItem>
      </TouchableOpacity>
    );
  }
}
