import { connect } from "react-redux";
import component from "./FidelidadeScreen";
import EmpresaRemote from "../../../services/remote/empresa.remote";

const empresaRemote = new EmpresaRemote();

const mapStateToProps = (state, props) => {
  return {};
};

const mapDispatchToProps = (dispatch, props) => ({
  async selectedEmpresa(empresa) {
    await empresaRemote.setEmpresaDefault(empresa);

    dispatch({
      type: "APP.EMPRESA.EMPRESA_SELECTED",
      payload: {
        empresaSelecionada: { ...empresa }
      }
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(component);
