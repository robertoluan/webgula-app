import React from "react";
import { Alert, ScrollView, FlatList, RefreshControl } from "react-native";
import PageEmpty from "../../../shared/components/PageEmpty";
import ContainerPage from "../../../shared/components/ContainerPage";
import SkeletonLoading from "../../../shared/components/SkeletonLoading";
import EstabelecimentoItem from "./EstabelecimentoItem";
import FidelidadeRemote from "../../../services/remote/fidelidade.remote";

const Api = new FidelidadeRemote();

export default class FidelidadeScreen extends React.PureComponent {
  state = {
    items: [],
    isSearching: false
  };

  static navigationOptions = {
    title: "Fidelidade"
  };

  componentDidMount = async () => {
    this.onLoad();
  };

  onLoad = async () => {
    this.setState({
      isSearching: true
    });

    try {
      const items = await Api.findAllFidelidade();

      this.setState({
        items: items
      });
    } catch (e) {
      Alert.alert("Aviso", "Houve um problema ao trazer as empresas favoritas");
    } finally {
      this.setState({
        isSearching: false
      });
    }
  };

  onPressItem = async item => {
    await this.props.selectedEmpresa({
      id: item.cadEmpresaGlobalId,
      chaveGrupoEmpresarial: item.chaveGrupoEmpresarial,
      empresaBaseId: item.cadEmpresaGlobalId
    });

    this.props.navigation.navigate("Lancamentos", {
      item
    });
  };

  render() {
    const { isSearching, items } = this.state;

    return (
      <ContainerPage padding>
        {!isSearching && !items.length ? (
          <PageEmpty
            icon="fa-frown-o"
            content="Nenhuma campanha de fidelidade encontrada."
          />
        ) : null}

        <SkeletonLoading lines={5} show={isSearching}>
          <ScrollView
            refreshControl={
              <RefreshControl
                refreshing={isSearching}
                onRefresh={() => this.onLoad()}
              />
            }
          >
            <FlatList
              data={items}
              renderItem={({ item }) => (
                <EstabelecimentoItem
                  key={item.id}
                  onPressItem={() => this.onPressItem(item)}
                  item={item}
                />
              )}
            />
          </ScrollView>
        </SkeletonLoading>
      </ContainerPage>
    );
  }
}
