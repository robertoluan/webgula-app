import { connect } from "react-redux";
import component from "./VipsScreen";

const mapStateToProps = (state, props) => {
  let app = state.app;

  return {
    auth: (app.auth && app.auth.userInfo) || {}
  };
};

const mapDispatchToProps = (dispatch, props) => ({
  init() {
    dispatch({
      type: "APP.EMPRESA.EMPRESA_SELECTED",
      payload: {
        empresaSelecionada: {}
      }
    });
  },
  async selecionarEmpresa(empresa) {
    dispatch({
      type: "APP.EMPRESA.EMPRESA_SELECTED",
      payload: {
        empresaSelecionada: { ...empresa }
      }
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(component);
