import React from "react";
import { View, ActivityIndicator, TouchableOpacity, Alert } from "react-native";
import { ListItem, Thumbnail, Left, Body, Right } from "native-base";
import { colors } from "../../../theme";
import TextCustom from "../../../shared/components/TextCustom";
import Icon from "../../../shared/components/Icon";
import EmpresaRemote from "../../../services/remote/empresa.remote";
import { formatMoney } from "../../../infra/helpers";

const empresaRemoteApi = new EmpresaRemote();

const image = require("../../../../assets/images/empty.jpeg");

export default class EstabelecimentoItem extends React.Component {
  state = {
    loading: false
  };

  setVip = async () => {
    const { item, onSavedVip } = this.props;

    this.setState({
      loading: true
    });

    try {
      await empresaRemoteApi.setVip(item);

      onSavedVip();
    } catch (e) {
    } finally {
      this.setState({
        loading: false
      });
    }
  };

  render() {
    let { item, onPressItem } = this.props;
    let { loading } = this.state;

    let wrapperImage = image;

    if (item.logoUrl) {
      wrapperImage = {
        uri: item.logoUrl
      };
    }

    return (
      <TouchableOpacity>
        {loading ? <ActivityIndicator size="large" /> : null}
        <ListItem
          style={{
            paddingBottom: 0,
            borderRadius: 6,
            backgroundColor: "#F7F7F7",
            marginTop: 5,
            marginBottom: 5
          }}
          noIndent={true}
          thumbnail
          noBorder
          onPress={() => {
            if (!item.flgAceitoCliente) {
              return Alert.alert(
                "Aviso",
                `A empresa ${
                  item.nome
                } lhe convidou para ser cliente vip. Deseja aceitar o convite?`,
                [
                  {
                    text: "Cancelar"
                  },
                  {
                    text: "Aceitar",
                    onPress: async () => this.setVip()
                  }
                ]
              );
            }

            if (
              onPressItem !== undefined &&
              typeof onPressItem === "function"
            ) {
              return onPressItem(item);
            }

            return null;
          }}
        >
          <Left>
            <Thumbnail resizeMode={"contain"} source={wrapperImage} />
          </Left>
          <Body>
            <TextCustom style={{ fontSize: 16, color: "#222" }}>
              {item.nome}
            </TextCustom>

            <View style={{ flexDirection: "row" }}>
              <TextCustom
                style={{
                  color: colors.grayColor,
                  fontSize: 11, 
                  marginTop: 0
                }}
              >
                {item.nomeCidade} - {item.nomeBairro}
              </TextCustom>
            </View>

            <View style={{ flexDirection: "column" }}>
              <TextCustom
                style={{
                  color: colors.grayColor,
                  fontSize: 11,
                  marginTop: 0
                }}
              >
                {item.tipoFavoritoNome}
              </TextCustom>
            </View>
          </Body>
          <Right>
            {item.flgAceitoCliente && (
              <Icon style={{ color: "#999" }} size={22} name="fa-thumbs-up" />
            )}
            {!item.flgAceitoCliente && (
              <Icon size={22} style={{ color: "#ffb34d" }} name="fa-clock-o" />
            )}
          </Right>
        </ListItem>
      </TouchableOpacity>
    );
  }
}
