import React from "react";
import { Alert, ScrollView, RefreshControl, FlatList } from "react-native";
import EmpresaRemote from "../../../services/remote/empresa.remote";
import PageEmpty from "../../../shared/components/PageEmpty";
import ContainerPage from "../../../shared/components/ContainerPage";
import ModalUserQrCode from "../../../shared/components/ModalUserQrCode";
import SkeletonLoading from "../../../shared/components/SkeletonLoading";
import EstabelecimentoItem from "./EstabelecimentoItem";

const Api = new EmpresaRemote();

export default class VipsScreen extends React.PureComponent {
  state = {
    items: [],
    isSearching: false,
    openQrCode: false,
    empresa: {}
  };

  static navigationOptions = {
    title: "Vip"
  };

  componentDidMount = async () => {
    await this.props.init();

    this.onLoad();
  };

  onLoad = async () => {
    this.setState({
      isSearching: true
    });

    try {
      const items = await Api.findAllVips();

      this.setState({
        items: items
      });
    } catch (e) {
      Alert.alert("Aviso", "Houve um problema ao trazer as empresas vips");
    } finally {
      this.setState({
        isSearching: false
      });
    }
  };

  onPressItem = async item => {
    if (item.flgAceitoCliente) {
      this.setState({ openQrCode: true, empresa: { ...item } });
    }
  };

  onSavedVip() {
    this.onLoad();
  }

  render() {
    const { isSearching, items, openQrCode, empresa } = this.state;

    return (
      <ContainerPage padding>
        {!isSearching && !items.length ? (
          <PageEmpty icon="fa-frown-o" content="Nenhuma empresa vip." />
        ) : null}

        <ModalUserQrCode
          open={openQrCode}
          onClose={() => this.setState({ openQrCode: false })}
          item={empresa}
        />

        <SkeletonLoading lines={5} show={isSearching}>
          <ScrollView
            refreshControl={
              <RefreshControl
                refreshing={isSearching}
                onRefresh={() => this.onLoad()}
              />
            }
          >
            <FlatList
              data={items}
              renderItem={({ item }) => (
                <EstabelecimentoItem
                  onSavedVip={() => this.onSavedVip()}
                  key={item.id}
                  onPressItem={this.onPressItem}
                  item={item}
                />
              )}
            />
          </ScrollView>
        </SkeletonLoading>
      </ContainerPage>
    );
  }
}
