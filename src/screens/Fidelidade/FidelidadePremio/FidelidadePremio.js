import React from "react";
import {
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  Alert,
  ScrollView
} from "react-native";
import colors from "../../../theme/colors";
import TextCustom from "../../../shared/components/TextCustom";
import ContainerPage from "../../../shared/components/ContainerPage";
import ImageView from "react-native-image-view";
import ButtonLoading from "../../../shared/components/ButtonLoading";
import FidelidadeRemote from "../../../services/remote/fidelidade.remote";
import { Toast } from "native-base";
import RangeQuantidade from "../../Delivery/ProdutosGruposItem/components/RangeQuantidade";
import Input from "../../../shared/components/Input";

const imagem = require("../../../../assets/images/noimage.png");

const Api = new FidelidadeRemote();

export default class FidelidadePremio extends React.PureComponent {
  state = {
    visible: false,
    premio: {}
  };

  static navigationOptions = {
    title: "Detalhes do Prêmio"
  };

  componentDidMount = () => {
    const premio = this.props.navigation.getParam("premio");
    premio.quantidade = 1;

    this.setState({
      premio: { ...premio }
    });
  };

  abrirPreview = () => {
    this.setState({
      visible: true
    });
  };

  solicitarResgate = async () => {
    const { premio } = this.state;

    this.setState({
      isLoading: true
    });

    try {
      const res = await Api.solicitarResgatePremio({
        fidelidadeContaId: premio.fidelidadeContaId,
        premioId: premio.id,
        qtdePremio: premio.quantidade,
        observacao: premio.observacao
      });

      if (res && res.type === "exception") {
        let mensagem = res.error;

        if (res.exception && res.exception.response) {
          mensagem = res.exception.response.data.message;
        }

        Alert.alert("Aviso", mensagem);
      } else {
        Toast.show({ text: "Solicitação de resgate feita com sucesso!" });
        this.props.navigation.goBack();
      }

    } catch (e) {
      Alert.alert("Aviso", "Não foi possível resgatar o prêmio.");
    } finally {
      this.setState({
        isLoading: false
      });
    }
  };

  onChange = (prop, value) => {
    const { premio } = this.state;

    premio[prop] = value;

    this.setState({
      premio: { ...premio }
    });
  };

  render = () => {
    const { visible, premio, isLoading } = this.state;

    let wrapperImagem = {};

    if (premio.imagemUrlCompleta) {
      wrapperImagem = {
        uri: premio.imagemUrlCompleta
      };
    } else {
      wrapperImagem = imagem;
    }

    return (
      <ContainerPage
        style={{
          backgroundColor: "#f5f5f5"
        }}
      >
        <ImageView
          images={[
            {
              source: wrapperImagem,
              title: "",
              width: 806,
              height: 720
            }
          ]}
          imageIndex={0}
          onClose={() => this.setState({ visible: false })}
          isVisible={visible}
        />

        <ScrollView>
          <View
            style={{
              marginTop: 0,
              left: 0,
              top: -10,
              right: 0
            }}
          >
            <TouchableOpacity onPress={() => this.abrirPreview()}>
              <Image
                style={{
                  width: Dimensions.get("window").width,
                  height: Dimensions.get("window").width / 2
                }}
                source={wrapperImagem}
              />
            </TouchableOpacity>
          </View>

          <View
            style={{
              top: -40,
              paddingRight: 16,
              paddingLeft: 16,
              paddingBottom: 12,
              marginRight: 16,
              marginLeft: 16,
              backgroundColor: "white",
              borderRadius: 4
            }}
          >
            <TextCustom
              style={{
                fontSize: 18,
                textAlign: "center",
                fontFamily: "Montserrat-Bold",
                marginBottom: 26,
                marginTop: 16,
                color: colors.primaryColor
              }}
            >
              {premio.nome}
            </TextCustom>

            <View>
              <TextCustom
                style={{
                  fontSize: 36,
                  fontFamily: "Montserrat-Bold",
                  textAlign: "center",
                  color: "#222"
                }}
              >
                {premio.pontoCusto}
              </TextCustom>
            </View>

            <View style={{ marginTop: 16, marginBottom: 16 }}>
              <View
                style={{
                  flex: 1,
                  flexDirection: "row",
                  justifyContent: "center",
                  textAlign: "center"
                }}
              >
                <TextCustom>{premio.texto}</TextCustom>
              </View>
            </View>

            <View style={{ marginTop: 16, paddingLeft: 8, paddingRight: 8 }}>
              <TextCustom style={{ bottom: -12, textAlign: "center" }}>
                Observação (máximo 50 caracteres)
              </TextCustom>
              <Input
                color="#333"
                placeholder="Escreva uma observação!"
                transparent
                value={premio.observacao}
                onChangeText={value => this.onChange("observacao", value)}
              />
            </View>

            <View
              style={{
                alignItems: "center",
                marginTop: 16,
                marginBottom: 16
              }}
            >
              <RangeQuantidade
                min={1}
                max={10}
                onChange={value => this.onChange("quantidade", value)}
                value={premio.quantidade}
              />
            </View>

            <View style={{ marginTop: 32 }}>
              <ButtonLoading
                loading={isLoading}
                onPress={() => this.solicitarResgate()}
                title="Solicitar Resgate"
              />
            </View>
          </View>
        </ScrollView>
      </ContainerPage>
    );
  };
}
