import { connect } from "react-redux";
import component from "./LancamentosScreen";

const mapStateToProps = (state, props) => {
  let app = state.app || {};

  const empresaSelecionada = (app.empresa || {}).empresaSelecionada || {};

  return {
    isAuth: app.auth && app.auth.isAuth,
    isClientCredentials: app.auth && app.auth.isClientCredentials,
    empresaSelecionada,
    auth: (app.auth && app.auth.userInfo) || {}
  };
};

const mapDispatchToProps = (dispatch, props) => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(component);
