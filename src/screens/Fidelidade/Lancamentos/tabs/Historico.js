import React from "react";
import { View, FlatList, TouchableOpacity, ScrollView } from "react-native";
import { Left, Body, Right, ListItem } from "native-base";
import TextCustom from "../../../../shared/components/TextCustom";
import ViewBottom from "../../../../shared/components/ViewBottom";
import { colors } from "../../../../theme";
import moment from "moment";
import { formatMoney } from "../../../../infra/helpers";
import FidelidadeRemote from "../../../../services/remote/fidelidade.remote";
import Icon from "../../../../shared/components/Icon";
import SkeletonLoading from "../../../../shared/components/SkeletonLoading";
import PageEmpty from "../../../../shared/components/PageEmpty";

const Api = new FidelidadeRemote();

function ListItemHistoricoMes({ item, itemSelected, onPress }) {
  const isSelected = itemSelected.id == item.id;

  return (
    <React.Fragment>
      <ListItem
        style={{
          paddingTop: 16,
          paddingBottom: 16,
          borderBottomColor: isSelected ? colors.primaryColor : "#f7f7f7",
          borderBottomWidth: 1
        }}
        onPress={() => onPress(item)}
        noIndent
        noBorder
      >
        <Body>
          <TextCustom
            style={{
              fontSize: 14,
              fontFamily: isSelected ? "Montserrat-Bold" : "Montserrat-Regular",
              color: isSelected ? colors.primaryColor : null
            }}
          >
            {item.mes}
          </TextCustom>
        </Body>
      </ListItem>
    </React.Fragment>
  );
}

function getIconTipoMovimentacao(tipoLancamentoId) {
  let icon;
  let color = colors.accentColor1;

  if (tipoLancamentoId === 1) {
    icon = "fa-trophy";
  }

  if (tipoLancamentoId === 2) {
    icon = "fa-heartbeat";
  }

  if (tipoLancamentoId === 3) {
    icon = "fa-arrow-circle-o-left";
  }

  if (tipoLancamentoId === 4) {
    icon = "fa-plus-square";
  }

  if (tipoLancamentoId === 5) {
    icon = "fa-minus-circle";
  }

  if (tipoLancamentoId === 6) {
    icon = "fa-magic";
  }

  if (tipoLancamentoId === 7) {
    icon = "fa-hourglass-end";
  }
  if (tipoLancamentoId === 8) {
    icon = "ion-md-paper";
  }

  return {
    icon,
    color
  };
}

function ListItemHistoricoExtrato({ value }) {
  const data = moment(value.data).format("DD/MM/YYYY");

  return (
    <TouchableOpacity>
      <ListItem noBorder noIndent header>
        <Body>
          <TextCustom style={{ fontSize: 12 }}>{data}</TextCustom>
        </Body>
      </ListItem>

      <FlatList
        data={value.items}
        renderItem={({ item }) => {
          const status = getIconTipoMovimentacao(item.tipoLancamentoId);
          const color =
            item.flgDebitoCredto === "D"
              ? colors.primaryColorDark
              : colors.greenButton;

          return (
            <ListItem
              style={{
                paddingTop: 0,
                paddingBottom: 0,
                borderBottomColor: "#f7f7f7",
                borderBottomWidth: 1
              }}
              noIndent
              noBorder
              avatar
            >
              <Left>
                <View
                  style={{
                    width: 30,
                    height: 30,
                    top: -7,
                    paddingTop: 7,
                    borderRadius: 30,
                    alignItems: "center",
                    backgroundColor: color
                  }}
                >
                  <Icon size={16} color="white" name={status.icon} />
                </View>
              </Left>

              <Body>
                <TextCustom
                  style={{
                    fontSize: 15
                  }}
                >
                  {item.nomeTipoLancamento}
                </TextCustom>
              </Body>
              <Right>
                <TextCustom
                  style={{
                    fontSize: 15,
                    fontFamily: "Montserrat-Bold",
                    color: color
                  }}
                >
                  {item.flgDebitoCredto === "D" ? "" : "+"} {item.ponto}
                </TextCustom>
              </Right>
            </ListItem>
          );
        }}
      />
    </TouchableOpacity>
  );
}

export default class Historico extends React.PureComponent {
  state = {
    items: [],
    total: 0,
    isLoading: false
  };

  componentDidMount = async () => {
    const { item } = this.props;

    this.setState({
      isLoading: true
    });

    try {
      const data = await Api.findHistoricoLancamento(item.fidelidadeContaId);

      let grupo = {};

      data.forEach(item => {
        const dataLancamento = moment(item.dataLancamento).format("YYYY-MM-DD");

        grupo[dataLancamento] = grupo[dataLancamento] || {
          items: [],
          data: item.dataLancamento
        };
        grupo[dataLancamento].items.push(item);
      });

      let retorno = [];

      for (let prop in grupo) {
        retorno.push(grupo[prop]);
      }

      this.setState({
        items: retorno
      });
    } catch (e) {
    } finally {
      this.setState({
        isLoading: false
      });
    }
  };

  navigateTo = async screen => {
    this.props.navigation.navigate(screen);
  };

  render() {
    const { items, isLoading } = this.state;
    const { item } = this.props;

    return (
      <View
        style={{
          flex: 1,
          paddingBottom: 60,
          paddingHorizontal: isLoading ? 16 : 0
        }}
      >
        {/* <View style={{ backgroundColor: "#f7f7f7" }}>
          <FlatList
            data={data}
            horizontal
            showsHorizontalScrollIndicator={false}
            renderItem={({ item }) => (
              <ListItemHistoricoMes
                onPress={listItem => this.onSelected(listItem)}
                itemSelected={selected}
                item={item}
              />
            )}
          />
        </View> */}

        <SkeletonLoading lines={5} show={isLoading}>
          <ScrollView>
            <FlatList
              data={items}
              renderItem={({ item }) => (
                <ListItemHistoricoExtrato value={item} />
              )}
            />
          </ScrollView>
        </SkeletonLoading>

        {!isLoading && !items.length ? (
          <PageEmpty
            icon="fa-frown-o"
            content="Ainda não há lançamentos realizados."
          />
        ) : null}

        <ViewBottom style={{ margin: 0 }}>
          <ListItem
            noIndent
            style={{
              backgroundColor: "#f7f7f7",
              flexDirection: "row",
              paddingTop: 10,
              paddingBottom: 10,
              paddingLeft: 16,
              paddingRight: 16
            }}
          >
            <Body>
              <TextCustom
                style={{
                  fontSize: 18
                }}
              >
                Saldo de Pontos
              </TextCustom>
            </Body>
            <Right>
              <TextCustom
                style={{
                  fontSize: 20,
                  textAlign: "right"
                }}
              >
                {formatMoney(item.saldoAtualConta, 0)}
              </TextCustom>
            </Right>
          </ListItem>
        </ViewBottom>
      </View>
    );
  }
}
