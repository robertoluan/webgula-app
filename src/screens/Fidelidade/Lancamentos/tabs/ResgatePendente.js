import React from "react";
import { View, FlatList, ScrollView } from "react-native";
import { Body, Right, ListItem } from "native-base";
import TextCustom from "../../../../shared/components/TextCustom";
import moment from "moment";
import FidelidadeRemote from "../../../../services/remote/fidelidade.remote";
import SkeletonLoading from "../../../../shared/components/SkeletonLoading";
import PageEmpty from "../../../../shared/components/PageEmpty";

const Api = new FidelidadeRemote();

export function ListItemHistorico({ value, index }) {
  const data = moment(value.dataHoraLanc).format("DD/MM/YYYY hh:mm");

  return (
    <ListItem
      style={{
        paddingTop: 14,
        paddingBottom: 14,
        backgroundColor: index % 2 == 0 ? "#f7f7f7" : null
      }}
      noIndent
      noBorder
    >
      <Body>
        <TextCustom note style={{ fontSize: 9 }}>
          {data} - Status:{" "}
          {value.flgStatus == "C"
            ? "Cancelado"
            : value.flgStatus == "P"
            ? "Pendente"
            : "Entregue"}
        </TextCustom>
        <TextCustom
          style={{
            fontSize: 14
          }}
        >
          {value.premioNome}
        </TextCustom>
      </Body>
      <Right style={{ width: 100 }}>
        <TextCustom note style={{ fontSize: 9 }}>
          {value.premioPontoCusto} pontos x {value.qtdePremio}
        </TextCustom>

        <TextCustom
          style={{
            fontSize: 13,
            fontFamily: "Montserrat-Bold"
          }}
        >
          {value.qtdePontos} pts.
        </TextCustom>
      </Right>
    </ListItem>
  );
}

export default class ResgatePendente extends React.PureComponent {
  state = {
    items: [],
    isLoading: false
  };

  componentDidMount = async () => {
    const { item } = this.props;

    this.setState({
      isLoading: true
    });

    try {
      const data = await Api.findHistoricoResgate(item.fidelidadeId);

      this.setState({
        items: data
      });
    } catch (e) {
    } finally {
      this.setState({
        isLoading: false
      });
    }
  };

  navigateTo = async screen => {
    this.props.navigation.navigate(screen);
  };

  render() {
    const { items, isLoading } = this.state;

    return (
      <View style={{ flex: 1, paddingHorizontal: isLoading ? 16 : 0 }}>
        <SkeletonLoading lines={4} show={isLoading}>
          <ScrollView>
            <FlatList
              data={items}
              renderItem={({ item, index }) => (
                <ListItemHistorico index={index} value={item} />
              )}
            />
          </ScrollView>
        </SkeletonLoading>

        {!isLoading && !items.length ? (
          <PageEmpty
            icon="fa-frown-o"
            content="Ainda não há lançamentos realizados."
          />
        ) : null}
      </View>
    );
  }
}
