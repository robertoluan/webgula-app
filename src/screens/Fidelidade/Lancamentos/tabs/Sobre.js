import React from "react";
import { View, StyleSheet, Image, Dimensions } from "react-native";
import TextCustom from "../../../../shared/components/TextCustom";
import FidelidadeRemote from "../../../../services/remote/fidelidade.remote";
import SkeletonLoading from "../../../../shared/components/SkeletonLoading";

const fidelidadeRemote = new FidelidadeRemote();

class Sobre extends React.PureComponent {
  state = {
    isLoading: false,
    fidelidade: {}
  };

  componentDidMount = async () => {
    this.setState({
      isLoading: true
    });

    try {
      const data = await fidelidadeRemote.findFidelidadeCampanhaAtiva();

      this.setState({
        fidelidade: data
      });
    } catch (e) {
    } finally {
      this.setState({
        isLoading: false
      });
    }
  };

  displayTexto = texto => {
    let splites = texto.split("*");

    return (
      <View>
        {splites.map(item => (
          <TextCustom>{item}</TextCustom>
        ))}
      </View>
    );
  };

  render = () => {
    const { isLoading } = this.state;
    const { fidelidade } = this.state;

    const imagem = {
      uri: fidelidade.imagemUrl
    };

    return (
      <View style={{ flex: 1 }}>
        <SkeletonLoading lines={4} show={isLoading}>
          <View style={{ marginBottom: 16 }}>
            {fidelidade.imagemUrl && (
              <Image
                style={{ height: 200, width: null, flex: 1 }}
                source={imagem}
              />
            )}
          </View>
          <View style={{ ...styles.containerView, marginBottom: 20 }}>
            {this.displayTexto(fidelidade.textoCliente || "")}
          </View>

          <View style={{ ...styles.containerView, marginBottom: 20 }}>
            {fidelidade.pontosAdesao ? (
              <TextCustom>
                Pontos para Adesão: {fidelidade.pontosAdesao} pontos
              </TextCustom>
            ) : null}
          </View>
        </SkeletonLoading>
      </View>
    );
  };
}

const styles = StyleSheet.create({
  containerView: {
    marginTop: 16,
    paddingHorizontal: 16,
    flexWrap: "wrap",
    justifyContent: "center"
  }
});

export default Sobre;
