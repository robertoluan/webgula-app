import React from "react";
import {
  View,
  FlatList,
  Image,
  ScrollView,
  TouchableOpacity
} from "react-native";
import { Left, Body, CardItem, Card, ListItem } from "native-base";
import Icon from "../../../../shared/components/Icon";
import TextCustom from "../../../../shared/components/TextCustom";
import { colors } from "../../../../theme";
import FidelidadeRemote from "../../../../services/remote/fidelidade.remote";
import SkeletonLoading from "../../../../shared/components/SkeletonLoading";
import PageEmpty from "../../../../shared/components/PageEmpty";

const Api = new FidelidadeRemote();

function ListItemRecompensaHeader({ item }) {
  return (
    <ListItem
      style={{
        paddingTop: 0,
        paddingBottom: 0,
        marginTop: 20,
        marginBottom: 20,
        marginLeft: 0
      }}
      noIndent
      noBorder
    >
      <Body>
        <View
          style={{
            position: "absolute",
            left: -32,
            top: -6,
            borderRadius: 30,
            width: 30,
            height: 30,
            paddingTop: 8,
            backgroundColor: "#f7f7f7",
            alignItems: "center"
          }}
        >
          <Icon size={14} name="ion-md-pricetags" />
        </View>

        <TextCustom
          style={{
            fontSize: 20,
            marginLeft: 16,
            color: colors.primaryColorDark
          }}
        >
          {item.grupo} pontos
        </TextCustom>
      </Body>
    </ListItem>
  );
}

function ListItemRecompensa({ produtos, onPress }) {
  return (
    <FlatList
      data={produtos}
      horizontal
      // style={{ flex: 1 }}
      pagingEnabled
      showsHorizontalScrollIndicator={false}
      renderItem={({ item }) => (
        <TouchableOpacity activeOpacity={0.8} onPress={() => onPress(item)}>
          <View
            style={{
              // flex: 1,
              marginLeft: 16,
              marginRight: 16
            }}
          >
            <Card>
              <CardItem>
                <Left>
                  <Body>
                    <TextCustom
                      style={{
                        fontFamily: "Montserrat-Bold"
                      }}
                    >
                      {item.nome}
                    </TextCustom>
                  </Body>
                </Left>
              </CardItem>

              <CardItem cardBody>
                <Image
                  source={{
                    uri: item.imagemUrlCompleta
                  }}
                  style={{ height: 200, width: null, flex: 1 }}
                />
              </CardItem>
            </Card>
          </View>
        </TouchableOpacity>
      )}
    />
  );
}

export default class Recompensas extends React.PureComponent {
  state = {
    isLoading: false,
    items: []
  };

  componentDidMount = async () => {
    const { item } = this.props;

    this.setState({
      isLoading: true
    });

    try {
      const data = await Api.findPremiacoes(item.fidelidadeId);

      let grupos = {};

      data.forEach(item => {
        grupos[item.pontoCusto] = grupos[item.pontoCusto] || {
          grupo: item.pontoCusto,
          items: []
        };
        grupos[item.pontoCusto].items.push(item);
      });

      const items = [];

      for (const grupo in grupos) {
        items.push(grupos[grupo]);
      }

      this.setState({
        items: items
      });
    } catch (e) {
    } finally {
      this.setState({
        isLoading: false
      });
    }
  };

  navigateTo = async (screen, params) => {
    this.props.navigation.navigate(screen, params);
  };

  onPreviewPremio = async premio => {
    premio.fidelidadeContaId = this.props.item.fidelidadeContaId;

    this.props.navigation.navigate("FidelidadePremio", { premio });
  };

  render = () => {
    const { items, isLoading } = this.state;

    return (
      <View style={{ flex: 1, paddingHorizontal: isLoading ? 16 : 0  }}>
        <SkeletonLoading lines={5} show={isLoading}>
          <ScrollView>
            <FlatList
              data={items}
              renderItem={({ item }) => (
                <React.Fragment>
                  <View
                    style={{
                      borderLeftColor: "#efefef",
                      borderLeftWidth: 1,
                      marginLeft: 26
                    }}
                  >
                    <ListItemRecompensaHeader item={item} />
                  </View>
                  <View
                    style={{
                      borderLeftColor: "#efefef",
                      borderLeftWidth: 1,
                      marginLeft: 26,
                      paddingLeft: 16
                    }}
                  >
                    <ListItemRecompensa
                      produtos={item.items}
                      onPress={premio => this.onPreviewPremio(premio)}
                    />
                  </View>
                </React.Fragment>
              )}
            />
          </ScrollView>
        </SkeletonLoading>


        {!isLoading && !items.length ? (
          <PageEmpty
            icon="fa-frown-o"
            content="Não existe nenhuma recompensa."
          />
        ) : null}
      </View>
    );
  };
}
