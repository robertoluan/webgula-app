import React from "react";
import HeaderEmpresa from "../../../shared/components/HeaderEmpresa";
import { View } from "react-native";
import ContainerPage from "../../../shared/components/ContainerPage";
import Historico from "./tabs/Historico";
import Recompensas from "./tabs/Recompensas";
import ResgatePendente from "./tabs/ResgatePendente";
import { colors } from "../../../theme";
import ScrollableTabView from "react-native-scrollable-tab-view";
import Sobre from "./tabs/Sobre";

export default class LancamentosScreen extends React.PureComponent {
  state = {
    page: 0
  };

  navigateTo = async screen => {
    this.props.navigation.navigate(screen);
  };

  static navigationOptions = {
    title: "Lançamentos"
  };

  renderTab = () => {
    const { page } = this.state;

    const item = this.props.navigation.getParam("item");

    if (page === 0) {
      return <Historico item={item} init={this.props.init} />;
    }

    if (page === 1) {
      return <Sobre navigation={this.props.navigation} item={item} />;
    }

    if (page === 2) {
      return <Recompensas navigation={this.props.navigation} item={item} />;
    }

    if (page === 3) {
      return <ResgatePendente navigation={this.props.navigation} item={item} />;
    }
  };

  render() {
    return (
      <ContainerPage>
        <View>
          <HeaderEmpresa navigation={this.props.navigation} />
        </View>

        <View style={{ flex: 1 }}>
          <View style={{ height: 60 }}>
            <ScrollableTabView
              tabBarActiveTextColor={colors.primaryColor}
              tabBarBackgroundColor={colors.white}
              tabBarUnderlineStyle={{
                height: 1,
                backgroundColor: colors.primaryColor
              }}
              page={this.state.page}
              onChangeTab={({ i }) => {
                this.setState({ page: i });
              }}
              tabBarTextStyle={{
                fontSize: 12,
                paddingTop: 10,
                textAlign: "center",
                fontWeight: "normal",
                fontFamily: "Montserrat-Regular"
              }}
            >
              <View tabLabel="Histórico" />
              <View tabLabel="Sobre" />
              <View tabLabel="Recompensas" />
              <View tabLabel="Resgates" />
            </ScrollableTabView>
          </View>

          {this.renderTab()}
        </View>
      </ContainerPage>
    );
  }
}
