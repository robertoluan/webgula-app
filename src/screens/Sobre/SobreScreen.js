import React from "react";
import { ScrollView } from "react-native";
import TextCustom from "../../shared/components/TextCustom";
import ContainerPage from "../../shared/components/ContainerPage";

export default class SobreScreen extends React.PureComponent {
  static navigationOptions = {
    title: "Sobre o WebGula Menu"
  };

  render() {
    return (
      <ContainerPage>
        <ScrollView>
          <TextCustom
            style={{
              padding: 16,
              fontSize: 16
            }}
          >
            Aplicativo com funcionalidades voltadas para bares, restaurantes,
            deliveries e padarias. Encontre lugares para ir ou para pedir suas
            comidas e bebidas. Navegue pelo cardápio, acompanhe sua conta.
            Participe dos planos de fidelidade de cada empresa.
          </TextCustom>
          <TextCustom
            style={{
              padding: 16,
              fontSize: 14
            }}
          >
            Versão beta 2019.1
          </TextCustom>
          <TextCustom
            style={{
              padding: 16,
              fontSize: 14
            }}
          >
            Tacto sistemas
          </TextCustom>
          <TextCustom
            style={{
              padding: 16,
              paddingTop: 0,
              fontSize: 14,
              color: "green"
            }}
          >
            (65) 3028-7728
          </TextCustom>
        </ScrollView>
      </ContainerPage>
    );
  }
}
