import React from "react";
import { Toast, DatePicker } from "native-base";
import { ScrollView, StyleSheet, View } from "react-native";
import { Row, Col } from "react-native-easy-grid";
import Input from "../../shared/components/Input";
import ButtonLoading from "../../shared/components/ButtonLoading";
import { colors } from "../../theme";
import TextCustom from "../../shared/components/TextCustom";
import ContainerPage from "../../shared/components/ContainerPage";
import InputSexo from "../../shared/components/Input/InputSexo";
import moment from "moment";

class TutorialScreen extends React.PureComponent {
  state = {
    isLoading: false,
    nome: "",
    email: null,
    sexo: null,
    dataNascimento: null
  };

  componentDidMount = () => {
    let dateNative = null;

    if (this.props.dados.dataNascimento) {
      let data = moment(this.props.dados.dataNascimento); //.format("YYYY-MM-DD");

      dateNative = new Date(
        data.format("YYYY"),
        Number(data.format("MM")) - 1,
        data.format("DD")
      );
    }

    this.setState({
      nome: this.props.dados.nome,
      email: this.props.dados.email,
      sexo: this.props.dados.sexo,
      cpf: this.props.dados.cpf,
      data_nascimento: dateNative
    });
  };

  componentWillUpdate = nextProps => {
    if (nextProps.dados !== this.props.dados && !this.state.nome) {
      let dateNative = null;

      if (nextProps.dados.dataNascimento) {
        let data = moment(nextProps.dados.dataNascimento);

        dateNative = new Date(
          data.format("YYYY"),
          Number(data.format("MM")) - 1,
          data.format("DD")
        );
      }

      this.setState({
        nome: nextProps.dados.nome,
        email: nextProps.dados.email,
        cpf: nextProps.dados.cpf,
        sexo: nextProps.dados.sexo,
        data_nascimento: dateNative
      });
    }
  };

  continuar = async () => {
    let { nome, email, cpf, sexo, data_nascimento } = this.state;

    if (!nome) {
      Toast.show({
        text: "Informe seu nome!"
      });
      return;
    }

    if (nome.split(" ").length < 2) {
      Toast.show({
        text: "Informe seu nome com sobrenome!"
      });
      return;
    }

    if (nome.length < 10) {
      Toast.show({
        text: "Nome precisa ter no mínimo 10 caracteres"
      });
      return;
    }

    if (!email) {
      Toast.show({
        text: "Informe seu e-mail!"
      });
      return;
    }

    const { salvar } = this.props;

    this.setState({
      isLoading: true
    });

    const model = {
      nome,
      sexo,
      email,
      cpf,
      dataNascimento: data_nascimento
    };

    const codigoSms = this.props.navigation.getParam("codigoSms");

    try {
      const res = await salvar(model, codigoSms);

      if (res && !res.success) {
        Toast.show({
          text: "Houve um problema ao salvar."
        });
      } else {
        Toast.show({
          text: "Operação realizada com sucesso!"
        });
        this.props.navigation.replace("Home");
      }
    } catch (e) {
      Toast.show({
        text: "Ops. Houve um problema ao salvar."
      });
    } finally {
      this.setState({
        isLoading: false
      });
    }
  };

  render = () => {
    const { isLoading } = this.state;

    let placeholder = "99/99/9999";

    if (this.state.data_nascimento) {
      placeholder = moment(this.state.data_nascimento).format("DD/MM/YYYY");
    }

    return (
      <ContainerPage style={[styles.wrapper]}>
        <ScrollView style={styles.container} keyboardShouldPersistTaps="always">
          <View style={styles.titleWrapper}>
            <TextCustom style={styles.title}>Olá, </TextCustom>

            <TextCustom style={styles.subtitle}>
              Seja bem vindo ao Webgula. Precisamos de alguns dados seu para
              continuarmos =)
            </TextCustom>
          </View>

          <View style={styles.formContainer}>
            <Row>
              <Col>
                <Input
                  label="Nome (Obrigatório)"
                  style={styles.input}
                  value={this.state.nome}
                  onChangeText={nome => this.setState({ nome })}
                />
              </Col>
            </Row>

            <Row style={{ marginTop: 8 }}>
              <Col>
                <Input
                  label="Email (Obrigatório)"
                  keyboardType="email-address"
                  value={this.state.email}
                  onChangeText={email => this.setState({ email })}
                />
              </Col>
            </Row>

            <Row>
              <Col>
                <InputSexo
                  label="Sexo (Opcional)"
                  value={this.state.sexo}
                  onChangeText={sexo => this.setState({ sexo })}
                />
              </Col>
            </Row>

            <Row>
              <Col>
                <TextCustom style={styles.labelFloat}>
                  Data de Nascimento (Opcional)
                </TextCustom>
                <View style={styles.inputDatePicker}>
                  <DatePicker
                    locale={"pt-br"}
                    modalTransparent={false}
                    animationType={"fade"}
                    androidMode={"default"}
                    transparent
                    placeHolderTextStyle={{
                      color: "#333",
                      fontSize: 14,
                      left: -10
                    }}
                    placeHolderText={placeholder}
                    defaultDate={this.state.data_nascimento}
                    onDateChange={data_nascimento => {
                      this.setState({ data_nascimento });
                    }}
                  />
                </View>
              </Col>
            </Row>

            <Row>
              <Col style={{ marginTop: 32, marginBottom: 48 }}>
                <ButtonLoading
                  title="Continuar"
                  loading={isLoading}
                  onPress={() => this.continuar()}
                />
              </Col>
            </Row>
          </View>
        </ScrollView>
      </ContainerPage>
    );
  };
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: colors.white
  },
  titleWrapper: {
    flex: 1,
    top: 10,
    bottom: 30
  },
  title: {
    fontSize: 18,
    textAlign: "center",
    color: "black",
    marginBottom: 10
  },
  subtitle: {
    fontSize: 13,
    textAlign: "center",
    paddingHorizontal: 32,
    color: colors.grayColor
  },
  container: {
    top: 25,
    flex: 2,
    marginRight: 0
  },
  labelFloat: {
    fontSize: 13,
    bottom: -8,
    position: "relative"
  },
  formContainer: {
    flex: 1,
    top: 40,
    marginRight: 16,
    marginLeft: 16
  },
  input: {
    marginTop: 10
  },
  inputDatePicker: {
    marginTop: 8,
    paddingLeft: 8,
    borderRadius: 6,
    borderWidth: 1,
    borderColor: "#ccc"
  }
});

export default TutorialScreen;
