import TutorialScreen from "./TutorialScreen";
import { connect } from "react-redux";
import AccountRemote from "../../services/remote/accounts.remote";

const Api = new AccountRemote();

const mapStateToProps = (state, props) => {
  let app = state.app || {};

  return {
    isAuth: app.auth && app.auth.isAuth,
    dados: (app.auth && app.auth.userInfo) || {}
  };
};

const mapDispatchToProps = (dispatch, props) => ({
  async salvar(model, codigoSms) {
    // nao existe mais essa api return Api.atualizarPerfil(model, codigoSms);
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TutorialScreen);
