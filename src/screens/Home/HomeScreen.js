import React from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  ImageBackground,
  Alert
} from 'react-native';
import logoImg from '../../../assets/images/logo-primary.png';
import imagemBackground from '../../../assets/images/background.jpg';
import { colors } from '../../theme';
import TextCustom from '../../shared/components/TextCustom';
import ContainerPage from '../../shared/components/ContainerPage';
import ButtonLoading from '../../shared/components/ButtonLoading';
import { checkAlertAuth } from '../../services/actions/app/auth.actions';
import { withNavigationFocus } from 'react-navigation';
import styled from 'styled-components';

const ViewBottomStyled = styled(View)`
  background-color: white;
  position: absolute;
  bottom: 0;
  left: 0;
  bottom: 0;
  right: 0;
  padding: 16px;
  margin: 0 16px;
  border-top-right-radius: 8px;
  border-top-left-radius: 8px;
  /* box-shadow: 0px 5px 5px #ccc; */
  shadow-opacity: 0.75;
  shadow-radius: 5px;
  shadow-color: rgba(0, 0, 0, 0.3);
  shadow-offset: 0px 0px;
`;

class HomeScreen extends React.PureComponent {
  static navigationOptions = {
    header: null
  };

  state = { value: 0 };

  componentDidMount = async () => {
    await this.props.resetEmpresaSelecionada();
  };

  componentDidUpdate = async prevProps => {
    if (prevProps.isFocused !== this.props.isFocused && this.props.isFocused) {
      await this.props.resetEmpresaSelecionada();
    }
  };

  configurarEnderecoEntrega = async () => {
    const { isClientCredentials } = this.props;

    if (isClientCredentials) {
      return checkAlertAuth(this.props.navigation);
    }

    await this.props.getEnderecos();

    setTimeout(async () => {
      let enderecoEntrega = this.props.endereco;

      if (!enderecoEntrega.id) {
        // Não possui endereço no local storage e vai buscar no servidor
        this.props.setLoading();

        await this.props.getEnderecosServidor();

        if (this.props.enderecos.length) {
          enderecoEntrega =
            this.props.enderecos.filter(item => item.flgPrincipal)[0] || null;

          // Veio enderecos do servidor, então seto um como padrão!;
          await this.props.setEnderecoAtual(enderecoEntrega);
        }

        this.props.closeLoading();
      }

      if (!enderecoEntrega || !enderecoEntrega.id) {
        return Alert.alert(
          'Aviso',
          'Antes de continuar você precisa informar um endereço para entrega!',
          [
            {
              text: 'Cancelar'
            },
            {
              text: 'Ok. Informar!',
              onPress: () => {
                this.props.navigation.navigate('Endereco', {
                  VeioExterno: true,
                  ViewDestino: 'DeliveryEstabelecimentos'
                });
              }
            }
          ]
        );

        // nesse momento existe endereço de entrega
      } else {
        this.props.navigation.navigate('DeliveryEstabelecimentos');
      }
    }, 100);
  };

  buscarEstabelecimento = async () => {
    this.props.resetEmpresaSelecionada();

    this.props.navigation.navigate('Estabelecimentos');
  };

  goDelivery = async () => {
    await this.props.resetEmpresaSelecionada();

    await this.configurarEnderecoEntrega();
  };

  getFidelidade = async () => {
    const { isClientCredentials } = this.props;

    if (isClientCredentials) {
      return checkAlertAuth(this.props.navigation);
    }

    await this.props.resetEmpresaSelecionada();

    this.props.navigation.navigate('Fidelidade');
  };

  render = () => {
    return (
      <ContainerPage>
        <ImageBackground
          source={imagemBackground}
          style={{ width: '100%', height: '100%' }}
        >
          <View style={styles.wrapper}>
            <TouchableOpacity activeOpacity={0.8} style={styles.imgContainer}>
              <Image source={logoImg} style={styles.logo} />
            </TouchableOpacity>
          </View>

          <ViewBottomStyled>
            <View style={styles.container}>
              <View style={styles.btnMenu}>
                <ButtonLoading
                  backgroundColor={'transparent'}
                  style={{
                    borderWidth: 1,
                    borderRadius: 6,
                    borderColor: colors.primaryColor
                  }}
                  height={45}
                  titleColor={colors.primaryColor}
                  onPress={() => this.buscarEstabelecimento()}
                  title={
                    <TextCustom
                      style={{
                        fontFamily: 'Montserrat-Bold'
                      }}
                    >
                      MENU
                    </TextCustom>
                  }
                />
              </View>

              {/* <View style={styles.btnMenu}>
                  <TouchableOpacity
                    style={styles.btnMenuClick}
                    activeOpacity={0.7}
                    onPress={() => this.goDelivery()}
                  >
                    <Icon style={styles.icon} name="fa-motorcycle" />

                    <TextCustom style={styles.textMenu}> Delivery </TextCustom>
                  </TouchableOpacity>
                </View> */}

              <View style={styles.btnMenu}>
                <ButtonLoading
                  backgroundColor={'transparent'}
                  style={{
                    borderWidth: 1,
                    borderRadius: 6,
                    borderColor: colors.primaryColor
                  }}
                  titleColor={colors.primaryColor}
                  height={45}
                  onPress={() => this.getFidelidade()}
                  title={
                    <TextCustom
                      style={{
                        fontFamily: 'Montserrat-Bold'
                      }}
                    >
                      FIDELIDADE
                    </TextCustom>
                  }
                />
              </View>
            </View>
          </ViewBottomStyled>
        </ImageBackground>
      </ContainerPage>
    );
  };
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1
  },
  container: {
    marginBottom: 60
  },
  imgContainer: {
    alignItems: 'center',
    top: 120
  },
  logo: {
    alignSelf: 'center',
    resizeMode: 'contain',
    width: 220
  },
  btnMenu: {
    marginTop: 12,
    marginBottom: 12,
    marginLeft: 8,
    marginRight: 8
  }
});

export default withNavigationFocus(HomeScreen);
