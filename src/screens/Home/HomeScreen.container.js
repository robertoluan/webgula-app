import { connect } from "react-redux";
import component from "./HomeScreen";
import {
  getEndereco,
  setEnderecoAtual
} from "../../services/actions/app/auth.actions";
import { setLoading } from "../../services/actions/layout/global.actions";
import AccountsRemote from "../../services/remote/accounts.remote";

const accountsRemote = new AccountsRemote();

const mapStateToProps = (state, props) => {
  let notificacoes = [];
  const app = state.app || {};

  if (state.app.dashboard) {
    notificacoes = state.app.dashboard.notificacoes;
  }

  const endereco = (app.endereco && app.endereco.atual) || {};
  const enderecos = (app.endereco && app.endereco.items) || {};

  return {
    endereco,
    enderecos,
    auth: app.auth,
    isAuth: app.auth && app.auth.isAuth,
    isClientCredentials: app.auth && app.auth.isClientCredentials,
    notificacoes,
    dados: (app.auth && app.auth.userInfo) || {}
  };
};

const mapDispatchToProps = (dispatch, props) => ({
  async getEnderecos() {
    dispatch(getEndereco());
  },
  async getEnderecosServidor() {
    await accountsRemote.getEnderecosPessoa();

    await dispatch(getEndereco());
  },

  async setLoading() {
    dispatch(setLoading(true));
  },

  async closeLoading() {
    dispatch(setLoading(false));
  },

  async setEnderecoAtual(endereco) {
    return dispatch(setEnderecoAtual(endereco));
  },

  async resetEmpresaSelecionada() {
    return dispatch({
      type: "APP.EMPRESA.EMPRESA_SELECTED",
      payload: {
        empresaSelecionada: {}
      }
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(component);
