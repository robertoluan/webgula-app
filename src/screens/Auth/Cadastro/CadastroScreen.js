import React from "react";
import { StyleSheet, View, Alert } from "react-native";
import Input from "../../../shared/components/Input";
import InputSexo from "../../../shared/components/Input/InputSexo";
import TextCustom from "../../../shared/components/TextCustom";
import ButtonLoading from "../../../shared/components/ButtonLoading";
import ContainerPage from "../../../shared/components/ContainerPage";
import AppIntroSlider from "react-native-app-intro-slider";
import colors from "../../../theme/colors";
import InputMask from "../../../shared/components/Input/InputMask";
import { isNumber } from "../../../infra/utils/number.helpers";
import { Row, Col } from "react-native-easy-grid";
import { validateEmail, trim } from "../../../infra/helpers";

class CadastroScreen extends React.PureComponent {
  state = {
    isLoading: false,
    nome: "", //"roberto da cruz",
    email: "", // "roberto.giacomette18@gmail.com",
    telefone: "", // "65992611837",
    ddi: "55",
    senha: null, //"123456",
    sexo: null, //"m",
    indexSlide: 0
  };

  static navigationOptions = {
    title: "Faça seu Cadastro",
    headerRight: null
  };

  _renderItem = item => {
    return (
      <View style={styles.slide}>
        <TextCustom style={styles.title}>{item.title}</TextCustom>
        <View>{item.content}</View>
      </View>
    );
  };

  isValidSlide = targetIndex => {
    const { indexSlide } = this.state;

    if (indexSlide === 0) {
      const nome = this.state.nome;

      if (!nome) {
        return Alert.alert("Aviso", "Informe o nome!");
      }

      if (nome.split(" ").length < 2) {
        return Alert.alert("Aviso", "Informe seu nome com sobrenome!");
      }

      if (nome.length < 10) {
        return Alert.alert("Aviso", "Nome precisa ter no mínimo 10 caracteres");
      }
    }
    if (indexSlide === 1) {
      if (!this.state.email) {
        return Alert.alert("Aviso", "Informe o email!");
      }

      if (!validateEmail(this.state.email)) {
        return Alert.alert("Aviso", "Email está em um formato incorreto!");
      }
    }
    if (indexSlide === 2) {
      const telefone = this.state.telefone;
      const ddi = this.state.ddi;

      if (!telefone) {
        return Alert.alert("Aviso", "Informe o Telefone!");
      }

      if (!ddi) {
        return Alert.alert("Aviso", "Informe o Código do País!");
      }

      if (!isNumber(telefone)) {
        return Alert.alert("Aviso", "Telefone está em formato incorreto!");
      }

      if (!isNumber(ddi)) {
        return Alert.alert(
          "Aviso",
          "Código do País está em formato incorreto!"
        );
      }

      if (telefone.toString().length < 11) {
        return Alert.alert(
          "Aviso",
          "Telefone deve possui o DDD juntamente com o número!"
        );
      }

      if (telefone.toString().length > 11) {
        return Alert.alert(
          "Aviso",
          "Telefone é maior do que o tamanho permitido de 11 digitos!"
        );
      }
    }

    if (indexSlide === 4) {
      if (!this.state.senha) {
        return Alert.alert("Aviso", "Informe o senha!");
      }
      if (this.state.senha.length < 6) {
        return Alert.alert(
          "Aviso",
          "A senha precisa ter no mínimo 4 caracteres!"
        );
      }
    }

    this._goToSlide(targetIndex);

    return true;
  };

  salvar = async () => {
    if (!this.isValidSlide(4)) return;

    const model = {
      nome: trim(this.state.nome),
      sexo: trim(this.state.sexo),
      telefone: trim(this.state.ddi + this.state.telefone),
      email: trim(this.state.email),
      senha: trim(this.state.senha)
    };

    this.setState({
      isLoading: true
    });

    try {
      const res = await this.props.cadastrar(model);

      if (res.success === false) {
        let errorText = "";

        if (typeof res.error === "object") {
          for (let prop in res.error) {
            errorText = res.error[prop][0];
          }
        }

        Alert.alert("Aviso", errorText || "Código incorreto!");

        return;
      }

      this.props.navigation.navigate("LoginSegundaEtapa", {
        VeioCadastro: true
      });
    } catch (e) {
      Alert.alert("Aviso", "Houve um problema ao realizar esta operação!");
    } finally {
      this.setState({
        isLoading: false
      });
    }
  };

  _goToSlide = (index, goToNative = true) => {
    this.setState({
      indexSlide: index
    });

    if (goToNative) {
      this.refs.appIntroSlider.goToSlide(index);
    }

    return true;
  };

  render = () => {
    const slides = [
      {
        key: "nome",
        title: "Informe seu nome",
        content: (
          <View style={styles.viewItem}>
            <View style={styles.viewItemInput}>
              <Input
                label="Nome"
                style={styles.input}
                value={this.state.nome}
                onChangeText={nome => this.setState({ nome })}
              />
            </View>

            <View style={styles.viewItemButton}>
              <ButtonLoading
                title="Próximo"
                titleColor={colors.white}
                backgroundColor={colors.primaryColor}
                onPress={async () => this.isValidSlide(1)}
              />
            </View>
          </View>
        )
      },
      {
        key: "email",
        title: "Informe seu Email",
        content: (
          <View style={styles.viewItem}>
            <View style={styles.viewItemInput}>
              <Input
                label="Email"
                style={styles.input}
                keyboardType="email-address"
                value={this.state.email}
                onChangeText={email => this.setState({ email })}
              />
            </View>
            <View style={styles.viewItemButton}>
              <ButtonLoading
                title="Próximo"
                titleColor={colors.white}
                backgroundColor={colors.primaryColor}
                onPress={async () => this.isValidSlide(2)}
              />
            </View>
          </View>
        )
      },
      {
        key: "telefone",
        title: "Informe seu Telefone",
        content: (
          <View style={styles.viewItem}>
            <View style={(styles.viewItemInput, { marginBottom: 60 })}>
              <Row>
                <Col size={30} style={{ paddingRight: 10 }}>
                  <Input
                    label={"País"}
                    keyboardType="numeric"
                    value={this.state.ddi}
                    onChangeText={ddi => this.setState({ ddi })}
                  />
                </Col>
                <Col size={70}>
                  <InputMask
                    label={"Telefone"}
                    type={"cel-phone"}
                    options={{
                      maskType: "BRL",
                      withDDD: true,
                      dddMask: "(99) "
                    }}
                    value={this.state.telefone}
                    onChangeText={value => this.setState({ telefone: value })}
                  />
                </Col>
              </Row>
            </View>

            <View style={styles.viewItemButton}>
              <ButtonLoading
                title="Próximo"
                titleColor={colors.white}
                backgroundColor={colors.primaryColor}
                onPress={async () => this.isValidSlide(3)}
              />
            </View>
          </View>
        )
      },
      {
        key: "sexo",
        title: "Informe seu Sexo",
        content: (
          <View style={styles.viewItem}>
            <View style={styles.viewItemInput}>
              <InputSexo
                label="Sexo (Opcional)"
                value={this.state.sexo}
                onChangeText={sexo => this.setState({ sexo })}
              />
            </View>

            <View style={styles.viewItemButton}>
              <ButtonLoading
                title="Próximo"
                titleColor={colors.white}
                backgroundColor={colors.primaryColor}
                onPress={async () => this.isValidSlide(4)}
              />
            </View>
          </View>
        )
      },
      {
        key: "senha",
        title: "Escolha uma senha",
        content: (
          <View style={styles.viewItem}>
            <View style={styles.viewItemInput}>
              <Input
                label={"Senha"}
                style={styles.input}
                secureTextEntry
                value={this.state.senha}
                onChangeText={senha => this.setState({ senha })}
              />
            </View>
            <View style={styles.viewItemButton}>
              <ButtonLoading
                title="Salvar"
                titleColor={colors.white}
                loading={this.state.isLoading}
                backgroundColor={colors.primaryColor}
                onPress={async () => this.salvar()}
              />
            </View>
          </View>
        )
      }
    ];

    return (
      <ContainerPage>
        <AppIntroSlider
          renderItem={this._renderItem}
          slides={slides}
          onSlideChange={index => {
            if (this.isValidSlide(this.state.indexSlide)) {
              this._goToSlide(index, false);
            } else {
              this._goToSlide(this.state.indexSlide);
            }
          }}
          ref={"appIntroSlider"}
          activeDotStyle={{ backgroundColor: colors.primaryColor }}
          skipLabel={null}
          nextLabel={null}
          prevLabel={null}
          doneLabel={null}
        />
      </ContainerPage>
    );
  };
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1
  },
  viewItemButton: {
    marginTop: 40
  },
  title: {
    textAlign: "center",
    fontSize: 18,
    marginTop: 40,
    marginBottom: 40
  },
  viewItem: {
    marginRight: 16,
    marginLeft: 16
  },
  viewItemInput: {},
  slide: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "#f7f7f7"
  },
  container: {},
  input: {
    marginBottom: 10
  },
  btnLogin: {
    marginTop: 40,
    backgroundColor: "white",
    paddingBottom: 4,
    paddingTop: 4,
    marginBottom: 20
  },
  textBtnLogin: {
    color: "#DC0C17"
  }
});

export default CadastroScreen;
