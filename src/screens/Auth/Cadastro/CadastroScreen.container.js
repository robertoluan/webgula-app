import CadastroScreen from "./CadastroScreen";
import { connect } from "react-redux"; 
import AccountsRemote from "../../../services/remote/accounts.remote";

const accountsRemote = new AccountsRemote();

const mapDispatchToProps = (dispatch, props) => ({
  async cadastrar(body) {
    return await accountsRemote.registrarUsuario(body);
  }
});

export default connect(
  null,
  mapDispatchToProps
)(CadastroScreen);
