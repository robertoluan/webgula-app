import RecuperarSenha from "./RecuperarSenha";
import { connect } from "react-redux"; 
import AccountsRemote from "../../../services/remote/accounts.remote";

const accountsRemote = new AccountsRemote();

const mapStateToProps = (state, props) => {
  let app = state.app || {};

  return {
    isAuth: app.auth && app.auth.isAuth,
    dados: (app.auth && app.auth.userInfo) || {}
  };
};

const mapDispatchToProps = (dispatch, props) => ({
  async recuperarSenha(email) {
    return accountsRemote.recuperarSenha(email);
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RecuperarSenha);
