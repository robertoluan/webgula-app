import React from "react";
import { Button, Toast } from "native-base";
import {
  TouchableOpacity,
  ScrollView,
  StyleSheet,
  View,
  Alert,
  Image
} from "react-native";
import Input from "../../../shared/components/Input";
import logoImg from "../../../../assets/images/logo-secondary.png";
import ButtonLoading from "../../../shared/components/ButtonLoading";
import { colors } from "../../../theme";
import TextCustom from "../../../shared/components/TextCustom";
import ContainerPage from "../../../shared/components/ContainerPage";
import { validateEmail } from "../../../infra/helpers";

class RecuperarSenha extends React.PureComponent {
  static navigationOptions = {
    title: "Recuperar Senha",
    headerRight: null
  };

  state = {
    email: "",
    isLoading: false,
    dataUltimoEnvio: null
  };

  recuperarSenha = async () => {
    const { email } = this.state;

    if (!email) {
      return Alert.alert("Aviso", "Informe o email.");
    }

    if (!validateEmail(email)) {
      return Alert.alert("Aviso", "Email está em um formato incorreto!");
    }

    this.setState({
      isLoading: true
    });

    try {
      const { success } = await this.props.recuperarSenha(email);

      if (!success) {
        Alert.alert("Aviso", "E-mail não existente!");
      } else {
        Alert.alert(
          "Aviso",
          "Foi enviado um email para recuperação da sua senha",
          [
            {
              text: "Ok",
              onPress: () => this.props.navigation.replace("Login")
            }
          ]
        );
      }
    } catch (e) {
      Alert.alert("Aviso", "Ops. Houve um problema ao realizar esta operação!");
    } finally {
      this.setState({
        isLoading: false
      });
    }
  };

  render = () => {
    return (
      <ContainerPage style={styles.wrapper}>
        <ScrollView style={styles.container} keyboardShouldPersistTaps="always">
          <TouchableOpacity style={styles.imgContainer}>
            <Image source={logoImg} style={styles.logo} />
          </TouchableOpacity>

          <View style={styles.formContainer}>
            <View>
              <Input
                label="Email"
                dark
                borderless
                keyboardType="email-address"
                value={this.state.email}
                onChangeText={email => this.setState({ email })}
              />
            </View>

            <View style={{ marginBottom: 30, marginTop: 16 }}>
              <ButtonLoading
                title="Recuperar"
                titleColor={colors.primaryColor}
                backgroundColor={"white"}
                loading={this.state.isLoading}
                onPress={() => this.recuperarSenha()}
              />

              <Button
                transparent
                light
                disabled={this.state.isLoading}
                block={true}
                style={{ marginTop: 16 }}
                onPress={() => this.props.navigation.replace("Login")}
              >
                <TextCustom style={styles.textBtnCadastrar}>
                  {"Fazer Login"}
                </TextCustom>
              </Button>
            </View>
          </View>
        </ScrollView>
      </ContainerPage>
    );
  };
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    flexDirection: "column",
    // justifyContent: "center",
    backgroundColor: colors.primaryColor
  },
  container: {
    marginRight: 0
  },
  logo: {
    alignSelf: "center",
    resizeMode: "contain",
    top: 30,
    marginBottom: 100,
    width: 170
  },
  textBtnCadastrar: {
    color: colors.white
  },
  formContainer: {
    flex: 1,
    marginRight: 16,
    marginLeft: 16,
    marginBottom: 10
  }
});

export default RecuperarSenha;
