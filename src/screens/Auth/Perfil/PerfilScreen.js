import React from "react";
import { DatePicker, Toast } from "native-base";
import {
  StyleSheet,
  ScrollView,
  Alert,
  Image,
  View,
  TouchableOpacity,
  ActivityIndicator
} from "react-native";
import { Row, Col } from "react-native-easy-grid";
import ImagePicker from "react-native-image-picker";
import ImageView from "react-native-image-view";
import moment from "moment";
import Input from "../../../shared/components/Input";
import Icon from "../../../shared/components/Icon";
import TextCustom from "../../../shared/components/TextCustom";
import ContainerPage from "../../../shared/components/ContainerPage";
import ButtonLoading from "../../../shared/components/ButtonLoading";
import InputSexo from "../../../shared/components/Input/InputSexo";
import InputMask from "../../../shared/components/Input/InputMask";
import { colors } from "../../../theme";

const options = {
  title: "Selecionar Imagem",
  cancelButtonTitle: "Cancelar",
  takePhotoButtonTitle: "Tirar da Camera",
  chooseFromLibraryButtonTitle: "Procurar nos Arquivos",
  storageOptions: {
    skipBackup: true,
    path: "images"
  }
};

const imageFake = require("../../../../assets/images/profile.png");

class PerfilScreen extends React.PureComponent {
  state = {
    visible: false,
    isLoadingFile: false,
    avatarSource: null,
    data_nascimento: null,
    cpf: "",
    nome: null,
    sexo: null,
    imagemUrl: null,
    imagemPreview: [],
    isLoading: false
  };

  componentDidMount = () => {
    let dateNative = null;

    const { auth } = this.props;

    if (auth.dataNascimento) {
      let data = moment(auth.dataNascimento);

      dateNative = new Date(
        data.format("YYYY"),
        Number(data.format("MM")) - 1,
        data.format("DD")
      );
    }

    let imagemUrl = null;

    if (typeof auth.imagemUrl === "string") {
      imagemUrl = { uri: auth.imagemUrl };
    } else if (typeof auth.imagemUrl === "object") {
      imagemUrl = auth.imagemUrl;
    }

    this.setState({
      nome: auth.nome,
      cpf: auth.cpf,
      email: auth.email,
      sexo: auth.sexo,
      data_nascimento: dateNative,
      imagemUrl: imagemUrl || imageFake
    });
  };

  static navigationOptions = {
    title: "Geral"
  };

  async salvar() {
    this.setState({
      isLoading: true
    });

    try {
      let { cpf, nome, sexo, data_nascimento } = this.state;

      if (!nome) {
        Toast.show({
          text: "Informe seu nome!"
        });
        return;
      }

      if (nome.split(" ").length < 2) {
        Toast.show({
          text: "Informe seu nome com sobrenome!"
        });
        return;
      }

      if (nome.length < 10) {
        Toast.show({
          text: "Nome precisa ter no mínimo 10 caracteres"
        });
        return;
      }

      let model = {
        cpf: cpf,
        nome: nome,
        sexo: sexo,
        dataNascimento: data_nascimento
      };

      let res = await this.props.salvar(model);

      if (res && res.type == "exception") {
        Alert.alert("Aviso", "Houve um problema ao salvar o perfil!");
      } else {
        Toast.show({ text: "Perfil salvo com sucesso!" });
        this.forceUpdate();
      }
    } catch (e) {
      Alert.alert("Aviso", "Ops. Houve um problema ao salvar o perfil!");
    } finally {
      this.setState({
        isLoading: false
      });
    }
  }

  async abrirFoto() {
    const self = this;

    ImagePicker.showImagePicker(options, async response => {
      if (response.didCancel) {
      } else if (response.error) {
      } else if (response.customButton) {
      } else {
        this.setState({ isLoadingFile: true });

        try {
          const res = await self.props.upload(response);

          if (res && res.type === "exception") {
            Alert.alert(
              "Aviso",
              "Houve um problema ao realizar está operação!"
            );
          } else {
            await this.props.atualizarImagem(res.imagemUrl);

            this.setState({
              imagemUrl: {
                uri: res.imagemUrl
              }
            });

            Toast.show({ text: "Imagem do perfil alterada com sucesso!" });
          }
        } catch (e) {
          if (e.response && e.response.data && e.response.data.message) {
            Alert.alert("Aviso", e.response.data.message);
          }
        } finally {
          this.setState({ isLoadingFile: false });
        }
      }
    });
  }

  async abrirPreview() {
    const { imagemUrl } = this.state;

    const imagemPreview = [
      {
        source: imagemUrl || imageFake,
        title: "",
        width: 806,
        height: 720
      }
    ];

    this.setState({ visible: true, imagemPreview });
  }

  render() {
    const {
      isLoading,
      visible,
      imagemUrl,
      data_nascimento,
      imagemPreview,
      isLoadingFile
    } = this.state;

    let placeholder = "99/99/9999";

    if (data_nascimento) {
      placeholder = moment(data_nascimento).format("DD/MM/YYYY");
    }

    return (
      <ContainerPage>
        <ImageView
          images={imagemPreview}
          imageIndex={0}
          onClose={() => this.setState({ visible: false })}
          isVisible={visible}
        />

        <ScrollView>
          <View style={styles.profileImage}>
            <TouchableOpacity onPress={() => this.abrirPreview()}>
              {isLoadingFile ? <ActivityIndicator /> : null}

              <Image
                style={{
                  borderWidth: 4,
                  borderColor: "#fafafa",
                  width: 110,
                  height: 110,
                  borderRadius: 55
                }}
                resizeMode="cover"
                source={imagemUrl}
                onError={() => {
                  this.setState({
                    imagemUrl: imageFake
                  });
                }}
              />
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.profileImageBadge}
              onPress={() => this.abrirFoto()}
            >
              <Icon color="white" size={15} name="ion-md-camera" />
            </TouchableOpacity>
          </View>

          <Row style={{ marginBottom: 16 }}>
            <Col style={styles.inputsContainer}>
              <Input
                style={styles.input}
                label="Nome"
                transparent
                value={this.state.nome}
                onChangeText={nome => this.setState({ nome })}
              />
              <TextCustom style={styles.labelFloat}>
                Data de Nascimento
              </TextCustom>
              <View style={styles.inputDatePicker}>
                <DatePicker
                  locale={"pt-br"}
                  modalTransparent={false}
                  animationType={"fade"}
                  androidMode={"default"}
                  transparent
                  placeHolderTextStyle={{
                    color: "#666",
                    fontSize: 14,
                    left: -10
                  }}
                  // formatChosenDate
                  placeHolderText={placeholder}
                  defaultDate={this.state.data_nascimento}
                  onDateChange={data_nascimento =>
                    this.setState({ data_nascimento })
                  }
                />
              </View>
              <Input
                label="Email"
                style={styles.input}
                transparent
                keyboardType="email-address"
                value={this.state.email}
                disabled={true}
                onChangeText={email => this.setState({ email })}
              />
              <InputMask
                label="CPF"
                type="cpf"
                style={{ marginBottom: 0 }}
                value={this.state.cpf}
                onChangeText={cpf => this.setState({ cpf })}
              />
              <View style={styles.inputSelect}>
                <InputSexo
                  label="Sexo"
                  transparent
                  disabled
                  value={this.state.sexo}
                  onChangeText={sexo => this.setState({ sexo })}
                />
              </View>
            </Col>
          </Row>
        </ScrollView>

        <View
          style={{
            backgroundColor: "#F7F7F7",
            marginTop: 16,
            paddingBottom: 8,
            paddingTop: 8,
            paddingLeft: 16,
            paddingRight: 16
          }}
        >
          <ButtonLoading
            title="Salvar"
            loading={isLoading}
            onPress={() => this.salvar()}
          />
        </View>
      </ContainerPage>
    );
  }
}

const styles = StyleSheet.create({
  labelFloat: {
    fontSize: 13,
    bottom: -4,
    color: "#666",
    position: "relative"
  },
  profileImage: {
    width: 130,
    marginTop: 20,
    paddingBottom: 10,
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center"
  },
  profileImageBadge: {
    width: 35,
    height: 35,
    borderRadius: 35,
    bottom: 10,
    right: 0,
    paddingTop: 10,
    backgroundColor: colors.primaryColor,
    color: "white",
    position: "absolute",
    alignItems: "center",
    alignSelf: "center"
  },
  inputsContainer: {
    top: 20,
    marginRight: 16,
    marginLeft: 16
  },
  input: {
    marginBottom: 10
  },
  inputSelect: {
    marginBottom: 10,
    marginTop: -10
  },
  inputDatePicker: {
    marginTop: 6,
    marginBottom: 11,
    paddingLeft: 8,
    borderRadius: 6,
    borderWidth: 1,
    borderColor: "#ccc"
  }
});

export default PerfilScreen;
