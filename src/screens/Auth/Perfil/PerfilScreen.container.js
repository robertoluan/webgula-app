import PerfilScreen from "./PerfilScreen";
import { connect } from "react-redux";
import { setLoading } from "../../../services/actions/layout/global.actions";
import AccountRemote from "../../../services/remote/accounts.remote";
import authContext from "../../../infra/auth";

const Api = new AccountRemote();

const mapStateProps = (state, props) => {
  let app = state.app || {};

  return {
    auth2: app.auth || {},
    auth: (app.auth && app.auth.userInfo) || {}
  };
};

const mapDispatchToProps = (dispatch, props) => ({
  async salvar(model) {
    return await Api.atualizarUsuario(model);
  },

  async atualizarImagem(imagemUrlCompleta) {
    let authData = await authContext.userDataAsync();

    authData.userInfo.imagemUrl = imagemUrlCompleta || null;

    authContext.setAuth({ ...authData });

    // dispatch({
    //   type: "APP.AUTH.LOAD",
    //   userInfo: authData.userInfo,
    //   isAuth: true,
    //   token: authContext.token,
    //   access_token: authContext.access_token,
    // });
  },

  async upload(file) {
    return await Api.uploadImage(file);
  },

  setLoading() {
    dispatch(setLoading(true, "Salvando..."));
  },

  clearLoading() {
    dispatch(setLoading(false));
  }
});

export default connect(
  mapStateProps,
  mapDispatchToProps
)(PerfilScreen);
