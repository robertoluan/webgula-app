import LoginEmailSenha from "./LoginEmailSenha";
import LoginSegundaEtapa from "./LoginSegundaEtapa";
import Senha from "./Senha";
import Perfil from "./Perfil";
import Login from "./Login";
import Cadastro from "./Cadastro";
import Endereco from "./Endereco";
import Contato from "./Contato";
import RecuperarSenha from "./RecuperarSenha";

export default {
  LoginEmailSenha,
  LoginSegundaEtapa,
  Senha,
  Perfil,
  Login,
  Cadastro,
  Endereco,
  Contato,
  RecuperarSenha
};
