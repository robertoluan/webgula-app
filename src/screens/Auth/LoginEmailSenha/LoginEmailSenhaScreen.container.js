import LoginEmailSenhaScreen from "./LoginEmailSenhaScreen";
import AccountsRemote from "../../../services/remote/accounts.remote";
import { connect } from "react-redux";
import { setLoading } from "../../../services/actions/layout/global.actions";
import authContext from "../../../infra/auth";
import { Clipboard, Alert } from "react-native";
import { setAuth } from "../../../services/actions/app/auth.actions";

const accountsRemote = new AccountsRemote();

const mapStateToProps = (state, props) => {
  let app = state.app || {};

  return {
    isAuth: app.auth && app.auth.isAuth,
    isClientCredentials: app.auth && app.auth.isClientCredentials,
    auth: (app.auth && app.auth.userInfo) || {}
  };
};

const mapDispatchToProps = (dispatch, props) => ({
  async logar(email, senha) {
    const result = await authContext.login(email, senha);

    if (result.success) {
      if (__DEV__) {
        Clipboard.setString(result.token);
      }

      try {
        await accountsRemote.atualizarToken();
      } catch (e) {}
    }

    return result;
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginEmailSenhaScreen);
