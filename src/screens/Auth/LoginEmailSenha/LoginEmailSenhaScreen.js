import React from "react";
import { Toast } from "native-base";
import {
  TouchableOpacity,
  ScrollView,
  StyleSheet,
  View,
  Image,
  Alert
} from "react-native";
import { Row, Col } from "react-native-easy-grid";
import Input from "../../../shared/components/Input";
import logoImg from "../../../../assets/images/logo-secondary.png";
import ButtonLoading from "../../../shared/components/ButtonLoading";
import { colors } from "../../../theme";
import ContainerPage from "../../../shared/components/ContainerPage";
import { validateEmail } from "../../../infra/helpers";
import TextCustom from "../../../shared/components/TextCustom";

class LoginEmailSenhaScreen extends React.PureComponent {
  static navigationOptions = {
    header: null
  };

  state = {
    email: "",
    senha: "",
    isLoading: false,
    loadingPular: false,
    codigoEnviado: false
  };

  logar = async () => {
    const { email, senha } = this.state;
    const { logar } = this.props;

    if (!email) {
      Toast.show({
        text: "Informe o email!"
      });
      return;
    }

    if (!senha) {
      Toast.show({
        text: "Informe a senha!"
      });
      return;
    }

    if (!validateEmail(email)) {
      Toast.show({
        text: "Email está em um formato incorreto!"
      });
      return;
    }

    this.setState({
      isLoading: true
    });

    try {
      const response = await logar(email, senha);

      if (!response.success) {
        Alert.alert("Erro", response.error, [
          { text: "OK", onPress: () => {} }
        ]);
      } else {
        setTimeout(() => {
          this.updateRedirectApp(this.props);
        }, 300);
      }
    } catch (e) {
      Alert.alert("Login WebGula", "Ops! Insira um login e uma senha válidos.");
    } finally {
      this.setState({
        isLoading: false
      });
    }
  };

  updateRedirectApp = props => {
    const { auth, navigation } = props;

    if (auth["phone-confirmed"] !== "true") {
      return navigation.replace("LoginSegundaEtapa", {
        VeioLogin: true
      });
    }

    navigation.replace("Home");
  };

  novoCadastro = () => {
    this.props.navigation.navigate("Cadastro");
  };

  componentDidMount = async () => {
    const { isAuth } = this.props;

    if (isAuth) {
      return this.updateRedirectApp(this.props);
    }
  };

  pularLogin = async () => {
    return this.props.navigation.navigate("Home");
  };

  render = () => {
    const { senha, email } = this.state;

    return (
      <ContainerPage style={styles.wrapper}>
        <ScrollView style={styles.container} keyboardShouldPersistTaps="always">
          <TouchableOpacity>
            <Image source={logoImg} style={styles.logo} />
          </TouchableOpacity>

          <View style={styles.formContainer}>
            <View
              style={{
                marginRight: 10
              }}
            >
              <Input
                label={"Email"}
                dark
                borderless
                transparent
                keyboardType="email-address"
                value={email}
                inputStyle={{ height: 50 }}
                onChangeText={email => this.setState({ email })}
              />
            </View>

            <View
              style={{
                marginRight: 10
              }}
            >
              <Input
                label={"Senha"}
                dark
                borderless
                transparent
                secureTextEntry
                value={senha}
                inputStyle={{ height: 50 }}
                onChangeText={senha => this.setState({ senha })}
              />
            </View>

            <View>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate("RecuperarSenha")}
              >
                <TextCustom
                  style={{
                    color: "white",
                    fontSize: 12,
                    marginTop: 6,
                    marginBottom: 6
                  }}
                >
                  Recuperar Senha
                </TextCustom>
              </TouchableOpacity>
            </View>

            <View style={{ marginTop: 16 }}>
              <ButtonLoading
                title="Entrar"
                titleColor={colors.primaryColor}
                disabled={this.state.loadingPular}
                backgroundColor={"white"}
                loading={this.state.isLoading}
                onPress={() => this.logar()}
              />
            </View>

            <View style={{ marginTop: 32, height: 90, flexDirection: "row" }}>
              <View style={{ flex: 1 }}>
                <ButtonLoading
                  title="Novo Cadastro"
                  titleColor={colors.white}
                  backgroundColor={colors.primaryColorDark}
                  onPress={async () => this.novoCadastro()}
                />
              </View>
              <View style={{ flex: 1 }}>
                <ButtonLoading
                  title="Pular"
                  loading={this.state.loadingPular}
                  titleColor={colors.white}
                  backgroundColor={colors.primaryColor}
                  onPress={async () => this.pularLogin()}
                />
              </View>
            </View>
          </View>
        </ScrollView>
      </ContainerPage>
    );
  };
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: colors.primaryColor
  },
  container: {
    marginRight: 0
  },
  logo: {
    alignSelf: "center",
    resizeMode: "contain",
    top: 50,
    marginBottom: 100,
    width: 170
  },
  formContainer: {
    flex: 1,
    top: 30,
    flexDirection: "column",
    marginRight: 16,
    marginLeft: 16
  }
});

export default LoginEmailSenhaScreen;
