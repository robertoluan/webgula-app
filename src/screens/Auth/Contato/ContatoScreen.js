import React from "react";
import {
  Container,
  ListItem,
  Left,
  Toast,
  Fab,
  Text,
  Body,
  Icon as IconNativeBase,
  ActionSheet
} from "native-base";
import { ScrollView, Alert, View, FlatList, StyleSheet } from "react-native";
import TextCustom from "../../../shared/components/TextCustom";
import SkeletonLoading from "../../../shared/components/SkeletonLoading";
import { colors } from "../../../theme";
import Icon from "../../../shared/components/Icon";
import ModalContato from "../../../shared/components/ModalContato/ModalContato";

var BUTTONS = ["Editar", "Excluir", "Cancelar"];
var DESTRUCTIVE_INDEX = 1;
var CANCEL_INDEX = 2;

class ContatoScreen extends React.PureComponent {
  state = {
    items: [],
    isLoading: false,
    telefone: {},
    open: false
  };

  static navigationOptions = {
    title: "Contatos"
  };

  async componentDidMount() {
    this.setState({
      isLoading: true
    });

    try {
      const data = await this.props.init();

      this.setState({
        items: data
      });
    } catch (e) {
    } finally {
      this.setState({
        isLoading: false
      });
    }
  }

  onDelete = item => {
    Alert.alert("Aviso", "Deseja realmente excluir este endereço ?", [
      { text: "Não", onPress: () => {} },
      {
        text: "Sim",
        onPress: async () => {
          this.setState({
            isLoading: true
          });
          try {
            await this.props.onExcluir(item);
            Toast.show({
              text: "Operação realizada com sucesso!"
            });
            this.componentDidMount();
          } catch (e) {
          } finally {
            this.setState({
              isLoading: false
            });
          }
        }
      }
    ]);
  };

  onEdit = item => {
    this.setState({
      open: true,
      telefone: { ...item }
    });
  };

  onAdd = () => {
    this.setState({
      open: true
    });
  };

  get isEmpty() {
    const { items, isLoading } = this.state;
    return !isLoading && (!items || (items && !items.length));
  }

  render = () => {
    const { items, isLoading, open, telefone } = this.state;
    return (
      <Container>
        <ModalContato
          telefone={telefone}
          onSave={() => {
            this.setState({ open: false, telefone: {} });
            this.componentDidMount();
          }}
          onCancel={() => this.setState({ open: false })}
          open={open}
        />

        <ScrollView>
          <View style={styles.containerView}>
            <SkeletonLoading lines={5} show={isLoading}>
              <FlatList
                data={items}
                renderItem={({ item }) => (
                  <ListItem
                    style={{
                      paddingBottom: 8,
                      backgroundColor: "white"
                    }}
                    noIndent={true}
                    onPress={() =>
                      ActionSheet.show(
                        {
                          options: BUTTONS,
                          cancelButtonIndex: CANCEL_INDEX,
                          destructiveButtonIndex: DESTRUCTIVE_INDEX
                        },
                        buttonIndex => {
                          if (buttonIndex == 0) {
                            this.onEdit(item);
                          }
                          if (buttonIndex == 1) {
                            this.onDelete(item);
                          }
                        }
                      )
                    }
                    avatar
                  >
                    <Left>
                      <Icon size={22} name="fa-phone" />
                    </Left>

                    <Body>
                      <Text>
                        <TextCustom>
                          ({item.codDdd}) {item.telefone}
                        </TextCustom>
                      </Text>
                    </Body>
                  </ListItem>
                )}
              />
            </SkeletonLoading>
          </View>
        </ScrollView>

        {this.isEmpty ? (
          <View style={styles.viewEmpty}>
            <Icon style={styles.icon} name="fa-phone" />
            <TextCustom style={styles.textEmpty}>
              Nenhum registro existente.
            </TextCustom>
          </View>
        ) : null}

        <Fab
          style={{ backgroundColor: colors.primaryColor }}
          position="bottomRight"
          onPress={() => this.onAdd()}
        >
          <IconNativeBase name="add" />
        </Fab>
      </Container>
    );
  };
}

const styles = StyleSheet.create({
  containerView: {
    flex: 1,
    marginTop: 16,
    marginRight: 16,
    marginLeft: 16
  },
  viewEmpty: {
    marginTop: 25,
    marginBottom: 25,
    textAlign: "center",
    alignSelf: "center",
    justifyContent: "center",
    paddingTop: 70
  },
  textEmpty: {
    fontSize: 24,
    flex: 1,
    paddingRight: 32,
    paddingLeft: 32,
    marginTop: -30,
    textAlign: "center",
    opacity: 0.6
  },
  icon: {
    flex: 1,
    textAlign: "center",
    alignSelf: "center",
    fontSize: 60,
    opacity: 0.4,
    marginBottom: 0
  }
});

export default ContatoScreen;
