import ContatoScreen from "./ContatoScreen";
import { connect } from "react-redux";
import AccountRemote from "../../../services/remote/accounts.remote";

const Api = new AccountRemote();

const mapStateProps = (state, props) => {
  let app = state.app || {};

  return {
    auth: (app.auth && app.auth.userInfo) || {}
  };
};

const mapDispatchToProps = (dispatch, props) => ({
  async init() {
    return await Api.getTelefones();
  },
  async onExcluir(model) {
    return await Api.deleteTelefonePessoa(model);
  }
});

export default connect(
  mapStateProps,
  mapDispatchToProps
)(ContatoScreen);
