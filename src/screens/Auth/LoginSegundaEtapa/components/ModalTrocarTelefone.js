import React from "react";
import { View, StyleSheet, ActivityIndicator, Alert } from "react-native";
import { colors } from "../../../../theme";
import ButtonLoading from "../../../../shared/components/ButtonLoading";
import Dialog from "../../../../shared/components/Dialog";
import { Content, Toast } from "native-base";
import { isNumber } from "../../../../infra/utils/number.helpers";
import AccountRemove from "../../../../services/remote/accounts.remote";
import InputMask from "../../../../shared/components/Input/InputMask";
import Input from "../../../../shared/components/Input";
import { Row, Col } from "react-native-easy-grid";

class ModalTrocarTelefone extends React.Component {
  state = {
    telefone: "",
    ddi: "55",
    isLoading: false
  };

  constructor(props) {
    super(props);

    this.onChange = this.onChange.bind(this);
  }

  onChange = async telefone => {
    this.setState({
      telefone: telefone
    });
  };

  onSalvar = async () => {
    const { telefone, ddi } = this.state;

    if (!telefone) {
      Alert.alert("Aviso", "Informe o Telefone!");
      return;
    }

    if (!ddi) {
      Alert.alert("Aviso", "Informe o código do Pais!");
      return;
    }

    if (!isNumber(telefone)) {
      Alert.alert("Aviso", "Telefone está em formato incorreto!");
      return;
    }

    if (telefone.toString().length < 11) {
      Alert.alert(
        "Aviso",
        "Telefone deve possui o DDD juntamente com o número!"
      );
      return;
    }

    if (telefone.toString().length > 11) {
      Alert.alert(
        "Aviso",
        "Telefone é maior do que o tamanho permitido de 11 digitos!"
      );
      return;
    }

    this.setState({
      isLoading: true
    });

    try {
      const res = await this.props.enviarSolicitacaoTrocarTelefone(
        ddi + telefone
      );

      if ("success" in res) {
        if (res.success) {
          this.props.onOk({ ...res.data, telefone: ddi + telefone });
        } else {
          Alert.alert("Aviso", "Houve um problema ao realizar esta operação!");
        }
      }
    } catch (e) {
      Alert.alert("Aviso", "Ops. Houve um problema ao realizar esta operação!");
    } finally {
      this.setState({
        isLoading: false
      });
    }
  };

  render = () => {
    const { open } = this.props;

    return (
      <Dialog
        open={open}
        title={"Alterar Telefone"}
        onCancel={() => this.props.onCancel()}
        onOk={() => this.props.onOk()}
      >
        <Content
          style={{ borderBottomLeftRadius: 20, borderBottomRightRadius: 20 }}
        >
          <View style={styles.containerView}>
            <View style={{ flexDirection: "row" }}>
              <View style={{ paddingRight: 10, width: 70 }}>
                <Input
                  label={"País"}
                  keyboardType="numeric"
                  value={this.state.ddi}
                  onChangeText={ddi => this.setState({ ddi })}
                />
              </View>
              <View style={{ flex: 1 }}>
                <InputMask
                  label={"Novo Telefone"}
                  type={"cel-phone"}
                  options={{
                    maskType: "BRL",
                    withDDD: true,
                    dddMask: "(99) "
                  }}
                  value={this.state.telefone}
                  onChangeText={value => this.setState({ telefone: value })}
                />
              </View>
            </View>
          </View>

          <View style={styles.bottom}>
            <ButtonLoading
              loading={this.state.isLoading}
              onPress={() => this.onSalvar()}
              title="Salvar"
            />

            <View style={{ height: 10 }} />

            <ButtonLoading
              onPress={() => this.props.onCancel()}
              backgroundColor={"white"}
              titleColor={colors.grayColor}
              title="Cancelar"
            />
          </View>
        </Content>
      </Dialog>
    );
  };
}

const styles = StyleSheet.create({
  containerView: {
    marginTop: 16,
    flexWrap: "wrap",
    justifyContent: "center"
  },
  bottom: {
    marginTop: 50
  },
  selectDropdown: {
    borderWidth: 2,
    borderColor: colors.grayBackground,
    height: 40,
    marginBottom: 20,
    paddingTop: 10,
    paddingLeft: 6,
    borderRadius: 5
  }
});

export default ModalTrocarTelefone;
