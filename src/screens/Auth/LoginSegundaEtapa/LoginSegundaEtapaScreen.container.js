import LoginSegundaEtapaScreen from "./LoginSegundaEtapaScreen";
import { connect } from "react-redux";
import { setLoading } from "../../../services/actions/layout/global.actions";
import AccountsRemote from "../../../services/remote/accounts.remote";

const accountsRemote = new AccountsRemote();

const mapStateToProps = (state, props) => {
  let app = state.app || {};

  return {
    isAuth: app.auth && app.auth.isAuth,
    dados: (app.auth && app.auth.userInfo) || {}
  };
};

const mapDispatchToProps = (dispatch, props) => ({
  async confirmarSMSComTelefoneTrocado(novoTelefone, codigoSms, autorizacaoTrocarTelefone) {
    return accountsRemote.confirmarSMSComTelefoneTrocado(novoTelefone, codigoSms, autorizacaoTrocarTelefone);
  },

  async pedirSms(telefone) {
    return accountsRemote.pedirSMS(telefone);
  },

  async confirmarSMS(codigoSms) {
    return await accountsRemote.confirmarSMS(codigoSms);
  },

  setLoading() {
    dispatch(setLoading(true, "Entrando..."));
  },

  clearLoading() {
    dispatch(setLoading(false));
  },

  closeModal() {
    dispatch({
      type: "LAYOUT.MODAL_AUTH.CLOSE"
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginSegundaEtapaScreen);
