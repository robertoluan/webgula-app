import React from "react";
import { Button, Toast } from "native-base";
import {
  TouchableOpacity,
  ScrollView,
  StyleSheet,
  View,
  Alert,
  Image
} from "react-native";
import { Row, Col } from "react-native-easy-grid";
import Input from "../../../shared/components/Input";
import logoImg from "../../../../assets/images/logo-secondary.png";
import ButtonLoading from "../../../shared/components/ButtonLoading";
import { colors } from "../../../theme";
import TextCustom from "../../../shared/components/TextCustom";
import ContainerPage from "../../../shared/components/ContainerPage";
import moment from "moment";
import ModalTrocarTelefone from "./components/ModalTrocarTelefone";
import AccountsRemote from "../../../services/remote/accounts.remote";
import authContext from "../../../infra/auth";

class LoginSegundaEtapaScreen extends React.PureComponent {
  static navigationOptions = {
    headerRight: null
  };

  state = {
    codigoSms: "",
    isLoading: false,
    dataUltimoEnvio: null,
    openModalTrocarTelefone: false,
    autorizacaoTrocarTelefone: null,
    telefoneTrocado: null
  };

  setStateAsync = async state => {
    return new Promise(resolve => this.setState({ ...state }, resolve));
  };

  confirmarSMS = async () => {
    if (!this.state.codigoSms) {
      return Alert.alert("Aviso", "Informe o código do SMS.");
    }

    // CASO VENHA DA TROCA DE TELEFONE ELE VALIDA O SMS EM OUTRO ENDPOINT
    if (this.state.autorizacaoTrocarTelefone) {
      return this.confirmarSMSTelefoneTrocado();
    }

    this.setState({
      isLoading: true
    });

    try {
      const res = await this.props.confirmarSMS(this.state.codigoSms);

      if (!res.success) {
        Alert.alert("Aviso", "Código de SMS incorreto!");
      } else {

        await authContext.updateUserInfo();

        this.props.navigation.replace("Home");
      }
    } catch (e) {
      Alert.alert("Aviso", "Houve um problema ao realizar esta operação!");
    } finally {
      this.setState({
        isLoading: false
      });
    }
  };

  componentDidMount = async () => {
    if (this.props.navigation.getParam("VeioLogin")) {
      await this.reenviarOrRecuperarCodigo();
    }

    if (this.props.navigation.getParam("VeioCadastro")) {
      this.setState({
        dataUltimoEnvio: moment()
      });
    }
  };

  validarTempoEnvioSMS = () => {
    const { dataUltimoEnvio } = this.state;

    if (dataUltimoEnvio) {
      const ms = moment().diff(moment(dataUltimoEnvio));
      const d = moment.duration(ms);

      if (d.seconds() < 30) {
        Alert.alert(
          "Aviso",
          "Aguarde " +
          (30 - d.seconds()) +
          "(s) para enviar a próxima solicitação!"
        );

        return true;
      }
    }

    return false;
  };

  reenviarOrRecuperarCodigo = async () => {
    // CASO TENHA ALTERADO O TELEFONE TROCA DE ENDPOINT
    if (this.state.autorizacaoTrocarTelefone) {
      return this.enviarSolicitacaoTrocarTelefone(this.state.telefoneTrocado);
    }

    if (this.validarTempoEnvioSMS()) return;

    const { pedirSms, dados } = this.props;

    this.setState({
      isLoading: true
    });

    let telefone = dados.telefone; // TODO mudar pra telefone

    if (telefone.length === 11) {
      telefone = "55" + telefone;
    }

    try {
      const res = await pedirSms(telefone);

      this.setState({
        dataUltimoEnvio: moment()
      });

      if (!res.success) {
        Alert.alert(
          "Aviso",
          "Houve um problema ao enviar o SMS, seu telefone está realmente correto?"
        );
      }

      Toast.show({
        text: "Foi enviado um código de SMS para este telefone."
      });
    } catch (e) {
      Alert.alert(
        "Aviso",
        "Houve um problema ao registrar, seu telefone está realmente correto?"
      );
    } finally {
      this.setState({
        isLoading: false
      });
    }
  };

  enviarSolicitacaoTrocarTelefone = async telefone => {
    if (this.validarTempoEnvioSMS()) return;
    if (this.state.isLoading) return;

    this.setState({
      telefoneTrocado: telefone,
      isLoading: true
    });

    const res = await new AccountsRemote().requisitarAutorizacaoTrocarTelefone(
      telefone
    );

    this.setState({
      dataUltimoEnvio: moment(),
      isLoading: false
    });

    return res;
  };

  confirmarSMSTelefoneTrocado = async () => {
    const { confirmarSMSComTelefoneTrocado } = this.props;

    const {
      codigoSms,
      telefoneTrocado,
      autorizacaoTrocarTelefone
    } = this.state;

    this.setState({
      isLoading: true
    });

    const telefone = telefoneTrocado;

    // Troca o celular e confirma SMS
    try {
      const res = await confirmarSMSComTelefoneTrocado(
        telefone,
        codigoSms,
        autorizacaoTrocarTelefone
      );

      console.log("RES confirmarSMSComTelefoneTrocado", res);

      if (!res.success) {
        Alert.alert("Aviso", "Houve um problema ao validar o SMS.");
        return;
      }

      // todo get userinfo
      await authContext.updateUserInfo();

      this.props.navigation.replace("Home");
    } catch (e) {
      Alert.alert(
        "Aviso",
        "Houve um problema ao registrar, o SMS está realmente correto ?"
      );
    } finally {
      this.setState({
        isLoading: false
      });
    }
  };

  onTrocarTelefone = async () => {
    this.setState({
      openModalTrocarTelefone: true
    });
  };

  render = () => {
    const { openModalTrocarTelefone } = this.state;

    return (
      <ContainerPage style={[styles.wrapper]}>
        {openModalTrocarTelefone ? (
          <ModalTrocarTelefone
            enviarSolicitacaoTrocarTelefone={async telefoneCompleto => {
              await this.setStateAsync({
                dataUltimoEnvio: null
              });

              return this.enviarSolicitacaoTrocarTelefone(telefoneCompleto);
            }}
            onCancel={() =>
              this.setState({
                openModalTrocarTelefone: false
              })
            }
            onOk={res => {
              if (res.autorizacao) {
                Alert.alert(
                  "Aviso",
                  "Telefone alterado com sucesso! Enviaremos um código SMS para confirmar o telefone.",
                  [
                    {
                      text: "OK",
                      onPress: () => {
                        this.setState({
                          autorizacaoTrocarTelefone: res.autorizacao,
                          telefoneTrocado: res.telefone,
                          openModalTrocarTelefone: false
                        });
                      }
                    }
                  ]
                );
              }
            }}
            open={openModalTrocarTelefone}
          />
        ) : null}

        <ScrollView style={styles.container} keyboardShouldPersistTaps="always">
          <TouchableOpacity style={styles.imgContainer}>
            <Image source={logoImg} style={styles.logo} />
          </TouchableOpacity>

          <View style={styles.formContainer}>
            <Row>
              <Col>
                <Input
                  label="Código SMS"
                  dark
                  borderless
                  keyboardType="numeric"
                  value={this.state.codigoSms}
                  onChangeText={codigoSms => this.setState({ codigoSms })}
                />
              </Col>
            </Row>

            <View>
              <TouchableOpacity onPress={() => this.onTrocarTelefone()}>
                <TextCustom
                  style={{ color: "white", marginTop: 6, marginBottom: 6 }}
                >
                  Trocar Telefone
                </TextCustom>
              </TouchableOpacity>
            </View>

            <Row style={{ marginBottom: 30 }}>
              <Col style={{ marginTop: 16 }}>
                <ButtonLoading
                  title="Entrar"
                  titleColor={colors.primaryColor}
                  backgroundColor={"white"}
                  loading={this.state.isLoading}
                  onPress={() => this.confirmarSMS()}
                />

                <Button
                  transparent
                  light
                  disabled={this.state.isLoading}
                  block={true}
                  style={{ marginTop: 16 }}
                  onPress={() => this.reenviarOrRecuperarCodigo()}
                >
                  <TextCustom style={styles.textBtnCadastrar}>
                    {"Reenviar Código"}
                  </TextCustom>
                </Button>
              </Col>
            </Row>
          </View>
        </ScrollView>
      </ContainerPage>
    );
  };
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    flexDirection: "column",
    // justifyContent: "center",
    backgroundColor: colors.primaryColor
  },
  container: {
    marginRight: 0
  },
  logo: {
    alignSelf: "center",
    resizeMode: "contain",
    top: 30,
    marginBottom: 100,
    width: 170
  },
  textBtnCadastrar: {
    color: colors.white
  },
  formContainer: {
    flex: 1,
    marginRight: 16,
    marginLeft: 16,
    marginBottom: 10
  }
});

export default LoginSegundaEtapaScreen;
