import React from "react";
import { ListItem, Left, Text, Body } from "native-base";
import TextCustom from "../../../shared/components/TextCustom";
import Icon from "../../../shared/components/Icon";
import { colors } from "../../../theme";
import { formatMoney } from "../../../infra/helpers";

class EnderecoItem extends React.PureComponent {
  render() {
    const { item, onPress, selected } = this.props;
    return (
      <ListItem
        style={{
          paddingBottom: 8,
          backgroundColor: "white",
          borderRadius: 5,
          borderColor: selected ? colors.primaryColor : "#ccc",
          borderWidth: 1,
          marginBottom: 10
        }}
        noIndent={true}
        onPress={() => onPress()}
        avatar
        noBorder
      >
        <Left>
          <Icon size={22} name="fa-compass" />
        </Left>

        <Body>
          <Text>
            <TextCustom>{item.nome}</TextCustom>
          </Text>
          <Text>
            {item.taxaEntrega ? (
              <TextCustom
                style={{
                  fontSize: 11
                }}
              >
                Taxa de Entrega{" "}
                <TextCustom
                  style={{ color: "green", fontFamily: "Montserrat-Bold" }}
                >
                  R$ {formatMoney(item.taxaEntrega)}{" "}
                </TextCustom>
              </TextCustom>
            ) : null}
          </Text>
          <Text note>
            <TextCustom>
              {item.logradouro},{item.nomeBairro},{item.nomeCidade} -{" "}
              {item.nomeEstado}
            </TextCustom>
          </Text>
        </Body>
      </ListItem>
    );
  }
}

export default EnderecoItem;
