import EnderecoScreen from "./EnderecoScreen";
import { connect } from "react-redux";
import AccountRemote from "../../../services/remote/accounts.remote";
import { setEnderecoAtual } from "../../../services/actions/app/auth.actions";

const Api = new AccountRemote();

const mapStateProps = (state, props) => {
  let app = state.app || {};

  return {
    enderecoEntrega: (app.endereco && app.endereco.atual) || {},
    auth: (app.auth && app.auth.userInfo) || {},
    empresaSelecionada: app.empresa && app.empresa.empresaSelecionada
  };
};

const mapDispatchToProps = (dispatch, props) => ({
  async init() {
    return await Api.getEnderecosPessoa();
  },
  async onExcluir(model) {
    return await Api.deleteEnderecosPessoa(model);
  },
  async setEnderecoAtual(endereco) {
    
    if (endereco && endereco.id) {
      await Api.setPrincipalEnderecosPessoa(endereco.id);
    }

    return dispatch(setEnderecoAtual(endereco));
  }
});

export default connect(
  mapStateProps,
  mapDispatchToProps
)(EnderecoScreen);
