import React from 'react';
import { Toast, Fab, Icon as IconNativeBase, ActionSheet } from 'native-base';
import { ScrollView, Alert, View, FlatList, StyleSheet } from 'react-native';
import TextCustom from '../../../shared/components/TextCustom';
import SkeletonLoading from '../../../shared/components/SkeletonLoading';
import { colors } from '../../../theme';
import Icon from '../../../shared/components/Icon';
import ModalEndereco from '../../../shared/components/ModalEndereco';
import EnderecoItem from './EnderecoItem';
import ContainerPage from '../../../shared/components/ContainerPage';
import ButtonLoading from '../../../shared/components/ButtonLoading';
import { withNavigationFocus } from 'react-navigation';

var BUTTONS = ['Definir como Entrega', 'Editar', 'Excluir', 'Cancelar'];
var DESTRUCTIVE_INDEX = 2;
var CANCEL_INDEX = 3;

class EnderecoScreen extends React.PureComponent {
  state = {
    items: [],
    open: false,
    endereco: {},
    isLoading: false
  };

  static navigationOptions = {
    title: 'Endereço de Entrega'
  };

  async componentDidMount() {
    this.setState({
      isLoading: true
    });

    try {
      const data = await this.props.init();

      this.setState({
        items: data
      });
    } catch (e) {
    } finally {
      this.setState({
        isLoading: false
      });
    }
  }

  componentDidUpdate = async prevProps => {
    if (prevProps.isFocused !== this.props.isFocused && this.props.isFocused) {
      await this.componentDidMount();
    }
  };

  onDelete = (item, isEnderecoEntrega = false) => {
    Alert.alert('Aviso', 'Deseja realmente excluir este endereço ?', [
      { text: 'Cancelar' },
      {
        text: 'Sim',
        onPress: async () => {
          this.setState({
            isLoading: true
          });

          try {
            await this.props.onExcluir(item);

            if (isEnderecoEntrega) {
              await this.props.setEnderecoAtual(null);
            }

            Toast.show({
              text: 'Operação realizada com sucesso!'
            });
            this.componentDidMount();
          } catch (e) {
          } finally {
            this.setState({
              isLoading: false
            });
          }
        }
      }
    ]);
  };

  onAddEntrega = async item => {
    if (
      this.props.empresaSelecionada &&
      this.props.empresaSelecionada.id &&
      !item.flgAceitaEntrega
    ) {
      return Alert.alert(
        'Aviso',
        'Ops. O Estabelecimento não entrega neste endereço!'
      );
    }

    const VeioExterno = !!this.props.navigation.getParam('VeioExterno');
    const ViewDestino = this.props.navigation.getParam('ViewDestino');

    this.setState({ isLoading: true });

    await this.props.setEnderecoAtual(item);

    this.setState({ isLoading: false });

    if (VeioExterno) {
      if (ViewDestino) {
        this.props.navigation.navigate(ViewDestino);
      } else {
        this.props.navigation.goBack();
      }
    }
  };

  onEdit = item => {
    this.setState({
      endereco: { ...item }
    });

    this.props.navigation.navigate('ModalEndereco', {
      endereco: { ...item }
    });
  };

  onAdd = () => {
    this.props.navigation.navigate('ModalEndereco');
  };

  get isEmpty() {
    const { items, isLoading } = this.state;
    return !isLoading && (!items || (items && !items.length));
  }

  render() {
    const { items = [], isLoading, open, endereco } = this.state;
    const { enderecoEntrega } = this.props;

    return (
      <ContainerPage style={{ backgroundColor: '#f7f7f7' }}>
        {/* {open ? (
          <ModalEndereco
            endereco={endereco}
            onSave={() => {
              this.setState({ open: false });
              this.componentDidMount();
            }}
            onCancel={() => this.setState({ open: false })}
            open={open}
          />
        ) : null} */}

        <ScrollView>
          <View
            style={{
              flex: 1,
              marginLeft: isLoading ? 16 : 0,
              marginRight: isLoading ? 16 : 0,
              marginTop: 16
            }}
          >
            <SkeletonLoading lines={4} show={isLoading}>
              <View style={styles.selectedAddress}>
                {enderecoEntrega.id ? (
                  <EnderecoItem
                    item={enderecoEntrega}
                    selected={true}
                    onPress={() =>
                      ActionSheet.show(
                        {
                          options: ['Editar', 'Excluir', 'Cancelar'],
                          cancelButtonIndex: 2,
                          destructiveButtonIndex: 1
                        },
                        buttonIndex => {
                          if (buttonIndex == 0) {
                            this.onEdit(enderecoEntrega);
                          }
                          if (buttonIndex == 1) {
                            this.onDelete(enderecoEntrega, true);
                          }
                        }
                      )
                    }
                  />
                ) : (
                  <View style={{ marginTop: 8, marginBottom: 8 }}>
                    <ButtonLoading
                      backgroundColor="#efefef"
                      titleColor="#333"
                      onPress={() => this.onAdd()}
                      title="Adicionar endereço de entrega!"
                    />
                  </View>
                )}
              </View>

              <View style={styles.containerList}>
                <FlatList
                  data={items.filter(item => item.id != enderecoEntrega.id)}
                  extraData={[]}
                  renderItem={({ item }) => (
                    <EnderecoItem
                      item={item}
                      onPress={() => {
                        ActionSheet.show(
                          {
                            options: BUTTONS,
                            cancelButtonIndex: CANCEL_INDEX,
                            destructiveButtonIndex: DESTRUCTIVE_INDEX
                          },
                          buttonIndex => {
                            if (buttonIndex == 0) {
                              this.onAddEntrega(item);
                            }
                            if (buttonIndex == 1) {
                              this.onEdit(item);
                            }
                            if (buttonIndex == 2) {
                              this.onDelete(item);
                            }
                          }
                        );
                      }}
                    />
                  )}
                />
              </View>
            </SkeletonLoading>
          </View>
        </ScrollView>

        {this.isEmpty ? (
          <View style={styles.viewEmpty}>
            <Icon style={styles.icon} name="fa-compass" />
            <TextCustom style={styles.textEmpty}>
              Nenhum registro existente.
            </TextCustom>
          </View>
        ) : null}

        <Fab
          style={{ backgroundColor: colors.primaryColor }}
          position="bottomRight"
          onPress={() => this.onAdd()}
        >
          <IconNativeBase name="add" />
        </Fab>
      </ContainerPage>
    );
  }
}

const styles = StyleSheet.create({
  containerList: {
    marginRight: 16,
    marginLeft: 16
  },
  selectedAddress: {
    marginBottom: 16,
    paddingLeft: 16,
    paddingRight: 16,
    backgroundColor: 'white'
  },
  viewEmpty: {
    marginTop: 25,
    marginBottom: 25,
    textAlign: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
    paddingTop: 70
  },
  textEmpty: {
    fontSize: 24,
    flex: 1,
    marginTop: -30,
    paddingRight: 32,
    paddingLeft: 32,
    textAlign: 'center',
    opacity: 0.6
  },
  icon: {
    flex: 1,
    textAlign: 'center',
    alignSelf: 'center',
    fontSize: 60,
    opacity: 0.4,
    marginBottom: 0
  }
});

export default withNavigationFocus(EnderecoScreen);
