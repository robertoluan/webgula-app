import React from "react";
import { Text, Toaster } from "native-base";
import { Platform, StyleSheet, View, ScrollView } from "react-native";
import { Row, Col } from "react-native-easy-grid";
import Input from "../../../shared/components/Input";
import Colors from "../../../theme/colors";
import ButtonLoading from "../../../shared/components/ButtonLoading";
import TextCustom from "../../../shared/components/TextCustom";
import authContext from "../../../infra/auth/auth-context";
import ContainerPage from "../../../shared/components/ContainerPage";

class SenhaScreen extends React.PureComponent {
  state = {
    senha: null,
    confimar_senha: null,
    isLoading: false
  };

  static navigationOptions = {
    title: "Faça seu Cadastro",
    headerRight: null
  };

  cadastrar = async () => {
    const { registrar } = this.props;

    if (!this.state.senha) {
      Toaster.show({ text: "Informe a Senha!" });
      return;
    }

    if (this.state.confimar_senha != this.state.senha) {
      Toaster.show({ text: "As senhas não são iguais!" });
      return;
    }

    const preCadastro = this.props.navigation.getParam("cadastro");

    const usuario = {
      name: preCadastro.nome,
      cpf: preCadastro.cpf,
      dateOfBirth: preCadastro.data_nascimento,
      password: preCadastro.senha
    };

    this.setState({
      isLoading: true
    });

    try {
      const res = await registrar(usuario);

      if (res.success) {
        if (res.data && res.data.CPF) {
          return Toaster.show({ text: "CPF já existente no sistema" });
        }

        if (res.data == "Fala ao criar o usuário") {
          return Toaster.show({ text: "CPF já existente no sistema" });
        }

        await authContext.login(usuario.name, usuario.password);

        this.props.navigation.replace("Home");

        this.setState({
          isLoading: false
        });
      } else {
        Toaster.show({ text: "Não foi possivel realizar esta operação" });

        this.setState({
          isLoading: false
        });
      }
    } catch (e) {
      Toaster.show({ text: "Não foi possivel realizar esta operação" });

      this.setState({
        isLoading: false
      });
    }
  };

  render() {
    return (
      <ContainerPage style={[styles.wrapper]}>
        <Text style={styles.titleContent}>
          Escolha agora uma senha para usar o aplicativo
        </Text>

        <ScrollView style={styles.container} keyboardShouldPersistTaps="always">
          <View style={styles.formContainer}>
            <Row style={{ marginBottom: 30 }}>
              <Col>
                <TextCustom style={styles.labelFloat}>Senha</TextCustom>
                <Input
                  secureTextEntry
                  style={styles.input}
                  transparent
                  value={this.state.senha}
                  onChangeText={senha => this.setState({ senha })}
                />

                <TextCustom style={styles.labelFloat}>
                  Confirmar Senha
                </TextCustom>
                <Input
                  secureTextEntry
                  style={styles.input}
                  transparent
                  value={this.state.confirmar_senha}
                  onChangeText={confimar_senha =>
                    this.setState({ confimar_senha })
                  }
                />
              </Col>
            </Row>

            <ButtonLoading
              title="Criar Cadastro"
              loading={this.state.isLoading}
              onPress={() => this.cadastrar()}
            />
          </View>
        </ScrollView>
      </ContainerPage>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1
  },
  container: {
    marginRight: 0
  },
  titleContent: {
    alignSelf: "center",
    fontSize: 20,
    textAlign: "center",
    paddingRight: 48,
    paddingLeft: 48,
    justifyContent: "center",
    top: 50,
    marginBottom: 80, 
    color: Colors.primaryColor
  },
  labelFloat: { 
    fontSize: 13,
    bottom: -8, 
    position: "relative"
  },
  loadingContainer: {
    borderRadius: Platform.OS == "ios" ? 8 : 0,
    backgroundColor: "#eee",
    flex: 2
  },
  formContainer: {
    marginRight: 16,
    marginLeft: 16
  },
  input: {
    marginBottom: 10
  }
});

export default SenhaScreen;
