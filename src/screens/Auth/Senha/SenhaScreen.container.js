import SenhaScreen from './SenhaScreen';
import { connect } from 'react-redux';
import { setLoading } from '../../../services/actions/layout/global.actions';
import authContext from '../../../infra/auth/auth-context';  

const mapDispatchToProps = (dispatch, props) => ({
	async registrar(model) {
		return authContext.registrar(model); 
	},

	setLoading() {
		dispatch(setLoading(true, 'Entrando...'));
	},

	clearLoading() {
		dispatch(setLoading(false));
	}
});

export default connect(null, mapDispatchToProps)(SenhaScreen); 
