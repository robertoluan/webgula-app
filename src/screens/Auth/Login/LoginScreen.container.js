import LoginScreen from "./LoginScreen";
import AccountsRemote from "../../../services/remote/accounts.remote";
import { connect } from "react-redux";
import { setLoading } from "../../../services/actions/layout/global.actions";
import authContext from "../../../infra/auth";

const accountsRemote = new AccountsRemote();

const mapStateToProps = (state, props) => {
  let app = state.app || {};

  return {
    isAuth: app.auth && app.auth.isAuth,
    isClientCredentials: app.auth && app.auth.isClientCredentials,
    auth: (app.auth && app.auth.userInfo) || {}
  };
};

const mapDispatchToProps = (dispatch, props) => ({
  async pedirSms(telefone) {
    return accountsRemote.registrarPhoneNumber({ PhoneNumber: telefone });
  },

  async clientAuthenticated() {
    await authContext.loginClientCredentials();
    let isClientCredentials = await authContext.isClientCredentialsAsync();

    return dispatch({
      type: "APP.AUTH.LOAD",
      userInfo: {},
      isAuth: false,
      isClientCredentials,
      token: authContext.token,
      refresh_token: authContext.refresh_token
    });
  },

  setLoading() {
    dispatch(setLoading(true, "Entrando..."));
  },

  clearLoading() {
    dispatch(setLoading(false));
  },

  closeModal() {
    dispatch({
      type: "LAYOUT.MODAL_AUTH.CLOSE"
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginScreen);
