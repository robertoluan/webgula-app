import React from "react";
import { Toast } from "native-base";
import {
  TouchableOpacity,
  ScrollView,
  StyleSheet,
  View,
  Image,
  Alert
} from "react-native";
import { Row, Col } from "react-native-easy-grid";
import Input from "../../../shared/components/Input";
import logoImg from "../../../../assets/images/logo-secondary.png";
import ButtonLoading from "../../../shared/components/ButtonLoading";
import { colors } from "../../../theme";
import { isNumber } from "../../../infra/utils/number.helpers";
import ContainerPage from "../../../shared/components/ContainerPage";
import InputMask from "../../../shared/components/Input/InputMask"; 

class LoginScreen extends React.PureComponent {
  static navigationOptions = {
    header: null
  };

  state = {
    phoneNumber: "",
    ddi: "55",
    isLoading: false,
    loadingPular: false,
    codigoEnviado: false
  };

  receberCodigo = async () => {
    const { phoneNumber, ddi } = this.state;
    const { pedirSms, navigation } = this.props;

    if (!ddi) {
      Toast.show({
        text: "Informe o código do Pais!"
      });
      return;
    }

    if (!phoneNumber) {
      Toast.show({
        text: "Informe o Telefone!"
      });
      return;
    }

    if (!isNumber(phoneNumber)) {
      Toast.show({
        text: "Telefone está em formato incorreto!"
      });
      return;
    }

    if (phoneNumber.toString().length < 11) {
      Toast.show({
        text: "Telefone deve possui o DDD juntamente com o número!"
      });
      return;
    }

    if (phoneNumber.toString().length > 11) {
      Toast.show({
        text: "Telefone é maior do que o tamanho permitido de 11 digitos!"
      });
      return;
    }

    this.setState({
      isLoading: true
    });

    const telefoneCompleto = ddi + "" + phoneNumber.toString();

    try {
      // TODO: considerar ddi
      const res = await pedirSms(telefoneCompleto);

      if (res.success == false) {
        Alert.alert(
          "Aviso",
          "Houve um problema ao registrar, seu telefone está realmente correto?"
        );

        return;
      }

      navigation.navigate("LoginSegundaEtapa", {
        telefone: telefoneCompleto
      });
    } catch (e) {
      Alert.alert(
        "Aviso",
        "Houve um problema ao registrar, seu telefone está realmente correto?"
      );
    } finally {
      this.setState({
        isLoading: false
      });
    }
  };

  componentWillUpdate = async nextProps => {
    if (nextProps.isAuth !== this.props.isAuth) {
      if (nextProps.isAuth) {
        this.redirectHome(nextProps);
      }
    }
  };

  redirectHome = props => {
    const { auth, navigation } = props;

    if (!auth.pessoaId) {
      return navigation.replace("Tutorial");
    }

    navigation.replace("Home");
  };

  componentDidMount = async () => {
    const { isAuth } = this.props;

    if (isAuth) {
      return this.redirectHome(this.props);
    }
  };

  pularLogin = async () => {
    const { isClientCredentials, clientAuthenticated, navigation } = this.props;

    if (!isClientCredentials) {
      this.setState({ loadingPular: true });
      try {
        await clientAuthenticated();
      } catch (e) {
      } finally {
        this.setState({ loadingPular: false });
      }
    }

    return navigation.navigate("Home");
  };

  render = () => {
    const { phoneNumber, ddi } = this.state;

    return (
      <ContainerPage style={styles.wrapper}>
        <ScrollView style={styles.container} keyboardShouldPersistTaps="always">
          <TouchableOpacity>
            <Image source={logoImg} style={styles.logo} />
          </TouchableOpacity>

          <View style={styles.formContainer}>
            <Row>
              <Col
                size={30}
                style={{
                  marginRight: 10
                }}
              >
                <Input
                  label={"Pais"}
                  dark
                  borderless
                  style={styles.input}
                  transparent
                  keyboardType="numeric"
                  value={ddi}
                  onChangeText={ddi => this.setState({ ddi })}
                />
              </Col>
              <Col size={70}>
                <InputMask
                  dark
                  borderless
                  label={"Telefone"}
                  type={"cel-phone"}
                  options={{
                    maskType: "BRL",
                    withDDD: true,
                    dddMask: "(99) "
                  }}
                  value={phoneNumber}
                  onChangeText={value => this.setState({ phoneNumber: value })}
                />
              </Col>
            </Row>

            <View style={{ marginTop: 16, marginBottom: 30 }}>
              

              <View style={{ marginTop: 16 }}>
                <ButtonLoading
                  title="Entrar"
                  titleColor={colors.primaryColor}
                  disabled={this.state.loadingPular}
                  backgroundColor={"white"}
                  loading={this.state.isLoading}
                  onPress={() => this.receberCodigo()}
                />
              </View>
              <View style={{ marginTop: 16 }}>
                <ButtonLoading
                  title="Pular"
                  loading={this.state.loadingPular}
                  titleColor={colors.white}
                  backgroundColor={colors.primaryColor}
                  onPress={async () => this.pularLogin()}
                />
              </View>
            </View>
          </View>
        </ScrollView>
      </ContainerPage>
    );
  };
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: colors.primaryColor
  },
  container: {
    marginRight: 0
  },
  logo: {
    alignSelf: "center",
    resizeMode: "contain",
    top: 50,
    marginBottom: 100,
    width: 170
  },
  formContainer: {
    flex: 1,
    top: 30,
    flexDirection: "column",
    marginRight: 16,
    marginLeft: 16
  },
  input: {
    marginBottom: 10
  }
});

export default LoginScreen;
